package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.SequenceManagementPage;
import io.ient.test.pages.enterprise.UserManagementPage;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class SequenceManagementTest extends FrameworkTestBase {

    @TestCaseID(ids = {"T3200"})
    @Test
    public void verifyNumberOfRecordsOnSequenceManagement() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        SequenceManagementPage sequenceManagementPage = new SequenceManagementPage(driver);
        sequenceManagementPage.clickOnSequenceManagementTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean numberOfRecordsStatus = sequenceManagementPage.verifyNumberOfRecords(40);
        softAssert.assertTrue(numberOfRecordsStatus, "Actual records are less than expected records");
        softAssert.assertAll();
    }

}
