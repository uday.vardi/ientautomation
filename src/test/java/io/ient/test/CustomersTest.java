package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.CustomersPage;
import io.ient.test.pages.enterprise.GlobalSettingsPage;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class CustomersTest extends FrameworkTestBase {

    @TestCaseID(ids = {"T1520"})
    @Test
    public void verifyNewCustomerCreation() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        CustomersPage customersPage = new CustomersPage(driver);
        customersPage.clickOnCustomersTab();
        String customerName = "CU_" + RandomDataUtils.getRandomAlphanumeric(4);
        String status = "Active";
        customersPage.createCustomer(customerName, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        customersPage.searchCustomerByName(customerName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean displayStatus = customersPage.isCustomerDisplayed(customerName);
        softAssert.assertTrue(displayStatus, "Failed to create new customer");
        String updateName = customerName + "_EDIT";
        customersPage.editCustomer(updateName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        customersPage.clearCustomerTextField();
        customersPage.searchCustomerByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean updatedCustomerDisplayStatus = customersPage.isCustomerDisplayed(updateName);
        softAssert.assertTrue(updatedCustomerDisplayStatus, "Failed to update customer");
        customersPage.deleteCustomer();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        customersPage.clearCustomerTextField();
        customersPage.searchCustomerByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        customersPage.clickOnSearchIcon();
        boolean isCustomerDeleted = customersPage.verifyNoRecordsMessage();
        softAssert.assertTrue(isCustomerDeleted, "Failed to delete customer");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-264"})
    @Test
    public void verifyCustomersPageFeatures() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        GlobalSettingsPage globalSettingsPage = new GlobalSettingsPage(driver);
        CustomersPage customersPage = new CustomersPage(driver);
        homePage.clickOnEnterprise();
        customersPage.clickOnCustomersTab();
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean isSearchBarDisplayed = globalSettingsPage.isSearchBarDisplayed();
        softAssert.assertTrue(isSearchBarDisplayed, "Failed to display searchbar");
        boolean isSelectColumnsDisplayed = globalSettingsPage.isSelectColumnsDisplayed();
        softAssert.assertTrue(isSelectColumnsDisplayed, "Failed to display select columns");
        boolean isFilterIconDisplayed = globalSettingsPage.isFilterIconDisplayed();
        softAssert.assertTrue(isFilterIconDisplayed, "Failed to display filter");
        globalSettingsPage.clickOnTopLeftVerticalIcon();
        boolean isAddNewButtonDisplayed = globalSettingsPage.isAddNewButtonDisplayed();
        softAssert.assertTrue(isAddNewButtonDisplayed, "Failed to display AddNewButton");
        boolean isDownloadAsPdfDisplayed = globalSettingsPage.isDownloadAsPdfDisplayed();
        softAssert.assertTrue(isDownloadAsPdfDisplayed, "Failed to display DownloadAsPdf");
        boolean isDownloadAsCsvDisplayed = globalSettingsPage.isDownloadAsCsvDisplayed();
        softAssert.assertTrue(isDownloadAsCsvDisplayed, "Failed to display DownloadAsCSV");
        boolean isNumberOfItemsPerPageEditable = globalSettingsPage.isNumberOfItemsPerPageEditable();
        softAssert.assertTrue(isNumberOfItemsPerPageEditable, "Failed to edit number of items per page");
        boolean isPaginationDisplayed = globalSettingsPage.isPaginationDisplayed();
        softAssert.assertTrue(isPaginationDisplayed, "Failed to display pagination");
        boolean isTotalCountDisplayed = globalSettingsPage.isTotalCountDisplayed();
        softAssert.assertTrue(isTotalCountDisplayed, "Failed to display total count");
        boolean isPageColumnDisplayed = globalSettingsPage.isPageColumnDisplayed("ID");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display ID column");
        isPageColumnDisplayed = globalSettingsPage.isPageColumnDisplayed("Status");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display Status column");
        boolean isActionEditAndDeleteDisplayed = globalSettingsPage.isActionEditAndDeleteDisplayed();
        softAssert.assertTrue(isActionEditAndDeleteDisplayed, "Failed to display action edit delete options");
        softAssert.assertAll();
    }

}