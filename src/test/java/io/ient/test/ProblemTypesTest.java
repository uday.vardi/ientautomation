package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.GlobalSettingsPage;
import io.ient.test.pages.enterprise.ProblemCasePage;
import io.ient.test.pages.enterprise.ProblemTypesPage;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class ProblemTypesTest extends FrameworkTestBase {

    @TestCaseID(ids = {"T1530"})
    @Test
    public void verifyNewProblemTypeCreation() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        ProblemTypesPage problemTypesPage = new ProblemTypesPage(driver);
        problemTypesPage.clickOnProblemTypesTab();
        String problemTypeName = "PT_" + RandomDataUtils.getRandomAlphanumeric(4);
        String status = "Active";
        problemTypesPage.createProblemType(problemTypeName, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        problemTypesPage.searchProblemTypeByName(problemTypeName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean displayStatus = problemTypesPage.isProblemTypeDisplayed(problemTypeName);
        softAssert.assertTrue(displayStatus, "Failed to create new problem type");
        String updateName = problemTypeName + "_EDIT";
        problemTypesPage.editProblemType(updateName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        problemTypesPage.clearProblemTypeTextField();
        problemTypesPage.searchProblemTypeByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean updatedProblemTypesDisplayStatus = problemTypesPage.isProblemTypeDisplayed(updateName);
        softAssert.assertTrue(updatedProblemTypesDisplayStatus, "Failed to update problem type");
        problemTypesPage.deleteProblemType();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        problemTypesPage.clearProblemTypeTextField();
        problemTypesPage.searchProblemTypeByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        problemTypesPage.clickOnSearchIcon();
        boolean isProblemTypeDeleted = problemTypesPage.verifyNoRecordsMessage();
        softAssert.assertTrue(isProblemTypeDeleted, "Failed to delete problem type");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-261", "IENT-262"})
    @Test
    public void verifyChildAndSubChildProblemTypeCreation() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        driver.sleep(WaitTime.SMALL_WAIT);
        homePage.clickOnEnterprise();
        ProblemTypesPage problemTypesPage = new ProblemTypesPage(driver);
        problemTypesPage.clickOnProblemTypesTab();
        String problemTypeName = "PT_" + RandomDataUtils.getRandomAlphanumeric(4);
        String childProblemTypeName = "ChildPT_" + RandomDataUtils.getRandomAlphanumeric(4);
        String subChildProblemTypeName = "SUBCPT_" + RandomDataUtils.getRandomAlphanumeric(4);
        String status = "Active";
        problemTypesPage.createProblemType(problemTypeName, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        problemTypesPage.createProblemType(childProblemTypeName, status, problemTypeName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        problemTypesPage.createProblemType(subChildProblemTypeName, status, childProblemTypeName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        problemTypesPage.searchProblemTypeByName(problemTypeName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean displayStatus = problemTypesPage.isProblemTypeDisplayed(problemTypeName);
        softAssert.assertTrue(displayStatus, "Failed to create new problem type");
        problemTypesPage.expandProblemType(problemTypeName);
        boolean childPT_displayStatus = problemTypesPage.isProblemTypeDisplayed(problemTypeName);
        softAssert.assertTrue(childPT_displayStatus, "Failed to display child problem type");
        problemTypesPage.expandProblemType(childProblemTypeName);
        boolean subChildPT_displayStatus = problemTypesPage.isProblemTypeDisplayed(subChildProblemTypeName);
        softAssert.assertTrue(subChildPT_displayStatus, "Failed to display sub child problem type");
        String subChildProblemTypeName_edit = subChildProblemTypeName + "_update";
        problemTypesPage.editProblemTypeBySearch(subChildProblemTypeName, subChildProblemTypeName_edit);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        problemTypesPage.searchProblemTypeByName(problemTypeName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        problemTypesPage.expandProblemType(problemTypeName);
        problemTypesPage.expandProblemType(childProblemTypeName);
        boolean subChildPT_edit_displayStatus = problemTypesPage.isProblemTypeDisplayed(subChildProblemTypeName_edit);
        softAssert.assertTrue(subChildPT_edit_displayStatus, "Failed to edit sub child problem type");
        problemTypesPage.deleteProblemType(subChildProblemTypeName_edit);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        problemTypesPage.searchProblemTypeByName(problemTypeName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        problemTypesPage.expandProblemType(problemTypeName);
        driver.sleep(WaitTime.SMALL_WAIT);
        problemTypesPage.deleteProblemType(childProblemTypeName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        problemTypesPage.searchProblemTypeByName(problemTypeName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        problemTypesPage.deleteProblemType(problemTypeName);
        driver.sleep(WaitTime.SMALL_WAIT);
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-722"})
    @Test
    public void verifyChildAndSubChildProblemTypesInProblemCase() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        driver.sleep(WaitTime.SMALL_WAIT);
        homePage.clickOnEnterprise();
        ProblemTypesPage problemTypesPage = new ProblemTypesPage(driver);
        problemTypesPage.clickOnProblemTypesTab();
        String problemTypeName = "PT_" + RandomDataUtils.getRandomAlphanumeric(4);
        String childProblemTypeName = "ChildPT_" + RandomDataUtils.getRandomAlphanumeric(4);
        String subChildProblemTypeName = "SUBCPT_" + RandomDataUtils.getRandomAlphanumeric(4);
        String status = "Active";
        problemTypesPage.createProblemType(problemTypeName, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        problemTypesPage.createProblemType(childProblemTypeName, status, problemTypeName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        problemTypesPage.createProblemType(subChildProblemTypeName, status, childProblemTypeName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        problemTypesPage.searchProblemTypeByName(problemTypeName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean displayStatus = problemTypesPage.isProblemTypeDisplayed(problemTypeName);
        softAssert.assertTrue(displayStatus, "Failed to create new problem type");
        problemTypesPage.expandProblemType(problemTypeName);
        boolean childPT_displayStatus = problemTypesPage.isProblemTypeDisplayed(problemTypeName);
        softAssert.assertTrue(childPT_displayStatus, "Failed to display child problem type");
        problemTypesPage.expandProblemType(childProblemTypeName);
        boolean subChildPT_displayStatus = problemTypesPage.isProblemTypeDisplayed(subChildProblemTypeName);
        softAssert.assertTrue(subChildPT_displayStatus, "Failed to display sub child problem type");
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        login.loginWithBuyerUser(testContext.app);
        homePage.clickOnSupplyChainIssueManagement();
        ProblemCasePage problemCasePage = new ProblemCasePage(driver);
        problemCasePage.clickOnAddButton();
        boolean isProblemTypeDisplayed = problemCasePage.isProblemTypeDisplayed(problemTypeName);
        softAssert.assertTrue(isProblemTypeDisplayed, "Failed to display primary type");
        driver.sleep(WaitTime.MEDIUM_WAIT);
        boolean isPrimaryNonConformanceProblemTypesDisplayed = problemCasePage.isPrimaryNonConformanceProblemTypesDisplayed(childProblemTypeName);
        softAssert.assertTrue(isPrimaryNonConformanceProblemTypesDisplayed, "Failed to display non conformance primary type");
        driver.sleep(WaitTime.MEDIUM_WAIT);
        boolean isSecondaryNonConformanceProblemTypesDisplayed = problemCasePage.isSecondaryNonConformanceProblemTypesDisplayed(subChildProblemTypeName);
        softAssert.assertTrue(isSecondaryNonConformanceProblemTypesDisplayed, "Failed to display secondary non conformance primary type");
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        login.loginWithBuyerAdmin(testContext.app);
        driver.sleep(WaitTime.SMALL_WAIT);
        homePage.clickOnEnterprise();
        problemTypesPage.clickOnProblemTypesTab();
        problemTypesPage.searchProblemTypeByName(problemTypeName);
        driver.sleep(WaitTime.SMALL_WAIT);
        problemTypesPage.expandProblemType(problemTypeName);
        problemTypesPage.expandProblemType(childProblemTypeName);
        problemTypesPage.deleteProblemType(subChildProblemTypeName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        problemTypesPage.searchProblemTypeByName(problemTypeName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        problemTypesPage.expandProblemType(problemTypeName);
        driver.sleep(WaitTime.SMALL_WAIT);
        problemTypesPage.deleteProblemType(childProblemTypeName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        problemTypesPage.searchProblemTypeByName(problemTypeName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        problemTypesPage.deleteProblemType(problemTypeName);
        driver.sleep(WaitTime.SMALL_WAIT);
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-260"})
    @Test
    public void verifyProblemTypePageFeatures() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        GlobalSettingsPage globalSettingsPage = new GlobalSettingsPage(driver);
        ProblemTypesPage problemTypesPage = new ProblemTypesPage(driver);
        problemTypesPage.clickOnProblemTypesTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean isSearchBarDisplayed = globalSettingsPage.isSearchBarDisplayed();
        softAssert.assertTrue(isSearchBarDisplayed, "Failed to display searchbar");
        boolean isSelectColumnsDisplayed = globalSettingsPage.isSelectColumnsDisplayed();
        softAssert.assertTrue(isSelectColumnsDisplayed, "Failed to display select columns");
        boolean isFilterIconDisplayed = globalSettingsPage.isFilterIconDisplayed();
        softAssert.assertTrue(isFilterIconDisplayed, "Failed to display filter");
        globalSettingsPage.clickOnTopLeftVerticalIcon();
        boolean isAddNewButtonDisplayed = globalSettingsPage.isAddNewButtonDisplayed();
        softAssert.assertTrue(isAddNewButtonDisplayed, "Failed to display AddNewButton");
        boolean isDownloadAsPdfDisplayed = globalSettingsPage.isDownloadAsPdfDisplayed();
        softAssert.assertTrue(isDownloadAsPdfDisplayed, "Failed to display DownloadAsPdf");
        boolean isDownloadAsCsvDisplayed = globalSettingsPage.isDownloadAsCsvDisplayed();
        softAssert.assertTrue(isDownloadAsCsvDisplayed, "Failed to display DownloadAsCSV");
        boolean isNumberOfItemsPerPageEditable = globalSettingsPage.isNumberOfItemsPerPageEditable();
        softAssert.assertTrue(isNumberOfItemsPerPageEditable, "Failed to edit number of items per page");
        boolean isPaginationDisplayed = globalSettingsPage.isPaginationDisplayed();
        softAssert.assertTrue(isPaginationDisplayed, "Failed to display pagination");
        boolean isTotalCountDisplayed = globalSettingsPage.isTotalCountDisplayed();
        softAssert.assertTrue(isTotalCountDisplayed, "Failed to display total count");
        boolean isPageColumnDisplayed = globalSettingsPage.isPageColumnDisplayed("ID");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display ID column");
        isPageColumnDisplayed = globalSettingsPage.isPageColumnDisplayed("Status");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display Status column");
        boolean isActionEditAndDeleteDisplayed = globalSettingsPage.isActionEditAndDeleteDisplayed();
        softAssert.assertTrue(isActionEditAndDeleteDisplayed, "Failed to display action edit delete options");
        softAssert.assertAll();
    }

}