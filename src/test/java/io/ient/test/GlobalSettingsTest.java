package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.*;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class GlobalSettingsTest extends FrameworkTestBase {

    @TestCaseID(ids = {"T1510"})
    @Test
    public void verifyNewProblemQualifierCreation() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        GlobalSettingsPage globalSettingsPage = new GlobalSettingsPage(driver);
        globalSettingsPage.clickOnGlobalSettingsTab();
        String problemQualifierName = "PQ_" + RandomDataUtils.getRandomAlphanumeric(4);
        String status = "Active";
        globalSettingsPage.createProblemQualifier(problemQualifierName, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        globalSettingsPage.searchProblemQualifierByName(problemQualifierName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean displayStatus = globalSettingsPage.isProblemQualifierDisplayed(problemQualifierName);
        softAssert.assertTrue(displayStatus, "Failed to create new problem qualifier");
        String updateName = problemQualifierName + "_EDIT";
        globalSettingsPage.editProblemQualifier(updateName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        globalSettingsPage.clearProblemQualifierTextField();
        globalSettingsPage.searchProblemQualifierByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean updatedProblemQualifierDisplayStatus = globalSettingsPage.isProblemQualifierDisplayed(updateName);
        softAssert.assertTrue(updatedProblemQualifierDisplayStatus, "Failed to update problem qualifier");
        globalSettingsPage.deleteProblemQualifier();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        globalSettingsPage.clearProblemQualifierTextField();
        globalSettingsPage.searchProblemQualifierByName(updateName);
        globalSettingsPage.clickOnSearchIcon();
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean isProblemQualifierDeleted = globalSettingsPage.verifyNoRecordsMessage();
        softAssert.assertTrue(isProblemQualifierDeleted, "Failed to delete problem qualifier");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-727" , "IENT-1638" })
    @Test
    public void verifyTenantManagementTab() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        TenantsPage tenantsPage = new TenantsPage(driver);
        GlobalSettingsPage globalSettingsPage = new GlobalSettingsPage(driver);
        boolean isTenantManagementTabDisplayed = globalSettingsPage.isTenantManagementTabDisplayed();
        softAssert.assertTrue(isTenantManagementTabDisplayed, "Failed to display tenant management tab");
        boolean isApplicationApprovalTabDisplayed = globalSettingsPage.isApplicationApprovalDisplayed();
        softAssert.assertFalse(isApplicationApprovalTabDisplayed, "Application approval tab should not display");
        tenantsPage.clickOnTenantsManagementTab();
        boolean isEditButtonDisplayed = tenantsPage.isEditButtonDisplayed();
        softAssert.assertTrue(isEditButtonDisplayed, "Failed to display Edit Button");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-258"})
    @Test
    public void verifyProblemQualifierPageFeatures() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        GlobalSettingsPage globalSettingsPage = new GlobalSettingsPage(driver);
        globalSettingsPage.clickOnGlobalSettingsTab();
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean isSearchBarDisplayed = globalSettingsPage.isSearchBarDisplayed();
        softAssert.assertTrue(isSearchBarDisplayed, "Failed to display searchbar");
        boolean isSelectColumnsDisplayed = globalSettingsPage.isSelectColumnsDisplayed();
        softAssert.assertTrue(isSelectColumnsDisplayed, "Failed to display select columns");
        boolean isFilterIconDisplayed = globalSettingsPage.isFilterIconDisplayed();
        softAssert.assertTrue(isFilterIconDisplayed, "Failed to display filter");
        globalSettingsPage.clickOnTopLeftVerticalIcon();
        boolean isAddNewButtonDisplayed = globalSettingsPage.isAddNewButtonDisplayed();
        softAssert.assertTrue(isAddNewButtonDisplayed, "Failed to display AddNewButton");
        boolean isDownloadAsPdfDisplayed = globalSettingsPage.isDownloadAsPdfDisplayed();
        softAssert.assertTrue(isDownloadAsPdfDisplayed, "Failed to display DownloadAsPdf");
        boolean isDownloadAsCsvDisplayed = globalSettingsPage.isDownloadAsCsvDisplayed();
        softAssert.assertTrue(isDownloadAsCsvDisplayed, "Failed to display DownloadAsCSV");
        boolean isNumberOfItemsPerPageEditable = globalSettingsPage.isNumberOfItemsPerPageEditable();
        softAssert.assertTrue(isNumberOfItemsPerPageEditable, "Failed to edit number of items per page");
        boolean isPaginationDisplayed = globalSettingsPage.isPaginationDisplayed();
        softAssert.assertTrue(isPaginationDisplayed, "Failed to display pagination");
        boolean isTotalCountDisplayed = globalSettingsPage.isTotalCountDisplayed();
        softAssert.assertTrue(isTotalCountDisplayed, "Failed to display total count");
        boolean isPageColumnDisplayed = globalSettingsPage.isPageColumnDisplayed("ID");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display ID column");
        isPageColumnDisplayed = globalSettingsPage.isPageColumnDisplayed("Status");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display Status column");
//        boolean isActionEditAndDeleteDisplayed = globalSettingsPage.isActionEditAndDeleteDisplayed();
//        softAssert.assertTrue(isActionEditAndDeleteDisplayed, "Failed to display action edit delete options");
        softAssert.assertAll();
    }


    @TestCaseID(ids = {"IENT-982"})
    @Test
    public void verifyGlobalSettingsPageTabs() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        GlobalSettingsPage globalSettingsPage = new GlobalSettingsPage(driver);
        globalSettingsPage.clickOnGlobalSettingsTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean isProblemQualifierDisplayed = globalSettingsPage.isProblemQualifierTabDisplayed();
        softAssert.assertTrue(isProblemQualifierDisplayed, "Failed to display problem qualifiers tab");
        boolean isCustomerDisplayed = globalSettingsPage.isCustomersTabDisplayed();
        softAssert.assertTrue(isCustomerDisplayed, "Failed to display customers tab");
        boolean isProblemTypeDisplayed = globalSettingsPage.isProblemTypeTabDisplayed();
        softAssert.assertTrue(isProblemTypeDisplayed, "Failed to display problem types tab");
        boolean isCommodityGroupsDisplayed = globalSettingsPage.isCommodityGroupsTabDisplayed();
        softAssert.assertTrue(isCommodityGroupsDisplayed, "Failed to display commodity groups tab");
        boolean isRejectReasonsDisplayed = globalSettingsPage.isRejectReasonsTabDisplayed();
        softAssert.assertTrue(isRejectReasonsDisplayed, "Failed to display reject reasons tab");
        boolean isPartsDisplayed = globalSettingsPage.isPartsTabDisplayed();
        softAssert.assertTrue(isPartsDisplayed, "Failed to display parts tab");
        boolean isProductionPhaseDisplayed = globalSettingsPage.isProductionPhaseTabDisplayed();
        softAssert.assertTrue(isProductionPhaseDisplayed, "Failed to display production phase tab");
        boolean isDisruptionStatusDisplayed = globalSettingsPage.isDisruptionStatusTabDisplayed();
        softAssert.assertTrue(isDisruptionStatusDisplayed, "Failed to display disruption status tab");
        boolean isSuppliersDisplayed = globalSettingsPage.isSuppliersTabDisplayed();
        softAssert.assertTrue(isSuppliersDisplayed, "Failed to display suppliers tab");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-1541"})
    @Test
    public void verifyPartSupplierAndSupplierLocation(){
        SoftAssert softAssert=new SoftAssert();
        Login login=new Login(driver);
        login.loginWithBuyerUser(testContext.app);
        HomePage homePage=new HomePage(driver);
        SuppliersPage suppliersPage = new SuppliersPage(driver);
        PartsPage partsPage = new PartsPage(driver);
        SupplierLocationsPage supplierLocationsPage = new SupplierLocationsPage(driver);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnSupplyChainIssueManagement();
        ProblemCasePage problemCasePage=new ProblemCasePage(driver);
        String problemType="1";
        String primaryNonConformance="1";
        String secondaryNonConformance="1";
        String part="1";
        String buyerLocation="1";
        String caseStatus="open";
        String priority="P1";
        String supplierLocation="1";
        String supplier="1";
        String shortDescription=RandomDataUtils.getRandomAlphanumeric(5);
        problemCasePage.createProblemCase(problemType, primaryNonConformance, secondaryNonConformance, part, buyerLocation, caseStatus, priority, supplierLocation, shortDescription, supplier);
        driver.sleep(WaitTime.LONG_WAIT);
        driver.sleep(WaitTime.LONG_WAIT);
        String selectedPart = problemCasePage.getProblemCaseSelectedPart();
        String selectedSupplier = problemCasePage.getProblemCaseSelectedSupplier();
        String selectedSupplierLocation = problemCasePage.getProblemCaseSelectedSupplierLocation();
        driver.refresh();
        driver.sleep(WaitTime.SMALL_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        login.loginWithBuyerAdmin(testContext.app);
        homePage.clickOnEnterprise();
        partsPage.clickOnPartsTab();
        partsPage.searchPartByName(selectedPart);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean partDisplayedStatus = partsPage.isPartDisplayed(selectedPart);
        softAssert.assertTrue(partDisplayedStatus, "Part is not displaying");
        homePage.clickOnEnterprise();
        suppliersPage.clickOnSuppliersTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        suppliersPage.searchSupplierByName(selectedSupplier);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean supplierDisplayedStatus = suppliersPage.isSupplierDisplayed(selectedSupplier);
        softAssert.assertTrue(supplierDisplayedStatus, "Supplier is not displaying");
        supplierLocationsPage.clickOnSupplierLocationsTab();
        driver.sleep(WaitTime.XSMALL_WAIT);
        supplierLocationsPage.searchLocationByName(selectedSupplierLocation);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean locationDisplayStatus = supplierLocationsPage.isLocationDisplayed(selectedSupplierLocation);
        softAssert.assertTrue(locationDisplayStatus, "Supplier location is not displaying");
        softAssert.assertAll();
    }

}