package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.SupplierShippingLanesPage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class SupplierShippingLanesTest extends FrameworkTestBase {

    private static final Logger logger = LogManager.getLogger(SupplierShippingLanesPage.class);
    LogFactory log = new LogFactory(logger);


    @TestCaseID(ids = {"IENT-2792"})
    @Test
    public void shippingLanesCreationWithApprovedStatus() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSupplierAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnShippingLanes();
        driver.sleep(WaitTime.SMALL_WAIT);
        SupplierShippingLanesPage supplierShippingLanesPage = new SupplierShippingLanesPage(driver);
        String part = "1";
        String source = "1";
        String destination = "1";
        String transitTime = "20";
        String transitMode = "1";
        String caseStatus = "Save as Draft";
        String shortDescription = RandomDataUtils.randomAlphabetic(8);
        String approverName = testContext.app.getBuyerUser();
        supplierShippingLanesPage.createShippingLanes(part, source, destination, transitTime, transitMode, caseStatus, approverName, shortDescription);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierShippingLanesPage.searchByShortDescription(shortDescription);
        boolean newShippingLanesCreatedStatus = supplierShippingLanesPage.isShippingLanesDisplayed(part);
        softAssert.assertTrue(newShippingLanesCreatedStatus, "Failed to create new Shipping Lanes");
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierShippingLanesPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        String submitToBuyerStatus = "Submit to Buyer";
        supplierShippingLanesPage.editStatus(submitToBuyerStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithBuyerUser(testContext.app);
        homePage.clickOnShippingLanes();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierShippingLanesPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean shippingLanesSubmitToBuyerStatus = supplierShippingLanesPage.isShippingLanesDisplayed(part);
        softAssert.assertTrue(shippingLanesSubmitToBuyerStatus, "Failed to find Submit to Buyer Shipping Lanes");
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierShippingLanesPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        String reworkStatus = "Rework";
        supplierShippingLanesPage.editStatus(reworkStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithSupplierAdmin(testContext.app);
        homePage.clickOnShippingLanes();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierShippingLanesPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean shippingLanesReworkStatus = supplierShippingLanesPage.isShippingLanesDisplayed(part);
        softAssert.assertTrue(shippingLanesReworkStatus, "Failed to find Rework Shipping Lanes");
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierShippingLanesPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierShippingLanesPage.editStatus(submitToBuyerStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.SMALL_WAIT);

        login.loginWithBuyerUser(testContext.app);
        homePage.clickOnShippingLanes();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierShippingLanesPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean shippingLanesLastStatus = supplierShippingLanesPage.isShippingLanesDisplayed(part);
        softAssert.assertTrue(shippingLanesLastStatus, "Failed to find Submit to Buyer Shipping Lanes");
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierShippingLanesPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        String approvedStatus = "Approved";
        supplierShippingLanesPage.editStatus(approvedStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithSupplierAdmin(testContext.app);
        homePage.clickOnShippingLanes();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierShippingLanesPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean shippingLanesApprovedStatus = supplierShippingLanesPage.isShippingLanesDisplayed(part);
        softAssert.assertTrue(shippingLanesApprovedStatus, "Failed to find Approved Shipping Lanes");
        driver.sleep(WaitTime.SMALL_WAIT);
        try {
            supplierShippingLanesPage.selectsRecordAndClicksEdit();
            driver.sleep(WaitTime.SMALL_WAIT);
            String rejectedStatus = "Rejected";
            supplierShippingLanesPage.editStatus(rejectedStatus);
        } catch (Exception e) {
            log.info("This Shipping Lanes record is no longer editable");
        }

    }

    @TestCaseID(ids = {"IENT-2792"})
    @Test
    public void shippingLanesCreationWithRejectedStatus() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSupplierAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnShippingLanes();
        driver.sleep(WaitTime.SMALL_WAIT);
        SupplierShippingLanesPage supplierShippingLanesPage = new SupplierShippingLanesPage(driver);
        String part = "1";
        String source = "1";
        String destination = "1";
        String transitTime = "20";
        String transitMode = "1";
        String caseStatus = "Submit to Buyer";
        String shortDescription = RandomDataUtils.randomAlphabetic(8);
        String approverName = testContext.app.getBuyerUser();
        supplierShippingLanesPage.createShippingLanes(part, source, destination, transitTime, transitMode, caseStatus, approverName, shortDescription);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierShippingLanesPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean newShippingLanesCreatedStatus = supplierShippingLanesPage.isShippingLanesDisplayed(part);
        softAssert.assertTrue(newShippingLanesCreatedStatus, "Failed to create new Shipping Lanes");
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithBuyerUser(testContext.app);
        homePage.clickOnShippingLanes();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierShippingLanesPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean shippingLanesSubmitToBuyerStatus = supplierShippingLanesPage.isShippingLanesDisplayed(part);
        softAssert.assertTrue(shippingLanesSubmitToBuyerStatus, "Failed to find Submit to Buyer Shipping Lanes");
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierShippingLanesPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        String rejectedStatus = "Rejected";
        supplierShippingLanesPage.editStatus(rejectedStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithSupplierAdmin(testContext.app);
        homePage.clickOnShippingLanes();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierShippingLanesPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean shippingLanesApprovedStatus = supplierShippingLanesPage.isShippingLanesDisplayed(part);
        softAssert.assertTrue(shippingLanesApprovedStatus, "Failed to find Approved Shipping Lanes");
        driver.sleep(WaitTime.SMALL_WAIT);
        try {
            supplierShippingLanesPage.selectsRecordAndClicksEdit();
            driver.sleep(WaitTime.SMALL_WAIT);
            String approvedStatus = "Approved";
            supplierShippingLanesPage.editStatus(approvedStatus);
        } catch (Exception e) {
            log.info("This Shipping Lanes record is no longer editable");
        }

    }

    @TestCaseID(ids = {"IENT-2793"})
    @Test
    public void verifyBuyerAdminShippingLanes(){
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        String part = "1";
        homePage.clickOnShippingLanes();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        SupplierShippingLanesPage supplierShippingLanesPage = new SupplierShippingLanesPage(driver);
        supplierShippingLanesPage.clickFilterIcon();
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierShippingLanesPage.buyerAdminFilters();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean isShippingLanesDisplayed = supplierShippingLanesPage.isShippingLanesDisplayed(part);
        softAssert.assertTrue(isShippingLanesDisplayed, "Failed to find Shipping Lanes with given filters");
    }

    @TestCaseID(ids = {"IENT-2794","IENT-2795"})
    @Test
    public void verifySupplierAdminShippingLanes(){
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSupplierAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        String part = "pt";
        homePage.clickOnShippingLanes();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        SupplierShippingLanesPage supplierShippingLanesPage = new SupplierShippingLanesPage(driver);
        supplierShippingLanesPage.clickFilterIcon();
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierShippingLanesPage.supplierAdminFilters();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean isShippingLanesDisplayed = supplierShippingLanesPage.isShippingLanesDisplayed(part);
        softAssert.assertTrue(isShippingLanesDisplayed, "Failed to find Shipping Lanes with given filters");
    }

}