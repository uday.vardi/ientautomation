package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.AuditLogPage;
import io.ient.test.pages.enterprise.RoleManagementPage;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.ArrayList;
import java.util.List;

public class RoleManagementTest extends FrameworkTestBase {

    @TestCaseID(ids = {"T1101"})
    @Test
    public void verifyRolesOnRoleManagementPage() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        RoleManagementPage roleManagementPage = new RoleManagementPage(driver);
        roleManagementPage.clickOnRoleManagementTab();
        roleManagementPage.selectMaxItemsPerPage();
        driver.sleep(WaitTime.SMALL_WAIT);
        List<String> expected_rolesList =  new ArrayList<String>();
        expected_rolesList.add("SUPPLIER_USER");
        expected_rolesList.add("SUPPLIER_ADMIN");
        expected_rolesList.add("BUYER_USER");
        expected_rolesList.add("IMPERSONATE_USER");
        expected_rolesList.add("ACCOUNTS_TEAM");
        expected_rolesList.add("BULLETIN_MANAGER");
        expected_rolesList.add("BULLETIN_APPROVER");
        expected_rolesList.add("SUPPLIER_QUALITY_MANAGER");
        boolean rolesDisplayStatus = roleManagementPage.verifyRolesList(expected_rolesList);
        softAssert.assertTrue(rolesDisplayStatus, "Failed to display expected roles");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"T1100"})
    @Test
    public void verifyNewRoleCreation() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        RoleManagementPage roleManagementPage = new RoleManagementPage(driver);
        roleManagementPage.clickOnRoleManagementTab();
        String name = "ROLE_" + RandomDataUtils.randomAlphabetic(4);
        String code = "CODE_" + RandomDataUtils.randomAlphabetic(4);
        String status = "Active";
        roleManagementPage.createRole(name, code, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        roleManagementPage.searchRoleByName(name);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        boolean displayStatus = roleManagementPage.isRoleDisplayed(name, code);
        softAssert.assertTrue(displayStatus, "Failed to create new role");
        String updateName = name + "_EDIT";
        roleManagementPage.editRole(updateName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        roleManagementPage.clearSearchTextField();
        roleManagementPage.searchRoleByName(updateName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        boolean updatedRoleDisplayStatus = roleManagementPage.isRoleDisplayed(updateName, code);
        softAssert.assertTrue(updatedRoleDisplayStatus, "Failed to update role");
        roleManagementPage.deleteRole();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        roleManagementPage.clearSearchTextField();
        roleManagementPage.searchRoleByName(updateName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        roleManagementPage.clickOnSearchIcon();
        boolean isRoleDeleted = roleManagementPage.verifyNoRecordsMessage();
        softAssert.assertTrue(isRoleDeleted, "Failed to delete role");
        softAssert.assertAll();
    }


    @TestCaseID(ids = {"T1102"})
    @Test
    public void verifyRoleCreationAuditLog() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        RoleManagementPage roleManagementPage = new RoleManagementPage(driver);
        roleManagementPage.clickOnRoleManagementTab();
        String name = "ROLE_" + RandomDataUtils.randomAlphabetic(4);
        String code = "CODE_" + RandomDataUtils.randomAlphabetic(4);
        String status = "Active";
        roleManagementPage.createRole(name, code, status);
        driver.sleep(WaitTime.SMALL_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.SMALL_WAIT);
        roleManagementPage.searchRoleByName(name);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean displayStatus = roleManagementPage.isRoleDisplayed(name, code);
        softAssert.assertTrue(displayStatus, "Failed to create new role");
        String roleId = roleManagementPage.getRoleId();
        AuditLogPage auditLogPage = new AuditLogPage(driver);
        homePage.clickOnEnterprise();
        auditLogPage.clickOnAuditLogTab();
        driver.sleep(WaitTime.LONG_WAIT);
        auditLogPage.searchById(roleId);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        String entityName = "Role";
        String action = "CREATE";
        boolean entityDisplayStatus = auditLogPage.isAuditLogDisplayed(entityName, roleId, action);
        softAssert.assertTrue(entityDisplayStatus, "Failed to display new role audit log");
        softAssert.assertAll();
    }

}