package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.SupplierCapacityAppPage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class SupplierCapacityAppTest extends FrameworkTestBase {

    private static final Logger logger = LogManager.getLogger(SupplierCapacityAppPage.class);
    LogFactory log = new LogFactory(logger);


    @TestCaseID(ids = {"IENT-2734"})
    @Test
    public void supplierCapacityCreationWithApprovedStatus() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSupplierAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnSupplierCapacity();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        SupplierCapacityAppPage supplierCapacityPage = new SupplierCapacityAppPage(driver);
        String part = "1";
        String supplierLocation = "1";
        String locationType = "1";
        String capacity = "200";
        String upside = "100";
        String caseStatus = "Save As Draft";
        String approverName = testContext.app.getBuyerUser();
        String shortDescription = RandomDataUtils.randomAlphabetic(8);
        supplierCapacityPage.createSupplierCapacity(part, supplierLocation, locationType, capacity, upside, approverName, caseStatus, shortDescription);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierCapacityPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean newSupplierCapacityPageCreatedStatus = supplierCapacityPage.isSupplierCapacityDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(newSupplierCapacityPageCreatedStatus, "Failed to create new Supplier Capacity");
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierCapacityPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        String submitToBuyerStatus = "Submit to Buyer";
        supplierCapacityPage.editStatus(submitToBuyerStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.SMALL_WAIT);

        login.loginWithBuyerUser(testContext.app);
        homePage.clickOnSupplierCapacity();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierCapacityPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean supplierCapacitySubmitToBuyerStatus = supplierCapacityPage.isSupplierCapacityDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(supplierCapacitySubmitToBuyerStatus, "Failed to find Submit to Buyer Supplier Capacity");
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierCapacityPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        String reworkStatus = "Rework";
        supplierCapacityPage.editStatus(reworkStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.SMALL_WAIT);

        login.loginWithSupplierAdmin(testContext.app);
        homePage.clickOnSupplierCapacity();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierCapacityPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean supplierCapacityReworkStatus = supplierCapacityPage.isSupplierCapacityDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(supplierCapacityReworkStatus, "Failed to find Rework Supplier Capacity");
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierCapacityPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierCapacityPage.editStatus(submitToBuyerStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.SMALL_WAIT);

        login.loginWithBuyerUser(testContext.app);
        homePage.clickOnSupplierCapacity();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierCapacityPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean supplierCapacityLastStatus = supplierCapacityPage.isSupplierCapacityDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(supplierCapacityLastStatus, "Failed to find Submit to Buyer Supplier Capacity");
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierCapacityPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        String approvedStatus = "Approved";
        supplierCapacityPage.editStatus(approvedStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.SMALL_WAIT);

        login.loginWithSupplierAdmin(testContext.app);
        homePage.clickOnSupplierCapacity();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierCapacityPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean supplierCapacityApprovedStatus = supplierCapacityPage.isSupplierCapacityDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(supplierCapacityApprovedStatus, "Failed to find Approved Supplier Capacity");
        driver.sleep(WaitTime.SMALL_WAIT);
        try {
            supplierCapacityPage.selectsRecordAndClicksEdit();
            driver.sleep(WaitTime.SMALL_WAIT);
            String rejectedStatus = "Rejected";
            supplierCapacityPage.editStatus(rejectedStatus);
        } catch (Exception e) {
            log.info("This Supplier Capacity record is no longer editable");
        }

    }

    @TestCaseID(ids = {"IENT-2734"})
    @Test
    public void shippingCalendarCreationWithRejectedStatus() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSupplierAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnSupplierCapacity();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        SupplierCapacityAppPage supplierCapacityPage = new SupplierCapacityAppPage(driver);
        String part = "1";
        String supplierLocation = "1";
        String locationType = "1";
        String capacity = "200";
        String upside = "100";
        String caseStatus = "Submit to Buyer";
        String approverName = testContext.app.getBuyerUser();
        String shortDescription = RandomDataUtils.randomAlphabetic(8);
        supplierCapacityPage.createSupplierCapacity(part, supplierLocation, locationType, capacity, upside, approverName, caseStatus, shortDescription);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierCapacityPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean newSupplierCapacityPageCreatedStatus = supplierCapacityPage.isSupplierCapacityDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(newSupplierCapacityPageCreatedStatus, "Failed to create new Supplier Capacity");
        driver.sleep(WaitTime.SMALL_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.SMALL_WAIT);

        login.loginWithBuyerUser(testContext.app);
        homePage.clickOnSupplierCapacity();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierCapacityPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean supplierCapacitySubmitToBuyerStatus = supplierCapacityPage.isSupplierCapacityDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(supplierCapacitySubmitToBuyerStatus, "Failed to find Submit to Buyer Supplier Capacity");
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierCapacityPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        String rejectedStatus = "Rejected";
        supplierCapacityPage.editStatus(rejectedStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.SMALL_WAIT);

        login.loginWithSupplierAdmin(testContext.app);
        homePage.clickOnSupplierCapacity();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierCapacityPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean supplierCapacityApprovedStatus = supplierCapacityPage.isSupplierCapacityDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(supplierCapacityApprovedStatus, "Failed to find Approved Supplier Capacity");
        driver.sleep(WaitTime.SMALL_WAIT);
        try {
            supplierCapacityPage.selectsRecordAndClicksEdit();
            driver.sleep(WaitTime.SMALL_WAIT);
            String approvedStatus = "Approved";
            supplierCapacityPage.editStatus(approvedStatus);
        } catch (Exception e) {
            log.info("This Supplier Capacity record is no longer editable");
        }

    }

    @TestCaseID(ids = {"IENT-2735"})
    @Test
    public void verifyBuyerAdminSupplierCapacity(){
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        String supplierLocation = "mpl";
        String locationType = "slt";
        homePage.clickOnSupplierCapacity();
        driver.sleep(WaitTime.SMALL_WAIT);
        SupplierCapacityAppPage supplierCapacityPage = new SupplierCapacityAppPage(driver);
        supplierCapacityPage.clickFilterIcon();
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierCapacityPage.buyerAdminFilters();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean supplierCapacityApprovedStatus = supplierCapacityPage.isSupplierCapacityDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(supplierCapacityApprovedStatus, "Failed to find Supplier Capacity with given filters");
    }

    @TestCaseID(ids = {"IENT-2736"})
    @Test
    public void verifySupplierAdminSupplierCapacity(){
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSupplierAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        String supplierLocation = "mpl";
        String locationType = "slt";
        homePage.clickOnSupplierCapacity();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        SupplierCapacityAppPage supplierCapacityPage = new SupplierCapacityAppPage(driver);
        supplierCapacityPage.clickFilterIcon();
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierCapacityPage.supplierAdminFilters();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean supplierCapacityApprovedStatus = supplierCapacityPage.isSupplierCapacityDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(supplierCapacityApprovedStatus, "Failed to find Supplier Capacity with given filters");
    }

}