package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.EmailTemplatesPage;
import io.ient.test.pages.enterprise.PortalConfigurationPage;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class PortalConfigurationTest extends FrameworkTestBase {

    @TestCaseID(ids = {"T1700"})
    @Test
    public void verifyEditPortalConfigurationTemplate() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        PortalConfigurationPage portalConfigurationPage = new PortalConfigurationPage(driver);
        portalConfigurationPage.clickOnPortalConfigurationTab();
        String name = "NAME_" + RandomDataUtils.randomAlphabetic(5);
        String body = "BodyText_" + RandomDataUtils.randomAlphabetic(7);
        portalConfigurationPage.editPortalConfiguration(name, body);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean isHeaderTextMatched = portalConfigurationPage.verifyHeaderName(name);
        softAssert.assertTrue(isHeaderTextMatched, "Failed to update portal configuration");
        softAssert.assertAll();
    }
    @TestCaseID(ids = {"T1701"})
    @Test
    public void verifyPortalConfigurationPageFeaturesWithBuyerAdmin() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        PortalConfigurationPage portalConfigurationPage = new PortalConfigurationPage(driver);
        portalConfigurationPage.clickOnPortalConfigurationTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        portalConfigurationPage.clickEditButton();
        driver.sleep(WaitTime.SMALL_WAIT);

        boolean isOrganizationNameTextBoxDisplayed = portalConfigurationPage.isOrganizationNameTextBoxDisplayed();
        softAssert.assertTrue(isOrganizationNameTextBoxDisplayed, "Failed to display OrganizationName TextBox");

        boolean isBodyContentTextAreaDivDisplayed = portalConfigurationPage.isBodyContentTextAreaDivDisplayed();
        softAssert.assertTrue(isBodyContentTextAreaDivDisplayed, "Failed to display BodyContentTextAreaDiv TextBox");

        boolean isBodyImageFieldDisplayed = portalConfigurationPage.isBodyImageFieldDisplayed();
        softAssert.assertTrue(isBodyImageFieldDisplayed, "Failed to display BodyImageField");

        boolean isLogoImageFieldDisplayed = portalConfigurationPage.isLogoImageFieldDisplayed();
        softAssert.assertTrue(isLogoImageFieldDisplayed, "Failed to display LogoImageField");

        boolean isBodyColorFieldDisplayed = portalConfigurationPage.isBodyColorFieldDisplayed();
        softAssert.assertTrue(isBodyColorFieldDisplayed, "Failed to display BodyColorField");

        boolean isClearButtonDisplayed = portalConfigurationPage.isClearButtonDisplayed();
        softAssert.assertTrue(isClearButtonDisplayed, "Failed to display ClearButton");

        boolean isCancelButtonDisplayed = portalConfigurationPage.isCancelButtonDisplayed();
        softAssert.assertTrue(isCancelButtonDisplayed, "Failed to display CancelButton");

        boolean isSubmitButtonDisplayed = portalConfigurationPage.isSubmitButtonDisplayed();
        softAssert.assertTrue(isSubmitButtonDisplayed, "Failed to display SubmitButton");
    }
}
