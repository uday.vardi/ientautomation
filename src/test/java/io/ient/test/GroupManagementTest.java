package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.GroupManagementPage;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class GroupManagementTest extends FrameworkTestBase {

    @TestCaseID(ids = {"T1243"})
    @Test
    public void verifyNewGroupCreation() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        GroupManagementPage groupManagementPage = new GroupManagementPage(driver);
        groupManagementPage.clickOnGroupManagementTab();
        String uniqueName = "UG" + RandomDataUtils.randomAlphabetic(5);
        String name = "GP_" + RandomDataUtils.randomAlphabetic(5);
        String status = "Active";
        groupManagementPage.createGroup(uniqueName, name, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        groupManagementPage.clearSearchTextField();
        groupManagementPage.searchGroupByName(name);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean displayStatus = groupManagementPage.isGroupDisplayed(name);
        softAssert.assertTrue(displayStatus, "Failed to create new group");
        String updateName = name + "_EDIT";
        groupManagementPage.editGroup(updateName, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        groupManagementPage.clearSearchTextField();
        groupManagementPage.searchGroupByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean updatedGroupDisplayStatus = groupManagementPage.isGroupDisplayed(updateName);
        softAssert.assertTrue(updatedGroupDisplayStatus, "Failed to update group");
        groupManagementPage.deleteGroup();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        groupManagementPage.clearSearchTextField();
        groupManagementPage.searchGroupByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        groupManagementPage.clickOnSearchIcon();
        boolean isGroupDeleted = groupManagementPage.verifyNoRecordsMessage();
        softAssert.assertTrue(isGroupDeleted, "Failed to delete group");
        softAssert.assertAll();
    }

}
