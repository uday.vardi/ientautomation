package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.DateUtils;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.SupplierProductTransitionsPage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class SupplierProductTransitionsTest extends FrameworkTestBase {

    private static final Logger logger = LogManager.getLogger(SupplierProductTransitionsPage.class);
    LogFactory log = new LogFactory(logger);


    @TestCaseID(ids = {"IENT-2750"})
    @Test
    public void productTransitionsCreationWithApprovedStatus() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSupplierAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnProductTransitions();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        SupplierProductTransitionsPage productTransitionsPage = new SupplierProductTransitionsPage(driver);
        String part = "1";
        String manufacturer = RandomDataUtils.randomAlphabetic(8);
        String lifecycleStatus = "Phase-out";
        String lastTimeBuyDate = DateUtils.getFormattedDateAndTimeWithTimeZone(5, 0,"d","IST");
        String estimatedEOLDate = DateUtils.getFormattedDateAndTimeWithTimeZone(7, 0,"d","IST");
        String caseStatus = "Save as Draft";
        String alternateNewPart = "brakes";
        String newPartAvailableDate = DateUtils.getFormattedDateAndTimeWithTimeZone(8, 0,"d","IST");
        String shortDescription = RandomDataUtils.randomAlphabetic(8);
        String approverName = testContext.app.getBuyerUser();
        productTransitionsPage.createProductTransitions(part, manufacturer, lifecycleStatus, lastTimeBuyDate, estimatedEOLDate, caseStatus, approverName, alternateNewPart, newPartAvailableDate, shortDescription);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        productTransitionsPage.searchByManufacturer(manufacturer);
        boolean newProductTransitionsCreatedStatus = productTransitionsPage.isProductTransitionsDisplayed(part);
        softAssert.assertTrue(newProductTransitionsCreatedStatus, "Failed to create new Product Transitions");
        driver.sleep(WaitTime.SMALL_WAIT);
        productTransitionsPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        String submitToBuyerStatus = "Submit to Buyer";
        productTransitionsPage.editStatus(submitToBuyerStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);

        login.loginWithBuyerUser(testContext.app);
        homePage.clickOnProductTransitions();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        productTransitionsPage.searchByManufacturer(manufacturer);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean productTransitionsSubmitToBuyerStatus = productTransitionsPage.isProductTransitionsDisplayed(part);
        softAssert.assertTrue(productTransitionsSubmitToBuyerStatus, "Failed to find Submit to Buyer Product Transitions");
        driver.sleep(WaitTime.SMALL_WAIT);
        productTransitionsPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        String reworkStatus = "Rework";
//        productTransitionsPage.addComment();
        productTransitionsPage.editStatus(reworkStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);

        login.loginWithSupplierAdmin(testContext.app);
        homePage.clickOnProductTransitions();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        productTransitionsPage.searchByManufacturer(manufacturer);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean productTransitionsReworkStatus = productTransitionsPage.isProductTransitionsDisplayed(part);
        softAssert.assertTrue(productTransitionsReworkStatus, "Failed to find Rework Product Transitions");
        driver.sleep(WaitTime.SMALL_WAIT);
        productTransitionsPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        productTransitionsPage.editStatus(submitToBuyerStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);

        login.loginWithBuyerUser(testContext.app);
        homePage.clickOnProductTransitions();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        productTransitionsPage.searchByManufacturer(manufacturer);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean productTransitionsLastStatus = productTransitionsPage.isProductTransitionsDisplayed(part);
        softAssert.assertTrue(productTransitionsLastStatus, "Failed to find Submit to Buyer Product Transitions");
        driver.sleep(WaitTime.SMALL_WAIT);
        productTransitionsPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        String approvedStatus = "Approved";
        productTransitionsPage.editStatus(approvedStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);

        login.loginWithSupplierAdmin(testContext.app);
        homePage.clickOnProductTransitions();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        productTransitionsPage.searchByManufacturer(manufacturer);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean productTransitionsApprovedStatus = productTransitionsPage.isProductTransitionsDisplayed(part);
        softAssert.assertTrue(productTransitionsApprovedStatus, "Failed to find Approved Product Transitions");
        driver.sleep(WaitTime.SMALL_WAIT);
        try {
            productTransitionsPage.selectsRecordAndClicksEdit();
            driver.sleep(WaitTime.SMALL_WAIT);
            String rejectedStatus = "Rejected";
            productTransitionsPage.editStatus(rejectedStatus);
        } catch (Exception e) {
            log.info("This Product Transitions record is no longer editable");
        }

    }

    @TestCaseID(ids = {"IENT-2750"})
    @Test
    public void productTransitionsCreationWithRejectedStatus() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSupplierAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnProductTransitions();
        driver.sleep(WaitTime.SMALL_WAIT);
        SupplierProductTransitionsPage productTransitionsPage = new SupplierProductTransitionsPage(driver);
        String part = "1";
        String manufacturer = RandomDataUtils.randomAlphabetic(8);
        String lifecycleStatus = "Phase-out";
        String lastTimeBuyDate = DateUtils.getFormattedDateAndTimeWithTimeZone(5, 0,"d","IST");
        String estimatedEOLDate = DateUtils.getFormattedDateAndTimeWithTimeZone(7, 0,"d","IST");
        String caseStatus = "Submit to Buyer";
        String alternateNewPart = "1";
        String newPartAvailableDate = DateUtils.getFormattedDateAndTimeWithTimeZone(8, 0,"d","IST");
        String shortDescription = RandomDataUtils.randomAlphabetic(8);
        String approverName = testContext.app.getBuyerUser();
        productTransitionsPage.createProductTransitions(part, manufacturer, lifecycleStatus, lastTimeBuyDate, estimatedEOLDate, caseStatus, approverName, alternateNewPart, newPartAvailableDate, shortDescription);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        productTransitionsPage.searchByManufacturer(manufacturer);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean newProductTransitionsCreatedStatus = productTransitionsPage.isProductTransitionsDisplayed(part);
        softAssert.assertTrue(newProductTransitionsCreatedStatus, "Failed to create new Product Transitions");
        driver.sleep(WaitTime.SMALL_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);

        login.loginWithBuyerUser(testContext.app);
        homePage.clickOnProductTransitions();
        driver.sleep(WaitTime.SMALL_WAIT);
        productTransitionsPage.searchByManufacturer(manufacturer);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean productTransitionsSubmitToBuyerStatus = productTransitionsPage.isProductTransitionsDisplayed(part);
        softAssert.assertTrue(productTransitionsSubmitToBuyerStatus, "Failed to find Submit to Buyer Product Transitions");
        driver.sleep(WaitTime.SMALL_WAIT);
        productTransitionsPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        String rejectedStatus = "Rejected";
//        productTransitionsPage.addComment();
        productTransitionsPage.editStatus(rejectedStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);

        login.loginWithSupplierAdmin(testContext.app);
        homePage.clickOnProductTransitions();
        driver.sleep(WaitTime.SMALL_WAIT);
        productTransitionsPage.searchByManufacturer(manufacturer);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean productTransitionsApprovedStatus = productTransitionsPage.isProductTransitionsDisplayed(part);
        softAssert.assertTrue(productTransitionsApprovedStatus, "Failed to find Approved Product Transitions");
        driver.sleep(WaitTime.SMALL_WAIT);
        try {
            productTransitionsPage.selectsRecordAndClicksEdit();
            driver.sleep(WaitTime.SMALL_WAIT);
            String approvedStatus = "Approved";
            productTransitionsPage.editStatus(approvedStatus);
        } catch (Exception e) {
            log.info("This Product Transitions record is no longer editable");
        }

    }

    @TestCaseID(ids = {"IENT-2751"})
    @Test
    public void verifyBuyerAdminSupplierCapacity(){
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerUser(testContext.app);
        HomePage homePage = new HomePage(driver);
        String part = "pt";
        homePage.clickOnProductTransitions();
        driver.sleep(WaitTime.SMALL_WAIT);
        SupplierProductTransitionsPage productTransitionsPage = new SupplierProductTransitionsPage(driver);
        productTransitionsPage.clickFilterIcon();
        driver.sleep(WaitTime.SMALL_WAIT);
        productTransitionsPage.buyerAdminFilters();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean productTransitionsApprovedStatus = productTransitionsPage.isProductTransitionsDisplayed(part);
        softAssert.assertTrue(productTransitionsApprovedStatus, "Failed to find Product Transitions with given filters");
    }

    @TestCaseID(ids = {"IENT-2752","IENT-2753"})
    @Test
    public void verifySupplierAdminSupplierCapacity(){
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSupplierAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        String part = "pt";
        homePage.clickOnProductTransitions();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        SupplierProductTransitionsPage productTransitionsPage = new SupplierProductTransitionsPage(driver);
        productTransitionsPage.clickFilterIcon();
        driver.sleep(WaitTime.SMALL_WAIT);
        productTransitionsPage.supplierAdminFilters();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean productTransitionsApprovedStatus = productTransitionsPage.isProductTransitionsDisplayed(part);
        softAssert.assertTrue(productTransitionsApprovedStatus, "Failed to find Product Transitions with given filters");
    }

}