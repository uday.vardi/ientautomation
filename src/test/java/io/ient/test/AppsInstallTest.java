package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.AppsInstallPage;
import io.ient.test.pages.enterprise.SupplierShippingCalendarPage;
import io.ient.test.pages.enterprise.SupplyChainIssueMgmtPage;
import io.ient.test.pages.enterprise.TenantsPage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class AppsInstallTest extends FrameworkTestBase {

    private static final Logger logger = LogManager.getLogger(AppsInstallPage.class);
    LogFactory log = new LogFactory(logger);

//    String tenantName = "flick";

    public void getInstallationApproved(String appName) {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        HomePage homePage = new HomePage(driver);
        AppsInstallPage appsInstallPage = new AppsInstallPage(driver);
        TenantsPage tenantsPage = new TenantsPage(driver);
        login.loginWithSuperAdmin(testContext.app);
        homePage.clickOnEnterprise();
        appsInstallPage.clickOnAppInstallRequestsTab();
        String tenantName = testContext.app.getAgency();
        tenantsPage.clearSearchTextField();
        tenantsPage.searchTenantByTenantName(tenantName);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean isAppInstallRequestDisplayed = appsInstallPage.isApplicationInstallRequestRowDisplayed(tenantName,appName);
        softAssert.assertTrue(isAppInstallRequestDisplayed, "Failed to search app install request");
        if(isAppInstallRequestDisplayed){
            String approveComment = RandomDataUtils.randomAlphabetic(5).toLowerCase();
            appsInstallPage.approveApplicationInstallationRequest(tenantName, approveComment,appName);
            appsInstallPage.clickOnApproveTab();
            driver.sleep(WaitTime.SMALL_WAIT);
            homePage.clickOnAdminDropDownLink();
            homePage.clickOnLogoutLink();
        }
        softAssert.assertAll();
    }

    public void installAndDeploy(String appName){
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnMarketPlaceButton();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        AppsInstallPage appsInstallPage = new AppsInstallPage(driver);
        boolean isInstallStatusDisplayed = appsInstallPage.isApplicationInstallTextDisplayed(appName);
        softAssert.assertTrue(isInstallStatusDisplayed, "Approved Install not found for Shipping Calendar App");
        if(isInstallStatusDisplayed){
            appsInstallPage.installApplication(appName);
            driver.sleep(WaitTime.MEDIUM_WAIT);
            boolean isAppDisplayedUnderInstalledApps = appsInstallPage.isAppDisplayedUnderInstalledApps(appName);
            softAssert.assertTrue(isAppDisplayedUnderInstalledApps, "Failed to Shipping Calendar App under Installed Apps");
            homePage.clickOnProject();
            driver.sleep(WaitTime.MEDIUM_WAIT);
            driver.refresh();
            boolean isAppDisplayedInProjects = appsInstallPage.isApplicationDisplayedInProjects(appName);
            softAssert.assertTrue(isAppDisplayedInProjects, "Failed to display app in projects after installation");
            driver.sleep(WaitTime.MEDIUM_WAIT);
            appsInstallPage.deployInstalledApp();
            driver.sleep(WaitTime.MEDIUM_WAIT);
        }
        softAssert.assertAll();
    }

    public void appInstallationAndDeployment(String appName) {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        SupplyChainIssueMgmtPage supplyChainIssueMgmtPage = new SupplyChainIssueMgmtPage(driver);
        homePage.clickOnMarketPlaceButton();
        driver.sleep(WaitTime.MEDIUM_WAIT);

        AppsInstallPage appsInstallPage = new AppsInstallPage(driver);
        boolean isAppAlreadyUnderInstalledApps = appsInstallPage.isAppDisplayedUnderInstalledApps(appName);
        if(!isAppAlreadyUnderInstalledApps){
            appsInstallPage.searchByApplicationName(appName);
            driver.sleep(WaitTime.SMALL_WAIT);
            boolean appDisplayStatus = appsInstallPage.isAppDisplayed(appName);
            softAssert.assertTrue(appDisplayStatus, "App is not appeared");
            driver.sleep(WaitTime.SMALL_WAIT);
            boolean isRequestAccessDisplayedStatus = appsInstallPage.isApplicationRequestAccessTextDisplayed(appName);
            boolean isAccessRequestedDisplayedStatus = appsInstallPage.isApplicationAccessRequestedTextDisplayed(appName);
            boolean isInstallDisplayedStatus = appsInstallPage.isApplicationInstallTextDisplayed(appName);
            if(isRequestAccessDisplayedStatus == true && isAccessRequestedDisplayedStatus == false && isInstallDisplayedStatus == false){
                log.info("*************** App Installation Status is REQUEST ACCESS ***********");
                appsInstallPage.clickOnApplicationRequestAccessIcon(appName);
                supplyChainIssueMgmtPage.clickOnDeleteDialogYesButton();
                driver.sleep(WaitTime.SMALL_WAIT);
                homePage.clickOnAdminDropDownLink();
                homePage.clickOnLogoutLink();
                getInstallationApproved(appName);
                installAndDeploy(appName);
            }else if(isRequestAccessDisplayedStatus == false && isAccessRequestedDisplayedStatus == true && isInstallDisplayedStatus == false){
                log.info("*************** App Installation Status is ACCESS REQUESTED ***************");
                homePage.clickOnAdminDropDownLink();
                homePage.clickOnLogoutLink();
                getInstallationApproved(appName);
                installAndDeploy(appName);
            }else if(isRequestAccessDisplayedStatus == false && isAccessRequestedDisplayedStatus == false && isInstallDisplayedStatus == true){
                log.info("*************** App Installation Status is INSTALL ***************");
                appsInstallPage.installApplication(appName);
                driver.sleep(WaitTime.MEDIUM_WAIT);
                boolean isAppDisplayedUnderInstalledApps = appsInstallPage.isAppDisplayedUnderInstalledApps(appName);
                softAssert.assertTrue(isAppDisplayedUnderInstalledApps, "Failed to find installed app under Installed Apps");
                homePage.clickOnProject();
                driver.sleep(WaitTime.MEDIUM_WAIT);
                driver.refresh();
                boolean isAppDisplayedInProjects = appsInstallPage.isApplicationDisplayedInProjects(appName);
                softAssert.assertTrue(isAppDisplayedInProjects, "Failed to display app in projects after installation");
                driver.sleep(WaitTime.MEDIUM_WAIT);
                appsInstallPage.deployInstalledApp();
                driver.sleep(WaitTime.MEDIUM_WAIT);
            }
        }else{
            homePage.clickOnProject();
            driver.sleep(WaitTime.MEDIUM_WAIT);
            driver.refresh();
            boolean isAppDisplayedInProjects = appsInstallPage.isApplicationDisplayedInProjects(appName);
            softAssert.assertTrue(isAppDisplayedInProjects, "Failed to display app in projects after installation");
            driver.sleep(WaitTime.MEDIUM_WAIT);
            appsInstallPage.deployInstalledApp();
            driver.sleep(WaitTime.MEDIUM_WAIT);
        }
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-2784"})
    @Test
    public void getShippingCalendarApp(){
        appInstallationAndDeployment("Shipping Calendar");
    }

    @TestCaseID(ids = {"IENT-3073"})
    @Test
    public void getBulletinsApp(){
        appInstallationAndDeployment("Bulletins");
    }

    @TestCaseID(ids = {"IENT-2791"})
    @Test
    public void getShippingLanesApp(){
        appInstallationAndDeployment("Shipping Lanes");
    }

    @TestCaseID(ids = {"IENT-2796"})
    @Test
    public void getPartSourcingAttributesApp(){
        appInstallationAndDeployment("Part Sourcing Attributes");
    }

    @TestCaseID(ids = {"IENT-2733"})
    @Test
    public void getSupplierCapacityApp(){
        appInstallationAndDeployment("Supplier Capacity");
    }

    @TestCaseID(ids = {"IENT-2749"})
    @Test
    public void getProductTransitionsApp(){
        appInstallationAndDeployment("Product Transitions");
    }

    @TestCaseID(ids = {"IENT-2695"})
    @Test
    public void getHolidayCalendarApp(){
        appInstallationAndDeployment("Holiday Calendar");
    }
}