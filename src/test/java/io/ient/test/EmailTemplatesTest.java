package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.EmailTemplatesPage;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class EmailTemplatesTest extends FrameworkTestBase {

    @TestCaseID(ids = {"IENT-735"})
    @Test
    public void verifyNewTemplateCreation() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        EmailTemplatesPage emailTemplatesPage = new EmailTemplatesPage(driver);
        emailTemplatesPage.clickOnEmailTemplatesTab();
        String name = "TEMPLATE_" + RandomDataUtils.randomAlphabetic(5);
        String subject = "Subject_" + RandomDataUtils.randomAlphabetic(5);
        String body = "Test_" + RandomDataUtils.randomAlphabetic(5);
        emailTemplatesPage.createTemplate(name, subject, body);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        emailTemplatesPage.searchTemplateByName(name);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean displayStatus = emailTemplatesPage.isTemplateDisplayed(name);
        softAssert.assertTrue(displayStatus, "Failed to create new template");
        String updateName = name + "_EDIT";
        emailTemplatesPage.editTemplate(updateName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        emailTemplatesPage.clearSearchTextField();
        emailTemplatesPage.searchTemplateByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean updatedTemplateDisplayStatus = emailTemplatesPage.isTemplateDisplayed(updateName);
        softAssert.assertTrue(updatedTemplateDisplayStatus, "Failed to update template");
        emailTemplatesPage.deleteTemplate();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        emailTemplatesPage.clearSearchTextField();
        emailTemplatesPage.searchTemplateByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        emailTemplatesPage.clickOnSearchIcon();
        boolean isTemplateDeleted = emailTemplatesPage.verifyNoRecordsMessage();
        softAssert.assertTrue(isTemplateDeleted, "Failed to delete template");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-1411"})
    @Test
    public void verifyEmailTemplatePageFeaturesWithBuyerAdmin() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        EmailTemplatesPage emailTemplatesPage = new EmailTemplatesPage(driver);
        emailTemplatesPage.clickOnEmailTemplatesTab();
        driver.sleep(WaitTime.LONG_WAIT);
        boolean isSearchBarDisplayed = emailTemplatesPage.isSearchBarDisplayed();
        softAssert.assertTrue(isSearchBarDisplayed, "Failed to display searchbar");
        boolean isSelectColumnsDisplayed = emailTemplatesPage.isSelectColumnsDisplayed();
        softAssert.assertTrue(isSelectColumnsDisplayed, "Failed to display select columns");
        boolean isFilterIconDisplayed = emailTemplatesPage.isFilterIconDisplayed();
        softAssert.assertTrue(isFilterIconDisplayed, "Failed to display filter");
        emailTemplatesPage.clickOnTopLeftVerticalIcon();
        boolean isAddNewButtonDisplayed = emailTemplatesPage.isAddNewButtonDisplayed();
        softAssert.assertTrue(isAddNewButtonDisplayed, "Failed to display AddNewButton");
        boolean isDownloadAsPdfDisplayed = emailTemplatesPage.isDownloadAsPdfDisplayed();
        softAssert.assertTrue(isDownloadAsPdfDisplayed, "Failed to display DownloadAsPdf");
        boolean isDownloadAsCsvDisplayed = emailTemplatesPage.isDownloadAsCsvDisplayed();
        softAssert.assertTrue(isDownloadAsCsvDisplayed, "Failed to display DownloadAsCSV");
        boolean isNumberOfItemsPerPageEditable = emailTemplatesPage.isNumberOfItemsPerPageEditable();
        softAssert.assertTrue(isNumberOfItemsPerPageEditable, "Failed to edit number of items per page");
        boolean isPaginationDisplayed = emailTemplatesPage.isPaginationDisplayed();
        softAssert.assertTrue(isPaginationDisplayed, "Failed to display pagination");
        boolean isPageColumnDisplayed = emailTemplatesPage.isPageColumnDisplayed("ID");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display ID column");
        isPageColumnDisplayed = emailTemplatesPage.isPageColumnDisplayed("Subject");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display Subject column");
        boolean isActionEditAndDeleteDisplayed = emailTemplatesPage.isActionEditAndDeleteDisplayed();
        softAssert.assertTrue(isActionEditAndDeleteDisplayed, "Failed to display action edit delete options");
        softAssert.assertAll();
    }


    @TestCaseID(ids = {"IENT-1412"})
    @Test
    public void verifyEmailTemplatePageFeaturesWithSuperAdmin() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSuperAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        EmailTemplatesPage emailTemplatesPage = new EmailTemplatesPage(driver);
        emailTemplatesPage.clickOnEmailTemplatesTab();
        driver.sleep(WaitTime.LONG_WAIT);
        boolean isSearchBarDisplayed = emailTemplatesPage.isSearchBarDisplayed();
        softAssert.assertTrue(isSearchBarDisplayed, "Failed to display searchbar");
        boolean isSelectColumnsDisplayed = emailTemplatesPage.isSelectColumnsDisplayed();
        softAssert.assertTrue(isSelectColumnsDisplayed, "Failed to display select columns");
        boolean isFilterIconDisplayed = emailTemplatesPage.isFilterIconDisplayed();
        softAssert.assertTrue(isFilterIconDisplayed, "Failed to display filter");
        emailTemplatesPage.clickOnTopLeftVerticalIcon();
        boolean isAddNewButtonDisplayed = emailTemplatesPage.isAddNewButtonDisplayed();
        softAssert.assertTrue(isAddNewButtonDisplayed, "Failed to display AddNewButton");
        boolean isDownloadAsPdfDisplayed = emailTemplatesPage.isDownloadAsPdfDisplayed();
        softAssert.assertTrue(isDownloadAsPdfDisplayed, "Failed to display DownloadAsPdf");
        boolean isDownloadAsCsvDisplayed = emailTemplatesPage.isDownloadAsCsvDisplayed();
        softAssert.assertTrue(isDownloadAsCsvDisplayed, "Failed to display DownloadAsCSV");
        boolean isNumberOfItemsPerPageEditable = emailTemplatesPage.isNumberOfItemsPerPageEditable();
        softAssert.assertTrue(isNumberOfItemsPerPageEditable, "Failed to edit number of items per page");
        boolean isPaginationDisplayed = emailTemplatesPage.isPaginationDisplayed();
        softAssert.assertTrue(isPaginationDisplayed, "Failed to display pagination");
        boolean isPageColumnDisplayed = emailTemplatesPage.isPageColumnDisplayed("ID");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display ID column");
        isPageColumnDisplayed = emailTemplatesPage.isPageColumnDisplayed("Subject");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display Subject column");
        boolean isActionEditAndDeleteDisplayed = emailTemplatesPage.isActionEditAndDeleteDisplayed();
        softAssert.assertTrue(isActionEditAndDeleteDisplayed, "Failed to display action edit delete options");
        softAssert.assertAll();
    }

}
