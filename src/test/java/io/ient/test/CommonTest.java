package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.CommonSectionPage;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.*;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class CommonTest extends FrameworkTestBase {

    @TestCaseID(ids = {"IENT-3394"})
    @Test
    public void verifyColumnsReArrangement() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        HomePage homePage = new HomePage(driver);
        CommonSectionPage commonSectionPage = new CommonSectionPage(driver);
        String fieldName = "ID";

        //Role management
        login.loginWithBuyerAdmin(testContext.app);
        homePage.clickOnEnterprise();
        RoleManagementPage roleManagementPage = new RoleManagementPage(driver);
        roleManagementPage.clickOnRoleManagementTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        int columnNumber_before_reorder_rm = commonSectionPage.getColumnNumber(fieldName);
        System.out.println("Column number role management: "+columnNumber_before_reorder_rm);
        commonSectionPage.clickOnColumnSelectionIcon();
        commonSectionPage.reorderColumn(fieldName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        login.loginWithBuyerAdmin(testContext.app);
        driver.sleep(WaitTime.SMALL_WAIT);
        homePage.clickOnEnterprise();
        roleManagementPage.clickOnRoleManagementTab();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        int columnNumber_after_reorder_rm = commonSectionPage.getColumnNumber(fieldName);
        System.out.println("Column number role management: "+columnNumber_after_reorder_rm);
        commonSectionPage.clickOnColumnSelectionIcon();
        commonSectionPage.resetReorderColumn(fieldName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        softAssert.assertFalse(columnNumber_before_reorder_rm==columnNumber_after_reorder_rm, "Failed to reorder columns in role management page");

        //User management
        login.loginWithBuyerAdmin(testContext.app);
        homePage.clickOnEnterprise();
        UserManagementPage UserManagementPage = new UserManagementPage(driver);
        UserManagementPage.clickOnUserManagementTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        int columnNumber_before_reorder_um = commonSectionPage.getColumnNumber(fieldName);
        System.out.println("Column number user management: "+columnNumber_before_reorder_um);
        commonSectionPage.clickOnColumnSelectionIcon();
        commonSectionPage.reorderColumn(fieldName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        login.loginWithBuyerAdmin(testContext.app);
        driver.sleep(WaitTime.SMALL_WAIT);
        homePage.clickOnEnterprise();
        UserManagementPage.clickOnUserManagementTab();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        int columnNumber_after_reorder_um = commonSectionPage.getColumnNumber(fieldName);
        System.out.println("Column number user management: "+columnNumber_after_reorder_um);
        commonSectionPage.clickOnColumnSelectionIcon();
        commonSectionPage.resetReorderColumn(fieldName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        softAssert.assertFalse(columnNumber_before_reorder_um==columnNumber_after_reorder_um, "Failed to reorder columns in user management page");

        //Business organizations
        login.loginWithBuyerAdmin(testContext.app);
        homePage.clickOnEnterprise();
        BusinessOrganizationsPage businessOrganizationsPage = new BusinessOrganizationsPage(driver);
        businessOrganizationsPage.clickOnBusinessOrganizationsTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        int columnNumber_before_reorder_bo = commonSectionPage.getColumnNumber(fieldName);
        System.out.println("Column number business organizations: "+columnNumber_before_reorder_bo);
        commonSectionPage.clickOnColumnSelectionIcon();
        commonSectionPage.reorderColumn(fieldName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        login.loginWithBuyerAdmin(testContext.app);
        driver.sleep(WaitTime.SMALL_WAIT);
        homePage.clickOnEnterprise();
        businessOrganizationsPage.clickOnBusinessOrganizationsTab();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        int columnNumber_after_reorder_bo = commonSectionPage.getColumnNumber(fieldName);
        System.out.println("Column number business organizations: "+columnNumber_after_reorder_bo);
        commonSectionPage.clickOnColumnSelectionIcon();
        commonSectionPage.resetReorderColumn(fieldName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        softAssert.assertFalse(columnNumber_before_reorder_bo==columnNumber_after_reorder_bo, "Failed to reorder columns in business organizations page");


        //Global Settings
        login.loginWithBuyerAdmin(testContext.app);
        homePage.clickOnEnterprise();
        GlobalSettingsPage globalSettingsPage = new GlobalSettingsPage(driver);
        globalSettingsPage.clickOnGlobalSettingsTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        int columnNumber_before_reorder_gs = commonSectionPage.getColumnNumber(fieldName);
        System.out.println("Column number global settings: "+columnNumber_before_reorder_gs);
        commonSectionPage.clickOnColumnSelectionIcon();
        commonSectionPage.reorderColumn(fieldName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        login.loginWithBuyerAdmin(testContext.app);
        driver.sleep(WaitTime.SMALL_WAIT);
        homePage.clickOnEnterprise();
        globalSettingsPage.clickOnGlobalSettingsTab();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        int columnNumber_after_reorder_gs = commonSectionPage.getColumnNumber(fieldName);
        System.out.println("Column number global settings: "+columnNumber_after_reorder_gs);
        commonSectionPage.clickOnColumnSelectionIcon();
        commonSectionPage.resetReorderColumn(fieldName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        softAssert.assertFalse(columnNumber_before_reorder_gs==columnNumber_after_reorder_gs, "Failed to reorder columns in global settings page");

        //Division Management
        login.loginWithBuyerAdmin(testContext.app);
        homePage.clickOnEnterprise();
        BulletinSubscriptionsPage bulletinSubscriptionsPage = new BulletinSubscriptionsPage(driver);
        bulletinSubscriptionsPage.clickOnDivisionManagementTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        int columnNumber_before_reorder_dm = commonSectionPage.getColumnNumber(fieldName);
        System.out.println("Column number division management: "+columnNumber_before_reorder_dm);
        commonSectionPage.clickOnColumnSelectionIcon();
        commonSectionPage.reorderColumn(fieldName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        login.loginWithBuyerAdmin(testContext.app);
        driver.sleep(WaitTime.SMALL_WAIT);
        homePage.clickOnEnterprise();
        bulletinSubscriptionsPage.clickOnDivisionManagementTab();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        int columnNumber_after_reorder_dm = commonSectionPage.getColumnNumber(fieldName);
        System.out.println("Column number division management: "+columnNumber_after_reorder_dm);
        commonSectionPage.clickOnColumnSelectionIcon();
        commonSectionPage.resetReorderColumn(fieldName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        softAssert.assertFalse(columnNumber_before_reorder_dm==columnNumber_after_reorder_dm, "Failed to reorder columns in division management page");


        //Import export history
        String fieldName_ieh = "From";
        login.loginWithBuyerAdmin(testContext.app);
        homePage.clickOnEnterprise();
        homePage.clickOnImportExportHistoryTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        int columnNumber_before_reorder_ieh = commonSectionPage.getColumnNumber(fieldName_ieh);
        System.out.println("Column number import export history: "+columnNumber_before_reorder_ieh);
        commonSectionPage.clickOnColumnSelectionIcon();
        commonSectionPage.reorderColumn(fieldName_ieh);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        login.loginWithBuyerAdmin(testContext.app);
        driver.sleep(WaitTime.SMALL_WAIT);
        homePage.clickOnEnterprise();
        homePage.clickOnImportExportHistoryTab();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        int columnNumber_after_reorder_ieh = commonSectionPage.getColumnNumber(fieldName_ieh);
        System.out.println("Column number import export history: "+columnNumber_after_reorder_ieh);
        commonSectionPage.clickOnColumnSelectionIcon();
        commonSectionPage.resetReorderColumn(fieldName_ieh);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        softAssert.assertFalse(columnNumber_before_reorder_ieh==columnNumber_after_reorder_ieh, "Failed to reorder columns in import export history page");

        //Supply chain issue management
        String fieldName_scm = "Version";
        login.loginWithBuyerAdmin(testContext.app);
        homePage.clickOnHome();
        homePage.clickOnSupplyChainIssueManagement();
        driver.sleep(WaitTime.SMALL_WAIT);
        int columnNumber_before_reorder_scm = commonSectionPage.getColumnNumber(fieldName_scm);
        System.out.println("Column number Supply chain issue management: "+columnNumber_before_reorder_scm);
        commonSectionPage.clickOnColumnSelectionIcon();
        commonSectionPage.reorderColumn(fieldName_scm);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        login.loginWithBuyerAdmin(testContext.app);
        driver.sleep(WaitTime.SMALL_WAIT);
        homePage.clickOnHome();
        homePage.clickOnSupplyChainIssueManagement();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        int columnNumber_after_reorder_scm = commonSectionPage.getColumnNumber(fieldName_scm);
        System.out.println("Column number Supply chain issue management: "+columnNumber_after_reorder_scm);
        commonSectionPage.clickOnColumnSelectionIcon();
        commonSectionPage.resetReorderColumn(fieldName_scm);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        softAssert.assertFalse(columnNumber_before_reorder_scm==columnNumber_after_reorder_scm, "Failed to reorder columns in Supply chain issue management page");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-3401"})
    @Test
    public void verifyContactSupportEmail() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        HomePage homePage = new HomePage(driver);
        CommonSectionPage commonSectionPage = new CommonSectionPage(driver);
        login.loginWithBuyerAdmin(testContext.app);
        commonSectionPage.openContactSupportPopup();
        String expectedContactSupportText = "Please reach us at contact@rrssi.com for support";
        String contactEmail = "mailto:contact@rrssi.com";
        driver.sleep(WaitTime.SMALL_WAIT);
        String actualContactSupportText = commonSectionPage.getContactSupportText().trim();
        softAssert.assertTrue(actualContactSupportText.equals(expectedContactSupportText), "Failed to display contact support text for buyer admin");
        softAssert.assertTrue(commonSectionPage.isContactSupportEmailDisplayed(contactEmail), "Failed to display contact support email for buyer admin");
        commonSectionPage.closeContactSupportPopup();
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        //Buyer User
        login.loginWithBuyerUser(testContext.app);
        commonSectionPage.openContactSupportPopup();
        driver.sleep(WaitTime.SMALL_WAIT);
        String actualContactSupportText_bu = commonSectionPage.getContactSupportText().trim();
        softAssert.assertTrue(actualContactSupportText_bu.equals(expectedContactSupportText), "Failed to display contact support text for buyer user");
        softAssert.assertTrue(commonSectionPage.isContactSupportEmailDisplayed(contactEmail), "Failed to display contact support email for buyer user");
        commonSectionPage.closeContactSupportPopup();
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        //Supplier User
        login.loginWithSupplierUser(testContext.app);
        commonSectionPage.openContactSupportPopup();
        driver.sleep(WaitTime.SMALL_WAIT);
        String actualContactSupportText_su = commonSectionPage.getContactSupportText().trim();
        softAssert.assertTrue(actualContactSupportText_su.equals(expectedContactSupportText), "Failed to display contact support text for supplier user");
        softAssert.assertTrue(commonSectionPage.isContactSupportEmailDisplayed(contactEmail), "Failed to display contact support email for supplier user");
        commonSectionPage.closeContactSupportPopup();
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        //Supplier Admin
        login.loginWithSupplierUser(testContext.app);
        commonSectionPage.openContactSupportPopup();
        driver.sleep(WaitTime.SMALL_WAIT);
        String actualContactSupportText_sa = commonSectionPage.getContactSupportText().trim();
        softAssert.assertTrue(actualContactSupportText_sa.equals(expectedContactSupportText), "Failed to display contact support text for supplier admin");
        softAssert.assertTrue(commonSectionPage.isContactSupportEmailDisplayed(contactEmail), "Failed to display contact support email for supplier admin");
        commonSectionPage.closeContactSupportPopup();
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        softAssert.assertAll();

    }
}