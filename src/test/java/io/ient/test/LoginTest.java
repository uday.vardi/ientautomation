package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.LoginPage;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class LoginTest extends FrameworkTestBase {

    @TestCaseID(ids = {"T1000"})
    @Test
    public void verifyBuyerUserLogin() {
        SoftAssert softAssert = new SoftAssert();
        String appUrl = testContext.app.getAppUrl();
        String organizationName = testContext.app.getAgency().toLowerCase();
        String userName = testContext.app.getBuyerUser();
        String password = testContext.app.getBuyerPassword();
        LoginPage loginPage = new LoginPage(driver);
        HomePage homePage = new HomePage(driver);
        loginPage.goToApplication(appUrl);
        loginPage.changeOrganizationLogin(organizationName);
        loginPage.login(userName, password);
        boolean isLoggedIn = homePage.isHomePage();
        softAssert.assertTrue(isLoggedIn, "Failed to login as buyer user");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"T1001"})
    @Test
    public void verifyBuyerAdminLogin() {
        SoftAssert softAssert = new SoftAssert();
        String appUrl = testContext.app.getAppUrl();
        String organizationName = testContext.app.getAgency().toLowerCase();
        String userName = testContext.app.getBuyerAdminUser();
        String password = testContext.app.getBuyerAdminPassword();
        LoginPage loginPage = new LoginPage(driver);
        HomePage homePage = new HomePage(driver);
        loginPage.goToApplication(appUrl);
        loginPage.changeOrganizationLogin(organizationName);
        loginPage.login(userName, password);
        boolean isLoggedIn = homePage.isHomePage();
        softAssert.assertTrue(isLoggedIn, "Failed to login as buyer admin");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"T1002"})
    @Test
    public void verifySupplierAdminLogin() {
        SoftAssert softAssert = new SoftAssert();
        String appUrl = testContext.app.getAppUrl();
        String organizationName = testContext.app.getAgency().toLowerCase();
        String userName = testContext.app.getSupplierAdminUser();
        String password = testContext.app.getSupplierAdminPassword();
        LoginPage loginPage = new LoginPage(driver);
        HomePage homePage = new HomePage(driver);
        loginPage.goToApplication(appUrl);
        loginPage.changeOrganizationLogin(organizationName);
        loginPage.login(userName, password);
        boolean isLoggedIn = homePage.isHomePage();
        softAssert.assertTrue(isLoggedIn, "Failed to login as supplier admin");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"T1003"})
    @Test
    public void verifySupplierUserLogin() {
        SoftAssert softAssert = new SoftAssert();
        String appUrl = testContext.app.getAppUrl();
        String organizationName = testContext.app.getAgency().toLowerCase();
        String userName = testContext.app.getSupplierUser();
        String password = testContext.app.getSupplierAdminPassword();
        LoginPage loginPage = new LoginPage(driver);
        HomePage homePage = new HomePage(driver);
        loginPage.goToApplication(appUrl);
        loginPage.changeOrganizationLogin(organizationName);
        loginPage.login(userName, password);
        boolean isLoggedIn = homePage.isHomePage();
        softAssert.assertTrue(isLoggedIn, "Failed to login as supplier user");
        softAssert.assertAll();
    }
}
