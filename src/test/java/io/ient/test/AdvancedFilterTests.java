package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.DateUtils;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.CommonSectionPage;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.*;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class AdvancedFilterTests extends FrameworkTestBase {



    @TestCaseID(ids = {"IENT-2729"})
    @Test
    public void verifyAdvancedFilterFunctionality() {
        CommonSectionPage commonSectionPage = new CommonSectionPage(driver);
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        UserManagementPage UserManagementPage = new UserManagementPage(driver);
        UserManagementPage.clickOnUserManagementTab();
        String userName = "user." + RandomDataUtils.randomAlphabetic(5).toLowerCase();
        String firstName = RandomDataUtils.randomAlphabetic(5).toLowerCase();
        String lastName = RandomDataUtils.randomAlphabetic(5).toLowerCase();
        String emailAddress = RandomDataUtils.getRandomAlphanumeric(5).toLowerCase() + "@gmail.com";
        String role = "Buyer User";
        String salutation = "Mrs";
        String status = "Active";
        UserManagementPage.createUser(userName, firstName, lastName, emailAddress, role, salutation, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        String startDate = DateUtils.getFormattedDateAndTimeWithTimeZone(-1, 0,"d","IST");
        String endDate = DateUtils.getFormattedDateAndTimeWithTimeZone(2, 0,"d","IST");
        String filterName = "Last Updated";
        String condition = "between";
        commonSectionPage.createAdvancedFilter(filterName, condition, startDate, endDate);
        driver.sleep(WaitTime.LONG_WAIT);
        boolean userDisplayStatus = UserManagementPage.isUserDisplayed(userName.toLowerCase(), firstName);
        softAssert.assertTrue(userDisplayStatus, "Failed to filter user");

        homePage.clickOnEnterprise();
        GroupManagementPage groupManagementPage = new GroupManagementPage(driver);
        groupManagementPage.clickOnGroupManagementTab();
        String uniqueName = "UG" + RandomDataUtils.randomAlphabetic(5);
        String name = "GP_" + RandomDataUtils.randomAlphabetic(5);
        status = "Active";
        groupManagementPage.createGroup(uniqueName, name, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        commonSectionPage.createAdvancedFilter(filterName, condition, startDate, endDate);
        driver.sleep(WaitTime.LONG_WAIT);
        boolean groupDisplayStatus = groupManagementPage.isGroupDisplayed(name);
        softAssert.assertTrue(groupDisplayStatus, "Failed to filter group");

        CommodityGroupsPage commodityGroupsPage = new CommodityGroupsPage(driver);
        homePage.clickOnEnterprise();
        commodityGroupsPage.clickOnCommodityGroupTab();
        String commodityGroupName = "CG_" + RandomDataUtils.getRandomAlphanumeric(4);
        status = "Active";
        commodityGroupsPage.createCommodityGroup(commodityGroupName, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        commonSectionPage.createAdvancedFilter(filterName, condition, startDate, endDate);
        driver.sleep(WaitTime.LONG_WAIT);
        boolean commodityGroupDisplayStatus = commodityGroupsPage.isCommodityGroupDisplayed(commodityGroupName);
        softAssert.assertTrue(commodityGroupDisplayStatus, "Failed to filter commodity group");

        homePage.clickOnEnterprise();
        SuppliersPage suppliersPage = new SuppliersPage(driver);
        suppliersPage.clickOnSuppliersTab();
        String registeredBusinessName = "RBN_" + RandomDataUtils.randomAlphabetic(8);
        String registeredTradeName = "RTN_" + RandomDataUtils.randomAlphabetic(7);
        String businessWebsite = "https://www." + RandomDataUtils.getRandomAlphanumeric(5) + ".com";
        String whichOfTheseLanguagesAreYouDoingBusinessIn = "Lang_" + RandomDataUtils.getRandomAlphanumeric(4);
        String whatIsYourGovernmentIssuesBusinessRegistrationNumber = "RN" + RandomDataUtils.randomNumeric(9);
        String ifYouHaveDunBradstreetNumberDunsPleaseIncludeItHere = RandomDataUtils.randomNumeric(2);
        String inWhatYearWasTheBusinessEstablished = RandomDataUtils.randomNumeric(4);
        String ifYouAreNotTheOwnerWhoEstablishedTheBusinessInWhatYearDidYouAcquireIt = RandomDataUtils.randomNumeric(4);
        String pleaseProvideAShortDescriptionOfYourBusiness = RandomDataUtils.randomAlphabetic(20);
        String pleaseProvideADescriptionOfYourBusinessProductsAndServices = RandomDataUtils.randomAlphabetic(30);
        String primaryProductServiceCategory = "Primary_" + RandomDataUtils.randomAlphabetic(4);
        String secondaryProductServiceCategory = "Secondary_" + RandomDataUtils.randomAlphabetic(4);
        String numberOfFullTimeEmployees = RandomDataUtils.randomNumeric(4);
        String whereDoYouCurrentlyFocusOnSellingYourProductsServices = "CU_" + RandomDataUtils.randomAlphabetic(4);
        String whatAreYourBusinessCurrentAndFutureTargetMarkets = "CU_" + RandomDataUtils.randomAlphabetic(4);
        String otherRelevantBusinessCertificationSForExampleIsoDiversityEnvironmentalEtc = RandomDataUtils.randomAlphabetic(20);
        String inWhichCountriesDoesYourBusinessCurrentlyProvideProductsOrServicesDistributionArea = RandomDataUtils.randomAlphabetic(30);
        String whatIsYourBusinessCurrentLegalStructure = "Structure_" + RandomDataUtils.randomAlphabetic(4);
        String whereIsYourPrincipalPlaceOfBusiness = "Place_" + RandomDataUtils.randomAlphabetic(4);
        String address1 = "AD1_" + RandomDataUtils.getRandomAlphanumeric(4);
        String address2 = "AD2_" + RandomDataUtils.getRandomAlphanumeric(4);
        String town = RandomDataUtils.randomAlphabetic(7);
        String stateProvince = RandomDataUtils.randomAlphabetic(7);
        String zipPostalCode = RandomDataUtils.randomNumeric(6);
        String country = RandomDataUtils.randomAlphabetic(4);
        String useDifferentBillingAddress = RandomDataUtils.getRandomAlphanumeric(4);
        String ownerManagerFirstName = RandomDataUtils.randomAlphabetic(4);
        String ownerManagerLastName = RandomDataUtils.randomAlphabetic(4);
        String ownerManagerGender = RandomDataUtils.randomAlphabetic(4);
        String ownerManagerEmail = RandomDataUtils.randomAlphabetic(5) + "@gmail.com";
        String ownerManagerOfficeNumber = RandomDataUtils.randomNumeric(10);
        String ownerManagerMobileNumber = RandomDataUtils.randomNumeric(10);
        String ownerManagerFaxNumber = RandomDataUtils.randomNumeric(10);
        String whatIsTheOwnerManagersJobTitle = RandomDataUtils.randomAlphabetic(8);
        String whatWereYourBusinessRevenuesIn2020PleaseEnterInUsDollars = RandomDataUtils.getRandomAlphanumeric(4);
        String whatWereYourBusinessRevenuesIn2019PleaseEnterInUsDollars = RandomDataUtils.getRandomAlphanumeric(4);
        suppliersPage.createSuppliers(registeredBusinessName, registeredTradeName, businessWebsite, whichOfTheseLanguagesAreYouDoingBusinessIn, whatIsYourGovernmentIssuesBusinessRegistrationNumber, ifYouHaveDunBradstreetNumberDunsPleaseIncludeItHere, inWhatYearWasTheBusinessEstablished, ifYouAreNotTheOwnerWhoEstablishedTheBusinessInWhatYearDidYouAcquireIt, pleaseProvideAShortDescriptionOfYourBusiness, pleaseProvideADescriptionOfYourBusinessProductsAndServices, primaryProductServiceCategory, secondaryProductServiceCategory, numberOfFullTimeEmployees, whereDoYouCurrentlyFocusOnSellingYourProductsServices, whatAreYourBusinessCurrentAndFutureTargetMarkets, otherRelevantBusinessCertificationSForExampleIsoDiversityEnvironmentalEtc, inWhichCountriesDoesYourBusinessCurrentlyProvideProductsOrServicesDistributionArea, whatIsYourBusinessCurrentLegalStructure, whereIsYourPrincipalPlaceOfBusiness, address1, address2, town, stateProvince, zipPostalCode, country, useDifferentBillingAddress, ownerManagerFirstName, ownerManagerLastName, ownerManagerGender, ownerManagerEmail, ownerManagerOfficeNumber, ownerManagerMobileNumber, ownerManagerFaxNumber, whatIsTheOwnerManagersJobTitle, whatWereYourBusinessRevenuesIn2020PleaseEnterInUsDollars, whatWereYourBusinessRevenuesIn2019PleaseEnterInUsDollars);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        commonSectionPage.createAdvancedFilter(filterName, condition, startDate, endDate);
        driver.sleep(WaitTime.LONG_WAIT);
        boolean supplierDisplayStatus = suppliersPage.isSupplierDisplayed(registeredBusinessName);
        softAssert.assertTrue(supplierDisplayStatus, "Failed to filter supplier");
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.SMALL_WAIT);
        login.loginWithBuyerUser(testContext.app);
        homePage.clickOnHome();
        homePage.clickOnSupplyChainIssueManagement();
        ProblemCasePage problemCasePage = new ProblemCasePage(driver);
        String problemType = "1";
        String primaryNonConformance = "1";
        String secondaryNonConformance = "1";
        String part = "1";
        String buyerLocation = "1";
        String caseStatus = "open";
        String priority = "P1";
        String supplierLocation = "1";
        String supplier = "1";
        String shortDescription = RandomDataUtils.randomNumeric(5);
        driver.sleep(WaitTime.SMALL_WAIT);
        problemCasePage.createProblemCase(problemType, primaryNonConformance, secondaryNonConformance, part, buyerLocation, caseStatus, priority, supplierLocation, shortDescription, supplier);
        driver.sleep(WaitTime.LONG_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.LONG_WAIT);
        String problemCaseId = problemCasePage.getProblemCaseId();
        homePage.clickOnHome();
        homePage.clickOnSupplyChainIssueManagement();
        driver.sleep(WaitTime.LONG_WAIT);
        commonSectionPage.createAdvancedFilter("Final Response Due", condition, startDate, endDate);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        String problemCaseStatus = "Open Problem Case";
        boolean isProblemCaseFiltered = problemCasePage.isProblemCaseDisplayed(problemCaseId, problemCaseStatus);
        softAssert.assertTrue(isProblemCaseFiltered, "Failed to filter problem case");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-2730"})
    @Test
    public void verifyAdvancedFilterFunctionality2() {
        CommonSectionPage commonSectionPage = new CommonSectionPage(driver);
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        BusinessOrganizationsPage businessOrganizationsPage = new BusinessOrganizationsPage(driver);
        businessOrganizationsPage.clickOnBusinessOrganizationsTab();
        String organizationName = "BO_" + RandomDataUtils.randomNumeric(5);
        String duns = RandomDataUtils.randomNumeric(5);
        String costRecoveryThreshold = RandomDataUtils.randomNumeric(5);
        String defaultCurrency = "XUA";
        String localTimeZone = "Alaska Standard Time (AST)";
        String status = "Active";
        businessOrganizationsPage.createBusinessOrganization(organizationName, duns, costRecoveryThreshold, defaultCurrency, localTimeZone, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        String filterName = "DUNS";
        String condition = "is";
        commonSectionPage.createAdvancedFilter(filterName, condition, duns);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        boolean businessOrganizationDisplayStatus = businessOrganizationsPage.isBusinessOrganizationDisplayed(organizationName, duns);
        softAssert.assertTrue(businessOrganizationDisplayStatus, "Failed to filter business organization");

        homePage.clickOnEnterprise();
        PlantsPage plantsPage = new PlantsPage(driver);
        plantsPage.clickOnPlantsTab();
        String locationName = "AP_" + RandomDataUtils.randomAlphabetic(4);
        duns = RandomDataUtils.randomNumeric(4);
        costRecoveryThreshold = RandomDataUtils.randomNumeric(4);
        defaultCurrency = "XUA";
        localTimeZone = "Alaska Standard Time (AST)";
        status = "Active";
        plantsPage.createPlant(locationName, duns, costRecoveryThreshold, defaultCurrency, localTimeZone, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        commonSectionPage.createAdvancedFilter(filterName, condition, "PLANT:"+duns);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        boolean plantDisplayStatus = plantsPage.isPlantDisplayed(locationName);
        softAssert.assertTrue(plantDisplayStatus, "Failed to filter plant");

        homePage.clickOnEnterprise();
        PartsPage partsPage = new PartsPage(driver);
        partsPage.clickOnPartsTab();
        String partsName = "PART_" + RandomDataUtils.getRandomAlphanumeric(4);
        String versionName = "V" + RandomDataUtils.randomNumeric(2);
        String supplier = "1";
        String price = "$ 2000";
        String currency = "XUA";
        partsPage.createPart(partsName, versionName, supplier, price, currency);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        filterName = "Name";
        condition = "is";
        commonSectionPage.createAdvancedFilter(filterName, condition, partsName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        boolean partsDisplayStatus = partsPage.isPartDisplayed(partsName);
        softAssert.assertTrue(partsDisplayStatus, "Failed to filter part");
        softAssert.assertAll();
    }
}