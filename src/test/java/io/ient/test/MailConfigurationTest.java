package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.MailConfigurationPage;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class MailConfigurationTest extends FrameworkTestBase {

    @TestCaseID(ids = {"T1600"})
    @Test
    public void verifyEditMailConfigurationTemplate() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        MailConfigurationPage mailConfigurationPage = new MailConfigurationPage(driver);
        mailConfigurationPage.clickOnMailConfigurationTab();
        String host = "smtppro.zoho.com";
        String port = "465";
        String username = "no-reply@rrssi.com";
        String password = "Tokyo.2021";
        String name = "RRSSI";
        mailConfigurationPage.editMailConfiguration(host, port, username, password, name);
        driver.sleep(WaitTime.XSMALL_WAIT);
        softAssert.assertAll();

    }

    @TestCaseID(ids = {"IENT-3085"})
    @Test
    public void verifyMailConfigurationPageFeaturesWithBuyerAdmin() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        MailConfigurationPage mailConfigurationPage = new MailConfigurationPage(driver);
        mailConfigurationPage.clickOnMailConfigurationTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        mailConfigurationPage.clickEditButton();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean isHostTextBoxDisplayed = mailConfigurationPage.isHostTextBoxDisplayed();
        softAssert.assertTrue(isHostTextBoxDisplayed, "Failed to display Host TextBox");
        boolean isPortTextBoxDisplayed = mailConfigurationPage.isPortTextBoxDisplayed();
        softAssert.assertTrue(isPortTextBoxDisplayed, "Failed to display Port TextBox");
        boolean isUsernameTextBoxDisplayed = mailConfigurationPage.isUsernameTextBoxDisplayed();
        softAssert.assertTrue(isUsernameTextBoxDisplayed, "Failed to display Username TextBox");
        boolean isPasswordTextBoxDisplayed = mailConfigurationPage.isPasswordTextBoxDisplayed();
        softAssert.assertTrue(isPasswordTextBoxDisplayed, "Failed to display Password TextBox");
        boolean isNameTextBoxDisplayed = mailConfigurationPage.isNameTextBoxDisplayed();
        softAssert.assertTrue(isNameTextBoxDisplayed, "Failed to display Name TextBox");
        boolean isEmailTextBoxDisplayed = mailConfigurationPage.isEmailTextBoxDisplayed();
        softAssert.assertTrue(isEmailTextBoxDisplayed, "Failed to display Email TextBox");
        boolean isStarttlsCheckBoxDisplayed = mailConfigurationPage.isStarttlsCheckBoxDisplayed();
        softAssert.assertTrue(isStarttlsCheckBoxDisplayed, "Failed to display Starttls CheckBox");
        boolean isAuthCheckBoxDisplayed = mailConfigurationPage.isAuthCheckBoxDisplayed();
        softAssert.assertTrue(isAuthCheckBoxDisplayed, "Failed to display Auth CheckBox");
        boolean isSslCheckBoxDisplayed = mailConfigurationPage.isSslCheckBoxDisplayed();
        softAssert.assertTrue(isSslCheckBoxDisplayed, "Failed to display SSL CheckBox");
        boolean isDebugCheckBoxDisplayed = mailConfigurationPage.isDebugCheckBoxDisplayed();
        softAssert.assertTrue(isDebugCheckBoxDisplayed, "Failed to display Debug CheckBox");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-3086"})
    @Test
    public void verifyMailConfigurationPageFeaturesWithSuperAdmin() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSuperAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        MailConfigurationPage mailConfigurationPage = new MailConfigurationPage(driver);
        mailConfigurationPage.clickOnMailConfigurationTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        mailConfigurationPage.clickEditButton();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean isHostTextBoxDisplayed = mailConfigurationPage.isHostTextBoxDisplayed();
        softAssert.assertTrue(isHostTextBoxDisplayed, "Failed to display Host TextBox");
        boolean isPortTextBoxDisplayed = mailConfigurationPage.isPortTextBoxDisplayed();
        softAssert.assertTrue(isPortTextBoxDisplayed, "Failed to display Port TextBox");
        boolean isUsernameTextBoxDisplayed = mailConfigurationPage.isUsernameTextBoxDisplayed();
        softAssert.assertTrue(isUsernameTextBoxDisplayed, "Failed to display Username TextBox");
        boolean isPasswordTextBoxDisplayed = mailConfigurationPage.isPasswordTextBoxDisplayed();
        softAssert.assertTrue(isPasswordTextBoxDisplayed, "Failed to display Password TextBox");
        boolean isNameTextBoxDisplayed = mailConfigurationPage.isNameTextBoxDisplayed();
        softAssert.assertTrue(isNameTextBoxDisplayed, "Failed to display Name TextBox");
        boolean isEmailTextBoxDisplayed = mailConfigurationPage.isEmailTextBoxDisplayed();
        softAssert.assertTrue(isEmailTextBoxDisplayed, "Failed to display Email TextBox");
        boolean isStarttlsCheckBoxDisplayed = mailConfigurationPage.isStarttlsCheckBoxDisplayed();
        softAssert.assertTrue(isStarttlsCheckBoxDisplayed, "Failed to display Starttls CheckBox");
        boolean isAuthCheckBoxDisplayed = mailConfigurationPage.isAuthCheckBoxDisplayed();
        softAssert.assertTrue(isAuthCheckBoxDisplayed, "Failed to display Auth CheckBox");
        boolean isSslCheckBoxDisplayed = mailConfigurationPage.isSslCheckBoxDisplayed();
        softAssert.assertTrue(isSslCheckBoxDisplayed, "Failed to display SSL CheckBox");
        boolean isDebugCheckBoxDisplayed = mailConfigurationPage.isDebugCheckBoxDisplayed();
        softAssert.assertTrue(isDebugCheckBoxDisplayed, "Failed to display Debug CheckBox");
        softAssert.assertAll();
    }
}
