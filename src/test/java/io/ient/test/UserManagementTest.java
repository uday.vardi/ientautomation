package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.LoginPage;
import io.ient.test.pages.enterprise.KeyCloakPage;
import io.ient.test.pages.enterprise.RoleManagementPage;
import io.ient.test.pages.enterprise.TenantsPage;
import io.ient.test.pages.enterprise.UserManagementPage;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class UserManagementTest extends FrameworkTestBase {

    @TestCaseID(ids = {"T1200"})
    @Test
    public void verifyNewUserCreation() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        UserManagementPage UserManagementPage = new UserManagementPage(driver);
        UserManagementPage.clickOnUserManagementTab();
        String userName = "user." + RandomDataUtils.randomAlphabetic(5).toLowerCase();
        String firstName = RandomDataUtils.randomAlphabetic(5).toLowerCase();
        String lastName = RandomDataUtils.randomAlphabetic(5).toLowerCase();
        String emailAddress = RandomDataUtils.getRandomAlphanumeric(5).toLowerCase() + "@gmail.com";
        String role = "Buyer User";
        String salutation = "Mrs";
        String status = "Active";
        UserManagementPage.createUser(userName, firstName, lastName, emailAddress, role, salutation, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        UserManagementPage.searchUserByUserName(userName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean displayStatus = UserManagementPage.isUserDisplayed(userName.toLowerCase(), firstName);
        softAssert.assertTrue(displayStatus, "Failed to create new user");
        String updateFirstName = firstName + "_EDIT";
        UserManagementPage.editUser(updateFirstName);
        driver.sleep(WaitTime.SMALL_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.SMALL_WAIT);
        UserManagementPage.clearSearchTextField();
        UserManagementPage.searchUserByUserName(userName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean updatedUserDisplayStatus = UserManagementPage.isUserDisplayed(userName.toLowerCase(), updateFirstName);
        softAssert.assertTrue(updatedUserDisplayStatus, "Failed to update user");
        UserManagementPage.deleteUser();
        driver.sleep(WaitTime.SMALL_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.SMALL_WAIT);
        UserManagementPage.clearSearchTextField();
        UserManagementPage.searchUserByUserName(userName);
        UserManagementPage.clickOnSearchIcon();
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean isUserDeleted = UserManagementPage.verifyNoRecordsMessage();
        softAssert.assertTrue(isUserDeleted, "Failed to delete user");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-115_1"})
    @Test
    public void verifyUserManagementPageFeatures() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        UserManagementPage UserManagementPage = new UserManagementPage(driver);
        UserManagementPage.clickOnUserManagementTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean isSearchBarDisplayed = UserManagementPage.isSearchBarDisplayed();
        softAssert.assertTrue(isSearchBarDisplayed, "Failed to display searchbar");
        boolean isSelectColumnsDisplayed = UserManagementPage.isSelectColumnsDisplayed();
        softAssert.assertTrue(isSelectColumnsDisplayed, "Failed to display select columns");
        boolean isFilterIconDisplayed = UserManagementPage.isFilterIconDisplayed();
        softAssert.assertTrue(isFilterIconDisplayed, "Failed to display filter");
        boolean isNewUserButtonDisplayed = UserManagementPage.isNewUserButtonDisplayed();
        softAssert.assertTrue(isNewUserButtonDisplayed, "Failed to display new user button");
        UserManagementPage.clickOnTopLeftVerticalIcon();
        boolean isImportFromAribaDisplayed = UserManagementPage.isImportFromAribaDisplayed();
        softAssert.assertTrue(isImportFromAribaDisplayed, "Failed to display Import From Ariba");
        boolean isImportFromLocalDisplayed = UserManagementPage.isImportFromLocalDisplayed();
        softAssert.assertTrue(isImportFromLocalDisplayed, "Failed to display Import From Local");
        boolean verifyUsersList = UserManagementPage.verifyUsersList();
        softAssert.assertTrue(verifyUsersList, "Failed to display users list");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-115_2"})
    @Test
    public void verifyUserManagementPageColumns() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        UserManagementPage UserManagementPage = new UserManagementPage(driver);
        UserManagementPage.clickOnUserManagementTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean isPageColumnDisplayed = UserManagementPage.isPageColumnDisplayed("ID");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display ID column");
        isPageColumnDisplayed = UserManagementPage.isPageColumnDisplayed("User Name");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display User Name column");
        isPageColumnDisplayed = UserManagementPage.isPageColumnDisplayed("First Name");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display First Name column");
        isPageColumnDisplayed = UserManagementPage.isPageColumnDisplayed("Last Name");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display Last Name column");
        isPageColumnDisplayed = UserManagementPage.isPageColumnDisplayed("Email");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display Email column");
        isPageColumnDisplayed = UserManagementPage.isPageColumnDisplayed("Roles");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display Roles column");
        isPageColumnDisplayed = UserManagementPage.isPageColumnDisplayed("Status");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display Status column");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-115_3"})
    @Test
    public void verifyUserActiveAndInactiveFunctionality() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        UserManagementPage userManagementPage = new UserManagementPage(driver);
        TenantsPage tenantsPage = new TenantsPage(driver);
        KeyCloakPage keyCloakPage = new KeyCloakPage(driver);
        String appUrl = testContext.app.getAppUrl();
        LoginPage loginPage = new LoginPage(driver);
        homePage.clickOnEnterprise();
        userManagementPage.clickOnUserManagementTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        String userName = "user." + RandomDataUtils.randomAlphabetic(5).toLowerCase();
        String firstName = RandomDataUtils.randomAlphabetic(5).toLowerCase();
        String lastName = RandomDataUtils.randomAlphabetic(5).toLowerCase();
        String emailAddress = RandomDataUtils.getRandomAlphanumeric(5).toLowerCase() + "@gmail.com";
        String role = "Buyer User";
        String salutation = "Mrs";
        String status = "Active";
        String password = "Test@1234";
        userManagementPage.createUser(userName, firstName, lastName, emailAddress, role, salutation, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        login.loginToKeycloak(testContext.app);
        String tenantName = testContext.app.getAgency().toLowerCase();
        keyCloakPage.clickOnTenantName(tenantName);
        keyCloakPage.clickOnUsers();
        keyCloakPage.clickOnViewAllUsers();
        keyCloakPage.searchUserByName(userName);
        keyCloakPage.updateUserPassword(userName, password);
        driver.sleep(WaitTime.SMALL_WAIT);
        keyCloakPage.clickOnAdminDropDownLink();
        keyCloakPage.clickOnSignOutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.SMALL_WAIT);
        login.loginWithUserCredentialsWithoutValidation(tenantName, userName, password, testContext.app);
        homePage.clickOnAcceptTermsAndConditions();
        tenantsPage.clickOnSubmitButton();
        boolean loggedInStatus = homePage.isHomePage();
        softAssert.assertTrue(loggedInStatus, "Failed to login with new user");
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        loginPage.goToApplication(appUrl);
        login.loginWithBuyerAdmin(testContext.app);
        homePage.clickOnEnterprise();
        userManagementPage.clickOnUserManagementTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean isNumberOfItemsPerPageEditable =  userManagementPage.isNumberOfItemsPerPageEditable();
        softAssert.assertTrue(isNumberOfItemsPerPageEditable, "Failed to edit number of items per page");
        userManagementPage.clearSearchTextField();
        userManagementPage.searchUserByUserName(userName);
        driver.sleep(WaitTime.SMALL_WAIT);
        userManagementPage.editUserStatus("Inactive");
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        String organizationName = testContext.app.getAgency().toLowerCase();
        loginPage.goToApplication(appUrl);
        loginPage.changeOrganizationLogin(organizationName);
        loginPage.login(userName, password);
        boolean isLoggedIn = homePage.isHomePage();
        softAssert.assertFalse(isLoggedIn, "Able to login with Inactive buyer user");
        driver.navigateBack();
        driver.sleep(WaitTime.SMALL_WAIT);
        driver.navigateBack();
        driver.sleep(WaitTime.SMALL_WAIT);
        login.loginWithBuyerAdmin(testContext.app);
        homePage.clickOnEnterprise();
        userManagementPage.clickOnUserManagementTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        userManagementPage.searchUserByUserName(userName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        userManagementPage.editUserStatus("Active");
        driver.sleep(WaitTime.MEDIUM_WAIT);
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-130"})
    @Test
    public void verifyNewUserCreationMessages() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        UserManagementPage UserManagementPage = new UserManagementPage(driver);
        UserManagementPage.clickOnUserManagementTab();
        String userName = "user." + RandomDataUtils.getRandomAlphanumeric(5);
        String expectedMessage = "Are you sure that you want to go back, any pending changes will be lost?";
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean isConfirmationMessageDisplayed = UserManagementPage.isConfirmationMessageDisplayed(userName, expectedMessage);
        softAssert.assertTrue(isConfirmationMessageDisplayed, "Failed to display confirmation message");
        UserManagementPage.clickOnAddButton();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean isSubmitButtonDisabled = UserManagementPage.isSubmitButtonDisabled();
        softAssert.assertTrue(isSubmitButtonDisabled, "Failed to disable submit button");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-941"})
    @Test
    public void verifyUserDeleteFunctionality() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        TenantsPage tenantsPage = new TenantsPage(driver);
        KeyCloakPage keyCloakPage = new KeyCloakPage(driver);
        String tenantName = testContext.app.getAgency().toLowerCase();
        UserManagementPage UserManagementPage = new UserManagementPage(driver);
        homePage.clickOnEnterprise();
        UserManagementPage.clickOnUserManagementTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        String userName = "user." + RandomDataUtils.randomAlphabetic(5).toLowerCase();
        String firstName = RandomDataUtils.randomAlphabetic(5).toLowerCase();
        String lastName = RandomDataUtils.randomAlphabetic(5).toLowerCase();
        String emailAddress = RandomDataUtils.getRandomAlphanumeric(5).toLowerCase() + "@mail.com";
        String role = "Buyer User";
        String salutation = "Mrs";
        String status = "Active";
        String password = "Test@1234";
        UserManagementPage.createUser(userName, firstName, lastName, emailAddress, role, salutation, status);
        driver.sleep(WaitTime.SMALL_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        login.loginToKeycloak(testContext.app);
        driver.sleep(WaitTime.SMALL_WAIT);
        keyCloakPage.clickOnTenantName(tenantName);
        keyCloakPage.clickOnUsers();
        keyCloakPage.clickOnViewAllUsers();
        keyCloakPage.searchUserByName(userName);
        keyCloakPage.updateUserPassword(userName, password);
        driver.sleep(WaitTime.SMALL_WAIT);
        keyCloakPage.clickOnAdminDropDownLink();
        keyCloakPage.clickOnSignOutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.SMALL_WAIT);
        login.loginWithUserCredentialsWithoutValidation(tenantName, userName, password, testContext.app);
        homePage.clickOnAcceptTermsAndConditions();
        tenantsPage.clickOnSubmitButton();
        boolean loggedInStatus = homePage.isHomePage();
        softAssert.assertTrue(loggedInStatus, "Failed to login with new tenant user");
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        String appUrl = testContext.app.getAppUrl();
        LoginPage loginPage = new LoginPage(driver);
        loginPage.goToApplication(appUrl);
        login.loginWithBuyerAdmin(testContext.app);
        homePage.clickOnEnterprise();
        UserManagementPage.clickOnUserManagementTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        UserManagementPage.clearSearchTextField();
        UserManagementPage.searchUserByUserName(userName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        UserManagementPage.deleteUser();
        driver.sleep(WaitTime.SMALL_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.SMALL_WAIT);
        UserManagementPage.clearSearchTextField();
        UserManagementPage.searchUserByUserName(userName);
        UserManagementPage.clickOnSearchIcon();
        boolean isUserDeleted = UserManagementPage.verifyNoRecordsMessage();
        softAssert.assertTrue(isUserDeleted, "Failed to delete user");
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        String organizationName = testContext.app.getAgency().toLowerCase();
        loginPage.goToApplication(appUrl);
        loginPage.changeOrganizationLogin(organizationName);
        loginPage.login(userName, password);
        boolean isLoggedIn = homePage.isHomePage();
        softAssert.assertFalse(isLoggedIn, "Able to login with deleted buyer user");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-796"})
    @Test
    public void verifyUserLoginWithInactiveRole() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        RoleManagementPage roleManagementPage = new RoleManagementPage(driver);
        KeyCloakPage keyCloakPage = new KeyCloakPage(driver);
        TenantsPage tenantsPage = new TenantsPage(driver);
        String tenantName = testContext.app.getAgency().toLowerCase();
        String role = "Buyer User";
        homePage.clickOnEnterprise();
        UserManagementPage UserManagementPage = new UserManagementPage(driver);
        UserManagementPage.clickOnUserManagementTab();
        String userName = "user." + RandomDataUtils.randomAlphabetic(5);
        String firstName = RandomDataUtils.randomAlphabetic(5);
        String lastName = RandomDataUtils.randomAlphabetic(5);
        String emailAddress = RandomDataUtils.getRandomAlphanumeric(5) + "@gmail.com";
        String password = "Test@1234";
        String salutation = "Mrs";
        String userStatus = "Active";
        String inactiveRoleStatus = "Inactive";
        String activeRoleStatus = "Active";
        UserManagementPage.createUser(userName, firstName, lastName, emailAddress, role, salutation, userStatus);
        driver.sleep(WaitTime.SMALL_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.SMALL_WAIT);
        UserManagementPage.searchUserByUserName(userName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean userDisplayStatus = UserManagementPage.isUserDisplayed(userName, firstName);
        softAssert.assertTrue(userDisplayStatus, "Failed to create new user");
//        homePage.clickOnEnterprise();
//        roleManagementPage.clickOnRoleManagementTab();
//        roleManagementPage.searchRoleByName(role);
//        driver.sleep(WaitTime.SMALL_WAIT);
//        roleManagementPage.editRoleStatus(role, inactiveRoleStatus);
//        driver.sleep(WaitTime.SMALL_WAIT);
//        homePage.clickOnAdminDropDownLink();
//        homePage.clickOnLogoutLink();
//        driver.sleep(WaitTime.SMALL_WAIT);
//        login.loginToKeycloak(testContext.app);
//        keyCloakPage.clickOnTenantName(tenantName);
//        keyCloakPage.clickOnUsers();
//        keyCloakPage.clickOnViewAllUsers();
//        keyCloakPage.searchUserByName(userName);
//        keyCloakPage.updateUserPassword(userName, password);
//        driver.sleep(WaitTime.SMALL_WAIT);
//        keyCloakPage.clickOnAdminDropDownLink();
//        keyCloakPage.clickOnSignOutLink();
//        driver.sleep(WaitTime.SMALL_WAIT);
//        driver.refresh();
//        driver.sleep(WaitTime.SMALL_WAIT);
//        login.loginWithUserCredentialsWithoutValidation(tenantName, userName, password, testContext.app);
//        homePage.clickOnAcceptTermsAndConditions();
//        tenantsPage.clickOnSubmitButton();
//        boolean loggedInStatus = homePage.isHomePage();
//        softAssert.assertFalse(loggedInStatus, "Able to login with new inactive role user");
//        driver.sleep(WaitTime.SMALL_WAIT);
//        login.loginWithBuyerAdmin(testContext.app);
//        homePage.clickOnEnterprise();
//        roleManagementPage.clickOnRoleManagementTab();
//        roleManagementPage.searchRoleByName(role);
//        driver.sleep(WaitTime.XSMALL_WAIT);
//        roleManagementPage.editRoleStatus(role, activeRoleStatus);
//        driver.sleep(WaitTime.MEDIUM_WAIT);
//        homePage.clickOnEnterprise();
//        UserManagementPage.clickOnUserManagementTab();
//        UserManagementPage.clearSearchTextField();
//        UserManagementPage.searchUserByUserName(userName);
//        driver.sleep(WaitTime.XSMALL_WAIT);
//        boolean userSearchDisplayStatus = UserManagementPage.isUserDisplayed(userName, firstName);
//        softAssert.assertTrue(userSearchDisplayStatus, "Failed to search user");
        UserManagementPage.deleteUser();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-721"})
    @Test
    public void verifyUpdateUserProfile() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        UserManagementPage UserManagementPage = new UserManagementPage(driver);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnProfile();
        String firstName = "BA";
        UserManagementPage.editUserProfile(firstName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        homePage.clickOnEnterprise();
        UserManagementPage.clickOnUserManagementTab();
        UserManagementPage.searchUserByUserName(firstName);
        driver.sleep(WaitTime.SMALL_WAIT);
        String updateFirstName = "Admin";
        UserManagementPage.editUser(updateFirstName);
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-135"})
    @Test
    public void verifyNewSupplierUserCreation() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        UserManagementPage UserManagementPage = new UserManagementPage(driver);
        UserManagementPage.clickOnUserManagementTab();
        String userName = "su." + RandomDataUtils.randomAlphabetic(4).toLowerCase();
        String firstName = RandomDataUtils.randomAlphabetic(5).toLowerCase();
        String lastName = RandomDataUtils.randomAlphabetic(5).toLowerCase();
        String emailAddress = RandomDataUtils.getRandomAlphanumeric(5).toLowerCase() + "@gmail.com";
        String role = "Supplier User";
        String salutation = "Mr";
        String supplier = "1";
        String status = "Active";
        UserManagementPage.createSupplierUser(userName, firstName, lastName, emailAddress, role, salutation, supplier, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        UserManagementPage.searchUserByUserName(userName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean displayStatus = UserManagementPage.isUserDisplayed(userName, firstName);
        softAssert.assertTrue(displayStatus, "Failed to create new supplier user");
        String updateFirstName = "_EDIT";
        UserManagementPage.editUser(updateFirstName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        UserManagementPage.clearSearchTextField();
        UserManagementPage.searchUserByUserName(userName);
        boolean updatedUserDisplayStatus = UserManagementPage.isUserDisplayed(userName, updateFirstName);
        softAssert.assertTrue(updatedUserDisplayStatus, "Failed to update supplier user");
        UserManagementPage.deleteUser();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        UserManagementPage.clearSearchTextField();
        UserManagementPage.searchUserByUserName(userName);
        UserManagementPage.clickOnSearchIcon();
        boolean isUserDeleted = UserManagementPage.verifyNoRecordsMessage();
        softAssert.assertTrue(isUserDeleted, "Failed to delete supplier user");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-136"})
    @Test
    public void verifyNewSupplierAdminCreation() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        UserManagementPage UserManagementPage = new UserManagementPage(driver);
        UserManagementPage.clickOnUserManagementTab();
        String userName = "sa." + RandomDataUtils.randomAlphabetic(5).toLowerCase();
        String firstName = RandomDataUtils.randomAlphabetic(5).toLowerCase();
        String lastName = RandomDataUtils.randomAlphabetic(5).toLowerCase();
        String emailAddress = RandomDataUtils.getRandomAlphanumeric(5).toLowerCase() + "@gmail.com";
        String role = "Supplier Admin";
        String salutation = "Mr";
        String supplier = "1";
        String status = "Active";
        UserManagementPage.createSupplierUser(userName, firstName, lastName, emailAddress, role, salutation, supplier, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        UserManagementPage.searchUserByUserName(userName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean displayStatus = UserManagementPage.isUserDisplayed(userName, firstName);
        softAssert.assertTrue(displayStatus, "Failed to create new supplier admin");
        String updateFirstName = firstName + "_EDIT";
        UserManagementPage.editUser(updateFirstName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        UserManagementPage.clearSearchTextField();
        UserManagementPage.searchUserByUserName(userName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean updatedUserDisplayStatus = UserManagementPage.isUserDisplayed(userName, updateFirstName);
        softAssert.assertTrue(updatedUserDisplayStatus, "Failed to update supplier admin");
        UserManagementPage.deleteUser();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        UserManagementPage.clearSearchTextField();
        UserManagementPage.searchUserByUserName(userName);
        UserManagementPage.clickOnSearchIcon();
        boolean isUserDeleted = UserManagementPage.verifyNoRecordsMessage();
        softAssert.assertTrue(isUserDeleted, "Failed to delete supplier admin");
        softAssert.assertAll();
    }
}