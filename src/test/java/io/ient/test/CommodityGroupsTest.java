package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.CommodityGroupsPage;
import io.ient.test.pages.enterprise.GlobalSettingsPage;
import io.ient.test.pages.enterprise.ProductionPhasesPage;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class CommodityGroupsTest extends FrameworkTestBase {

    @TestCaseID(ids = {"T1540"})
    @Test
    public void verifyNewCommodityGroupCreation() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        CommodityGroupsPage commodityGroupsPage = new CommodityGroupsPage(driver);
        homePage.clickOnEnterprise();
        commodityGroupsPage.clickOnCommodityGroupTab();
        String commodityGroupName = "CG_" + RandomDataUtils.getRandomAlphanumeric(4);
        String status = "Active";
        commodityGroupsPage.createCommodityGroup(commodityGroupName, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        commodityGroupsPage.searchCommodityGroupByName(commodityGroupName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean displayStatus = commodityGroupsPage.isCommodityGroupDisplayed(commodityGroupName);
        softAssert.assertTrue(displayStatus, "Failed to create new commodity group");
        String updateName = commodityGroupName + "_EDIT";
        commodityGroupsPage.editCommodityGroup(updateName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        commodityGroupsPage.clearCommodityGroupTextField();
        commodityGroupsPage.searchCommodityGroupByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean updatedCommodityGroupDisplayStatus = commodityGroupsPage.isCommodityGroupDisplayed(updateName);
        softAssert.assertTrue(updatedCommodityGroupDisplayStatus, "Failed to update commodity group");
        commodityGroupsPage.deleteCommodityGroup();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        commodityGroupsPage.clearCommodityGroupTextField();
        commodityGroupsPage.searchCommodityGroupByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        commodityGroupsPage.clickOnSearchIcon();
        boolean isCommodityGroupDisplayed = commodityGroupsPage.verifyNoRecordsMessage();
        softAssert.assertTrue(isCommodityGroupDisplayed, "Failed to delete commodity group");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-670"})
    @Test
    public void verifyCommodityGroupsPageFeatures() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        GlobalSettingsPage globalSettingsPage = new GlobalSettingsPage(driver);
        CommodityGroupsPage commodityGroupsPage = new CommodityGroupsPage(driver);
        homePage.clickOnEnterprise();
        commodityGroupsPage.clickOnCommodityGroupTab();
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean isSearchBarDisplayed = globalSettingsPage.isSearchBarDisplayed();
        softAssert.assertTrue(isSearchBarDisplayed, "Failed to display searchbar");
        boolean isSelectColumnsDisplayed = globalSettingsPage.isSelectColumnsDisplayed();
        softAssert.assertTrue(isSelectColumnsDisplayed, "Failed to display select columns");
        boolean isFilterIconDisplayed = globalSettingsPage.isFilterIconDisplayed();
        softAssert.assertTrue(isFilterIconDisplayed, "Failed to display filter");
        globalSettingsPage.clickOnTopLeftVerticalIcon();
        boolean isAddNewButtonDisplayed = globalSettingsPage.isAddNewButtonDisplayed();
        softAssert.assertTrue(isAddNewButtonDisplayed, "Failed to display AddNewButton");
        boolean isDownloadAsPdfDisplayed = globalSettingsPage.isDownloadAsPdfDisplayed();
        softAssert.assertTrue(isDownloadAsPdfDisplayed, "Failed to display DownloadAsPdf");
        boolean isDownloadAsCsvDisplayed = globalSettingsPage.isDownloadAsCsvDisplayed();
        softAssert.assertTrue(isDownloadAsCsvDisplayed, "Failed to display DownloadAsCSV");
        boolean isNumberOfItemsPerPageEditable = globalSettingsPage.isNumberOfItemsPerPageEditable();
        softAssert.assertTrue(isNumberOfItemsPerPageEditable, "Failed to edit number of items per page");
        boolean isPaginationDisplayed = globalSettingsPage.isPaginationDisplayed();
        softAssert.assertTrue(isPaginationDisplayed, "Failed to display pagination");
        boolean isTotalCountDisplayed = globalSettingsPage.isTotalCountDisplayed();
        softAssert.assertTrue(isTotalCountDisplayed, "Failed to display total count");
        boolean isPageColumnDisplayed = globalSettingsPage.isPageColumnDisplayed("ID");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display ID column");
        isPageColumnDisplayed = globalSettingsPage.isPageColumnDisplayed("Name");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display Name column");
        isPageColumnDisplayed = globalSettingsPage.isPageColumnDisplayed("Status");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display Status column");
        boolean isActionEditAndDeleteDisplayed = globalSettingsPage.isActionEditAndDeleteDisplayed();
        softAssert.assertTrue(isActionEditAndDeleteDisplayed, "Failed to display action edit delete options");
        softAssert.assertAll();
    }

}