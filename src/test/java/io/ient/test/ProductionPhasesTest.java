package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.CustomersPage;
import io.ient.test.pages.enterprise.GlobalSettingsPage;
import io.ient.test.pages.enterprise.ProductionPhasesPage;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class ProductionPhasesTest extends FrameworkTestBase {

    @TestCaseID(ids = {"T1560"})
    @Test
    public void verifyNewProductionPhasesCreation() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        ProductionPhasesPage productionPhasesPage = new ProductionPhasesPage(driver);
        productionPhasesPage.clickOnProductionPhasesTab();
        String productionPhasesName = "PP_" + RandomDataUtils.getRandomAlphanumeric(4);
        String status = "Active";
        productionPhasesPage.createProductionPhases(productionPhasesName, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        productionPhasesPage.searchProductionPhasesByName(productionPhasesName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean displayStatus = productionPhasesPage.isProductionPhasesDisplayed(productionPhasesName);
        softAssert.assertTrue(displayStatus, "Failed to create new production phases");
        String updateName = productionPhasesName + "_EDIT";
        productionPhasesPage.editProductionPhases(updateName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        productionPhasesPage.clearProductionPhasesTextField();
        productionPhasesPage.searchProductionPhasesByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean updatedProductionPhasesDisplayStatus = productionPhasesPage.isProductionPhasesDisplayed(updateName);
        softAssert.assertTrue(updatedProductionPhasesDisplayStatus, "Failed to update production phases");
        productionPhasesPage.deleteProductionPhases();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        productionPhasesPage.clearProductionPhasesTextField();
        productionPhasesPage.searchProductionPhasesByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        productionPhasesPage.clickOnSearchIcon();
        boolean isProductionPhasesDisplayed = productionPhasesPage.verifyNoRecordsMessage();
        softAssert.assertTrue(isProductionPhasesDisplayed, "Failed to delete production phases");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-337"})
    @Test
    public void verifyProductionPhasePageFeatures() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        GlobalSettingsPage globalSettingsPage = new GlobalSettingsPage(driver);
        ProductionPhasesPage productionPhasesPage = new ProductionPhasesPage(driver);
        homePage.clickOnEnterprise();
        productionPhasesPage.clickOnProductionPhasesTab();
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean isSearchBarDisplayed = globalSettingsPage.isSearchBarDisplayed();
        softAssert.assertTrue(isSearchBarDisplayed, "Failed to display searchbar");
        boolean isSelectColumnsDisplayed = globalSettingsPage.isSelectColumnsDisplayed();
        softAssert.assertTrue(isSelectColumnsDisplayed, "Failed to display select columns");
        boolean isFilterIconDisplayed = globalSettingsPage.isFilterIconDisplayed();
        softAssert.assertTrue(isFilterIconDisplayed, "Failed to display filter");
        globalSettingsPage.clickOnTopLeftVerticalIcon();
        boolean isAddNewButtonDisplayed = globalSettingsPage.isAddNewButtonDisplayed();
        softAssert.assertTrue(isAddNewButtonDisplayed, "Failed to display AddNewButton");
        boolean isDownloadAsPdfDisplayed = globalSettingsPage.isDownloadAsPdfDisplayed();
        softAssert.assertTrue(isDownloadAsPdfDisplayed, "Failed to display DownloadAsPdf");
        boolean isDownloadAsCsvDisplayed = globalSettingsPage.isDownloadAsCsvDisplayed();
        softAssert.assertTrue(isDownloadAsCsvDisplayed, "Failed to display DownloadAsCSV");
        boolean isNumberOfItemsPerPageEditable = globalSettingsPage.isNumberOfItemsPerPageEditable();
        softAssert.assertTrue(isNumberOfItemsPerPageEditable, "Failed to edit number of items per page");
        boolean isPaginationDisplayed = globalSettingsPage.isPaginationDisplayed();
        softAssert.assertTrue(isPaginationDisplayed, "Failed to display pagination");
        boolean isTotalCountDisplayed = globalSettingsPage.isTotalCountDisplayed();
        softAssert.assertTrue(isTotalCountDisplayed, "Failed to display total count");
        boolean isPageColumnDisplayed = globalSettingsPage.isPageColumnDisplayed("ID");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display ID column");
        isPageColumnDisplayed = globalSettingsPage.isPageColumnDisplayed("Status");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display Status column");
        boolean isActionEditAndDeleteDisplayed = globalSettingsPage.isActionEditAndDeleteDisplayed();
        softAssert.assertTrue(isActionEditAndDeleteDisplayed, "Failed to display action edit delete options");
        String productionPhasesName = "PP_" + RandomDataUtils.getRandomAlphanumeric(4);
        String status = "Active";
        productionPhasesPage.createProductionPhases(productionPhasesName, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        productionPhasesPage.searchProductionPhasesByName(productionPhasesName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean displayStatus = productionPhasesPage.isProductionPhasesDisplayed(productionPhasesName);
        softAssert.assertTrue(displayStatus, "Failed to create new production phases");
        String updateName = productionPhasesName + "_EDIT";
        productionPhasesPage.editProductionPhases(updateName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        productionPhasesPage.clearProductionPhasesTextField();
        productionPhasesPage.searchProductionPhasesByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean updatedProductionPhasesDisplayStatus = productionPhasesPage.isProductionPhasesDisplayed(updateName);
        softAssert.assertTrue(updatedProductionPhasesDisplayStatus, "Failed to update production phases");
        productionPhasesPage.deleteProductionPhases();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        productionPhasesPage.clearProductionPhasesTextField();
        productionPhasesPage.searchProductionPhasesByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        productionPhasesPage.clickOnSearchIcon();
        boolean isProductionPhasesDisplayed = productionPhasesPage.verifyNoRecordsMessage();
        softAssert.assertTrue(isProductionPhasesDisplayed, "Failed to delete production phases");
        softAssert.assertAll();
    }

}