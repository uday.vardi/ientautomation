package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.DisruptionStatusPage;
import io.ient.test.pages.enterprise.GlobalSettingsPage;
import io.ient.test.pages.enterprise.ProductionPhasesPage;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class DisruptionStatusTest extends FrameworkTestBase {

    @TestCaseID(ids = {"T1570"})
    @Test
    public void verifyNewDisruptionStatusCreation() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        DisruptionStatusPage disruptionStatusPage = new DisruptionStatusPage(driver);
        disruptionStatusPage.clickOnDisruptionStatusTab();
        String DisruptionStatusName = "DS_" + RandomDataUtils.getRandomAlphanumeric(4);
        String status = "Active";
        disruptionStatusPage.createDisruptionStatus(DisruptionStatusName, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        disruptionStatusPage.searchDisruptionStatusByName(DisruptionStatusName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean displayStatus = disruptionStatusPage.isDisruptionStatusDisplayed(DisruptionStatusName);
        softAssert.assertTrue(displayStatus, "Failed to create new disruption status");
        String updateName = DisruptionStatusName + "_EDIT";
        disruptionStatusPage.editDisruptionStatus(updateName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        disruptionStatusPage.clearDisruptionStatusTextField();
        disruptionStatusPage.searchDisruptionStatusByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean updatedDisruptionStatusDisplayStatus = disruptionStatusPage.isDisruptionStatusDisplayed(updateName);
        softAssert.assertTrue(updatedDisruptionStatusDisplayStatus, "Failed to update disruption status");
        disruptionStatusPage.deleteDisruptionStatus();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        disruptionStatusPage.clearDisruptionStatusTextField();
        disruptionStatusPage.searchDisruptionStatusByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        disruptionStatusPage.clickOnSearchIcon();
        boolean isDisruptionStatusDeleted = disruptionStatusPage.verifyNoRecordsMessage();
        softAssert.assertTrue(isDisruptionStatusDeleted, "Failed to delete disruption status");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-676"})
    @Test
    public void verifyDisruptionStatusPageFeatures() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        GlobalSettingsPage globalSettingsPage = new GlobalSettingsPage(driver);
        DisruptionStatusPage disruptionStatusPage = new DisruptionStatusPage(driver);
        homePage.clickOnEnterprise();
        disruptionStatusPage.clickOnDisruptionStatusTab();
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean isSearchBarDisplayed = globalSettingsPage.isSearchBarDisplayed();
        softAssert.assertTrue(isSearchBarDisplayed, "Failed to display searchbar");
        boolean isSelectColumnsDisplayed = globalSettingsPage.isSelectColumnsDisplayed();
        softAssert.assertTrue(isSelectColumnsDisplayed, "Failed to display select columns");
        boolean isFilterIconDisplayed = globalSettingsPage.isFilterIconDisplayed();
        softAssert.assertTrue(isFilterIconDisplayed, "Failed to display filter");
        globalSettingsPage.clickOnTopLeftVerticalIcon();
        boolean isAddNewButtonDisplayed = globalSettingsPage.isAddNewButtonDisplayed();
        softAssert.assertTrue(isAddNewButtonDisplayed, "Failed to display AddNewButton");
        boolean isDownloadAsPdfDisplayed = globalSettingsPage.isDownloadAsPdfDisplayed();
        softAssert.assertTrue(isDownloadAsPdfDisplayed, "Failed to display DownloadAsPdf");
        boolean isDownloadAsCsvDisplayed = globalSettingsPage.isDownloadAsCsvDisplayed();
        softAssert.assertTrue(isDownloadAsCsvDisplayed, "Failed to display DownloadAsCSV");
        boolean isNumberOfItemsPerPageEditable = globalSettingsPage.isNumberOfItemsPerPageEditable();
        softAssert.assertTrue(isNumberOfItemsPerPageEditable, "Failed to edit number of items per page");
        boolean isPaginationDisplayed = globalSettingsPage.isPaginationDisplayed();
        softAssert.assertTrue(isPaginationDisplayed, "Failed to display pagination");
        boolean isTotalCountDisplayed = globalSettingsPage.isTotalCountDisplayed();
        softAssert.assertTrue(isTotalCountDisplayed, "Failed to display total count");
        boolean isPageColumnDisplayed = globalSettingsPage.isPageColumnDisplayed("ID");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display ID column");
        isPageColumnDisplayed = globalSettingsPage.isPageColumnDisplayed("Status");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display Status column");
        boolean isActionEditAndDeleteDisplayed = globalSettingsPage.isActionEditAndDeleteDisplayed();
        softAssert.assertTrue(isActionEditAndDeleteDisplayed, "Failed to display action edit delete options");
        String DisruptionStatusName = "DS_" + RandomDataUtils.getRandomAlphanumeric(4);
        String status = "Active";
        disruptionStatusPage.createDisruptionStatus(DisruptionStatusName, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        disruptionStatusPage.searchDisruptionStatusByName(DisruptionStatusName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean displayStatus = disruptionStatusPage.isDisruptionStatusDisplayed(DisruptionStatusName);
        softAssert.assertTrue(displayStatus, "Failed to create new disruption status");
        String updateName = DisruptionStatusName + "_EDIT";
        disruptionStatusPage.editDisruptionStatus(updateName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        disruptionStatusPage.clearDisruptionStatusTextField();
        disruptionStatusPage.searchDisruptionStatusByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean updatedDisruptionStatusDisplayStatus = disruptionStatusPage.isDisruptionStatusDisplayed(updateName);
        softAssert.assertTrue(updatedDisruptionStatusDisplayStatus, "Failed to update disruption status");
        disruptionStatusPage.deleteDisruptionStatus();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        disruptionStatusPage.clearDisruptionStatusTextField();
        disruptionStatusPage.searchDisruptionStatusByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        disruptionStatusPage.clickOnSearchIcon();
        boolean isDisruptionStatusDeleted = disruptionStatusPage.verifyNoRecordsMessage();
        softAssert.assertTrue(isDisruptionStatusDeleted, "Failed to delete disruption status");
        softAssert.assertAll();
    }

}