package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.DateUtils;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.*;
import io.ient.test.pages.project.ProjectSettingsPage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.ArrayList;
import java.util.List;

public class SupplierShippingCalendarTest extends FrameworkTestBase {

    private static final Logger logger = LogManager.getLogger(SupplierShippingCalendarPage.class);
    LogFactory log = new LogFactory(logger);


    @TestCaseID(ids = {"IENT-2785"})
    @Test
    public void shippingCalendarCreationWithApprovedStatus() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSupplierAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnShippingCalendar();
        driver.sleep(WaitTime.SMALL_WAIT);
        SupplierShippingCalendarPage supplierShippingCalendarPage = new SupplierShippingCalendarPage(driver);
        String supplierLocation = "1";
        String locationType = "1";
        String from = "Monday";
        String to = "Friday";
        String caseStatus = "Save As Draft";
        String shortDescription = RandomDataUtils.randomAlphabetic(8);
        String approverName = testContext.app.getBuyerUser();
        supplierShippingCalendarPage.createShippingCalendar(supplierLocation, locationType, from, to, approverName, caseStatus, shortDescription);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierShippingCalendarPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean newShippingCalendarCreatedStatus = supplierShippingCalendarPage.isShippingCalendarDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(newShippingCalendarCreatedStatus, "Failed to create new Shipping Calendar");
        supplierShippingCalendarPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        String submitToBuyerStatus = "Submit to Buyer";
        supplierShippingCalendarPage.editStatus(submitToBuyerStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithBuyerUser(testContext.app);
        homePage.clickOnShippingCalendar();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierShippingCalendarPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean shippingCalendarSubmitToBuyerStatus = supplierShippingCalendarPage.isShippingCalendarDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(shippingCalendarSubmitToBuyerStatus, "Failed to find Submit to Buyer Shipping Calendar");
        supplierShippingCalendarPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        String reworkStatus = "Rework";
        supplierShippingCalendarPage.editStatus(reworkStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithSupplierAdmin(testContext.app);
        homePage.clickOnShippingCalendar();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierShippingCalendarPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean shippingCalendarReworkStatus = supplierShippingCalendarPage.isShippingCalendarDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(shippingCalendarReworkStatus, "Failed to find Rework Shipping Calendar");
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierShippingCalendarPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierShippingCalendarPage.editStatus(submitToBuyerStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithBuyerUser(testContext.app);
        homePage.clickOnShippingCalendar();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierShippingCalendarPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean shippingCalendarLastStatus = supplierShippingCalendarPage.isShippingCalendarDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(shippingCalendarLastStatus, "Failed to find Submit to Buyer Shipping Calendar");
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierShippingCalendarPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        String approvedStatus = "Approved";
        supplierShippingCalendarPage.editStatus(approvedStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithSupplierAdmin(testContext.app);
        homePage.clickOnShippingCalendar();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierShippingCalendarPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean shippingCalendarApprovedStatus = supplierShippingCalendarPage.isShippingCalendarDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(shippingCalendarApprovedStatus, "Failed to find Approved Shipping Calendar");
        driver.sleep(WaitTime.SMALL_WAIT);
        try {
            supplierShippingCalendarPage.selectsRecordAndClicksEdit();
            driver.sleep(WaitTime.SMALL_WAIT);
            String rejectedStatus = "Rejected";
            supplierShippingCalendarPage.editStatus(rejectedStatus);
        } catch (Exception e) {
            log.info("This Shipping Calendar record is no longer editable");
        }

    }

    @TestCaseID(ids = {"IENT-2785"})
    @Test
    public void shippingCalendarCreationWithRejectedStatus() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSupplierAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnShippingCalendar();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        SupplierShippingCalendarPage supplierShippingCalendarPage = new SupplierShippingCalendarPage(driver);
        String supplierLocation = "1";
        String locationType = "1";
        String from = "Monday";
        String to = "Friday";
        String caseStatus = "Submit to Buyer";
        String shortDescription = RandomDataUtils.randomAlphabetic(8);
        String approverName = testContext.app.getBuyerUser();
        supplierShippingCalendarPage.createShippingCalendar(supplierLocation, locationType, from, to, approverName, caseStatus, shortDescription);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierShippingCalendarPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean newShippingCalendarCreatedStatus = supplierShippingCalendarPage.isShippingCalendarDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(newShippingCalendarCreatedStatus, "Failed to create new Shipping Calendar");
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithBuyerUser(testContext.app);
        homePage.clickOnShippingCalendar();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierShippingCalendarPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean shippingCalendarSubmitToBuyerStatus = supplierShippingCalendarPage.isShippingCalendarDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(shippingCalendarSubmitToBuyerStatus, "Failed to find Submit to Buyer Shipping Calendar");
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierShippingCalendarPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        String rejectedStatus = "Rejected";
        supplierShippingCalendarPage.editStatus(rejectedStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithSupplierAdmin(testContext.app);
        homePage.clickOnShippingCalendar();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierShippingCalendarPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean shippingCalendarApprovedStatus = supplierShippingCalendarPage.isShippingCalendarDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(shippingCalendarApprovedStatus, "Failed to find Approved Shipping Calendar");
        driver.sleep(WaitTime.SMALL_WAIT);
        try {
            supplierShippingCalendarPage.selectsRecordAndClicksEdit();
            driver.sleep(WaitTime.SMALL_WAIT);
            String approvedStatus = "Approved";
            supplierShippingCalendarPage.editStatus(approvedStatus);
        } catch (Exception e) {
            log.info("This Shipping Calendar record is no longer editable");
        }

    }

    @TestCaseID(ids = {"IENT-2786"})
    @Test
    public void verifyBuyerAdminShippingCalendars(){
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        String supplierLocation = "1";
        String locationType = "1";
        homePage.clickOnShippingCalendar();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        SupplierShippingCalendarPage supplierShippingCalendarPage = new SupplierShippingCalendarPage(driver);
        supplierShippingCalendarPage.clickFilterIcon();
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierShippingCalendarPage.buyerAdminFilters();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean shippingCalendarApprovedStatus = supplierShippingCalendarPage.isShippingCalendarDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(shippingCalendarApprovedStatus, "Failed to find Shipping Calendar with given filters");
    }

    @TestCaseID(ids = {"IENT-2787","IENT-2788"})
    @Test
    public void verifySupplierAdminShippingCalendars(){
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSupplierAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        String supplierLocation = "1";
        String locationType = "1";
        homePage.clickOnShippingCalendar();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        SupplierShippingCalendarPage supplierShippingCalendarPage = new SupplierShippingCalendarPage(driver);
        supplierShippingCalendarPage.clickFilterIcon();
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierShippingCalendarPage.supplierAdminFilters();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean shippingCalendarApprovedStatus = supplierShippingCalendarPage.isShippingCalendarDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(shippingCalendarApprovedStatus, "Failed to find Shipping Calendar with given filters");
    }

}