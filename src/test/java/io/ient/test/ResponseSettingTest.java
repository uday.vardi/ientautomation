package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.ResponseSettingPage;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class ResponseSettingTest extends FrameworkTestBase {

    @TestCaseID(ids = {"T1266"})
    @Test
    public void verifyNewResponseSettingCreation() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        ResponseSettingPage responseSettingPage = new ResponseSettingPage(driver);
        responseSettingPage.clickOnResponseSettingTab();
        driver.sleep(WaitTime.XSMALL_WAIT);
        String hours = 6+"";
        responseSettingPage.editResponseSettings(hours);
        boolean isResponseSettingsInputFieldsEdited = responseSettingPage.isResponseSettingsInputFieldsEdited(hours);
        softAssert.assertTrue(isResponseSettingsInputFieldsEdited, "Failed to edit response settings input fields");
        boolean isResponseSettingsTextAreaFieldsEdited = responseSettingPage.isResponseSettingsTextAreaFieldsEdited(hours);
        softAssert.assertTrue(isResponseSettingsTextAreaFieldsEdited, "Failed to edit response settings text area fields");
        softAssert.assertAll();

    }

}