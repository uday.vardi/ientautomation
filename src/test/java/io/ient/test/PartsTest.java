package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.GlobalSettingsPage;
import io.ient.test.pages.enterprise.PartsPage;
import io.ient.test.pages.enterprise.RejectReasonsPage;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class PartsTest extends FrameworkTestBase {

    @TestCaseID(ids = {"T1580"})
    @Test
    public void verifyNewPartsCreation() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        PartsPage partsPage = new PartsPage(driver);
        partsPage.clickOnPartsTab();
        String partsName = "PART_" + RandomDataUtils.getRandomAlphanumeric(4);
        String versionName = "V" + RandomDataUtils.randomNumeric(2);
        String supplier = "1";
        String price = "$ 2000";
        String currency = "XUA";
        partsPage.createPart(partsName, versionName, supplier, price, currency);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        partsPage.searchPartByName(partsName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean displayStatus = partsPage.isPartDisplayed(partsName);
        softAssert.assertTrue(displayStatus, "Failed to create new part");
        String updateName = partsName + "_EDIT";
        partsPage.editPart(updateName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        partsPage.clearPartTextField();
        partsPage.searchPartByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean updatedPartDisplayStatus = partsPage.isPartDisplayed(updateName);
        softAssert.assertTrue(updatedPartDisplayStatus, "Failed to update part");
        partsPage.deletePart();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        partsPage.clearPartTextField();
        partsPage.searchPartByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        partsPage.clickOnSearchIcon();
        boolean isPartDeleted = partsPage.verifyNoRecordsMessage();
        softAssert.assertTrue(isPartDeleted, "Failed to delete part");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-673"})
    @Test
    public void verifyPartsPageFeatures() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        GlobalSettingsPage globalSettingsPage = new GlobalSettingsPage(driver);
        PartsPage partsPage = new PartsPage(driver);
        homePage.clickOnEnterprise();
        partsPage.clickOnPartsTab();
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean isSearchBarDisplayed = globalSettingsPage.isSearchBarDisplayed();
        softAssert.assertTrue(isSearchBarDisplayed, "Failed to display searchbar");
        boolean isSelectColumnsDisplayed = globalSettingsPage.isSelectColumnsDisplayed();
        softAssert.assertTrue(isSelectColumnsDisplayed, "Failed to display select columns");
        boolean isFilterIconDisplayed = globalSettingsPage.isFilterIconDisplayed();
        softAssert.assertTrue(isFilterIconDisplayed, "Failed to display filter");
        globalSettingsPage.clickOnTopLeftVerticalIcon();
        boolean isAddNewButtonDisplayed = globalSettingsPage.isAddNewButtonDisplayed();
        softAssert.assertTrue(isAddNewButtonDisplayed, "Failed to display AddNewButton");
        boolean isDownloadAsPdfDisplayed = globalSettingsPage.isDownloadAsPdfDisplayed();
        softAssert.assertTrue(isDownloadAsPdfDisplayed, "Failed to display DownloadAsPdf");
        boolean isDownloadAsCsvDisplayed = globalSettingsPage.isDownloadAsCsvDisplayed();
        softAssert.assertTrue(isDownloadAsCsvDisplayed, "Failed to display DownloadAsCSV");
        boolean isNumberOfItemsPerPageEditable = globalSettingsPage.isNumberOfItemsPerPageEditable();
        softAssert.assertTrue(isNumberOfItemsPerPageEditable, "Failed to edit number of items per page");
        boolean isPaginationDisplayed = globalSettingsPage.isPaginationDisplayed();
        softAssert.assertTrue(isPaginationDisplayed, "Failed to display pagination");
        boolean isTotalCountDisplayed = globalSettingsPage.isTotalCountDisplayed();
        softAssert.assertTrue(isTotalCountDisplayed, "Failed to display total count");
        boolean isPageColumnDisplayed = globalSettingsPage.isPageColumnDisplayed("ID");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display ID column");
        isPageColumnDisplayed = globalSettingsPage.isPageColumnDisplayed("Supplier");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display Supplier column");
        boolean isActionEditAndDeleteDisplayed = globalSettingsPage.isActionEditAndDeleteDisplayed();
        softAssert.assertTrue(isActionEditAndDeleteDisplayed, "Failed to display action edit delete options");
        softAssert.assertAll();
    }

}