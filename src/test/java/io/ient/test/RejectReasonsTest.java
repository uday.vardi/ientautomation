package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.GlobalSettingsPage;
import io.ient.test.pages.enterprise.RejectReasonsPage;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class RejectReasonsTest extends FrameworkTestBase {

    @TestCaseID(ids = {"T1550"})
    @Test
    public void verifyNewRejectReasonCreation() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        RejectReasonsPage rejectReasonsPage = new RejectReasonsPage(driver);
        rejectReasonsPage.clickOnRejectReasonsTab();
        String customerName = "RR_" + RandomDataUtils.getRandomAlphanumeric(4);
        String status = "Active";
        rejectReasonsPage.createRejectReasons(customerName, status);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        rejectReasonsPage.searchRejectReasonByName(customerName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean displayStatus = rejectReasonsPage.isRejectReasonDisplayed(customerName);
        softAssert.assertTrue(displayStatus, "Failed to create new reject reason");
        String updateName = customerName + "_EDIT";
        rejectReasonsPage.editRejectReason(updateName);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        rejectReasonsPage.clearRejectReasonTextField();
        rejectReasonsPage.searchRejectReasonByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean updatedRejectReasonsDisplayStatus = rejectReasonsPage.isRejectReasonDisplayed(updateName);
        softAssert.assertTrue(updatedRejectReasonsDisplayStatus, "Failed to update reject reason");
        rejectReasonsPage.deleteRejectReason();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        rejectReasonsPage.clearRejectReasonTextField();
        rejectReasonsPage.searchRejectReasonByName(updateName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        rejectReasonsPage.clickOnSearchIcon();
        boolean isRejectReasonsDeleted = rejectReasonsPage.verifyNoRecordsMessage();
        softAssert.assertTrue(isRejectReasonsDeleted, "Failed to delete reject reason");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-672"})
    @Test
    public void verifyRejectReasonsPageFeatures() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        GlobalSettingsPage globalSettingsPage = new GlobalSettingsPage(driver);
        RejectReasonsPage rejectReasonsPage = new RejectReasonsPage(driver);
        homePage.clickOnEnterprise();
        rejectReasonsPage.clickOnRejectReasonsTab();
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean isSearchBarDisplayed = globalSettingsPage.isSearchBarDisplayed();
        softAssert.assertTrue(isSearchBarDisplayed, "Failed to display searchbar");
        boolean isSelectColumnsDisplayed = globalSettingsPage.isSelectColumnsDisplayed();
        softAssert.assertTrue(isSelectColumnsDisplayed, "Failed to display select columns");
        boolean isFilterIconDisplayed = globalSettingsPage.isFilterIconDisplayed();
        softAssert.assertTrue(isFilterIconDisplayed, "Failed to display filter");
        globalSettingsPage.clickOnTopLeftVerticalIcon();
        boolean isAddNewButtonDisplayed = globalSettingsPage.isAddNewButtonDisplayed();
        softAssert.assertTrue(isAddNewButtonDisplayed, "Failed to display AddNewButton");
        boolean isDownloadAsPdfDisplayed = globalSettingsPage.isDownloadAsPdfDisplayed();
        softAssert.assertTrue(isDownloadAsPdfDisplayed, "Failed to display DownloadAsPdf");
        boolean isDownloadAsCsvDisplayed = globalSettingsPage.isDownloadAsCsvDisplayed();
        softAssert.assertTrue(isDownloadAsCsvDisplayed, "Failed to display DownloadAsCSV");
        boolean isNumberOfItemsPerPageEditable = globalSettingsPage.isNumberOfItemsPerPageEditable();
        softAssert.assertTrue(isNumberOfItemsPerPageEditable, "Failed to edit number of items per page");
        boolean isPaginationDisplayed = globalSettingsPage.isPaginationDisplayed();
        softAssert.assertTrue(isPaginationDisplayed, "Failed to display pagination");
        boolean isTotalCountDisplayed = globalSettingsPage.isTotalCountDisplayed();
        softAssert.assertTrue(isTotalCountDisplayed, "Failed to display total count");
        boolean isPageColumnDisplayed = globalSettingsPage.isPageColumnDisplayed("ID");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display ID column");
        isPageColumnDisplayed = globalSettingsPage.isPageColumnDisplayed("Status");
        softAssert.assertTrue(isPageColumnDisplayed, "Failed to display Status column");
        boolean isActionEditAndDeleteDisplayed = globalSettingsPage.isActionEditAndDeleteDisplayed();
        softAssert.assertTrue(isActionEditAndDeleteDisplayed, "Failed to display action edit delete options");
        softAssert.assertAll();
    }
}