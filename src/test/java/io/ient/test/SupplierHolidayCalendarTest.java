package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.DateUtils;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.SupplierHolidayCalendarPage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class SupplierHolidayCalendarTest extends FrameworkTestBase {

    private static final Logger logger = LogManager.getLogger(SupplierHolidayCalendarPage.class);
    LogFactory log = new LogFactory(logger);


    @TestCaseID(ids = {"IENT-2696"})
    @Test
    public void holidayCalendarCreationWithApprovedStatus() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSupplierAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnHolidayCalendar();
        driver.sleep(WaitTime.SMALL_WAIT);
        SupplierHolidayCalendarPage supplierHolidayCalendarPage = new SupplierHolidayCalendarPage(driver);
        String supplierLocation = "1";
        String locationType = "1";
        String fromDate = DateUtils.getFormattedDateAndTimeWithTimeZone(3, 0,"d","IST");
        String toDate = DateUtils.getFormattedDateAndTimeWithTimeZone(4, 0,"d","IST");
        String caseStatus = "Save As Draft";
        String holidayDescription = RandomDataUtils.randomAlphabetic(8);
        String approverName = testContext.app.getBuyerUser();
        supplierHolidayCalendarPage.createHolidayCalendar(supplierLocation, locationType, fromDate, toDate, approverName, caseStatus, holidayDescription);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierHolidayCalendarPage.searchByHolidayDescription(holidayDescription);
        boolean newHolidayCalendarCreatedStatus = supplierHolidayCalendarPage.isHolidayCalendarDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(newHolidayCalendarCreatedStatus, "Failed to create new holiday Calendar");
        supplierHolidayCalendarPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        String submitToBuyerStatus = "Submit to Buyer";
        supplierHolidayCalendarPage.editStatus(submitToBuyerStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithBuyerUser(testContext.app);
        homePage.clickOnHolidayCalendar();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierHolidayCalendarPage.searchByHolidayDescription(holidayDescription);
        boolean holidayCalendarSubmitToBuyerStatus = supplierHolidayCalendarPage.isHolidayCalendarDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(holidayCalendarSubmitToBuyerStatus, "Failed to find Submit to Buyer Holiday Calendar");
        supplierHolidayCalendarPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        String reworkStatus = "Rework";
        supplierHolidayCalendarPage.editStatus(reworkStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithSupplierAdmin(testContext.app);
        homePage.clickOnHolidayCalendar();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierHolidayCalendarPage.searchByHolidayDescription(holidayDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean holidayCalendarReworkStatus = supplierHolidayCalendarPage.isHolidayCalendarDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(holidayCalendarReworkStatus, "Failed to find Rework Holiday Calendar");
        supplierHolidayCalendarPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierHolidayCalendarPage.editStatus(submitToBuyerStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithBuyerUser(testContext.app);
        homePage.clickOnHolidayCalendar();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierHolidayCalendarPage.searchByHolidayDescription(holidayDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean holidayCalendarLastStatus = supplierHolidayCalendarPage.isHolidayCalendarDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(holidayCalendarLastStatus, "Failed to find Submit to Buyer Holiday Calendar");
        supplierHolidayCalendarPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        String approvedStatus = "Approved";
        supplierHolidayCalendarPage.editStatus(approvedStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithSupplierAdmin(testContext.app);
        homePage.clickOnHolidayCalendar();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierHolidayCalendarPage.searchByHolidayDescription(holidayDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean holidayCalendarApprovedStatus = supplierHolidayCalendarPage.isHolidayCalendarDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(holidayCalendarApprovedStatus, "Failed to find Approved Holiday Calendar");
        driver.sleep(WaitTime.SMALL_WAIT);
        try {
            supplierHolidayCalendarPage.selectsRecordAndClicksEdit();
            driver.sleep(WaitTime.SMALL_WAIT);
            String rejectedStatus = "Rejected";
            supplierHolidayCalendarPage.editStatus(rejectedStatus);
        } catch (Exception e) {
            log.info("This Holiday Calendar record is no longer editable");
        }

    }

    @TestCaseID(ids = {"IENT-2696"})
    @Test
    public void holidayCalendarCreationWithRejectedStatus() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSupplierAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnHolidayCalendar();
        driver.sleep(WaitTime.SMALL_WAIT);
        SupplierHolidayCalendarPage supplierHolidayCalendarPage = new SupplierHolidayCalendarPage(driver);
        String supplierLocation = "1";
        String locationType = "1";
        String fromDate = DateUtils.getFormattedDateAndTimeWithTimeZone(22, 0,"d","IST");
        String toDate = DateUtils.getFormattedDateAndTimeWithTimeZone(22, 0,"d","IST");
        String caseStatus = "Submit to Buyer";
        String holidayDescription = RandomDataUtils.randomAlphabetic(8);
        String approverName = testContext.app.getBuyerUser();
        supplierHolidayCalendarPage.createHolidayCalendar(supplierLocation, locationType, fromDate, toDate, approverName, caseStatus, holidayDescription);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierHolidayCalendarPage.searchByHolidayDescription(holidayDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean newHolidayCalendarCreatedStatus = supplierHolidayCalendarPage.isHolidayCalendarDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(newHolidayCalendarCreatedStatus, "Failed to create new Holiday Calendar");
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithBuyerUser(testContext.app);
        homePage.clickOnHolidayCalendar();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierHolidayCalendarPage.searchByHolidayDescription(holidayDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean holidayCalendarSubmitToBuyerStatus = supplierHolidayCalendarPage.isHolidayCalendarDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(holidayCalendarSubmitToBuyerStatus, "Failed to find Submit to Buyer holiday Calendar");
        supplierHolidayCalendarPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        String rejectedStatus = "Rejected";
//        supplierHolidayCalendarPage.addComment();
        supplierHolidayCalendarPage.editStatus(rejectedStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithSupplierAdmin(testContext.app);
        homePage.clickOnHolidayCalendar();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierHolidayCalendarPage.searchByHolidayDescription(holidayDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean holidayCalendarApprovedStatus = supplierHolidayCalendarPage.isHolidayCalendarDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(holidayCalendarApprovedStatus, "Failed to find Approved Holiday Calendar");
        driver.sleep(WaitTime.SMALL_WAIT);
        try {
            supplierHolidayCalendarPage.selectsRecordAndClicksEdit();
            driver.sleep(WaitTime.SMALL_WAIT);
            String approvedStatus = "Approved";
            supplierHolidayCalendarPage.editStatus(approvedStatus);
        } catch (Exception e) {
            log.info("This Holiday Calendar record is no longer editable");
        }

    }

    @TestCaseID(ids = {"IENT-2697"})
    @Test
    public void verifyBuyerAdminHolidayCalendars(){
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        String supplierLocation = "mpl";
        String locationType = "slt";
        homePage.clickOnHolidayCalendar();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        SupplierHolidayCalendarPage supplierHolidayCalendarPage = new SupplierHolidayCalendarPage(driver);
        supplierHolidayCalendarPage.clickFilterIcon();
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierHolidayCalendarPage.buyerAdminFilters();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean holidayCalendarApprovedStatus = supplierHolidayCalendarPage.isHolidayCalendarDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(holidayCalendarApprovedStatus, "Failed to find holiday Calendar with given filters");
    }

    @TestCaseID(ids = {"IENT-2698","IENT-2699"})
    @Test
    public void verifySupplierAdminHolidayCalendars(){
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSupplierAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        String supplierLocation = "mpl";
        String locationType = "slt";
        homePage.clickOnHolidayCalendar();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        SupplierHolidayCalendarPage supplierHolidayCalendarPage = new SupplierHolidayCalendarPage(driver);
        supplierHolidayCalendarPage.clickFilterIcon();
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierHolidayCalendarPage.supplierAdminFilters();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean holidayCalendarApprovedStatus = supplierHolidayCalendarPage.isHolidayCalendarDisplayed(supplierLocation, locationType);
        softAssert.assertTrue(holidayCalendarApprovedStatus, "Failed to find Holiday Calendar with given filters");
    }

}