package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.GlobalSettingsPage;
import io.ient.test.pages.enterprise.TenantsPage;
import io.ient.test.pages.enterprise.UserManagementPage;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class ImpersonateTest extends FrameworkTestBase {

    @TestCaseID(ids = {"T1942"})
    @Test
    public void verifyImpersonateUser() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        UserManagementPage userManagementPage = new UserManagementPage(driver);
        userManagementPage.clickOnUserManagementTab();
        String adminUserName = testContext.app.getBuyerAdminUser();
        String role = "Impersonate User";
        driver.sleep(WaitTime.XSMALL_WAIT);
        userManagementPage.searchUserByUserName(adminUserName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        String userAdminLastName = userManagementPage.getUserLastName(adminUserName);
        boolean impersonateRoleDisplayStatus = userManagementPage.isImpersonateUserDisplayed(adminUserName, role);
        driver.sleep(WaitTime.SMALL_WAIT);
        userManagementPage.editUserRole(role, impersonateRoleDisplayStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        String userName = testContext.app.getBuyerUser();
        userManagementPage.searchUserByUserName(userName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        String userLastName = userManagementPage.getUserLastName(userName);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnImpersonateLink();
        driver.sleep(WaitTime.XSMALL_WAIT);
        homePage.searchImpersonateUserByLastName(userLastName);
        driver.sleep(WaitTime.SMALL_WAIT);
        homePage.clickOnUserLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean userTitleStatus = homePage.verifyUserTitle(userLastName);
        softAssert.assertTrue(userTitleStatus, "Failed to find impersonate user title");
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnEndImpersonateLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean endOfImpersonateTitleStatus = homePage.verifyUserTitle(userAdminLastName);
        softAssert.assertTrue(endOfImpersonateTitleStatus, "Failed to find end of impersonate user title");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-1116"})
    @Test
    public void verifySuperAdminImpersonateBuyerAdmin() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSuperAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnEnterprise();
        TenantsPage tenantsPage = new TenantsPage(driver);
        String tenantName = testContext.app.getAgency();
        String buyerAdminName = testContext.app.getBuyerAdminUser();
        tenantsPage.clickOnTenantsTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        tenantsPage.searchTenantByTenantName(tenantName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        tenantsPage.impersonateTenant();
        driver.sleep(WaitTime.SMALL_WAIT);
        homePage.searchUserByName(buyerAdminName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        homePage.clickOnUserLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        homePage.clickOnEnterprise();
        driver.sleep(WaitTime.SMALL_WAIT);
        GlobalSettingsPage globalSettingsPage = new GlobalSettingsPage(driver);
        boolean isTenantManagementTabDisplayed_BA = globalSettingsPage.isTenantManagementTabDisplayed();
        softAssert.assertTrue(isTenantManagementTabDisplayed_BA, "Failed to display tenant management tab");
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnEndImpersonateLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        homePage.clickOnEnterprise();
        boolean isTenantManagementTabDisplayed_SA = globalSettingsPage.isTenantManagementTabDisplayed();
        softAssert.assertFalse(isTenantManagementTabDisplayed_SA, "Failed to hide tenant management tab for Super Admin");
        softAssert.assertAll();
    }

    @TestCaseID(ids = {"IENT-1120"})
    @Test
    public void verifySuperAdminImpersonateSupplierAdmin() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSuperAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        TenantsPage tenantsPage = new TenantsPage(driver);
        String tenantName = testContext.app.getAgency();
        String supplierAdminName = testContext.app.getSupplierAdminUser();
        homePage.clickOnEnterprise();
        tenantsPage.clickOnTenantsTab();
        driver.sleep(WaitTime.SMALL_WAIT);
        tenantsPage.searchTenantByTenantName(tenantName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        tenantsPage.impersonateTenant();
        driver.sleep(WaitTime.SMALL_WAIT);
        homePage.searchUserByName(supplierAdminName);
        driver.sleep(WaitTime.XSMALL_WAIT);
        homePage.clickOnUserLink();
        driver.sleep(WaitTime.XSMALL_WAIT);
        boolean isTodoButtonDisplayed = homePage.isToDoButtonDisplayed();
        softAssert.assertTrue(isTodoButtonDisplayed, "Failed to display todo button");
        boolean isEnterpriseButtonDisplayed = homePage.isEnterPriseButtonDisplayed();
        softAssert.assertFalse(isEnterpriseButtonDisplayed, "Enterprise button is displayed");
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnEndImpersonateLink();
        driver.sleep(WaitTime.SMALL_WAIT);
        softAssert.assertAll();
    }
}