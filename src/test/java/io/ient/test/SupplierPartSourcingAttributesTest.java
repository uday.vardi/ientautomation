package io.ient.test;

import io.ient.test.context.FrameworkTestBase;
import io.ient.test.facets.Login;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.framework.utils.RandomDataUtils;
import io.ient.test.framework.utils.annotations.TestCaseID;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.enterprise.SupplierPartSourcingAttributesPage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class SupplierPartSourcingAttributesTest extends FrameworkTestBase {

    private static final Logger logger = LogManager.getLogger(SupplierPartSourcingAttributesPage.class);
    LogFactory log = new LogFactory(logger);


    @TestCaseID(ids = {"IENT-2797"})
    @Test
    public void partSourcingAttributesCreationWithApprovedStatus() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSupplierAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnPartSourcingAttributes();
        driver.sleep(WaitTime.SMALL_WAIT);
        SupplierPartSourcingAttributesPage supplierPartSourcingAttributesPage = new SupplierPartSourcingAttributesPage(driver);
        String part = "1";
        String supplierLocation = "1";
        String manufacturingTime = "20";
        String orderProcessingTime = "19";
        String pickTime = "18";
        String quarantineTime = "17";
        String minimumOrderQuantity = "22";
        String caseStatus = "Save As Draft";
        String shortDescription = RandomDataUtils.randomAlphabetic(8);
        String approverName = testContext.app.getBuyerUser();
        supplierPartSourcingAttributesPage.createPartSourcingAttributes(part,supplierLocation,manufacturingTime,orderProcessingTime,pickTime,quarantineTime,minimumOrderQuantity, approverName, caseStatus,shortDescription);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierPartSourcingAttributesPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean newPartSourcingAttributeCreatedStatus = supplierPartSourcingAttributesPage.isPartSourcingAttributesDisplayed(part, supplierLocation);
        softAssert.assertTrue(newPartSourcingAttributeCreatedStatus, "Failed to create new Part Sourcing Attribute");
        supplierPartSourcingAttributesPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        String submitToBuyerStatus = "Submit to Buyer";
        supplierPartSourcingAttributesPage.editStatus(submitToBuyerStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithBuyerUser(testContext.app);
        homePage.clickOnPartSourcingAttributes();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierPartSourcingAttributesPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean psaSubmitToBuyerStatus = supplierPartSourcingAttributesPage.isPartSourcingAttributesDisplayed(part, supplierLocation);
        softAssert.assertTrue(psaSubmitToBuyerStatus, "Failed to find Submit to Buyer Part Sourcing Attribute");
        supplierPartSourcingAttributesPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        String reworkStatus = "Rework";
        supplierPartSourcingAttributesPage.editStatus(reworkStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithSupplierAdmin(testContext.app);
        homePage.clickOnPartSourcingAttributes();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierPartSourcingAttributesPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean psaReworkStatus = supplierPartSourcingAttributesPage.isPartSourcingAttributesDisplayed(part, supplierLocation);
        softAssert.assertTrue(psaReworkStatus, "Failed to find Rework Part Sourcing Attribute");
        supplierPartSourcingAttributesPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierPartSourcingAttributesPage.editStatus(submitToBuyerStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithBuyerUser(testContext.app);
        homePage.clickOnPartSourcingAttributes();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierPartSourcingAttributesPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean psaLastStatus = supplierPartSourcingAttributesPage.isPartSourcingAttributesDisplayed(part, supplierLocation);
        softAssert.assertTrue(psaLastStatus, "Failed to find Submit to Buyer Part Sourcing Attribute");
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierPartSourcingAttributesPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        String approvedStatus = "Approved";
        supplierPartSourcingAttributesPage.editStatus(approvedStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithSupplierAdmin(testContext.app);
        homePage.clickOnPartSourcingAttributes();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierPartSourcingAttributesPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean psaApprovedStatus = supplierPartSourcingAttributesPage.isPartSourcingAttributesDisplayed(part,supplierLocation);
        softAssert.assertTrue(psaApprovedStatus, "Failed to find Approved Part Sourcing Attribute");
        driver.sleep(WaitTime.SMALL_WAIT);
        try {
            supplierPartSourcingAttributesPage.selectsRecordAndClicksEdit();
            driver.sleep(WaitTime.SMALL_WAIT);
            String rejectedStatus = "Rejected";
            supplierPartSourcingAttributesPage.editStatus(rejectedStatus);
        } catch (Exception e) {
            log.info("This Part Sourcing Attribute record is no longer editable");
        }

    }

    @TestCaseID(ids = {"IENT-2797"})
    @Test
    public void partSourcingAttributesCreationWithRejectedStatus() {
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSupplierAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        homePage.clickOnPartSourcingAttributes();
        driver.sleep(WaitTime.SMALL_WAIT);
        SupplierPartSourcingAttributesPage supplierPartSourcingAttributesPage = new SupplierPartSourcingAttributesPage(driver);
        String part = "1";
        String supplierLocation = "1";
        String manufacturingTime = "20";
        String orderProcessingTime = "19";
        String pickTime = "18";
        String quarantineTime = "17";
        String minimumOrderQuantity = "22";
        String caseStatus = "Submit to Buyer";
        String shortDescription = RandomDataUtils.randomAlphabetic(8);
        String approverName = testContext.app.getBuyerUser();
        supplierPartSourcingAttributesPage.createPartSourcingAttributes(part,supplierLocation,manufacturingTime,orderProcessingTime,pickTime,quarantineTime,minimumOrderQuantity,approverName,caseStatus,shortDescription);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierPartSourcingAttributesPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean newPSACreatedStatus = supplierPartSourcingAttributesPage.isPartSourcingAttributesDisplayed(part,supplierLocation);
        softAssert.assertTrue(newPSACreatedStatus, "Failed to create new Part Sourcing Attribute");
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithBuyerUser(testContext.app);
        homePage.clickOnPartSourcingAttributes();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierPartSourcingAttributesPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean psaSubmitToBuyerStatus = supplierPartSourcingAttributesPage.isPartSourcingAttributesDisplayed(part,supplierLocation);
        softAssert.assertTrue(psaSubmitToBuyerStatus, "Failed to find Submit to Buyer Part Sourcing Attribute");
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierPartSourcingAttributesPage.selectsRecordAndClicksEdit();
        driver.sleep(WaitTime.SMALL_WAIT);
        String rejectedStatus = "Rejected";
        supplierPartSourcingAttributesPage.editStatus(rejectedStatus);
        driver.sleep(WaitTime.MEDIUM_WAIT);
        driver.refresh();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        homePage.clickOnAdminDropDownLink();
        homePage.clickOnLogoutLink();

        login.loginWithSupplierAdmin(testContext.app);
        homePage.clickOnPartSourcingAttributes();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        supplierPartSourcingAttributesPage.searchByShortDescription(shortDescription);
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean psaApprovedStatus = supplierPartSourcingAttributesPage.isPartSourcingAttributesDisplayed(part,supplierLocation);
        softAssert.assertTrue(psaApprovedStatus, "Failed to find Approved Part Sourcing Attribute");
        driver.sleep(WaitTime.SMALL_WAIT);
        try {
            supplierPartSourcingAttributesPage.selectsRecordAndClicksEdit();
            driver.sleep(WaitTime.SMALL_WAIT);
            String approvedStatus = "Approved";
            supplierPartSourcingAttributesPage.editStatus(approvedStatus);
        } catch (Exception e) {
            log.info("This Part Sourcing Attribute record is no longer editable");
        }

    }

    @TestCaseID(ids = {"IENT-2798"})
    @Test
    public void verifyBuyerAdminPartSourcingAttributes(){
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithBuyerAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        String supplierLocation = "mpl";
        String part = "pt";
        homePage.clickOnPartSourcingAttributes();
        driver.sleep(WaitTime.SMALL_WAIT);
        SupplierPartSourcingAttributesPage supplierPartSourcingAttributesPage = new SupplierPartSourcingAttributesPage(driver);
        supplierPartSourcingAttributesPage.clickFilterIcon();
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierPartSourcingAttributesPage.buyerAdminFilters();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean psaApprovedStatus = supplierPartSourcingAttributesPage.isPartSourcingAttributesDisplayed(supplierLocation, part);
        softAssert.assertTrue(psaApprovedStatus, "Failed to find Part Sourcing Attribute with given filters");
    }

    @TestCaseID(ids = {"IENT-2799","IENT-2800"})
    @Test
    public void verifySupplierAdminPartSourcingAttributes(){
        SoftAssert softAssert = new SoftAssert();
        Login login = new Login(driver);
        login.loginWithSupplierAdmin(testContext.app);
        HomePage homePage = new HomePage(driver);
        String supplierLocation = "mpl";
        String part = "pt";
        homePage.clickOnPartSourcingAttributes();
        driver.sleep(WaitTime.MEDIUM_WAIT);
        SupplierPartSourcingAttributesPage supplierPartSourcingAttributesPage = new SupplierPartSourcingAttributesPage(driver);
        supplierPartSourcingAttributesPage.clickFilterIcon();
        driver.sleep(WaitTime.SMALL_WAIT);
        supplierPartSourcingAttributesPage.supplierAdminFilters();
        driver.sleep(WaitTime.SMALL_WAIT);
        boolean psaApprovedStatus = supplierPartSourcingAttributesPage.isPartSourcingAttributesDisplayed(supplierLocation, part);
        softAssert.assertTrue(psaApprovedStatus, "Failed to find Part Sourcing Attribute with given filters");
    }

}