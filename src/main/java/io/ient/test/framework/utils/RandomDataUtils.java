package io.ient.test.framework.utils;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class RandomDataUtils {

        private static final Logger log = LogManager.getLogger(RandomDataUtils.class);
        private static final String ALPHABATIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        private static final String ALPHA_NUMERIC = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        private static final String NUMERIC = "1234567890";
        public static String randomNumeric(int count) {
            return randomNumber(count);
        }
        public static String getRandomAlphanumeric(int count) {return randomAlphanumeric(count);}


        /**
         * returns a random numeric string
         * @param length the length of the random string
         * @return random numeric string
         */
        private static String randomNumber(int length) {
            StringBuilder sb = new StringBuilder();
            while (length > 0) {
                int index = (int)(Math.random()* NUMERIC.length());
                sb.append(NUMERIC.charAt(index));
                length --;
            }
            return sb.toString();
        }

        /**
         * returns a random alphanumeric string
         * @param length the length of the random string
         * @return random alphanumeric string
         */
        private static String randomAlphanumeric(int length) {
            StringBuilder sb = new StringBuilder();
            while (length > 0) {
                int index = (int)(Math.random()* ALPHA_NUMERIC.length());
                sb.append(ALPHA_NUMERIC.charAt(index));
                length --;
            }
            return sb.toString();
        }

        /**
         * returns a random alphabatic string
         * @param length the length of the random string
         * @return random alphabatic string
         */
        public static String randomAlphabetic(int length) {
            StringBuilder sb = new StringBuilder();
            while (length > 0) {
                int index = (int)(Math.random()* ALPHABATIC_STRING.length());
                sb.append(ALPHABATIC_STRING.charAt(index));
                length --;
            }
            return sb.toString();
        }
    }
