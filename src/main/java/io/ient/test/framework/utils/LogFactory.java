package io.ient.test.framework.utils;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.ient.test.context.FrameworkTestBase;
import io.ient.test.context.TestNgContext;
import io.ient.test.framework.exception.FrameworkException;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;
import org.apache.log4j.spi.Filter;
import org.apache.log4j.spi.LoggingEvent;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

/**
 * Factory class to dynamically create log4j appenders and put them in a hashmap
 * There is an appender created for each test case that filters log events for the thread in question. One thread runs per bucket.
 */
public class LogFactory {

    private static Logger log = LogManager.getRootLogger();
    private static HashMap<String, FileAppender> appenders;
    private static final String LOGGER_NAME = "io.ient";
    private static final String FRAMEWORK_APPENDER_NAME = "framework-file";
    private static final String FRAMEWORK_LOG_FILENAME = "ient-framework.log";
    private static final String LOG_PATH = "logfiles";
    public ExtentTest extentTest = null;
    Logger loggerInstance;

    public LogFactory(Logger logger) {
        loggerInstance = logger;
        if(extentTest != null)
        {
//            FrameworkTestBase frameworkTestBase = new FrameworkTestBase();
//            extentTest = frameworkTestBase.extentTestManager.getTest();
        }
    }

    public void info(String message)
    {
        loggerInstance.info(message);

        if(extentTest == null)
        {

            FrameworkTestBase frameworkTestBase = new FrameworkTestBase();
            extentTest = frameworkTestBase.extentTestManager.getTest();
        }

        if(extentTest != null)
        {
            extentTest.log(Status.INFO, message);
        }
    }

    public void error(String message)
    {
        loggerInstance.error(message);
    }

    public void error(Exception exception)
    {
        loggerInstance.error(exception);
    }

    public void error(FrameworkException frameworkException)
    {
        loggerInstance.error(frameworkException);
    }

    public void warn(String message)
    {
        loggerInstance.warn(message);
    }

    public void warn(Exception message)
    {
        loggerInstance.warn(message);
    }

    public void debug(String message)
    {
        loggerInstance.debug(message);
    }

    public void error(String message, InterruptedException exception)
    {
        loggerInstance.error(message, exception);
    }

    /**
     * setupFrameworkLogger : Sets up the framework logger appender dynamically
     * This is the overall log file containing all log events from a run
     */
    public static void setupFrameworkLogger() {
        try {
            Logger logger = Logger.getLogger(LOGGER_NAME);
            RollingFileAppender fwkAppender = (RollingFileAppender) logger.getAppender(FRAMEWORK_APPENDER_NAME);
            fwkAppender.setFile(Paths.get(String.valueOf(PathUtils.LOG_FILES_PATH), FRAMEWORK_LOG_FILENAME).toString());
            fwkAppender.activateOptions();
            log.debug("Setup framework log file appender");
        } catch (Exception e) {
            log.warn("Unable to setup framework log file appender");
            throw new FrameworkException("Error setting up framework log file appender", e);
        }
    }

    /**
     * closeFrameworkLogger : Closes the framework logger appender and removes it from the default logger
     * This is the necessary as open appenders will stop log file deletion
     */
    public static void closeFrameworkLogger() {
        try {
            Logger logger = Logger.getLogger(LOGGER_NAME);
            RollingFileAppender fwkAppender = (RollingFileAppender) logger.getAppender(FRAMEWORK_APPENDER_NAME);
            fwkAppender.close();
            Logger.getLogger(LOGGER_NAME).removeAppender(FRAMEWORK_APPENDER_NAME);
            log.debug("Closed and removed framework log file appender");
        } catch (Exception e) {
            log.warn("Unable to close and remove framework log file appender");
            throw new FrameworkException("Error closing and removing framework log file appender", e);
        }
    }

    /**
     * createLogAppender : Create a log appender for running thread and add it to a hashmap of appenders
     * This is used to create a separate log for each test case execution. Log events are then filtered by the current thread
     */
    public static void createLogAppender(TestNgContext context, ExtentTest extentTest) {
        try {
            if (appenders == null) appenders = new HashMap<>();
            String currentTestMethodName = context.getTestMethodName();
            String currentThreadTestMethodName = Thread.currentThread().getId() + "-" + currentTestMethodName;
            if (!appenders.containsKey(currentThreadTestMethodName)) {
                log.debug(String.format("Creating an appender for thread/testcase : %s", currentThreadTestMethodName));
                FileAppender fileAppender = new FileAppender();
                fileAppender.setName(currentThreadTestMethodName);
                Path testLogsPath = Paths.get(String.valueOf(PathUtils.LOG_FILES_PATH), currentTestMethodName + ".log");
                fileAppender.setFile(testLogsPath.toString());
                fileAppender.setLayout(new PatternLayout("%t %d{yyyy-MM-dd HH:mm:ss} %-5p %c{1}:%L - %m%n"));
                fileAppender.setThreshold(Level.DEBUG);
                fileAppender.setAppend(true);
                fileAppender.addFilter(new Filter() {
                    final String currentThread = Thread.currentThread().getName();
                    @Override
                    public int decide(LoggingEvent loggingEvent) {
                        if (loggingEvent.getThreadName().equals(currentThread)) {
                            return ACCEPT;
                        } else {
                            return DENY;
                        }
                    }
                });
                fileAppender.activateOptions();
                appenders.put(currentThreadTestMethodName, fileAppender);
            } else {
                log.debug(String.format("Appender already created for thread/testcase : %s", currentThreadTestMethodName));
            }
            activateLogAppenderForThisThread(context);
            log.debug("Appender is setup for test case");
        } catch (Exception e) {
            log.error("Error creating a new log appender");
            throw new FrameworkException("Error creating a new log appender", e);
        }

    }

    /**
     * activateLogAppenderForThisThread : Activate a log appender for running thread so it starts collecting log events
     * This is used to activate an appender. Once activate all log events for that thread get written to the log file
     */
    private static void activateLogAppenderForThisThread(TestNgContext context) {
        try {
            String currentTestMethodName = context.getTestMethodName();
            String currentThreadTestMethodName = Thread.currentThread().getId() + "-" + currentTestMethodName;
            if (appenders.containsKey(currentThreadTestMethodName)){
                log.debug(String.format("Activating the logger for this thread/testcase : %s and this appender : %s", currentThreadTestMethodName, appenders.get(currentThreadTestMethodName).getName()));
                FileAppender fileAppender = appenders.get(currentThreadTestMethodName);
                Logger.getLogger(LOGGER_NAME).addAppender(fileAppender);
            }
        } catch (Exception e) {
            log.error("Error adding appender to Logger");
            throw new FrameworkException("Error adding appender to Logger", e);
        }
    }

    /**
     * removeLogAppenderForThisThread : Removes an appender for this thread as we no longer need this appender after a test case completes
     * This is used to close/remove an appender for this thread. Once closed no more log events are written
     */
    public static void removeLogAppenderForThisThread(TestNgContext context) {
        try {
            String currentTestMethodName = context.getTestMethodName();
            String currentThreadTestMethodName = Thread.currentThread().getId() + "-" + currentTestMethodName;
            if (appenders.containsKey(currentThreadTestMethodName)){
                log.debug(String.format("Removing the logger for this thread/testcase : %s and this appender : %s", currentThreadTestMethodName, appenders.get(currentThreadTestMethodName).getName()));
                FileAppender fileAppender = appenders.get(currentThreadTestMethodName);
                fileAppender.close();
                Logger.getLogger(LOGGER_NAME).removeAppender(fileAppender);
            }
        } catch (Exception e) {
            log.error("Error removing this appender from Logger");
            throw new FrameworkException("Error removing this appender from Logger", e);
        }
    }

}
