package io.ient.test.framework.utils;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class StringDataUtils {

        private static final Logger log = LogManager.getLogger(StringDataUtils.class);

    /**
     * Converts string to camel case
     * @param str : string input
     * @return returns camel camel
     */
    public static String convertToCamel(String str) {
        // Capitalize first letter of string
        String[] wordsList = str.split(" ");
        StringBuilder stringBuilder = new StringBuilder();
        for (String word : wordsList) {
            int stringLength = word.length();
            String firstLetter = word.substring(0, 1);
            String remainingLetters = word.substring(1, stringLength).toLowerCase();
            String finalString = firstLetter.concat(remainingLetters);
            stringBuilder.append(finalString);
            stringBuilder.append(" ");
        }
        return stringBuilder.toString();
    }

    /**
     * Converts string to camel case
     * @param str : string input
     * @return returns camel camel
     */
    public static String convertToCamel_oneWord(String str) {
        // Capitalize first letter of string
        StringBuilder stringBuilder = new StringBuilder();
        int stringLength = str.length();
        String firstLetter = str.substring(0, 1);
        firstLetter = firstLetter.toUpperCase();
        String remainingLetters = str.substring(1, stringLength).toLowerCase();
        String finalString = firstLetter.concat(remainingLetters);
        stringBuilder.append(finalString);
        return stringBuilder.toString();
    }
    }
