package io.ient.test.framework.utils;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;

public class PathUtils {

    /**
     * directory paths these are set after call to setupDirectories
     */
    public static Path JAR_BASE_PATH;
    public static Path DRIVERS_PATH;
    public static Path SCREENSHOTS_PATH;
    public static Path LOG_FILES_PATH;
    public static Path UPLOADS_PATH;

    //directory names
    private static final String DRIVERS_NAME = "selenium-drivers";
    private static final String SCREENSHOTS_NAME = "test-screenshots";
    private static final String LOG_FILES = "logfiles";
    public static final String UPLOADS_FILES = "uploads";
    //flag to indicate if already setup
    private static Boolean alreadySetup = false;


    //logger
    private static final Logger log = LogManager.getLogger(PathUtils.class);


    /**
     * Creates needed directories
     * Should be called during beforeSuite
     * */
    public static void setupDirectories() throws Exception {
        try {
            if (!alreadySetup) {
                //get base directory
                URL jarUrl = ClassLoader.getSystemClassLoader().getResource(".");
                String jarPath = URLDecoder.decode(jarUrl.getPath(),"UTF-8");
                File jarDir = new File(jarPath);
                JAR_BASE_PATH = jarDir.toPath();
                log.debug(String.format("JAR base directory : %s", JAR_BASE_PATH));
                //create web drivers directory
                DRIVERS_PATH = createDirectory(DRIVERS_NAME);
                //create screenshots directory
                SCREENSHOTS_PATH = createDirectory(SCREENSHOTS_NAME);
                //create logs directory
                LOG_FILES_PATH = createDirectory(LOG_FILES);
                //create uploads directory
                UPLOADS_PATH = createDirectory(UPLOADS_FILES);
                //mark as already setup
                alreadySetup = true;
            }
        }
        catch (Exception e) {
            log.error("Unable to setup framework directories");
            log.error(e.getMessage());
            throw e;
        }
    }

    /**
     * createDirectory: Creates a directory relative to jar location
     * @param relativeDir Relative directory path
     * @return Path to the directory
     * @throws Exception
     */
    private static Path createDirectory(String relativeDir) throws Exception {
        Path filePath = Path.of(JAR_BASE_PATH.toString().replace("test-classes", ""));
        Path pathToCreate = filePath.resolve(relativeDir);
        //check if already exists, then create it
        if (Files.notExists(pathToCreate)) {
            //create it
            if (pathToCreate.toFile().mkdir()) {
                log.debug(String.format("Directory created : %s", pathToCreate));
            } else {
                throw new IOException(String.format("Unable to create directory : %s", pathToCreate));
            }
        }
        else {
            FileUtils.cleanDirectory(pathToCreate.toFile());
        }
        return pathToCreate;
    }
}
