package io.ient.test.framework.utils;


import io.ient.test.framework.exception.FrameworkException;
import lombok.AccessLevel;
import lombok.Getter;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.ITestContext;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class for overall framework setup methods
 */
public class CommonUtils {

    private static final Logger log = LogManager.getLogger(CommonUtils.class);

    /**
     * To match the date with reg expression
     * @param dateAndTimeFormat : date and time format
     * @param regExpression : eg expression
     * @return date with reg expression match status
     */
    public static boolean getDateAndTimeFormatMatchedStatus(String dateAndTimeFormat, String regExpression) {
        log.info("Start of the method getDateAndTimeFormatMatchedStatus");
                //Creating a pattern object
                Pattern pattern = Pattern.compile(regExpression);
                //Matching the compiled pattern in the String
                Matcher matcher = pattern.matcher(dateAndTimeFormat);
                boolean bool = matcher.matches();
                if(bool) {
                    log.info(String.format("Given date %s matched with %s format", dateAndTimeFormat, regExpression));
                } else {
                    log.info(String.format("Given date %s is not matched with %s format", dateAndTimeFormat, regExpression));
                }
        log.info("End of the method getDateAndTimeFormatMatchedStatus");
                return bool;
            }
    }
