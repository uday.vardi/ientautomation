package io.ient.test.framework.utils;


import io.ient.test.framework.exception.FrameworkException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.*;

public class DateUtils {

    private static final Logger log = LogManager.getLogger(DateUtils.class);

    /**
     * getFormattedDateWithTimeZone : get formated date N days from now in specified format and in specified time zone
     * @param numberOfDays : Number of days to add to the current date
     * @param dateFormat : Expected Date format
     * @param timeZone : Expected Time Zone
     * @return formatted date
     */
    public static String getFormattedDateWithTimeZone(int numberOfDays, String dateFormat, String timeZone) {
        DateFormat df = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        Date date;
        try {
            calendar.add(5, numberOfDays);
            date = calendar.getTime();
            df.setTimeZone(TimeZone.getTimeZone(timeZone));
        } catch (Exception ex) {
            log.error("Failed to get formatted date in specified time format and in specified time zone");
            throw new FrameworkException(ex);
        }
        return df.format(date);
    }

    /**
     * getFormattedDateAndTimeWithTimeZone : get formatted days and minutes from now in specified format and in specified time zone
     * @param numberOfDays : Number of days to add to the current date
     * @param numberOfHours : Number of hours to add to the current time
     * @param dateFormat : Expected Date format
     * @param timeZone : Expected Time Zone
     * @return returns formatted date and time
     */
    public static String getFormattedDateAndTimeWithTimeZone(int numberOfDays, int numberOfHours, String dateFormat, String timeZone) {
        DateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        Date date;
        try {
            calendar.add(5, numberOfDays);
            calendar.add(Calendar.HOUR, numberOfHours);
            date = calendar.getTime();
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
        } catch (Exception ex) {
            log.error("Failed to get formatted date in specified time format and in specified time zone");
            throw new FrameworkException(ex);
        }
        return simpleDateFormat.format(date);
    }


}

