package io.ient.test.framework.utils.extentreports;


import io.ient.test.framework.exception.FrameworkException;
import io.ient.test.framework.selenium.ResourcesUtils;
import io.ient.test.framework.selenium.SeleniumConfigs;
import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.utils.PathUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class FileUploadUtils {

    private static final Logger log = LogManager.getLogger(FileUploadUtils.class);
    private static final String UPLOAD_DIR_NAME = PathUtils.UPLOADS_FILES;
    private static final String UPLOAD_RESOURCE_PATH = "/testdata/";
    private final WebDriver webDriver;

    public FileUploadUtils(SeleniumDriver driver) {
        webDriver = driver.getWrappedDriver();
        log.info("In class FileUploadUtils");
    }

    /**
     * Returns upload file path
     * @param fileName : file name in string format
     * @return returns uploads path
     */
    public static String getUploadsPath(String fileName) {
        log.debug(String.format("Finding upload path for file: %s", fileName));
        try {
            String uploadsResourcesPath = UPLOAD_RESOURCE_PATH;
            uploadsResourcesPath = uploadsResourcesPath + fileName;
            Path uploadsExtPath = PathUtils.UPLOADS_PATH.resolve(fileName);
            String filePath = uploadsExtPath.toString();
            //check if already unpacked
            if (Files.notExists(uploadsExtPath)) {
                ResourcesUtils.unPackBinaryResource(uploadsResourcesPath, uploadsExtPath);
                log.debug(String.format("uploads location is: %s", uploadsExtPath));
            }
            return filePath;
        } catch (Exception e) {
            log.error("Cannot build uploads path for file");
            log.error(e);
            throw new FrameworkException(e);
        }
    }

    /**
     * Uploads file
     * @param locator : locator object
     * @param fileName : file name in string format
     */
    public void uploadFile(By locator, String fileName) {
        log.info("Start method to upload file");
        String uploadsPath = getUploadsPath(fileName);
        log.debug(String.format("Path : %s", uploadsPath));
        //setup fluent wait
        webDriver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
        Long timeout = SeleniumConfigs.getInteractionTimeout();
        FluentWait<WebDriver> wait = new FluentWait<>(webDriver)
                .withTimeout(Duration.ofSeconds(timeout))
                .pollingEvery(Duration.ofMillis(500));
        try {
            wait.until((Function<WebDriver, Boolean>) driver -> {
                try {
                    WebElement element = webDriver.findElement(locator);
                    JavascriptExecutor exec = (JavascriptExecutor) driver;
                    exec.executeScript("arguments[0].style = ''; arguments[0].style.display = 'block'; " +
                            "arguments[0].style.visibility = 'visible';", element);
                    element.sendKeys(uploadsPath);
                    log.info(String.format("Uploaded file: %s", uploadsPath));
                    return true;
                } catch (Exception e) {
                    return false;
                }
            });
        } catch (Exception e) {
            log.error("Unable to upload file: " + uploadsPath);
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            webDriver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
        log.info("End method to upload file in the from current directory");
    }


}

