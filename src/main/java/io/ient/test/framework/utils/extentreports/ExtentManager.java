package io.ient.test.framework.utils.extentreports;


import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import io.ient.test.context.AppDetails;

public class ExtentManager {

    public static final ExtentReports extentReports = new ExtentReports();

    public synchronized static ExtentReports createExtentReports() {
        AppDetails appDetails = new AppDetails();
        ExtentSparkReporter reporter = new ExtentSparkReporter("./extent-reports/extent-report.html");
        reporter.config().setReportName("IENT AUTOMATION Report");
        extentReports.attachReporter(reporter);
        String agency_name = appDetails.getAgency();
        String url = appDetails.getAppUrl();
        extentReports.setSystemInfo("Client Name", agency_name);
        extentReports.setSystemInfo("Url", url);
        return extentReports;
    }
}
