package io.ient.test.framework.selenium;

import com.google.gson.Gson;
import io.ient.test.framework.exception.FrameworkException;
import io.ient.test.framework.utils.LogFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JsonDataLoader {

    private static final Logger logger = LogManager.getLogger(JsonDataLoader.class);
    static LogFactory log = new LogFactory(logger);



    /**
     * Loads a resource file as a string
     * @param resourceName Path to resource file
     * @return String of resource file contents
     */
    public static String getResourceAsString(String resourceName) {
        log.info(String.format("Reading string resource: %s", resourceName));
        InputStream stream;
        BufferedReader reader = null;
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        try {
            stream = JsonDataLoader.class.getResourceAsStream(resourceName);
            reader = new BufferedReader(new InputStreamReader(stream));
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            return stringBuilder.toString();
        } catch (Exception e) {
            log.error(String.format("Unable to read string resource: %s", resourceName));
            throw new FrameworkException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                    log.error(String.format("Unable to close resource: %s", resourceName));
                }
            }
        }
    }

    /**
     * Loads a json data from a file
     * @param file Name of the json file
     * @return json data
     */
    public static JsonDataLoader loadJsonData(String file) {
        log.info(String.format("Loading form data from json file: %s", file));
        try {
            String json = JsonDataLoader.getResourceAsString(file);
            Gson gson = new Gson();
            JsonDataLoader jsonDataLoader = gson.fromJson(json, JsonDataLoader.class);
            System.out.println("EXPECTED DAT : "+jsonDataLoader.toString());
            return jsonDataLoader;
        } catch (Exception e) {
            log.error("Unable to load json file");
            throw new FrameworkException(e);
        }
    }
}
