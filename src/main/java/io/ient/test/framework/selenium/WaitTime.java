package io.ient.test.framework.selenium;

/**
 * Driver sleeps
 */
public enum WaitTime {
	XSMALL_WAIT,
    SMALL_WAIT,
    MEDIUM_WAIT,
    LONG_WAIT
}
