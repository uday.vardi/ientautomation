package io.ient.test.framework.selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.ient.test.context.TestNgContext;
import io.ient.test.framework.exception.FrameworkException;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.framework.utils.PathUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;


/**
 * Class that creates a new WebDriver instance
 */
public class DriverFactory {

	private static final Logger logger = LogManager.getLogger(DriverFactory.class);
	static LogFactory log = new LogFactory(logger);
	private static final String DRIVERS_RESOURCE_PATH = "/drivers/";
	private static final String CHROME_DRIVER = "chromedriver.exe";



	/**
	 * Gets the location of the driver to use for the specified browser
	 * Unpacks the driver executable if not already unpacked
	 * @param browserName Name of browser
	 * @return Path to the driver exe to use
	 */
	private static synchronized String getDriverPath(BrowserNames browserName) {
		log.debug("Getting path to web driver executable");
		try {
			//get resource path to executable
			String driverResourcesPath = DRIVERS_RESOURCE_PATH;
			String driverName;
			if (browserName == BrowserNames.chrome) {
				driverName = CHROME_DRIVER;
			} else {
				throw new FrameworkException(String.format("No driver available for browser: %s", browserName));
			}
			driverResourcesPath = driverResourcesPath + driverName;
			Path driverExtPath = PathUtils.DRIVERS_PATH.resolve(driverName);
			//check if already unpacked
			if (Files.notExists(driverExtPath)) {
				ResourcesUtils.unPackBinaryResource(driverResourcesPath, driverExtPath);
				log.debug(String.format("Driver location is: %s", driverExtPath));
			}
			return driverExtPath.toString();
		} catch (Exception e) {
			log.error("Unable to determine path to driver executable");
			log.error(e);
			throw new FrameworkException(e);
		}
	}


	/**
	 * Creates a local Chrome browser instance
	 * @return WebDriver instance for the new Chrome instance
	 */
	private static WebDriver getLocalChromeDriver(TestNgContext context) {
		log.debug("Creating local chrome browser instance");
		WebDriver driver;
		try {
			//setup driver location
			//System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY, getDriverPath(BrowserNames.chrome));
			WebDriverManager.chromedriver().setup();
			ChromeOptions chromeOptions = new ChromeOptions();
			//set chrome options path
			chromeOptions.setCapability(CapabilityType.TAKES_SCREENSHOT, true);
			chromeOptions.setAcceptInsecureCerts(true);
			//turn off "do you want to save the password"
			HashMap<String, Object> prefs = new HashMap<>();
			prefs.put("credentials_enable_service", Boolean.FALSE);
			prefs.put("profile.password_manager_enabled", Boolean.FALSE);
			chromeOptions.setExperimentalOption("prefs", prefs);
			chromeOptions.setExperimentalOption("useAutomationExtension", false);
			chromeOptions.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
			//turn off plugins
			chromeOptions.addArguments("--allow-running-insecure-content","--disable-plugins", "--no-sandbox");
			chromeOptions.setPageLoadStrategy(PageLoadStrategy.NONE);
			//create the driver
			driver = new ChromeDriver(chromeOptions);
			//delete all cookies
			driver.manage().deleteAllCookies();
			log.debug("Local chrome instance created");
			return driver;
		} catch (Exception e) {
			log.error("Error creating local chrome driver");
			log.error(e.toString());
			throw e;
		}
	}

	/**
	 * Creates the WebDriver instance
	 * @param browserName Name of browser to create
	 * @return The DefaultDriver instance for this browser
	 */
	public static SeleniumDriver getDriver(TestNgContext context, BrowserNames browserName) {
		log.debug(String.format("Creating browser of type: %s ", browserName));
		try {
			WebDriver driver;
			SeleniumDriver seleniumDriver;

			//create local instance
			switch (browserName) {
				case chrome:
					driver = getLocalChromeDriver(context);
					break;
				default:
					throw new FrameworkException(String.format("Unsupported browser type %s", browserName.toString()));
			}
			//setup and return
			seleniumDriver = new SeleniumDriver(driver);
			driver.manage().timeouts().pageLoadTimeout(SeleniumConfigs.getPageLoadTimeout(), TimeUnit.SECONDS);
			driver.manage().timeouts().setScriptTimeout(SeleniumConfigs.getScriptTimeout(), TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.MILLISECONDS);
			return seleniumDriver;
		} catch (Exception e) {
			log.error("Unable to create browser instance");
			log.error(e);
			throw new FrameworkException(e);
		}

	}


}