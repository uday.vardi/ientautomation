package io.ient.test.framework.selenium;

import io.ient.test.framework.utils.LogFactory;
import io.ient.test.framework.utils.PathUtils;
import io.ient.test.pages.HomePage;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.OutputType;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import java.io.File;
import java.nio.file.Path;
import org.apache.commons.io.FileUtils;
import java.nio.file.Paths;

public class ScreenShotService {

    private static final Logger logger = LogManager.getLogger(ScreenShotService.class);
    LogFactory log = new LogFactory(logger);

    private final WebDriver driver;
    public Path screenShot_filePath = null;

    /**
     * @param driver WebDriver instance
     */
    ScreenShotService(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Captures screenshots of the the page using native selenium TakesScreenshot
     * and saves to the target folder
     */
    public void captureScreenshot(ITestResult testResult) {
        try {
            Path outputPath = PathUtils.SCREENSHOTS_PATH;
            screenShot_filePath = Paths.get(outputPath.toString(),testResult.getTestClass().getName() + "." + testResult.getMethod().getMethodName() + ".png");
            File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenshotFile, new File(screenShot_filePath.toString()));
            log.info(String.format("Screenshot saved as: %s", screenShot_filePath.toString()));
        } catch (Exception e) {
            log.error(String.format("Exception while taking screenshot: %s", e));
        }
    }
}