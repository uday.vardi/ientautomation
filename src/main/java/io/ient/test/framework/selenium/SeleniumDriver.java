package io.ient.test.framework.selenium;

import io.ient.test.framework.exception.FrameworkException;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.HomePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * Class to wraps the selenium interactions
 */
public class SeleniumDriver {
    public WebDriver driver;

    private static final Logger logger = LogManager.getLogger(SeleniumDriver.class);
    LogFactory log = new LogFactory(logger);
    private String lastException;
    private Alert currentAlert;
    private final ScreenShotService screenShotService;

    public SeleniumDriver(WebDriver webdriver) {
        this.driver = webdriver;
        this.screenShotService = new ScreenShotService(webdriver);
        log.info("Driver Object : " + driver);
    }

    /**
     * Opens Url
     * @param url : application url
     */
    public void openUrl(String url) {
        driver.get(url);
        log.info("Navigating to url " + url);
    }

    /**
     * Returns current url
     * @return returns url
     */
    public String getUrl() {
        String currentUrl = driver.getCurrentUrl();
        log.info("Current url is  " + currentUrl);
        return currentUrl;
    }

    /**
     * Refreshes the page
     */
    public void refresh() {
        log.info("Refreshing the page");
        driver.navigate().refresh();
    }

    /**
     * Navigates back
     */
    public void navigateBack() {
        log.info("Navigates to back");
        driver.navigate().back();
    }

    /**
     * Closes the browser
     */
    public void closeBrowser() {
        log.info("Closes the browser");
        driver.close();
    }

    /**
     * Closes browser window
     */
    public void closeBrowserWindow() {
        log.info("Closes the browser window");
        driver.close();
    }

    /**
     * Quits the driver
     */
    public void quitDriver() {
        log.info("quits the driver");
        driver.quit();
    }

    public void waitForElementVisible(By by) {
        WebDriverWait wait = new WebDriverWait(driver, SeleniumConfigs.getInteractionTimeout());
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public void waitForElementNotVisible(By by) {
        log.info("Wait for invisibility of element : " + by.toString());
        WebDriverWait wait = new WebDriverWait(driver, SeleniumConfigs.getInteractionTimeout());
        wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
        log.info("End of Waiting for invisibility of element : ".toUpperCase() + by.toString());
    }


    /*
     * Waiting for element to be present and visible
     * @parameter by:  a locator to be waiting for
     * @return boolean: true if an element to be present and visible; otherwise return false
     */
    public boolean waitForElement(By by) {
        log.info("Starting method: waitForElement");
        this.waitForElementVisible(by);
        int cnt = 0;
        while (!isElementPresent(by) || !isPresentAndVisible(by)) {
            cnt++;
            log.info(String.format("%d, locator is not present and visible yet", cnt));
            if (cnt == 5) {
                break;
            }
        }
        log.info("Ending method: waitForElement");
        return isElementPresent(by) && isPresentAndVisible(by);
    }

    public boolean isElementMissing(By by) {
        log.info(String.format("Check to see whether locator %s is missing or not", by));
        boolean result = isElementPresent(by);
        return !result;
    }


    /**
     * Waits for the element to be Present
     * @param by  locator of the element
     * @return True/False
     */
    public boolean isElementPresent(By by) {
        log.info(String.format("Check to see whether locator %s is present", by));
        Boolean isPresent = isElementPresent(by, SeleniumConfigs.getInteractionTimeout().intValue());
        log.info(String.format("locator is %b", isPresent));
        return isPresent;
    }

    /**
     * Gets the attribute value of a given element.
     * @param by - element locator
     * @param attribute - name of the attribute
     * @return attribute value
     */
    public String getAttribute(By by, String attribute) {
        String attributeValue = driver.findElement(by).getAttribute(attribute);
        log.info("attribute value of " + attribute + " is " + attributeValue);
        return attributeValue;
    }

    /**
     * @param by -  element locator
     * @param attribute -  name of the attribute
     * @return attribute value
     */
    public String safeGetAttribute(By by, String attribute) {
        log.info(String.format("Getting attribute value: %s of web element: %s", attribute, by));
        this.lastException = null;
        final String[] elementAttribute = {null};
        //change implicit wait to allow polling
        driver.manage().timeouts().implicitlyWait(200, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(by);
                        elementAttribute[0] = element.getAttribute(attribute);
                        log.info(String.format("Element attribute is : %s", elementAttribute[0]));
                        return true;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error(String.format("Unable to get the html attribute : %s of web Element : %s : Because of : %s",
                    attribute, by, lastException));
            throw new FrameworkException(String.format("Unable to get the attribute of Locator : ", by));
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
        return elementAttribute[0];
    }

    /**
     * safeCheck - Clicks on a checkbox on the page
     * @param by - element locator
     */
    public void safeCheck(By by) {
        driver.manage().timeouts().implicitlyWait(200, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getInteractionTimeoutDuration());
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(by));
            WebElement element = driver.findElement(by);
            if (!element.isSelected())
                element.click();
            log.info(String.format("Clicked on checkbox %s", by.toString()));
        } catch (Exception e) {
            log.error(String.format("Unable to locate checkbox WebElement %s:", by));
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    public void safeCheckByEnabled(By by) {
        driver.manage().timeouts().implicitlyWait(200, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getInteractionTimeoutDuration());
        try {
            WebElement element = driver.findElement(by);
            wait.until(ExpectedConditions.stalenessOf(element));
            if (!element.isSelected())
                element.click();
            log.info(String.format("Clicked on checkbox %s", by.toString()));
        } catch (Exception e) {
            log.error(String.format("Unable to locate checkbox WebElement %s:", by));
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * safeUncheck - Unchecks a selected checkbox on the page
     * @param by - element locator
     */
    public void safeUncheck(By by) {
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(by));
            WebElement element = driver.findElement(by);
            if (element.isSelected())
                element.click();
            log.info(String.format("Unchecked the checkbox %s", by.toString()));
        } catch (Exception e) {
            log.error(String.format("Unable to locate checkbox WebElement %s:", by));
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * @param by element locator
     */
    public boolean toggleCheckBox(By by) {
        boolean selected = driver.findElement(by).isSelected();
        log.info("check box selected is " + selected);
        if (!selected) {
            driver.findElement(by).click();
        }
        return !selected;
    }

    public boolean safeToggleCheckBox(By by) {
        waitForElement(by);
        return toggleCheckBox(by);
    }

    /**
     * Waits until the specified element is Displayed
     * @param locator   Locator for element
     * @return True/False if the element was found to be displayed
     */
    public boolean isPresentAndVisible(By locator) {
        log.info(String.format("Checking if element is present and visible: %s", locator));
        this.lastException = null;
        //change implicit wait to allow polling
        driver.manage().timeouts().implicitlyWait(240, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            Boolean result = wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(locator);
                        return element.isDisplayed();
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
            log.info("Element was found and displayed");
            return result;
        } catch (Exception e) {
            log.error("Element is not present and visible");
            log.warn(lastException);
            return false;
        } finally {
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    public boolean isElementPresent(By by, int timeout) {
        Long lvalue = Long.valueOf(timeout);
        driver.manage().timeouts().implicitlyWait(240, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(Duration.ofSeconds(lvalue))
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            return wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        if (driver.findElements(by).size() > 0) {
                            log.info(String.format("%s element present and visible is %b", by, true));
                            return true;
                        }
                        return false;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error("Element is not presented");
            if (lastException != null)
                log.warn(lastException);
            return false;
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }


    public Integer getXpathCount(By by) {
        int size = driver.findElements(by).size();
        log.info("Xpath count is " + size);
        return size;
    }


    /**
     * Clicks on an element
     * @param locator   Locator for the element
     */
    public void safeClick(By locator) {
        this.lastException = null;
        //change implicit wait to allow polling
        driver.manage().timeouts().implicitlyWait(240, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(locator);
                        if (!(element.isEnabled() && element.isDisplayed()))
                            return false;
                        element.click();
                        log.error(String.format("Clicked on element : %s", locator.toString()));
                        return true;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error(String.format("Element is not click-able : %s : Because of : %s", locator, lastException));
            throw new FrameworkException(String.format("Locator is not click-able : ", locator));
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }


    /**
     * Gets the text of a web element
     * @param locator - element locator
     * @return String text
     */
    public String safeGetText(By locator) {
        this.lastException = null;
        final String[] elementText = {null};
        //change implicit wait to allow polling
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(locator);
                        elementText[0] = element.getText();
                        log.info(String.format("Element text is : %s", elementText[0]));
                        return true;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error(String.format("Unable to get the text of web Element : %s : Because of : %s", locator, lastException));
            throw new FrameworkException(String.format("Unable to get the text of Locator : ", locator));
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
        return elementText[0];
    }

    public String getText(By by) {
        String text = driver.findElement(by).getText();
        log.info("Text is " + text);
        return text;
    }

    /**
     * @param by - element locator
     * @return String value
     */
    public String safeGetValue(By by) {
        log.info("Start of Method: safeGetValue");
        this.lastException = null;
        final String[] valueAttribute = {null};
        //change implicit wait to allow polling
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(by);
                        valueAttribute[0] = element.getAttribute("value");
                        log.info(String.format("Element  value attribute is : %s", valueAttribute[0]));
                        return true;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error(String.format("Unable to get the value attribute of web Element : %s : Because of : %s", by, lastException));
            throw new FrameworkException(String.format("Unable to get the  value attribute of Locator : ", valueAttribute));
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
        return valueAttribute[0];
    }

    /**
     * safeMouseOver - performs a mouse over action on an element
     * @param locator - element locator
     */
    public void safeMouseOver(By locator) {
        log.info("Start of safeMouseOver action");
        this.lastException = null;
        //change implicit wait to allow polling
        driver.manage().timeouts().implicitlyWait(200, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(locator);
                        if (!(element.isEnabled() && element.isDisplayed()))
                            return false;
                        Actions builder = new Actions(driver);
                        Action myaction = builder.moveToElement(element).build();
                        myaction.perform();
                        sleep(WaitTime.XSMALL_WAIT);
                        log.error(String.format("Performed mouseover action on Element, %s", locator));
                        return true;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error(String.format("Unable to mouseover Element : %s : Because of : %s", locator, lastException));
            throw new FrameworkException(String.format("Unable to perform mouseover action on Locator : %s", locator));
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * set focus to element
     * @param locator  to the element
     */
    public void setElementFocus(By locator) {
        log.info("Getting Element from following locator: " + locator);
        WebElement element = driver.findElement(locator);
        log.info("Setting Focus to the following element: " + locator);
        new Actions(driver).moveToElement(element).perform();
    }

    /**
     * @param by - select element locator
     * @param optionValue - text option value
     */
    public void safeSelect(By by, String optionValue) {
        log.info("Start of Method: safeSelect");
        log.info(String.format("Selecting %s into %s", optionValue, by));
        this.lastException = null;
        //change implicit wait to allow polling
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(by);
                        Select dd = new Select(driver.findElement(by));
                        dd.selectByVisibleText(optionValue);
                        log.info("selected option " + optionValue);
                        return element.isDisplayed();
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
            log.info("Element was found and displayed");
        } catch (Exception e) {
            log.error("Element is not present and visible");
            log.warn(lastException);
        } finally {
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
        log.info("End of Method: safeSelect");
    }

    /**
     * @param by - select locator
     * @param value - select value
     */
    public void safeSelectByValue(By by, String value) {
        log.info("Start of Method: safeSelectByValue");
        log.info(String.format("Selecting %s into %s", value, by));
        this.lastException = null;
        //change implicit wait to allow polling
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(by);
                        Select dd = new Select(driver.findElement(by));
                        dd.selectByValue(value);
                        log.info("selected value " + value);
                        return element.isDisplayed();
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
            log.info("Element was found and displayed");
        } catch (Exception e) {
            log.error(String.format("Element is not present and visible to select by value, %s", value));
            log.warn(lastException);
        } finally {
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
        log.info("End of Method: safeSelectByValue");
    }

    public void waitForElementClickable(By by) {
        waitForElement(by);
        WebDriverWait wait = new WebDriverWait(driver, SeleniumConfigs.getInteractionTimeout());
        wait.until(ExpectedConditions.elementToBeClickable(by));
    }

    public String[] safeGetSelectOptions(By by) {
        waitForElement(by);
        return getSelectOptions(by);
    }

    public String[] getSelectOptions(By by) {
        this.waitForElement(by);
        WebElement ele = driver.findElement(by);
        Select se = new Select(ele);
        List<WebElement> items = se.getOptions();
        String options[] = new String[items.size()];
        for (int i = 0; i < options.length; i++) {
            options[i] = items.get(i).getText();
        }
        return options;
    }

    public boolean isChecked(By by) {
        waitForElement(by);
        boolean isCheck = driver.findElement(by).isSelected();
        log.info("Element checked is " + isCheck);
        return isCheck;
    }


    /**
     * @param by - select by
     * @return boolean - whether element is editable or not.
     */
    public boolean isEditable(By by) {
        boolean isEdit = driver.findElement(by).isEnabled();
        log.info("Is Element editable : " + isEdit);
        return isEdit;
    }


    public void select(By by, String value) {
        Select se = new Select(driver.findElement(by));
        log.debug(String.format("Selecting %s into %s", value, by));
        se.selectByVisibleText(value);
        log.info("Selected " + value + " in dropdown ");
    }

    /**
     * @param by - select by
     * @return boolean - whether checkbox is checked or not.
     */
    public boolean safeIsChecked(By by) {
        waitForElement(by);
        return isChecked(by);
    }

    public String getTitle() {
        String title = driver.getTitle();
        log.info("Page Title is " + title);
        return title;
    }

    public WebDriver getWrappedDriver() {
        return driver;
    }

    /**
     * Method allows the default driver session to sleep temporarily
     * @param sleep:  Duration in milliseconds
     */
    public void sleep(WaitTime sleep) {
        try {
            log.info(String.format("Wait for %d seconds...", SeleniumConfigs.getWaitTime(sleep) / 500));
            Thread.sleep(SeleniumConfigs.getWaitTime(sleep));
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            log.error("An Exception was caught while waiting %s: ", e);
        }
    }

    public void selectWindow(String windowTitle) {
        switchToWindow(windowTitle);
    }

    /**
     * Switches to an iframe specified by a locator
     *
     * @param locator   the locator of the iframe
     */
    public void switchToFrame(By locator) {
        log.info("Switching to iframe : " + locator.toString());
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(locator);
                        driver.switchTo().frame(element);
                        return true;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error("Unable to switch to iframe");
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * Switches to an iframe specified by a locator
     *
     * @param index   the index of the iframe
     */
    public void switchToFrame(int index) {
        log.info(String.format("Switching to iframe by index %s: ", index));
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        driver.switchTo().frame(index);
                        return true;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error("Unable to switch to iframe");
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * Switches context out of an iframe to the default content
     */
    public void switchToDefaultContent() {
        log.info("Switching to default content");
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        driver.switchTo().defaultContent();
                        return true;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error("Unable to switch to default content");
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * Switches to the parent iframe
     */
    public void switchToParentFrame() {
        log.info("Switching to parent iframe");
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        driver.switchTo().parentFrame();
                        return true;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error("Unable to switch to parent iframe");
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * Switches to a browser window
     *
     * @param nameOrHandle   name or handle of the window
     */
    public void switchToWindow(String nameOrHandle) {
        log.info(String.format("Switching to window %s: ", nameOrHandle));
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        driver.switchTo().window(nameOrHandle);
                        return true;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error("Unable to switch to window");
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }


    /**
     * closeWindow, closes Active window
     */
    public void closeWindow() {
        log.info("Start of method: closeWindow");
        log.info("Closing following Window - " + driver.getTitle());
        Set<String> windows = driver.getWindowHandles();
        if (windows.size() == 1)
            throw new FrameworkException("Closing the window will close the application");
        if (windows.size() > 1)
            driver.close();
        log.info("End of method: closeWindow");
    }

    /**
     * Waits for an alert to be present
     */
    public void switchToAlert() {
        log.info("Switching to an alert");
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        currentAlert = driver.switchTo().alert();
                        return true;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error("Unable to switch to Alert");
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }

    }

    /**
     * Spot check if an alert is present
     *
     * @return True if present, otherwise false
     */
    public boolean isAlertPresent() {
        boolean alertPresent = false;
        try {
            currentAlert = driver.switchTo().alert();
            alertPresent = true;
            log.info("Alert present...");
        } catch (NoAlertPresentException noe) {
            log.error("No Such Alert present.....");
        }
        return alertPresent;
    }

    /**
     * Gets the text from an alert box
     *
     * @return The text of the alert
     */
    public String getAlertText() {
        String alertText = "";
        if (isAlertPresent()) {
            alertText = currentAlert.getText();
            log.info("Text on alert is " + alertText);
        }
        return alertText;
    }

    public List<String> getLocatorTexts(By by) {
        List<WebElement> elements = driver.findElements(by);
        List<String> items = new ArrayList<>();
        for (int i = 0; i < elements.size(); i++) {
            items.add(elements.get(i).getText().trim());
        }
        return items;
    }

    public String getLocatorText(By by, int index) {
        List<WebElement> elements = driver.findElements(by);
        String t = "";
        for (int i = 0; i < elements.size(); i++) {
            if (index == i + 1) {
                WebElement e = elements.get(i);
                if (e.isDisplayed()) {
                    t = e.getText();
                    break;
                }
            }
        }
        return t;
    }

    public String getLocatorValue(By by, int index) {
        List<WebElement> elements = driver.findElements(by);
        String t = "";
        for (int i = 0; i < elements.size(); i++) {
            if (index == i + 1) {
                WebElement e = elements.get(i);
                if (e.isDisplayed()) {
                    t = e.getAttribute("value");
                    break;
                }
            }
        }
        return t;
    }


    /**
     * Get the first unmatched attribute index from a provided locator
     * @param by:  By object
     * @param expAttribute:  Attribute expected value to be matched
     * @param attrName:  Attribute name
     * @return index of the first matchless of a given locator
     */
    public int getFirstMatchlessAttributeIndex(By by, String expAttribute, String attrName) {
        this.waitForElement(by);
        List<WebElement> elements = driver.findElements(by);
        int i = 0;
        for (i = 0; i < elements.size(); i++) {
            WebElement e = elements.get(i);
            String t = e.getAttribute(attrName);
            if (t == null) {
                break;
            }
        }
        if (i >= elements.size())
            throw new FrameworkException(String.format("Failed to find %s attribute from locator %s", attrName, by));
        return i;
    }

    public List<String> getLocatorValues(By by) {
        List<WebElement> elements = driver.findElements(by);
        List<String> items = new ArrayList<>();
        for (int i = 0; i < elements.size(); i++) {
            items.add(elements.get(i).getAttribute("value"));
        }
        return items;
    }

    /**
     * clickNthElement : Click on the Nth element in a list of elements
     * @param by   Locator to list of elements
     * @param index   number of element to click
     */
    public void clickNthElement(By by, int index) {
        log.info(String.format("Trying to click on %s element in %s: ", index, by));
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        List<WebElement> elements = driver.findElements(by);
                        WebElement indexedElement = (WebElement) elements.toArray()[index - 1];
                        if (indexedElement.isEnabled()) {
                            indexedElement.click();
                            log.info(String.format("Clicked on Element: %s of Index: %d", by, index));
                            return true;
                        } else {
                            log.error(String.format("Element: %s of Index: %d is not enabled", by, index));
                            return false;
                        }
                    } catch (Exception e) {
                        log.info(String.format("Couldn't click on Element: %s of Index: %d ", by, index));
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error("Unable to click on element");
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * Returns cell position of the column header in the row
     * @param columnHeading   heading of the column eg. Record ID
     * @param locator   locator of the row
     * @return cell position of the header text
     */
    public int columnNumFromColumnHeading(String columnHeading, By locator) {
        log.info("Start Of Method: columnNumFromColumnHeading");
        int columnNum = 0;
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
            List<WebElement> rows = driver.findElements(locator);
            for (int rNum = 0; rNum < rows.size(); rNum++) {
                WebElement row = rows.get(rNum);
                List<WebElement> cells = row.findElements(By.tagName("th"));
                for (int cNum = 0; cNum < cells.size(); cNum++) {
                    String cellData = cells.get(cNum).getText().trim();
                    if (cellData.equals(columnHeading))
                        columnNum = cNum;
                }
            }
        } catch (Exception ex) {
            log.error("Failed to get Column Number based on Column Name");
            throw new FrameworkException(ex);
        } finally {
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
        log.info("End Of Method: columnNumFromColumnHeading");
        return columnNum;
    }


    public void selectElement(By by, int index, String text) {
        List<WebElement> elements = driver.findElements(by);

        for (int i = 0; i < elements.size(); i++) {
            if (index == i + 1) {
                WebElement e = elements.get(i);
                if (e.isDisplayed()) {
                    Select s = new Select(e);
                    s.selectByVisibleText(text);
                    break;
                }
            }
        }
    }

    public boolean isPresentAndDisplayed(By by, int index) {
        List<WebElement> elements = driver.findElements(by);
        boolean ret = false;
        for (int i = 0; i < elements.size(); i++) {
            if (index == i + 1) {
                WebElement e = elements.get(i);
                if (e.isDisplayed() && e.isEnabled()) {
                    ret = true;
                    break;
                }
            }
        }
        return ret;
    }

    /**
     * Write a provided texts to the specified index locator
     *
     * @param by:  a given locator
     * @param index:  the index of a list of locators
     * @param text:  the texts to be written
     */
    public void typeElement(By by, int index, String text) {
        log.info(String.format("Entering text %s onto element %s: ", text, by));
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        List<WebElement> elements = driver.findElements(by);
                        if (elements.size() < index)
                            return false;
                        for (int i = 0; i < elements.size(); i++) {
                            if (index == i + 1) {
                                WebElement e = elements.get(i);
                                if (e.isDisplayed() && e.isEnabled()) {
                                    e.clear();
                                    e.sendKeys(text);
                                    break;
                                } else {
                                    return false;
                                }
                            }
                        }
                        return true;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error("Unable to enter text");
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * Select a check-box per index
     *
     * @param by:   locator of a provided check-box
     * @param index:  locator position
     * @return true if a check-box is not checked; otherwise return false
     */
    public boolean toggleCheckBox(By by, int index) {
        List<WebElement> elements = driver.findElements(by);
        boolean selected = false;
        for (int i = 0; i < elements.size(); i++) {
            if (index == i + 1) {
                WebElement e = elements.get(i);
                if (e.isDisplayed()) {
                    selected = e.isSelected();
                    log.info(String.format("check box %d index selected is %b ", i + 1, selected));
                    if (!selected) {
                        e.click();
                        break;
                    }
                }
            }
        }
        return !selected;
    }

    public String getSelectedLabel(By by, int index) {
        List<WebElement> elements = driver.findElements(by);
        String selectedText = "";
        for (int i = 0; i < elements.size(); i++) {
            if (index == i + 1) {
                WebElement e = elements.get(i);
                if (e.isDisplayed()) {
                    Select s = new Select(e);
                    selectedText = s.getFirstSelectedOption().getText();
                    log.info(String.format("selected text %s in the drop down is ", selectedText));
                    return selectedText;
                }
            }
        }
        return selectedText;
    }

    public void windowMaximize() {
        log.info("maximizing window ... ");
        driver.manage().window().maximize();
        log.info(String.format("Window dimensions are %s", driver.manage().window().getSize()));
    }

    public String[] getAllWindowTitles() {
        String titles[];
        Set<String> windows = driver.getWindowHandles();
        int count = windows.size();
        log.info("Number of windows is " + count);
        titles = new String[count];
        Iterator<String> it = windows.iterator();
        List<String> windowTitles = new ArrayList<String>();
        while (it.hasNext()) {

            String title = it.next();
            windowTitles.add(title);
        }
        for (int i = 0; i < windowTitles.size(); i++) {
            titles[i] = windowTitles.get(i);
        }
        return titles;
    }

    /**
     * getAllWindowTitlesOrUrls : To get all window titles or urls
     * @return returns window title list
     */
    public List<String> getAllWindowTitlesOrUrls() {
        log.info("Start of method: getAllWindowTitlesOrUrls");
        List<String> windowTitleList = new ArrayList<>();
        try {
            String[] windowHandles = getAllWindowTitles();
            for (String windowTitle : windowHandles) {
                switchToWindow(windowTitle);
                String title = driver.getTitle();
                log.info("Window Title is : " + title);
                if (title.equals("")) {
                    String windowUrl = driver.getCurrentUrl();
                    log.info("Window Url is : " + windowUrl);
                    windowTitleList.add(windowUrl);
                } else {
                    windowTitleList.add(driver.getTitle());
                }
            }
        } catch (Exception ex) {
            log.error("Unable to get all open window titles/URLs");
            throw new FrameworkException(ex);
        }
        log.info("End of method: getAllWindowTitlesOrUrls");
        return windowTitleList;
    }

    /**
     * switchToWindowWithTitleOrURL : To switch window based on title or url
     * @param titleOrURL            : window title or url in string format
     * @param parentWindowTitleName : parent window title name in string format
     */
    public void switchToWindowWithTitleOrURL(String titleOrURL, String parentWindowTitleName) {
        log.info("Start of the method : switchToWindowWithTitleOrURL");
        boolean isTitleWindowFound = false;
        int windowIndex = 0;
        List<String> windowTitles = getAllWindowTitlesOrUrls();
        String[] windowHandles = getAllWindowTitles();
        try {
            for (String title : windowTitles) {
                if (!title.equals(parentWindowTitleName)) {
                    if (title.contains(titleOrURL)) {
                        log.info("Switching to window with following title or url : " + title);
                        windowIndex = windowTitles.indexOf(title);
                        isTitleWindowFound = true;
                    } else {
                        log.info("closing window with following title or url : " + title);
                        switchToWindow(windowHandles[windowTitles.indexOf(title)]);
                        closeWindow();
                    }
                }
            }
            if (isTitleWindowFound)
                switchToWindow(windowHandles[windowIndex]);
        } catch (Exception ex) {
            log.error("Unable to switch window with following title or url : " + titleOrURL);
            throw new FrameworkException(ex);
        }
        log.info("End of the method : switchToWindowWithTitleOrURL");
    }

    /**
     * switchToWindowWithTitleOrURL : To switch window based on title or url without closing opened window
     * @param titleOrURL : window title or url in string format
     */
    public void switchToWindowWithTitleOrURL(String titleOrURL) {
        log.info("Start of the method : switchToWindowWithTitleOrURL");
        boolean isTitleWindowFound = false;
        List<String> windowTitles = getAllWindowTitlesOrUrls();
        String[] windowHandles = getAllWindowTitles();
        try {
            for (String title : windowTitles) {
                if (title.contains(titleOrURL)) {
                    log.info("Switching to window with following title or url : " + title);
                    switchToWindow(windowHandles[windowTitles.indexOf(title)]);
                    isTitleWindowFound = true;
                    break;
                }
            }
            if (!isTitleWindowFound)
                throw new FrameworkException("Failed to switch window with following title or url : " + titleOrURL);
        } catch (Exception ex) {
            log.error("Unable to switch window with following title or url : " + titleOrURL);
            throw new FrameworkException(ex);
        }
        log.info("End of the method : switchToWindowWithTitleOrURL");
    }

    /**
     * getSelectedValue - Gets the selected option in a dropdown WebElement
     * @param locator  element locator
     */
    public String getSelectedValue(By locator) {
        log.info("Start of Method: getSelectedValue");
        this.lastException = null;
        final String[] selectedOption = {null};
        //change implicit wait to allow polling
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(locator);
                        Select dd = new Select(element);
                        selectedOption[0] = dd.getFirstSelectedOption().getAttribute("option");
                        if (selectedOption[0] == null) {
                            selectedOption[0] = dd.getFirstSelectedOption().getText();
                        }
                        log.info("Selected option " + selectedOption[0]);
                        return element.isDisplayed();
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
            log.info("Element was found and displayed");
        } catch (Exception e) {
            log.error("Element is not present and visible");
            log.warn(lastException);
        } finally {
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
        log.info("Option value for selected text is " + selectedOption[0]);
        return selectedOption[0];
    }

    /**
     * doubleClick - Perform a double click action on a webElement
     * @param by - element locator
     */
    public void doubleClick(By by) {
        this.lastException = null;
        //change implicit wait to allow polling
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(by);
                        Actions builder = new Actions(driver);
                        builder.moveToElement(element).doubleClick().build().perform();
                        log.info(String.format("Double clicked on webElement %s", by.toString()));
                        return true;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error(String.format("Unable to  double click WebElement %s:", by));
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * Perform drag and drop from one element to another
     * @param draggableLocator   draggable element locator
     * @param targetLocator   target element locator
     */
    public void dragAndDropToObject(By draggableLocator, By targetLocator) {
        log.info("Starting dragAndDropToObject");
        this.lastException = null;
        //change implicit wait to allow polling
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement draggable = driver.findElement(draggableLocator);
                        WebElement droppable = driver.findElement(targetLocator);
                        if (draggable.isEnabled() && draggable.isDisplayed()) {
                            Actions builder = new Actions(driver);
                            builder.clickAndHold(draggable).build().perform();
                            builder.moveToElement(droppable).build().perform();
                            builder.moveByOffset(-1, -1).build().perform();
                            builder.release().build().perform();
                            log.info("Successfully performed drag and drop");
                            return true;
                        } else {
                            return false;
                        }
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error(String.format("Unable to drag %s to %s", draggableLocator, targetLocator));
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * @param by  Element locator
     * @param x  draggable element position
     * @param y  target element position
     */
    public void dragAndDropToXY(By by, int x, int y) {
        log.info("Starting Fluent wait for locator to be present : " + by.toString());
        this.lastException = null;
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(by);
                        log.info("Starting drag from " + x);
                        String xto = Integer.toString(x);
                        String yto = Integer.toString(y);
                        String jsScript = ResourcesUtils.getResourceAsString(
                                "/jsScripts/mouseSimulationScript.js");
                        JavascriptExecutor exec = (JavascriptExecutor) driver;
                        exec.executeScript(jsScript, element, xto, yto);
                        log.info("Successfully performed drag and drop to XY " + xto + yto);
                        return true;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error("Unable to perform drag and drop to XY  - " + lastException);
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    public void windowFocus() {
        sleep(WaitTime.SMALL_WAIT);
    }

    public List<WebElement> findElements(By arg0) {
        return driver.findElements(arg0);
    }

    /**
     * returns total number of element
     * @param arg  element to search
     * @return total number of element
     */
    public int totalNumberOfElements(By arg) {
        log.info("Start of Method: totalNumberOfElements");
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(arg));
            log.info("End of Method: totalNumberOfElements");
            return driver.findElements(arg).size();
        } catch (Exception ex) {
            log.error("Timeout waiting for webElement to be present" + ex);
            throw new FrameworkException(ex);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * Enter text to Every text box
     * @param elementLoc  element locator
     * @param textBoxLocator text box locator to insert text
     */
    public void sendKeysToMultipleTextBox(By elementLoc, By textBoxLocator) {
        log.info("Start of Method: sendKeysToEveryElement");
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        wait.until(ExpectedConditions.visibilityOfElementLocated(elementLoc));
                        wait.until(ExpectedConditions.visibilityOfElementLocated(textBoxLocator));
                        List<WebElement> tableRowsBeforeSorting = driver.findElements(elementLoc);
                        int sortingnumber = totalNumberOfElements(elementLoc);
                        for (WebElement element : tableRowsBeforeSorting) {
                            element.findElement(textBoxLocator).clear();
                            element.findElement(textBoxLocator).sendKeys(sortingnumber + "");
                            sortingnumber--;
                        }
                        return true;
                    } catch (Exception ex) {
                        log.error("Failed to Enter text on text box");
                        log.error(ex);
                        return false;
                    }
                }
            });

            log.info("End of Method: sendKeysToEveryElement");
        } catch (Exception ex) {
            log.error("Failed  on following method: sendKeysToEveryElement");
            log.error(ex);
            throw new FrameworkException(ex);
        } finally {
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * get Text From All The Elements from a locator within a locator
     * @param elementLoc  locator to the elements
     * @param textBoxLocator  locator to get text from
     * @return list item with all the text
     */
    public List<String> getTextFromLocatorWithinLocator(By elementLoc, By textBoxLocator) {
        log.info("Start of Method: getTextFromAllTheElements");
        List<String> allString = new ArrayList<>();
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        List<WebElement> elements = driver.findElements(elementLoc);
                        for (WebElement element : elements) {
                            if (element.isDisplayed()) {
                                String recordTypeFilterNameBeforeSort = element.findElement(textBoxLocator).getText();
                                log.info("Found following text : " + recordTypeFilterNameBeforeSort);
                                allString.add(recordTypeFilterNameBeforeSort);
                            }
                        }
                        log.info("Start of Method: getTextFromAllTheElements");
                        return true;
                    } catch (Exception ex) {
                        log.error("Failed to get text from all the elements of the locator" + elementLoc);
                        log.error(ex);
                        return false;
                    }
                }
            });
            return allString;
        } catch (Exception e) {
            log.error("Unable to get Text From All Text Box Within The Locator" + elementLoc);
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * get Text From All The Elements within the locator
     * @param elementLoc  locator to the elements
     * @return list of string with all the text
     */
    public List<String> getTextsFromAllElementOfALocator(By elementLoc) {
        log.info("Start of Method: getTextFromAllTheElements");
        List<String> allString = new ArrayList<>();
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        List<WebElement> elements = driver.findElements(elementLoc);
                        String textFromLocator;
                        for (WebElement element : elements) {
                            if (element.isDisplayed()) {
                                textFromLocator = element.getText();
                                log.info("Found following text : " + textFromLocator);
                                allString.add(textFromLocator);
                            }
                        }
                        log.info("Start of Method: getTextFromAllTheElements");
                        return true;
                    } catch (Exception ex) {
                        log.error("Failed to get text from all the elements of the locator" + elementLoc);
                        log.error(ex);
                        return false;
                    }
                }
            });
            return allString;
        } catch (Exception e) {
            log.error("Unable to get text from all text for Element: " + elementLoc);
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * Click on link based on text inside a locator, clicks on first matching link only
     * @param textLoc  locator of texts
     * @param textToClick  text to find on the link and click
     */
    public void clickLinkBasedOnText(By textLoc, String textToClick) {
        log.info("Start of Method: clickLinkText");
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(textLoc));
            List<WebElement> elements = driver.findElements(textLoc);
            for (WebElement element : elements) {
                if (element.getText().trim().equals(textToClick)) {
                    log.info("Clicking on Following text: " + textToClick);
                    element.click();
                    log.info("End of Method: clickLinkText");
                    break;
                }
            }
        } catch (Exception ex) {
            log.error("Failed To click on Following text: " + textToClick);
            log.error(ex);
            throw new FrameworkException(ex);
        } finally {
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * Check All the checkbox within the Locator
     * @param elementLoc  locator for the checkboxes
     */
    public void checkAllCheckBoxesWithingTheLocator(By elementLoc) {
        log.info("Start of Method: clickAllElementsOfALocator");
        lastException = null;
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        List<WebElement> elements = driver.findElements(elementLoc);
                        for (WebElement element : elements) {
                            if (!element.isSelected())
                                element.click();
                        }
                        log.info("End of Method: clickAllElementsOfALocator");
                        return true;
                    } catch (Exception ex) {
                        lastException = ex.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error("Unable click All Elements Of ALocator: " + elementLoc);
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    public String getWindowHandle() {
        return driver.getWindowHandle();
    }

    public Set<String> getWindowHandles() {
        return driver.getWindowHandles();
    }

    public void quit() {
        driver.quit();
    }

    public WebDriverWait getWebDriverWait(int timeout) {
        return new WebDriverWait(driver, timeout);
    }

    public String getSelectedLabel(By selectLocator) {
        WebElement element = driver.findElement(selectLocator);
        Select dd = new Select(element);
        String selectedText = dd.getFirstSelectedOption().getText();
        log.info("selected text in the drop down is " + selectedText);
        return selectedText;
    }

    public boolean isPresentAndVisible(By by, int timeout) {
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            return wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(by);
                        if (!(element.isEnabled() && element.isDisplayed())) {
                            log.info(String.format("ELEMENT %s: is NOT displayed", by));
                            return false;
                        }
                        log.info(String.format("Element %s: is DISPLAYED", by));
                        return true;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.info(String.format("ELEMENT %s: is NOT displayed last", by));
            if (lastException != null)
                log.error(lastException);
            return false;
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * safeTypeOnSSNAndFEINTextField : To add text to ssn and fein text fields
     * @param locator : locator By object
     * @param text : input text in string format
     */
    public void safeTypeOnSSNAndFEINTextField(By locator, String text) {
        log.info("Start of the method : safeTypeOnSSNAndFEINTextField");
        log.debug(String.format("Performing text addition for : %s", locator));
        this.lastException = null;
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(myDriver -> {
                try {
                    WebElement element = driver.findElement(locator);
                    Actions actions = new Actions(driver);
                    actions.click(element)
                            .doubleClick(element)
                            .sendKeys(Keys.HOME).sendKeys(text).perform();
                    return true;
                } catch (Exception e) {
                    lastException = e.getMessage();
                    return false;
                }
            });
        } catch (Exception e) {
            log.error("Unable to add text for text field");
            log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
        log.info("End of the method : safeTypeOnSSNAndFEINTextField");
    }

    /**
     * Enters text onto an element
     *  @param locator   Locator for the element
     * @param text   Text to type into the element
     */
    public void safeType(By locator, String text) {
        log.info(String.format("Entering text %s onto element %s: ", text, locator));
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration())
                .ignoring(NoSuchElementException.class);
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(locator);
                        if (!(element.isEnabled() && element.isDisplayed()))
                            return false;
                        element.clear();
                        element.sendKeys(text);
                        String actualText = element.getText();
                        String actualValue = element.getAttribute("value");
                        if (actualText == null) actualText = "";
                        if (actualValue == null) actualValue = "";
                        return (actualText.trim().equalsIgnoreCase(text) || actualValue.trim().equalsIgnoreCase(text));
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error("Unable to enter text");
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * Enters text onto an element without clearing the text field
     * @param locator   Locator for the element
     * @param text   Text to type into the element
     */
    public void safeTypeNoClear(By locator, String text) {
        log.info(String.format("Entering text %s onto element %s: ", text, locator));
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(locator);
                        if (!(element.isEnabled() && element.isDisplayed()))
                            return false;
                        element.sendKeys(text);
                        return true;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error("Unable to enter text");
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    public void safeTypeNoCheck(By locator, String text) {
        log.info(String.format("Entering text %s onto element %s: ", text, locator));
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(locator);
                        if (!(element.isEnabled() && element.isDisplayed()))
                            return false;
                        element.clear();
                        element.sendKeys(text);
                        return true;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error("Unable to enter text");
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * Enters text onto an element without using clear method
     * @param locator  Locator for the element
     * @param text  Text to type into the element
     */
    public void safeTypeAppend(By locator, String text) {
        log.info(String.format("Entering text %s onto element %s: ", text, locator));
        //change implicit wait to allow polling
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration())
                .ignoring(NoSuchElementException.class);
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(locator);
                        if (!(element.isEnabled() && element.isDisplayed()))
                            return false;
                        element.sendKeys(text);
                        return true;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error("Unable to enter text");
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    public void waitForPageToLoadJScriptComplete() {
        log.info("Waiting for entire page to load i.e. get document ready state - complete");
        WebDriverWait wait = new WebDriverWait(driver, SeleniumConfigs.getInteractionTimeout());
        log.debug("Into this wait for entire page to load and get ready state to complete method");
        wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver wdriver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState")
                        .equals("complete");
            }
        });
    }

    public void safeClear(By loc) {
        this.waitForElement(loc);
        driver.findElement(loc).clear();
    }

    /**
     * acceptAlertIfPresent, accepts alert if present
     */
    public void acceptAlertIfPresent() {
        log.info("Verifying for availability of alert and click on it");
        try {
            log.info("Start of Method: acceptAlertIfPresent");
            WebDriverWait wait = new WebDriverWait(driver, SeleniumConfigs.getInteractionTimeout());
            wait.until(ExpectedConditions.alertIsPresent());
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            log.debug(String.format("Alert Text : %s", alertText));
            if ((alertText != null) && (alertText.contains("update Chrome") || alertText.contains("Reinstall Chrome"))) {
                alert.dismiss();
            } else {
                alert.accept();
            }
            log.info("End of Method: acceptAlertIfPresent");
        } catch (Exception e) {
            log.error("Failed to accept alert");
            log.warn(e);
            //TODO need to see if can throw an exception here
        }
    }

    public void safeSelectByIndex(By by, int i) {
        this.waitForElement(by);
        log.info(String.format("Selecting %s ", by));
        Select dd = new Select(driver.findElement(by));
        dd.selectByIndex(i);
        log.info("selected index " + i);

    }

    public void jSClick(By loc) {
        log.info("Clicking element using JSClick : " + loc.toString());
        WebElement element = driver.findElement(loc);
        JavascriptExecutor exec = (JavascriptExecutor) driver;
        exec.executeScript("arguments[0].scrollIntoView(true);", element);
        exec.executeScript("arguments[0].click();", element);
        log.info("Clicked on the element : " + loc.toString());
    }

    public void scrollToElementViaJavaScript(By loc) {
        log.info("Scroll to element via java script : " + loc.toString());
        WebElement element = driver.findElement(loc);
        JavascriptExecutor exec = (JavascriptExecutor) driver;
        exec.executeScript("arguments[0].scrollIntoView(true);", element);
        log.info("Scrolled to element via java script : " + loc.toString());
    }

    public void waitForElementToAppear(By by, int timeout, int pollingTime) {
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(by);
                        return element.isEnabled() && element.isDisplayed();
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error(String.format("Locator %s is not displayed", by));
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    public void waitForElementToAppear(By by, long l, int pollingTime) {
        log.info("Starting waitForElementToAppear method");
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(Duration.ofSeconds(l))
                .pollingEvery(Duration.ofSeconds(pollingTime));
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(by);
                        if (element.isEnabled() && element.isDisplayed())
                            return true;
                        else
                            return false;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error(String.format("Locator %s is not displayed", by));
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
            log.info("Done with waitForElementToAppear method");
        }
    }

    /**
     * Waits for an element to appear in the browser
     * @param by  Locator for the element
     */
    public void waitForElementToAppear(By by) {
        this.lastException = null;
        //change implicit wait to allow polling
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(by);
                        return element.isEnabled() && element.isDisplayed();
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error(String.format("Unable to locate WebElement %s:", by));
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * Waits until one of the elements is found
     * @param locator1  locator for first element
     * @param locator2  locator for second element
     * @return Locator of first element found
     */
    public By findFirstElement(By locator1, By locator2) {
        log.info("Start of method: findFirstElement");
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            By whatWasFound = wait.until(new Function<WebDriver, By>() {
                public By apply(WebDriver driver) {
                    WebElement element1 = null;
                    WebElement element2 = null;
                    //try locate element 1
                    try {
                        element1 = driver.findElement(locator1);
                    } catch (Exception e) {
                        lastException = e.getMessage();
                    }
                    //try locate element 2
                    try {
                        element2 = driver.findElement(locator2);
                    } catch (Exception e) {
                        lastException = e.getMessage();
                    }
                    //check what was found
                    if (element1 != null) {
                        log.info(String.format("WebElement [%s] was found first:", locator1));
                        return locator1;
                    } else {
                        if (element2 != null) {
                            log.info(String.format("WebElement [%s] was found first:", locator2));
                            return locator2;
                        } else {
                            return null;
                        }
                    }
                }
            });
            return whatWasFound;
        } catch (Exception e) {
            log.error("Unable to locate webElement");
            if (lastException != null)
                log.info(lastException);
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * Gets the available options of a dropdown element in the browser
     * @param by   Locator for the element
     */
    public List<String> getAllOptionsInDropdown(By by) {
        this.lastException = null;
        List<String> list = new ArrayList<>();
        //change implicit wait to allow polling
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(by));
            Select dropdown = new Select(driver.findElement(by));
            List<WebElement> options = dropdown.getOptions();
            for (WebElement we : options) {
                list.add(we.getText());
            }
            return list;
        } catch (Exception e) {
            log.error(String.format("Unable to locate WebElement %s:", by));
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * Holds Ctrl key and performs a left click on an element
     * @param locator   Element locator
     */
    public void ctrlLeftClick(By locator) {
        log.debug(String.format("Performing ctrl left click on element : %s", locator));
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(locator);
                        Actions actions = new Actions(driver);
                        actions.keyDown(Keys.LEFT_CONTROL).click(element).keyUp(Keys.LEFT_CONTROL);
                        actions.build().perform();
                        return true;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error("Unable to perform ctrl left click");
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * Holds Ctrl key and performs a right click on an element
     * @param locator   Element locator
     */
    public void ctrlRightClick(By locator) {
        log.debug(String.format("Performing ctrl left click on element : %s", locator));
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(locator);
                        Actions actions = new Actions(driver);
                        actions.keyDown(Keys.LEFT_CONTROL).contextClick(element).keyUp(Keys.LEFT_CONTROL);
                        actions.build().perform();
                        return true;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error("Unable to perform ctrl left click");
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * safeClearAndTypeByActionsClass : To clear and add text to unselectable text fields
     * @param locator  : locator By object
     * @param text     : input text in string format
     * @param hitEnter : Hits Enter button if True is given. Doesn't hit Enter button if false is given
     */
    public void safeClearAndTypeByActionsClass(By locator, String text, boolean hitEnter) {
        log.info(String.format("Performing text clearing and addition for : %s", locator));
        this.lastException = null;
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(myDriver -> {
                try {
                    WebElement element = driver.findElement(locator);
                    Actions actions = new Actions(driver);
                    if (hitEnter) {
                        actions.click(element)
                                .doubleClick(element)
                                .sendKeys(Keys.BACK_SPACE).sendKeys(text, Keys.ENTER).perform();
                    } else {
                        actions.click(element)
                                .doubleClick(element)
                                .sendKeys(Keys.BACK_SPACE).sendKeys(text).perform();
                    }
                    return true;
                } catch (Exception e) {
                    lastException = e.getMessage();
                    return false;
                }
            });
        } catch (Exception e) {
            log.error("Unable to clear and add text for text field");
            log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
    }

    /**
     * safeClearAndTypeByActionsClassWithoutEnter : To clear and add text to unselectable text fields with out using enter key
     * @param locator : locator By object
     * @param text : input text in string format
     */
    public void safeClearAndTypeByActionsClassWithoutEnter(By locator, String text) {
        log.info("Start of the method : safeClearAndTypeByActionsClassWithoutEnter");
        log.debug(String.format("Performing text clearing and addition for : %s", locator));
        this.lastException = null;
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration());
        try {
            wait.until(myDriver -> {
                try {
                    WebElement element = driver.findElement(locator);
                    Actions actions = new Actions(driver);
                    actions.click(element)
                            .doubleClick(element)
                            .sendKeys(Keys.BACK_SPACE).sendKeys(text).perform();
                    return true;
                } catch (Exception e) {
                    lastException = e.getMessage();
                    return false;
                }
            });
        } catch (Exception e) {
            log.error("Unable to clear and add text for text field");
            log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
        log.info("End of the method : safeClearAndTypeByActionsClassWithoutEnter");
    }

    public void connectToPopup() {
        windowFocus();
        String parentWindowHandler = driver.getWindowHandle();
        log.info(String.format("Parent window handle: %s", parentWindowHandler));
        String currentWindow = "";
        for (String winHandle : driver.getWindowHandles()) {
            currentWindow = winHandle;
            switchToWindow(currentWindow);
        }
        log.info(String.format("Select window: %s ", currentWindow));
    }

    /**
     * getCssValue - It will get the CssValue of a given element
     * @param by : Element locator
     * @param attribute : Name of the attribute
     * @return : Return the CssValue
     */
    public String getCssValue(By by, String attribute) {
        String cssValue = driver.findElement(by).getCssValue(attribute);
        log.info("Css value of " + attribute + " is " + cssValue);
        return cssValue;
    }

    /**
     * setAttributeValueText - It will set the CssValue text of a given element
     * @param loc : Element locator
     * @param attributeValueText : Name of the attribute
     */
    public void setAttributeValueText(By loc, String attributeValueText) {
        log.info(String.format("Setting text field attribute value text: %s onto element %s: ", attributeValueText, loc));
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(SeleniumConfigs.getInteractionTimeoutDuration())
                .pollingEvery(SeleniumConfigs.getPollingIntervalDuration())
                .ignoring(NoSuchElementException.class);
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        WebElement element = driver.findElement(loc);
                        JavascriptExecutor exec = (JavascriptExecutor) driver;
                        exec.executeScript("arguments[0].value=arguments[1];", element, attributeValueText);
                        return true;
                    } catch (Exception e) {
                        lastException = e.getMessage();
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            log.error("Unable to set element attribute value");
            if (lastException != null)
                log.error(lastException);
            throw new FrameworkException(e);
        } finally {
            //reset implicit wait
            driver.manage().timeouts().implicitlyWait(SeleniumConfigs.getImplicitWait(), TimeUnit.SECONDS);
        }
        log.info(String.format("Set element attribute value text: %s onto element %s: ", attributeValueText, loc));
    }

    /**
     * mouseMovementWithOffsetValues : To perform mouse movement
     * @param xOffset : x offset in integer format
     * @param yOffset : y offset in integer format
     */
    public void mouseMovementWithOffsetValues(int xOffset, int yOffset) {
        log.info("Starting mouseMovementWithOffsetValues");
        try {
            Actions builder = new Actions(driver);
            builder.moveByOffset(xOffset, yOffset).build().perform();
            builder.release().build().perform();
            log.info(String.format("Successfully moved mouse to %d and %d offset", xOffset, yOffset));
        } catch (Exception e) {
            log.error(String.format("Failed to move mouse to %d and %d offset", xOffset, yOffset));
            throw new FrameworkException(e);
        }
    }

    public ScreenShotService getScreenShotService() { return screenShotService; }


}
