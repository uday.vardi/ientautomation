package io.ient.test.framework.selenium;

import io.ient.test.framework.exception.FrameworkException;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.HomePage;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.*;
import java.nio.file.*;

public class ResourcesUtils {

    private static final Logger logger = LogManager.getLogger(ResourcesUtils.class);
    static LogFactory log = new LogFactory(logger);

    /** Unpacks a binary resource to different location
     * @param resourceName Path to resource
     * @param externalPath Path (and filename) where to copy to
     */
    public static void unPackBinaryResource(String resourceName, Path externalPath) {
        log.info(String.format("Unpacking binary resource : %s , to path : %s", resourceName, externalPath));
        //check if external already exists, and delete it
        try {
            if (Files.exists(externalPath)) {
                Files.delete(externalPath);
            }
        } catch (IOException e) {
            log.error("External resource already exists, and couldn't be deleted");
            throw new FrameworkException(e);
        }
        //extract resource
        InputStream stream = null;
        try {
            stream = ResourcesUtils.class.getResourceAsStream(resourceName);
            if (stream == null) {
                throw new IOException(String.format("Cannot get resource %s from Jar file", resourceName));
            }
            byte[] byteArr = IOUtils.toByteArray(stream);
            FileUtils.writeByteArrayToFile(externalPath.toFile(), byteArr);
        } catch (Exception e) {
            log.error(String.format("Exception unpacking resource : %s", resourceName));
            log.error(e.getMessage());
            throw new FrameworkException(e);
        } finally {
            try {
                if (stream != null)
                    stream.close();
            } catch (Exception e) {
                log.warn("Unable to close input stream");
            }
        }
    }

    /**
     * Loads a resource file as a string
     * @param resourceName Path to resource file
     * @return String of resource file contents
     */
    public static String getResourceAsString(String resourceName) {
        log.info(String.format("Reading string resource: %s", resourceName));
        InputStream stream;
        BufferedReader reader = null;
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        try {
            stream = ResourcesUtils.class.getResourceAsStream(resourceName);
            reader = new BufferedReader(new InputStreamReader(stream));
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            return stringBuilder.toString();
        } catch (Exception e) {
            log.error(String.format("Unable to read string resource: %s", resourceName));
            throw new FrameworkException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) { }
            }
        }
    }


    /**
     * Copies a resource source folder to a destination folder
     * @param sourceDirectory Resource source directory
     * @param destinationDirectory Destination directory
     */
    public static void copyResourceDirectory(String sourceDirectory, String destinationDirectory) {
        log.debug(String.format("Copying resource directory: %s to directory: %s", sourceDirectory, destinationDirectory));
        try {
            //get list of all files in resource directory
            String sourcePath = ResourcesUtils.class.getResource(sourceDirectory).getPath();
            log.debug(String.format("Full resource path : %s", sourcePath));
            File[] sourceFiles = new File(sourcePath).listFiles();
            log.debug(String.format("Number of files to copy: %s", sourceFiles.length));
            //copy each
            for (File sourceFile : sourceFiles) {
                Path destination = Paths.get(destinationDirectory).resolve(sourceFile.getName());
                log.debug(String.format("Copying file: %s to %s", sourceFile.getAbsolutePath(), destination));
                Files.copy(sourceFile.toPath(), destination, StandardCopyOption.REPLACE_EXISTING);
            }
            log.debug("All files copied");
        } catch (Exception e) {
            log.error("Error copying directory");
            log.error(e);
            throw new FrameworkException(e);
        }
    }


}