package io.ient.test.framework.selenium;


import io.ient.test.framework.exception.FrameworkException;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.HomePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.nio.charset.Charset;
import java.time.Duration;
import java.util.Locale;
import java.util.ResourceBundle;


public class SeleniumConfigs {

    private static final Logger logger = LogManager.getLogger(SeleniumConfigs.class);
    static LogFactory log = new LogFactory(logger);
    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("seleniumconfigs", Locale.ENGLISH);

    private static BrowserNames currentBrowserName;

    //Page load timeout
    public static long getPageLoadTimeout() {
        String value = getParam("selenium.pageloadtimeout");
        log.info(String.format("selenium.pageloadtimeout = %s", value));
        return Long.parseLong(value);
    }

    //Script time out
    public static long getScriptTimeout() {
        String value = getParam("selenium.scripttimeout");
        log.info(String.format("selenium.scripttimeout = %s", value));
        return Long.parseLong(value);
    }

    //Implicit wait
    public static long getImplicitWait() {
        String value = getParam("selenium.implicitwait");
        log.info(String.format("selenium.implicitwait = %s", value));
        return Long.parseLong(value);
    }

    // Interaction timeout as a duration
    public static Duration getInteractionTimeoutDuration() {
        String value = getParam("selenium.interactiontimeout");
        log.debug(String.format("selenium.interactiontimeout = %s", value));
        Long seconds = Long.parseLong(value);
        return Duration.ofSeconds(seconds);
    }

    // Interaction timeout as a long
    public static Long getInteractionTimeout() {
        String value = getParam("selenium.interactiontimeout");
        log.debug(String.format("selenium.interactiontimeout = %s", value));
        return Long.parseLong(value);
    }

    // Fluent wait polling interval
    public static Duration getPollingIntervalDuration() {
        String value = getParam("selenium.pollinginterval");
        log.debug(String.format("selenium.pollinginterval = %s", value));
        Long seconds = Long.parseLong(value);
        return Duration.ofSeconds(seconds);
    }

    //Read properties from file as string
    private static String getParam(String paramName) {
        Charset UTF_8 = Charset.forName("UTF-8");
        byte ptext[] = BUNDLE.getString(paramName).getBytes(UTF_8);
        String value = new String(ptext, UTF_8);
        if (value.length() < 1) {
            throw new IllegalArgumentException("Required parameter " + paramName + " was not set");
        }
        return value;
    }

    // Default driver wait time
    public static long getWaitTime(WaitTime waitTime) {
        String value;
        try {
            switch (waitTime) {
                case XSMALL_WAIT:
                    value = getParam("xsmall.wait");
                    break;
                case SMALL_WAIT:
                    value = getParam("small.wait");
                    break;
                case MEDIUM_WAIT:
                    value = getParam("medium.wait");
                    break;
                case LONG_WAIT:
                    value = getParam("long.wait");
                    break;
                default:
                    throw new Exception(String.format("Unable to get waitTime time for %s", waitTime));
            }
        } catch (Exception e) {
            throw new FrameworkException(e);
        }
        return Long.parseLong(value)*1000;
    }

    /**
     * Returns specific timeout value from selleniumconfigs.properties file
     * @return returns the time out from selenium.specifictimeout
     * the time out set in: seleniumconfigs.properties file
     */
    public static int getSpecificTimeOut() {
        String value = getParam("selenium.specifictimeout");
        log.debug(String.format("selenium.specifictimeout = %s", value));
        return Integer.parseInt(value);
    }

    /**
     * Gets the name of the browser from either system property (cmd line) or TestNg annotation
     * This is to be called in Before TestNg methods
     * @return
     */
    public static BrowserNames getBrowserName() {
        try {
            String systemBrowser = System.getProperty("browser.name");
            if (systemBrowser == null) {
                throw new Exception("browser.name system property not set");
            } else {
                log.info(String.format("System property browser.name=%s", systemBrowser));
            }
            currentBrowserName = BrowserNames.valueOf(systemBrowser);
            log.info(String.format("Browser name = %s", currentBrowserName));
            return currentBrowserName;
        } catch (Exception e) {
            log.error("Unable to get name of browser");
            log.error(e);
            throw new FrameworkException(e);
        }
    }

}
