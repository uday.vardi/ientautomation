package io.ient.test.framework.exception;

/**
 * Exception related to Yaml file access
 */
public class YamlFileExecption extends RuntimeException {

    public YamlFileExecption(String message) { super(message); }

    public YamlFileExecption(Exception e) {
        super(e);
    }
}
