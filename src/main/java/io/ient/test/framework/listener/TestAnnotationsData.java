package io.ient.test.framework.listener;

import io.ient.test.framework.exception.FrameworkException;
import io.ient.test.framework.utils.annotations.TestCaseID;
import org.apache.commons.exec.util.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.ITestResult;

/**
 * Class to hold all testng annotations data for a single test method
 */
public class TestAnnotationsData {

    public static final Logger log = LogManager.getLogger(TestAnnotationsData.class);

    private String testPackage;
    private String testClass;
    private String testMethod;
    private String[] TestCaseID = {"None"};

    public TestAnnotationsData() {

        testPackage = "None";
        testClass = "None";
        testMethod = "None";
    }

    public TestAnnotationsData(ITestResult iTestResult) {
        TestCaseID testCaseID = iTestResult.getMethod().getConstructorOrMethod().getMethod().getAnnotation(TestCaseID.class);
        if (testCaseID != null) {
            setTestCaseID(testCaseID.ids());
        }
    }

    /**
     * Setter for testPackage
     *
     * @param Data : package name
     */
    public void setTestPackage(String Data) { testPackage = Data; }

    /**
     * Getter for testPackage
     *
     * @return the test package name
     */
    public String getTestPackage() { return testPackage; }

    /**
     * Setter for testClass
     *
     * @param Data : class name
     */
    public void setTestClass(String Data) { testClass = Data; }

    /**
     * Getter for testClass
     *
     * @return the test method name
     */
    public String getTestClass() { return testClass; }

    /**
     * Setter for testMethod
     *
     * @param Data : method name from ITestResult result
     */
    public void setTestMethod(String Data) { testMethod = Data; }

    /**
     * Getter for testMethod
     *
     * @return the test method name
     */
    public String getTestMethod() {
        return testMethod;
    }


    /**
     * Setter for TestCaseID
     *
     * @param Data : TestCaseID got in beforeInvocation method from custom annotation.
     */
    public void setTestCaseID(String[] Data) {
        try {

            TestCaseID = new String[Data.length];
            for (int i = 0; i < Data.length; i++) {
                if (Data[i] != null && !Data[i].isEmpty()) {
                    this.TestCaseID[i] = Data[i];
                }
            }
        } catch (Exception e) {
            log.error("Error in setTestRailID method");
            log.error(e.getMessage());
            throw new FrameworkException(e);
        }
    }

    /**
     * Getter for TestCaseID
     *
     * @return the TestCaseID
     */
    public String getTestCaseID() {
        String testCaseString = "";
        try {
            if (TestCaseID != null) {
                testCaseString = StringUtils.toString(TestCaseID, ", ");
                return testCaseString;
            } else {
                return "None";
            }
        } catch (Exception e) {
            log.error("Error in getTestRailID method");
            log.error(e.getMessage());
            throw new FrameworkException(e);
        }
    }
}
