package io.ient.test.framework.listener;

/**
 * Class to hold all suite level data for a test run that we will need to create a custom report
 */
public class TestSuiteData {

    private String suiteStartTime;
    private String suiteFinishTime;
    private String suiteName;

    TestSuiteData() {
        suiteStartTime = null;
        suiteFinishTime = null;
        suiteName = null;
    }

    /**
     * Setter for suiteStartTime
     *
     * @param Data : suiteStartTime passed from  ITestContext in onStart
     */
    public void setSuiteStartTime(String Data) {
        suiteStartTime = Data;
    }

    /**
     * Getter for suiteStartTime
     *
     * @return the suiteStartTime
     */
    public String getSuiteStartTime() {
        return suiteStartTime;
    }

    /**
     * Setter for suiteFinishTime
     *
     * @param Data : suiteFinishTime passed from  ITestContext in onFinish
     */
    public void setSuiteFinishTime(String Data) {
        suiteFinishTime = Data;
    }

    /**
     * Getter for suiteFinishTime
     *
     * @return the suiteFinishTime
     */
    public String getSuiteFinishTime() {
        return suiteFinishTime;
    }

    /**
     * Setter for suitName
     *
     * @param Data : suiteName passed from  ITestContext in onFinish
     */
    public void setSuiteName(String Data) {
        suiteName = Data;
    }

    /**
     * Getter for suiteName
     *
     * @return the suiteName
     */
    public String getSuiteName() {
        return suiteName;
    }

}
