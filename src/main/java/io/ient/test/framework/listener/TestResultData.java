package io.ient.test.framework.listener;

import io.ient.test.framework.exception.FrameworkException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * Class to hold all result data for a single test method that we will need to create a custom report
 */
public class TestResultData {

    public static final Logger log = LogManager.getLogger(TestResultData.class);

    private String testMethodName;
    private String testStatus;
    private long testDuration;
    private String[] TestCaseID;
    private String errorString;

    public TestResultData() {
        testMethodName = null;
        testStatus = "None";
        testDuration = 0;
        errorString = "-";
    }

    /**
     * Setter for testMethodName
     *
     * @param Data : method name from ITestResult result
     */
    public void setTestMethodName(String Data) {
        testMethodName = Data;
    }

    /**
     * Getter for testMethodName
     *
     * @return the test method name
     */
    public String getTestMethodName() {
        return testMethodName;
    }

    /**
     * Setter for testStatus
     *
     * @param Data : Status string passed from onTestSuccess method
     */
    public void setTestStatus(String Data) {
        testStatus = Data;
    }

    /**
     * Getter for testStatus
     *
     * @return the test status
     */
    public String getTestStatus() {
        return testStatus;
    }

    /**
     * Setter for testDuration
     *
     * @param Data : Duration calculated and passed from onTestSuccess method
     */
    public void setTestDuration(long Data) {
        testDuration = Data;
    }

    /**
     * Getter for TestDuration
     *
     * @return the test duration
     */
    public long getTestDuration() {
        return testDuration;
    }

    /**
     * Setter for TestRailID
     *
     * @param Data : TestRailID got in beforeInvocation method from custom annotation.
     */
    public void setTestCaseID(String[] Data) {
        try {
            TestCaseID = new String[Data.length];
            for (int i = 0; i < Data.length; i++) {
                if (Data[i] != null && !Data[i].isEmpty()) {
                    this.TestCaseID[i] = Data[i];
                }
            }
        } catch (Exception e) {
            log.error("Error in setTestRailID method");
            log.error(e.getMessage());
            throw new FrameworkException(e);
        }
    }

    /**
     * Getter for TestRailID
     *
     * @return the TestRailID
     */
    public String getTestCaseID() {
        String testRailString = "";
        try {
            if (TestCaseID != null) {
                for (int i = 0; i < TestCaseID.length; i++) {
                    testRailString = testRailString + "{" + TestCaseID[i] + "} ";
                }
                return testRailString;
            } else {
                return "None";
            }
        } catch (Exception e) {
            log.error("Error in getTestRailID method");
            log.error(e.getMessage());
            throw new FrameworkException(e);
        }
    }

    /**
     * Setter for errorString
     *
     * @param Data : error string is passed from onTestFailure
     */
    public void setErrorString(String Data) {
        errorString = Data;
    }

    /**
     * Getter for errorString
     *
     * @return the errorString
     */
    public String getErrorString() {
        return errorString;
    }

}
