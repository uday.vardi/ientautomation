package io.ient.test.framework.data;


import com.esotericsoftware.yamlbeans.YamlReader;
import io.ient.test.framework.exception.FrameworkException;
import io.ient.test.framework.utils.LogFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.InputStreamReader;
import java.util.*;

/**
 * Class to load per test class, per test method yaml data files
 */
public class YamlDataLoader {

    private static final Logger logger = LogManager.getLogger(YamlDataLoader.class);
    LogFactory OUT = new LogFactory(logger);
    private static final String ENVIRONMENT_NAME = "env.name";
    private static final String ADMIN_FILE_NAME = "IENT_ADMIN";
    private static final String RESOURCE_PATH = "/env/config/";
    private static final Map<String, String> environmentDetailsMap = new LinkedHashMap<>();

    public Map<String, String> loadEnvironmentDetails() {
        Map<String, String> environmentDetailsMap = new LinkedHashMap<>();
        try {
            String value = System.getProperty(ENVIRONMENT_NAME);
            value = value.toUpperCase();
            if ((value == null) || (value.isEmpty())) {
                throw new Exception("No environment name specified");
            } else {
                OUT.info(String.format("Environment name = %s", value));
                String ENV_FILE = value + ".yaml";
                String yaml_resource_path = RESOURCE_PATH + ENV_FILE;
                OUT.info("yaml_resource_path env_details : " + yaml_resource_path);
                InputStreamReader is = new InputStreamReader(this.getClass().getResourceAsStream(yaml_resource_path), "UTF-8");
                YamlReader yaml = new YamlReader(is);
                Map map = (Map) yaml.read();
                Map<String, Object> environment_details = (Map<String, Object>) map.get("details");
                Map<String, Map<String, Map<String, String>>> environment_user_details = (Map<String, Map<String, Map<String, String>>>) environment_details.get("environments");

                String yaml_resource_path_admin = RESOURCE_PATH + ADMIN_FILE_NAME + ".yaml";
                OUT.info("yaml_resource_path env_details : " + yaml_resource_path_admin);
                InputStreamReader is_admin = new InputStreamReader(this.getClass().getResourceAsStream(yaml_resource_path_admin), "UTF-8");
                YamlReader yaml_admin = new YamlReader(is_admin);
                Map map_admin = (Map) yaml_admin.read();
                Map<String, Object> environment_details_admin = (Map<String, Object>) map_admin.get("details");
                Map<String, Map<String, Map<String, String>>> environment_user_details_admin = (Map<String, Map<String, Map<String, String>>>) environment_details_admin.get("environments");

                environmentDetailsMap.put("client_name", environment_details.get("client_name") + "");
                environmentDetailsMap.put("access_url", environment_details.get("access_url") + "");
                environmentDetailsMap.put("buyer_admin_username", environment_user_details.get("buyer_admin").get("username") + "");
                environmentDetailsMap.put("buyer_admin_password", environment_user_details.get("buyer_admin").get("password") + "");
                environmentDetailsMap.put("buyer_user_username", environment_user_details.get("buyer_user").get("username") + "");
                environmentDetailsMap.put("buyer_user_password", environment_user_details.get("buyer_user").get("password") + "");
                environmentDetailsMap.put("buyer_user2_username", environment_user_details.get("buyer_user2").get("username") + "");
                environmentDetailsMap.put("buyer_user2_password", environment_user_details.get("buyer_user2").get("password") + "");
                environmentDetailsMap.put("supplier_admin_username", environment_user_details.get("supplier_admin").get("username") + "");
                environmentDetailsMap.put("supplier_admin_password", environment_user_details.get("supplier_admin").get("password") + "");
                environmentDetailsMap.put("supplier_user_username", environment_user_details.get("supplier_user").get("username") + "");
                environmentDetailsMap.put("supplier_user_password", environment_user_details.get("supplier_user").get("password") + "");
                environmentDetailsMap.put("bulletin_manager_username", environment_user_details.get("bulletin_manager").get("username") + "");
                environmentDetailsMap.put("bulletin_manager_password", environment_user_details.get("bulletin_manager").get("password") + "");
                environmentDetailsMap.put("bulletin_approver_username", environment_user_details.get("bulletin_approver").get("username") + "");
                environmentDetailsMap.put("bulletin_approver_password", environment_user_details.get("bulletin_approver").get("password") + "");
                environmentDetailsMap.put("admin_client_name", environment_details_admin.get("client_name") + "");
                environmentDetailsMap.put("ient_admin_username", environment_user_details_admin.get("ient_admin").get("username") + "");
                environmentDetailsMap.put("ient_admin_password", environment_user_details_admin.get("ient_admin").get("password") + "");
                environmentDetailsMap.put("Keycloak_admin_username", environment_user_details_admin.get("Keycloak_admin").get("username") + "");
                environmentDetailsMap.put("Keycloak_admin_password", environment_user_details_admin.get("Keycloak_admin").get("password") + "");
                environmentDetailsMap.put("Keycloak_admin_url", environment_user_details_admin.get("Keycloak_admin").get("url") + "");
            }
        } catch (Exception e) {
            OUT.error(e);
            throw new FrameworkException(e);
        }
        return environmentDetailsMap;
    }

}
