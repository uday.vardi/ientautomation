package io.ient.test.framework;


import io.ient.test.context.NameGenerator;
import io.ient.test.framework.exception.FrameworkException;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.framework.utils.PathUtils;
import lombok.AccessLevel;
import lombok.Getter;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.ITestContext;

/**
 * Class for overall framework setup methods
 */
public class Framework {

    private static final Logger log = LogManager.getLogger(Framework.class);

    @Getter(AccessLevel.PUBLIC) private static String suiteName;
    @Getter(AccessLevel.PUBLIC) private static String suiteKey;

    /**
     * Setup the framework - called at start of the test suite
     */
    public static void setup(ITestContext context) {
        try {
            //setup output directories
            PathUtils.setupDirectories();
            //setup suite name
//            suiteKey = NameGenerator.generateSuiteKey();
//            suiteName = NameGenerator.generateSuiteName(suiteKey);
//            log.info(String.format("Setting up test framework for suite: %s", suiteName));
            // setup logger
            LogFactory.setupFrameworkLogger();
            log.info("End of framework setup");
        } catch (Exception e) {
            log.error("Error setting up framework");
            log.error(e);
            throw new FrameworkException(e);
        }
    }







}
