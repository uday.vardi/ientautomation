package io.ient.test.pages;


import io.ient.test.context.FrameworkTestBase;
import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;


public class CommonSectionPage extends BasePage{

	private static final Logger logger = LogManager.getLogger(FrameworkTestBase.class);
	LogFactory log = new LogFactory(logger);

	private static final By filterButton = By.cssSelector("button[id='ient-table-filter-btn']");
	private static final By advancedFilterLink = By.xpath("//a[text()='Advanced Filter']");
	private static final By advancedFilterEditLink = By.xpath("//a[text()='Edit']");
	private static final By filterFieldNameDropdown = By.xpath("(//div[@formarrayname='filters']/descendant::select)[1]");
	private static final By conditionDropdown = By.xpath("(//div[@formarrayname='filters']/descendant::select)[2]");
	private static final By filterField2NameDropdown = By.xpath("(//div[@formarrayname='filters']/descendant::select)[3]");
	private static final By startDateComboDiv = By.xpath("//div[@class='placeholder' and text()=' Start ']");
	private static final By startDateTextField = By.xpath("//input[@placeholder='Start']");
	private static final By endDateTextField = By.xpath("//input[@placeholder='End']");
	private static final By datePickerApplyButton = By.xpath("//span[text()='Apply']");
	private static final By advancedFilterSubmitButton = By.xpath("//span[text()='Submit']");
	private static final By dateHourPicker = By.xpath("//div[contains(@class,'visible')]/table[contains(@class,'table hour four column')]/descendant::td[@class='link' and text()='12:00 AM']");
	private static final By dateMinutePicker = By.xpath("//div[contains(@class,'visible')]/table[contains(@class,'table minute three column')]/descendant::td[contains(@class,'link')and text()='12:00 AM']");
	private static final By reorderColumnsButton = By.xpath("//button[span[text()='Reorder columns']]");
	private static final By reorderColumnPopUpCloseButton = By.xpath("//button[text()='Close']");
	private static final By columnSelectionButton = By.cssSelector("button[id='ient-table-col-selection-btn']");
	private static final By helpIconButton = By.cssSelector("i[class='help icon']");
	private static final By helpMenuItem = By.cssSelector("a[id='help-menu-item']");
	private static final By contactSupportPopUpText = By.xpath("//div[contains(@class,'ient-dialog-content')]/descendant::div[@class='header']");
	private static final By contactSupportPopUpCloseButton = By.xpath("//button[i[contains(@class,'ient-dialog-header-close-icon icon close')]]");
	private static String datePicker = "//div[contains(@class,'visible')]/table[contains(@class,'table day seven column')]/descendant::td[@class='link' and text()='%s']";
	private static String columnUpArrowButton = "//div[div[@class='filter-manage__items__item__input' and contains(text(),'%s')]]/descendant::i[contains(@id,'up')]";
	private static String columnDownArrowButton = "//div[div[@class='filter-manage__items__item__input' and contains(text(),'%s')]]/descendant::i[contains(@id,'down')]";
	private static String contactSupportEmailLinkText = "a[href='%s']";


	public CommonSectionPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - CommonSectionPage");
	}

	/**
	 * Creates advanced filter
	 * @param filedName : filter name in string format
	 * @param condition : condition in string format
	 * @param startDate : start date in string format
	 * @param endDate : end date in string format
	 */
	public void createAdvancedFilter(String filedName, String condition, String startDate, String endDate)
	{
		log.info("Start of the method createAdvancedFilter");
		driver.safeClick(filterButton);
		driver.safeClick(advancedFilterLink);
		driver.safeClick(advancedFilterEditLink);
		driver.safeSelect(filterFieldNameDropdown, filedName);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeSelect(conditionDropdown, condition);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(startDateComboDiv);
		driver.safeClick(startDateTextField);
		driver.safeClick(By.xpath(String.format(datePicker, startDate)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(dateHourPicker);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(dateMinutePicker);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(endDateTextField);
		driver.safeClick(By.xpath(String.format(datePicker, endDate)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(dateHourPicker);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(dateMinutePicker);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(datePickerApplyButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(advancedFilterSubmitButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method createAdvancedFilter");
	}

	/**
	 * Creates advanced filter
	 * @param filedName : filter name in string format
	 * @param condition : condition in string format
	 */
	public void createAdvancedFilter(String filedName, String condition, String fieldName2)
	{
		log.info("Start of the method createAdvancedFilter");
		driver.safeClick(filterButton);
		driver.safeClick(advancedFilterLink);
		driver.safeClick(advancedFilterEditLink);
		driver.safeSelect(filterFieldNameDropdown, filedName);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeSelect(conditionDropdown, condition);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeSelect(filterField2NameDropdown, fieldName2);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(advancedFilterSubmitButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method createAdvancedFilter");
	}

	/**
	 * Reorder column
	 * @param filedName : filter name in string format
	 */
	public void reorderColumn(String filedName)
	{
		log.info("Start of the method reorderColumn");
		driver.safeClick(reorderColumnsButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(columnDownArrowButton, filedName)));
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(reorderColumnPopUpCloseButton);
		log.info("End of the method reorderColumn");
	}

	/**
	 * Reset reorder column
	 * @param filedName : filter name in string format
	 */
	public void resetReorderColumn(String filedName)
	{
		log.info("Start of the method resetReorderColumn");
		driver.safeClick(reorderColumnsButton);
		List<WebElement> webElementList = driver.findElements(By.xpath(String.format("//table[@class='ui striped table actual-table']/thead/tr/th")));
		for(int i=0;i<webElementList.size()-1;i++) {
			driver.safeClick(By.xpath(String.format(columnUpArrowButton, filedName)));
			driver.sleep(WaitTime.SMALL_WAIT);
		}
		driver.safeClick(reorderColumnPopUpCloseButton);
		log.info("End of the method resetReorderColumn");
	}

	public Integer getColumnNumber(String columnName)
	{
		log.info("Start of the method getColumnNumber");
		int columnNumber = driver.columnNumFromColumnHeading(columnName, By.xpath("//table[@class='ui striped table actual-table']/thead/tr"));
		log.info("End of the method getColumnNumber");
		return columnNumber;
	}

	/**
	 * Clicks on column selection icon
	 */
	public void clickOnColumnSelectionIcon() {
		log.info("Start of the method clickOnColumnSelectionIcon");
		driver.safeClick(columnSelectionButton);
		log.info("End of the method clickOnColumnSelectionIcon");
	}

	/**
	 * Opens contact support popup
	 */
	public void openContactSupportPopup()
	{
		log.info("Start of the method openContactSupportPopup");
		driver.safeClick(helpIconButton);
		driver.safeClick(helpMenuItem);
		log.info("End of the method openContactSupportPopup");
	}

	/**
	 * Returns contact support text
	 * @return : returns contact support text in string format
	 */
	public String getContactSupportText()
	{
		log.info("Start of the method getContactSupportText");
		String contactSupportText = driver.safeGetText(contactSupportPopUpText);
		log.info("End of the method getContactSupportText");
		return contactSupportText;
	}

	/**
	 * Verifies whether contact support email is displayed
	 * * @param contactSupportEmail : contact support email in string format
	 */
	public boolean isContactSupportEmailDisplayed(String contactSupportEmail) {
		log.info("Start of the method isContactSupportEmailDisplayed");
		boolean isContactSupportEmailDisplayed = driver.isPresentAndVisible(By.cssSelector(String.format(contactSupportEmailLinkText, contactSupportEmail)));
		log.info("End of the method isContactSupportEmailDisplayed");
		return isContactSupportEmailDisplayed;
	}

	/**
	 * Closes contact support popup
	 */
	public void closeContactSupportPopup()
	{
		log.info("Start of the method closeContactSupportPopup");
		driver.safeClick(contactSupportPopUpCloseButton);
		log.info("End of the method closeContactSupportPopup");
	}
}
