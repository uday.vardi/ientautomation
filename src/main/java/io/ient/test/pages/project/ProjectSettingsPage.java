package io.ient.test.pages.project;

import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.framework.utils.StringDataUtils;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Arrays;
import java.util.List;


public class ProjectSettingsPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(ProjectSettingsPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By nameTextBox = By.cssSelector("input[placeholder='Name']");
	private static final By appDescriptionTextArea = By.cssSelector("textarea[placeholder='Description']");
	private static final By createButton = By.xpath("//button[text()='Create']");
	private static final By newButton = By.xpath("//span[text()='New']");
	private static final By publishButton = By.xpath("//button[text()='Publish']");
	private static final By publishDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By installDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By appSettingsTab = By.xpath("//a[@role='tab' and span[text()='App Settings']]");
	private static final By tagsTab = By.xpath("//a[@role='tab' and span[text()='Tags']]");
	private static final By formsTab = By.xpath("//a[@role='tab' and span[text()='Forms']]");
	private static final By emailTemplatesTab = By.xpath("//a[@role='tab' and span[text()='Email Templates']]");
	private static final By slaDefinitionsTab = By.xpath("//a[@role='tab' and span[text()='SLA Definition']]");
	private static final By deployConfigTab = By.xpath("//a[@role='tab' and span[text()='Deploy Config']]");
	private static final By workflowTab = By.xpath("//span[contains(text(),'Workflow')]");
	private static final By versionsTab = By.xpath("//span[contains(text(),'Versions')]");
	private static final By translationsTab = By.xpath("//span[contains(text(),'Translations')]");
	private static final By statusTab = By.xpath("//span[contains(text(),'Status')]");
	private static final By addNewTagButton = By.cssSelector("div[aria-hidden='false'] ient-button[icon='plus']>button");
	private static final By tagNameTextField = By.cssSelector("input[placeholder='Tag Name']");
	private static final By tagHintTextField = By.cssSelector("input[placeholder='Tag Hint Text']");
	private static final By rolesTextField = By.cssSelector("div[class='ui fluid multiple selection dropdown']");
	private static final By enabledCheckBox = By.cssSelector("input[formcontrolname='enabled']");
	private static final By itemsPerPageDropdown = By.cssSelector("div[class='selection ui dropdown ient-items-per-page-dropdown']");
	private static final By itemsPerPageDropdownOption = By.xpath("//div[@class='menu transition visible']/div[text()='20']");
	private static final By submittedToSupplierFormVerticalIcon = By.xpath("//tr[td[span[text()=' Submitted to Supplier ']]]/td/div/i");
	private static final By createCaseFormVerticalIcon = By.xpath("//tr[td[span[text()=' CreateCase ']]]/td/div/i");
	private static final By submitButton = By.xpath("//button[contains(text(),'Submit')]");
	private static final By closeButton = By.xpath("//i[contains(@class,'close-icon')]");
	private static String rolesDropdownItem = "//div[@class='menu transition visible']/div[text()='%s']";
	private static final By publishedText = By.xpath("//span[text()='Published']");
	private static String projectSettingsNameText = "//div[@class='header' and text()='%s']";
	private static String appNameText = "//div[@class='header' and text()=' %s ']";
	private static String projectSettingsDescriptionText = "//div[div[@class='header' and text()='%s']]/div[@class='description' and p[text()=' %s']]";
	private static String projectSettingsCardVerticalIcon = "//div[div[@class='header' and text()='%s'] and @class='content']/div/i[contains(@class,'ellipsis vertical icon')]";
	private static String tagsRolesText = "//tr[td/span[text()=' %s ']]/td[2]/span";
	private static String tagsRolesVerticalIcon = "//tr[td/span[text()=' %s ']]/td[4]/div/i[contains(@class,'ellipsis vertical icon')]";
	private static final By openLink = By.xpath("//div[@class='menu transition visible']/div[@data-value='open']");
	private static final By deleteLink = By.xpath("//div[@class='menu transition visible']/div[@data-value='delete']");
	private static final By SaveButton = By.xpath("//button[span[text()='Save']]");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By editLink = By.xpath("//div[contains(@class,'transition visible')]/div[text()=' Edit ']");
	private static final By viewLink = By.xpath("//div[@class='menu transition visible']/div[text()=' View ']");
	private static final By designTab = By.xpath("//a[span[text()='Design']]");
	private static final By tempPartCheckbox = By.xpath("//span[text()='Temp Part']");
	private static final By formFieldEditButton = By.xpath("//div[@ref='component' and div/input[@name='Create Case[tempPart]']]/parent::div/div/div[@ref='editComponent']");
	private static final By submittedToSupplierFormFieldEditButton = By.xpath("//div[@ref='component' and div/input[@name='Submitted to Supplier[tempPart]']]/parent::div/div/div[@ref='editComponent']");
	private static final By formFieldHiddenCheckbox = By.xpath("//div[contains(@class,'component-hidden')]/div[@class='ui checkbox']/input");
	private static final By formFieldSaveButton = By.cssSelector("button[ref='saveButton']");
	private static final By formFieldDesignSaveButton = By.cssSelector("ient-button[label='Save']");
	private static final By submittedToSupplierFormFieldDesignSaveButton = By.cssSelector("button[name='Submitted to Supplier[submit]']");
	private static final By formFieldDesignAPITab= By.xpath("//a[@ref='tabLink-tabs' and text()='API']");
	private static final By formFieldDesignAPIDataRemoveButton = By.cssSelector("button[ref='datagrid-properties-removeRow']");
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static final By addAnotherButton = By.xpath("//button[@ref='datagrid-properties-addRow']/i");
	private static final By keyTextField = By.cssSelector("input[id$='__key'][value='key']");
	private static final By valueTextField = By.cssSelector("input[id$='-key'][placeholder='Value']");
	private static final By tagsDropdownOptions = By.xpath("//div[@class='menu transition visible']/div");
	private static String availableAppInstallIcon = "//div[div[div[@class='header' and contains(text(),'%s')]]]/descendant::i[@class='download icon bigicon']";

	public ProjectSettingsPage(SeleniumDriver driver) {
		super(driver);
	}

	/**
	 * Creates app
	 * @param name : name in string format
	 * @param description : description in string format
	 */
	public void createApp(String name, String description) {
		log.info("Start of the method createApp");
		driver.safeClick(newButton);
		driver.safeType(nameTextBox, name);
		driver.safeType(appDescriptionTextArea, description);
		log.info("End of the method createApp");
	}

	/**
	 * Clicks on tags tab
	 */
	public void clickOnTagsTab() {
		log.info("Start of the method clickOnTagsTab");
		driver.safeClick(tagsTab);
		log.info("End of the method clickOnTagsTab");
	}

	/**
	 * Clicks on forms tab
	 */
	public void clickOnFormsTab() {
		log.info("Start of the method clickOnFormsTab");
		driver.safeClick(formsTab);
		log.info("End of the method clickOnFormsTab");
	}

	/**
	 * Creates tag
	 * @param tagName : tag name in string format
	 * @param rolesList  roles as list
	 */
	public void createTag(String tagName, List<String> rolesList) {
		log.info("Start of the method createTag");
		driver.safeClick(addNewTagButton);
		driver.safeType(tagNameTextField, tagName);
		driver.safeType(tagHintTextField, tagName);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.jSClick(rolesTextField);
		for (String role : rolesList) {
			driver.safeClick(By.xpath(String.format(rolesDropdownItem, role)));
			driver.sleep(WaitTime.XSMALL_WAIT);
		}
		driver.safeClick(tagNameTextField);
		driver.jSClick(enabledCheckBox);
		driver.jSClick(submitButton);
		log.info("End of the method createTag");
	}

	/**
	 * clicks on tags dropdown
	 */
	public void clickOnTagsDropdown() {
		log.info("Start of the method clickOnTagsDropdown");
		driver.safeClick(tagsTab);
		driver.safeClick(addNewTagButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method clickOnTagsDropdown");
	}

	/**
	 * returns tags list
	 */
	public List<String> getTagsList() {
		log.info("Start of the method getTagsList");
		driver.jSClick(rolesTextField);
		driver.sleep(WaitTime.SMALL_WAIT);
		List<String> tagsList = driver.getTextsFromAllElementOfALocator(tagsDropdownOptions);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(closeButton);
		log.info("End of the method getTagsList");
		return tagsList;
	}

	/**
	 * Creates and publishes tags
	 */
	public void createAndPublishTag() {
		log.info("Start of the method createAndPublishTag");
		driver.safeClick(createButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(publishButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(publishDialogYesButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method createAndPublishTag");
	}

	/**
	 * Publishes the application from app details
	 */
	public void publishAppInAppDetails() {
		log.info("Start of the method publishAppInAppDetails");
		driver.safeClick(publishButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(publishDialogYesButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method publishAppInAppDetails");
	}

	/**
	 * Verifies whether tags are published
	 */
	public boolean isTagsPublished() {
		log.info("Start of the method isTagsPublished");
		boolean isTagsPublished = driver.isPresentAndVisible(publishedText);
		log.info("End of the method isTagsPublished");
		return isTagsPublished;
	}


	/**
	 * Verifies whether app settings is displayed
	 * @param appName : app name in string format
	 */
	public boolean isAppSettingsDisplayed(String appName) {
		log.info("Start of the method isAppSettingsDisplayed");
		boolean isAppSettingsDisplayed = driver.isPresentAndVisible(By.xpath(String.format(projectSettingsNameText, appName)));
		log.info("End of the method isAppSettingsDisplayed");
		return isAppSettingsDisplayed;
	}

	/**
	 * Verifies whether app is displayed
	 * @param appName : app name in string format
	 */
	public boolean isAppDisplayed(String appName) {
		log.info("Start of the method isAppDisplayed");
		boolean isAppDisplayed = driver.isPresentAndVisible(By.xpath(String.format(appNameText, appName)));
		log.info("End of the method isAppDisplayed");
		return isAppDisplayed;
	}

	/**
	 * Verifies whether app settings description is displayed
	 * @param appName : app name in string format
	 * @param appDescription : app description in string format
	 */
	public boolean isAppSettingsAndDescriptionDisplayed(String appName, String appDescription) {
		log.info("Start of the method isAppSettingsAndDescriptionDisplayed");
		boolean isAppSettingsAndDescriptionDisplayed = driver.isPresentAndVisible(By.xpath(String.format(projectSettingsDescriptionText, appName, appDescription)));
		log.info("End of the method isAppSettingsAndDescriptionDisplayed");
		return isAppSettingsAndDescriptionDisplayed;
	}

	/**
	 * Deletes app
	 * @param appName : app name in string format
	 */
	public void deleteAppSetting(String appName) {
		log.info("Start of the method deleteApp");
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.jSClick(By.xpath(String.format(projectSettingsCardVerticalIcon, appName)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deleteApp");
	}

	/***
	 * Edits existing app settings
	 * @param appName : app Name in string format
	 * @param appDescription : app description in string format
	 */
	public void editAppSettingsDescription(String appName, String appDescription) {
		log.info("Start of the method editAppSettingsDescription");
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(projectSettingsCardVerticalIcon, appName)));
		driver.jSClick(openLink);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(appSettingsTab);
		driver.safeType(appDescriptionTextArea, appDescription);
		driver.safeClick(SaveButton);
		log.info("End of the method editAppSettingsDescription");
	}

	/***
	 * Edits existing project
	 * @param appName : app Name in string format
	 */
	public void editProject(String appName) {
		log.info("Start of the method editProject");
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(projectSettingsCardVerticalIcon, appName)));
		driver.jSClick(openLink);
		log.info("End of the method editProject");
	}

	/**
	 * Clicks on save button
	 */
	public void clickOnSaveButton() {
		log.info("Start of the method clickOnSaveButton");
		driver.safeClick(SaveButton);
		log.info("End of the method clickOnSaveButton");
	}

	/**
	 * Verifies tag roles
	 * @param tagName : tag name in string format
	 * @param rolesList : roles in list format
	 * @return returns roles match status
	 */
	public boolean isTagRolesDisplayed(String tagName, List<String> rolesList) {
		log.info("Start of the method isTagRolesDisplayed");
		driver.sleep(WaitTime.SMALL_WAIT);
		String tagRolesText = driver.safeGetText(By.xpath(String.format(tagsRolesText, tagName)));
		List<String> actualRolesList = List.of(tagRolesText.split(","));
		boolean isRolesMatched = false;
		for (String role : rolesList) {
			if (actualRolesList.contains(role))
				isRolesMatched = true;
			else {
				isRolesMatched = false;
				break;
			}
		}
		if (!isRolesMatched) {
			editTagRoles(tagName, rolesList);
			driver.sleep(WaitTime.LONG_WAIT);
			clickOnTagsTab();
			String editedTagRolesText = driver.safeGetText(By.xpath(String.format(tagsRolesText, tagName)));
			List<String> editedRolesList = List.of(tagRolesText.split(","));
			for (String role : rolesList) {
				if (editedRolesList.contains(role))
					isRolesMatched = true;
				else {
					isRolesMatched = false;
					break;
				}
			}
		}
		log.info("End of the method isTagRolesDisplayed");
		return isRolesMatched;
	}

	/**
	 * Edits tag roles
	 * @param tagName : tag name in string format
	 * @param rolesList  roles as list
	 */
	public void editTagRoles(String tagName, List<String> rolesList) {
		log.info("Start of the method editTagRoles");
		driver.safeClick(By.xpath(String.format(tagsRolesVerticalIcon, tagName)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(editLink);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.jSClick(rolesTextField);
		for (String role : rolesList) {
			if(!driver.isPresentAndVisible(By.xpath(String.format(rolesDropdownItem, role))))
			{
				role = role.replaceAll("_", " ");
				StringDataUtils stringDataUtils = new StringDataUtils();
				role = stringDataUtils.convertToCamel(role).trim();
				log.info("Role is : "+role);
			}
			driver.safeClick(By.xpath(String.format(rolesDropdownItem, role)));
			driver.sleep(WaitTime.XSMALL_WAIT);
		}
		driver.safeClick(tagNameTextField);
		driver.jSClick(submitButton);
		log.info("End of the method editTagRoles");
	}

	/**
	 * Verifies tag editable status
	 * @param tagName : tag name in string format
	 * @return returns editable status
	 */
	public boolean isTagEditEnabled(String tagName) {
		log.info("Start of the method isTagEditEnabled");
		boolean isTagEditable = driver.isPresentAndVisible(By.xpath(String.format(tagsRolesVerticalIcon, tagName)));
		log.info("End of the method isTagEditEnabled");
		return isTagEditable;
	}

	/**
	 * Selects the maximum items for page
	 */
	public void selectMaxItemsPerPage() {
		log.info("Start of the method selectMaxItemsPerPage");
		driver.safeClick(itemsPerPageDropdown);
		driver.safeClick(itemsPerPageDropdownOption);
		log.info("End of the method selectMaxItemsPerPage");
	}

	/***
	 * Searches status name
	 * @param statusName : status name in string format
	 */
	public void searchStatusName(String statusName) {
		log.info("Start of the method searchStatusName");
		driver.safeClear(searchTextBox);
		driver.safeType(searchTextBox, statusName);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(searchIconButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchStatusName");
	}

	/**
	 * Edits create case form
	 * @param hiddenStatus  field enable status as boolean format
	 */
	public void editCreateCaseForm(boolean hiddenStatus) {
		log.info("Start of the method editCreateCaseForm");
		driver.safeClick(createCaseFormVerticalIcon);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(editLink);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(designTab);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeMouseOver(tempPartCheckbox);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(formFieldEditButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		String attributeValue = driver.safeGetAttribute(formFieldHiddenCheckbox, "checked");
		if (hiddenStatus) {
			if (attributeValue == null) {
				driver.jSClick(formFieldHiddenCheckbox);
			}
		} else {
			if (attributeValue.equals("true")) {
				driver.jSClick(formFieldHiddenCheckbox);
			}
		}
		driver.safeClick(formFieldSaveButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(formFieldDesignSaveButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(SaveButton);
		log.info("End of the method editCreateCaseForm");
	}

	/**
	 * Edits submitted to supplier form
	 * @param key  key name
	 * @param value  value name
	 */
	public void editSubmittedToSupplierFormAPIData(String key, String value) {
		log.info("Start of the method editSubmittedToSupplierFormAPIData");
		driver.safeClick(submittedToSupplierFormVerticalIcon);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(editLink);
		driver.safeClick(designTab);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeMouseOver(tempPartCheckbox);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(submittedToSupplierFormFieldEditButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(formFieldDesignAPITab);
		driver.safeClick(addAnotherButton);
		driver.safeType(keyTextField, key);
		driver.safeType(valueTextField, value);
		driver.safeClick(formFieldSaveButton);
		driver.sleep(WaitTime.LONG_WAIT);
		//driver.safeClick(submittedToSupplierFormFieldDesignSaveButton);
		driver.safeClick(SaveButton);
		driver.sleep(WaitTime.LONG_WAIT);
		driver.safeMouseOver(formsTab);
		log.info("End of the method editSubmittedToSupplierFormAPIData");
	}

	/**
	 * Removes submitted to supplier form
	 */
	public void removeSubmittedToSupplierFormAPIData() {
		log.info("Start of the method removeSubmittedToSupplierFormAPIData");
		driver.safeClick(submittedToSupplierFormVerticalIcon);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(editLink);
		driver.safeClick(designTab);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeMouseOver(tempPartCheckbox);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(submittedToSupplierFormFieldEditButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(formFieldDesignAPITab);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		while(driver.isPresentAndVisible(formFieldDesignAPIDataRemoveButton))
		{
			driver.jSClick(formFieldDesignAPIDataRemoveButton);
			driver.sleep(WaitTime.SMALL_WAIT);
		}
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(formFieldSaveButton);
		driver.sleep(WaitTime.LONG_WAIT);
		//driver.safeClick(submittedToSupplierFormFieldDesignSaveButton);
		driver.safeClick(SaveButton);
		log.info("End of the method removeSubmittedToSupplierFormAPIData");
	}

	/**
	 * Verifies whether tags tab is displayed
	 */
	public boolean isTagsTabDisplayed() {
		log.info("Start of the method isTagsTabDisplayed");
		boolean isTagsTabDisplayed = driver.isPresentAndVisible(tagsTab);
		log.info("End of the method isTagsTabDisplayed");
		return isTagsTabDisplayed;
	}

	/**
	 * Verifies whether forms tab is displayed
	 */
	public boolean isFormsTabDisplayed() {
		log.info("Start of the method isFormsTabDisplayed");
		boolean isFormsTabDisplayed = driver.isPresentAndVisible(formsTab);
		log.info("End of the method isFormsTabDisplayed");
		return isFormsTabDisplayed;
	}

	/**
	 * Verifies whether email templates tab is displayed
	 */
	public boolean isEmailTemplatesTabDisplayed() {
		log.info("Start of the method isEmailTemplatesTabDisplayed");
		boolean isEmailTemplatesTabDisplayed = driver.isPresentAndVisible(emailTemplatesTab);
		log.info("End of the method isEmailTemplatesTabDisplayed");
		return isEmailTemplatesTabDisplayed;
	}

	/**
	 * Verifies whether sla definitions tab is displayed
	 */
	public boolean isSlaDefinitionsTabDisplayed() {
		log.info("Start of the method isSlaDefinitionsTabDisplayed");
		boolean isSlaDefinitionsTabDisplayed = driver.isPresentAndVisible(slaDefinitionsTab);
		log.info("End of the method isSlaDefinitionsTabDisplayed");
		return isSlaDefinitionsTabDisplayed;
	}

	/**
	 * Verifies whether workflow tab is displayed
	 */
	public boolean isWorkflowTabDisplayed() {
		log.info("Start of the method isWorkflowTabDisplayed");
		boolean isWorkflowTabDisplayed = driver.isPresentAndVisible(workflowTab);
		log.info("End of the method isWorkflowTabDisplayed");
		return isWorkflowTabDisplayed;
	}

	/**
	 * Verifies whether versions tab is displayed
	 */
	public boolean isVersionsTabDisplayed() {
		log.info("Start of the method isVersionsTabDisplayed");
		boolean isVersionsTabDisplayed = driver.isPresentAndVisible(versionsTab);
		log.info("End of the method isVersionsTabDisplayed");
		return isVersionsTabDisplayed;
	}

	/**
	 * Verifies whether translations tab is displayed
	 */
	public boolean isTranslationsTabDisplayed() {
		log.info("Start of the method isTranslationsTabDisplayed");
		boolean isTranslationsTabDisplayed = driver.isPresentAndVisible(translationsTab);
		log.info("End of the method isTranslationsTabDisplayed");
		return isTranslationsTabDisplayed;
	}

	/**
	 * Verifies whether status tab is displayed
	 */
	public boolean isStatusTabDisplayed() {
		log.info("Start of the method isStatusTabDisplayed");
		boolean isStatusTabDisplayed = driver.isPresentAndVisible(statusTab);
		log.info("End of the method isStatusTabDisplayed");
		return isStatusTabDisplayed;
	}

	/**
	 * Verifies whether deploy config tab is displayed
	 */
	public boolean isDeployConfigTabDisplayed() {
		log.info("Start of the method isDeployConfigTabDisplayed");
		boolean isDeployConfigTabDisplayed = driver.isPresentAndVisible(deployConfigTab);
		log.info("End of the method isDeployConfigTabDisplayed");
		return isDeployConfigTabDisplayed;
	}

	/**
	 * Installs available app
	 * @param appName application name
	 */
	public void installAvailableApp(String appName) {
		log.info("Start of the method installAvailableApp");
		driver.safeClick(By.xpath(String.format(availableAppInstallIcon, appName)));
		driver.safeClick(installDialogYesButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		log.info("End of the method installAvailableApp");
	}
}