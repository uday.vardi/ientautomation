package io.ient.test.pages;


import io.ient.test.framework.selenium.SeleniumDriver;


public class BasePage {

	protected SeleniumDriver driver;

	public BasePage(SeleniumDriver driver) {
		this.driver = driver;
	}
}
