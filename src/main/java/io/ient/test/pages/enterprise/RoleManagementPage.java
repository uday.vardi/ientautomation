package io.ient.test.pages.enterprise;


import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import java.util.List;


public class RoleManagementPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(RoleManagementPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By roleManagementTab = By.cssSelector("div[id='role-card']");
	private static final By addButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static final By nameTextBox = By.cssSelector("input[id='role-name']");
	private static final By codeTextBox = By.cssSelector("input[id='role-code']");
	private static final By statusDropdown = By.xpath("//div[select[@id='role-status']]");
	private static String statusDropdownOption = "//div[@role='option' and span[text()='%s']]";
	private static String roleRow = "//tr[contains(@id,'ient-table-row-no-') and td[span[contains(text(),'%s')]] and td[span[contains(text(),'%s')]]]";
	private static String rolesText = "//td[contains(@class,'ient-table-body')][4]";
	private static final By submitButton = By.cssSelector("button[id='submit-btn']");
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By editLink = By.cssSelector("div[id='edit']");
	private static final By deleteLink = By.cssSelector("div[id='delete']");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By noRecordsAvailableTextMessage = By.xpath("//div[@class='ui red message ng-star-inserted' and text()=' No records available ']");
	private static final By roleIdText = By.xpath("//tr[1]/td[2]/span");
	private static final By itemsPerPageDropdown = By.cssSelector("div[id='items-per-page']");
	private static final By maxItemsPerPage = By.cssSelector("div[id='page-item-20']");


	public RoleManagementPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - RoleManagementPage");
	}

	/***
	 * Clicks on role management tab
	 */
	public void clickOnRoleManagementTab() {
		log.info("Start of the method clickOnRoleManagementTab");
		driver.safeClick(roleManagementTab);
		log.info("End of the method clickOnRoleManagementTab");
	}

	/***
	 * Creates a new role
	 * @param name : name of role in string format
	 * @param code : code of a role in string format
	 * @param status : status in string format
	 */
	public void createRole(String name, String code, String status) {
		log.info("Start of the method createRole");
		driver.safeClick(addButton);
		driver.safeType(nameTextBox, name);
		driver.safeType(codeTextBox, code);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createRole");
	}

	/**
	 * Verifies whether role is displayed
	 * @param name : name of role in string format
	 * @param code : code of a role in string format
	 * @return returns creation status
	 */
	public boolean isRoleDisplayed(String name, String code) {
		log.info("Start of the method isRoleDisplayed");
		boolean isRoleDisplayed = driver.isPresentAndVisible(By.xpath(String.format(roleRow, name, code)));
		log.info("End of the method isRoleDisplayed");
		return isRoleDisplayed;
	}

	/***
	 * Searches role by name
	 * @param name : name of role in string format
	 */
	public void searchRoleByName(String name) {
		log.info("Start of the method searchRoleByName");
		driver.safeType(searchTextBox, name);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(searchIconButton);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchRoleByName");
	}

	public void clearSearchTextField() {
		log.info("Start of the method clearSearchTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearSearchTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	/***
	 * Edits existing role
	 * @param name : name of role in string format
	 */
	public void editRole(String name) {
		log.info("Start of the method editRole");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.safeType(nameTextBox, name);
		driver.safeClick(submitButton);
		log.info("End of the method editRole");
	}

	/***
	 * Edits existing role status
	 * @param name : name of role in string format
	 * @param status : status of role in string format
	 */
	public void editRoleStatus(String name, String status) {
		log.info("Start of the method editRole");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method editRole");
	}

	/***
	 * Deletes role
	 */
	public void deleteRole() {
		log.info("Start of the method deleteRole");
		driver.safeClick(verticalIconButton);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deleteRole");
	}

	/**
	 * Verifies No Records Available Message
	 * @return returns message display status
	 */
	public boolean verifyNoRecordsMessage() {
		log.info("Start of the method verifyNoRecordsMessage");
		boolean isMessageDisplayed = driver.isPresentAndVisible(noRecordsAvailableTextMessage);
		log.info("End of the method verifyNoRecordsMessage");
		return isMessageDisplayed;
	}

	/**
	 * Verifies roles list
	 * @param expectedRolesList : expected roles in list format
	 * @return returns roles list display status
	 */
	public boolean verifyRolesList(List<String> expectedRolesList) {
		log.info("Start of the method verifyRolesList");
		boolean isRolesDisplayed = false;
		List<String> actualRolesList = driver.getLocatorTexts(By.xpath(rolesText));
		log.info("actualRolesList : " + actualRolesList);
		log.info("expectedRolesList : " + expectedRolesList);
		for (String expected_role : expectedRolesList) {
			log.info("expected_role : " + expected_role);
			boolean status = actualRolesList.contains(expected_role);
			log.info("STATUS : " + status);
			if (status) {
				isRolesDisplayed = true;
			} else {
				isRolesDisplayed = false;
				break;
			}
		}
		log.info("End of the method verifyRolesList");
		return isRolesDisplayed;
	}

	/**
	 * Returns role id
	 * @return returns role id
	 */
	public String getRoleId() {
		log.info("Start of the method getRoleId");
		String roleId = driver.safeGetText(roleIdText);
		log.info("End of the method getRoleId");
		return roleId;
	}

	/***
	 * Select max items per page
	 */
	public void selectMaxItemsPerPage() {
		log.info("Start of the method selectMaxItemsPerPage");
		driver.safeClick(itemsPerPageDropdown);
		driver.safeClick(maxItemsPerPage);
		log.info("End of the method selectMaxItemsPerPage");
	}

}