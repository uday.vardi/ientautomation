package io.ient.test.pages.enterprise;


import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;


public class BulletinSubscriptionsPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(BulletinSubscriptionsPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By categoryManagementTab = By.cssSelector("div[id='category-manage-card']");
	private static final By divisionManagementTab = By.cssSelector("div[id='div-manage-card']");
	private static final By regionManagementTab = By.cssSelector("div[id='region-manage-card']");
	private static final By addButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static final By nameTextBox = By.cssSelector("input[name='data[name]']");
	private static final By statusDropdown = By.xpath("//div[select[@id='status']]");
	private static String statusDropdownOption = "//div[@role='option' and span[text()='%s']]";
	private static String bulletinSubscriptionCheckbox = "//label[contains(text(),'%s')]";
	private static String tableRow = "//tr[contains(@id,'ient-table-row-no-') and td[span[contains(text(),'%s')]]]";
	private static final By submitButton = By.cssSelector("button[name='data[submit]']");
	private static final By saveButtonInCaseForm = By.cssSelector("button[id='bottom-save-button']");
	private static final By saveButtonInBulletinSubscriptions = By.xpath("//button[text()='Save']");
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By viewButton = By.cssSelector("div[id='view']");
	private static final By editLink = By.cssSelector("div[id='edit']");
	private static final By editButtonInCaseForm = By.cssSelector("button[id='ient-btn-edit']");
	private static final By deleteLink = By.cssSelector("div[id='delete']");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By noRecordsAvailableTextMessage = By.xpath("//div[@class='ui red message ng-star-inserted' and text()=' No records available ']");
	private static final By titleTextBox = By.cssSelector("input[name='data[title]']");
	private static final By descriptionTextArea = By.xpath("//div[contains(@class,'ql-editor')]");
	private static final By reworkReasonTextArea = By.cssSelector("textarea[id='reworkReason']");
	private static final By rejectReasonTextArea = By.cssSelector("textarea[id='rejectReason']");
	private static final By expirationDate = By.cssSelector("i[ref='icon']");
	private static String expirationDatePicker = "//div[contains(@class,'open')]/descendant::span[@class='flatpickr-day ' and text()='%s']";
	private static final By categoryDropdown = By.xpath("//div[select[@id='category']]");
	private static final By divisionDropdown = By.xpath("//div[select[@id='division']]");
	private static final By regionDropdown = By.xpath("//div[select[@id='region']]");
	private static String categoryDropdownOptions = "div[id*='category-item-choice']>span";
	private static String bulletinCaseFormDropdownOption = "//div[@role='option' and span[text()='%s']]/span";
	private static String divisionDropdownOptions = "div[id*='division-item-choice']>span";
	private static String regionDropdownOptions = "div[id*='region-item-choice']>span";
	private static final By nextStateDropdown = By.xpath("//div[select[@id='caseStatus']]");
	private static final By publishToDropdown = By.xpath("//div[select[@id='publishTo']]");
	private static final By approverDropdown = By.xpath("//div[select[@id='approver']]");
	private static String recordList = "//label[text()='%s']";
	private static final By cancelButton = By.cssSelector("button[id='cancel-button']");
	private static final By confirmationMessageTextArea = By.cssSelector("textarea[autoresize='autoResize']");
	private static final By confirmationSubmitButton = By.cssSelector("ient-button[label='Submit']");
	private static final By bulletinCaseId = By.xpath("//*[@id='ient-table-row-no-0']/td[2]/a");
	private static final By statusColumn = By.cssSelector("button[id='ient-table-filter-btn']");
	private static final By attachmentsTab = By.xpath("//a[span[text()='Attachments']]");
	private static final By addAttachmentButton = By.cssSelector("button[id^='ient-buttton-']");
	private static final By verticalIconInAttachments = By.cssSelector("i[class*='vertical icon']");
	private static final By downloadLink = By.xpath("//a[text()='Download']");
	private static String fileNameTitle = "//div[contains(text(), '%s')]";

	public BulletinSubscriptionsPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - BulletinSubscriptionsPage");
	}

	/***
	 * Clicks on category management tab
	 */
	public void clickOnCategoryManagementTab() {
		log.info("Start of the method clickOnRoleManagementTab");
		driver.safeClick(categoryManagementTab);
		log.info("End of the method clickOnRoleManagementTab");
	}

	/***
	 * Clicks on division management tab
	 */
	public void clickOnDivisionManagementTab() {
		log.info("Start of the method clickOnDivisionManagementTab");
		driver.safeClick(divisionManagementTab);
		log.info("End of the method clickOnDivisionManagementTab");
	}

	/***
	 * Clicks on region management tab
	 */
	public void clickOnRegionManagementTab() {
		log.info("Start of the method clickOnRegionManagementTab");
		driver.safeClick(regionManagementTab);
		log.info("End of the method clickOnRegionManagementTab");
	}

	/***
	 * Creates a new record
	 * @param name : name of record in string format
	 * @param status : status in string format
	 */
	public void createRecord(String name, String status) {
		log.info("Start of the method createRecord");
		driver.safeClick(addButton);
		driver.safeType(nameTextBox, name);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createRecord");
	}

	/***
	 * Creates a new case
	 * @param title : title of case in string format
	 * @param description : description of case in string format
	 * @param expireDate : expiring date of case in string format
	 * @param category : category of case in string format
	 * @param division : division of case in string format
	 * @param region : region of case in string format
	 * @param nextState : next state of case in string format
	 * @param publishTo : publishTo in string format
	 * @param approver : approver in string format
	 */
	public void createBulletinCase(String title, String description, String expireDate, String category, String division, String region, String nextState, String publishTo, String approver) {
		log.info("Start of the method createBulletinCase");
		driver.sleep(WaitTime.LONG_WAIT);
		driver.safeClick(addButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeType(titleTextBox, title);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(descriptionTextArea);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeType(descriptionTextArea, description);
		driver.jSClick(expirationDate);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(expirationDatePicker, expireDate)));
		driver.jSClick(categoryDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(bulletinCaseFormDropdownOption, category)));
		driver.jSClick(divisionDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(bulletinCaseFormDropdownOption, division)));
		driver.jSClick(regionDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(bulletinCaseFormDropdownOption, region)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(descriptionTextArea);
		driver.safeClick(nextStateDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(bulletinCaseFormDropdownOption, nextState)));
		driver.jSClick(publishToDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(bulletinCaseFormDropdownOption, publishTo)));
		driver.safeClick(descriptionTextArea);
		driver.jSClick(approverDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(bulletinCaseFormDropdownOption, approver)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(saveButtonInCaseForm);
		log.info("End of the method createBulletinCase");
	}

	/***
	 * view bulletin case
	 */
	public void viewBulletinCase() {
		log.info("Start of the method viewBulletinCase");
		driver.safeClick(verticalIconButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(viewButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		log.info("End of the method viewBulletinCase");
	}

	/***
	 * edit a new case
	 * @param approveStatus : next state of case in string format
	 */
	public void editBulletinCase(String approveStatus) {
		log.info("Start of the method editBulletinCase");
		driver.safeClick(verticalIconButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(viewButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(editButtonInCaseForm);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.jSClick(nextStateDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(bulletinCaseFormDropdownOption, approveStatus)));
		driver.safeClick(saveButtonInCaseForm);
		log.info("End of the method editBulletinCase");
	}

	/***
	 * edit rework new case
	 * @param approveStatus : next state in string format
	 * @param reworkReason : rework reason in string format
	 */
	public void editReworkBulletinCase(String approveStatus, String reworkReason ) {
		log.info("Start of the method editReworkBulletinCase");
		driver.safeClick(verticalIconButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(viewButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(editButtonInCaseForm);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.jSClick(nextStateDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(bulletinCaseFormDropdownOption, approveStatus)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeType(reworkReasonTextArea, reworkReason);
		driver.safeClick(saveButtonInCaseForm);
		log.info("End of the method editReworkBulletinCase");
	}

	/***
	 * edit reject new case
	 * @param approveStatus : next state in string format
	 * @param rejectReason : rework reason in string format
	 */
	public void editRejectBulletinCase(String approveStatus, String rejectReason ) {
		log.info("Start of the method editRejectBulletinCase");
		driver.safeClick(verticalIconButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(viewButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(editButtonInCaseForm);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.jSClick(nextStateDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(bulletinCaseFormDropdownOption, approveStatus)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeType(rejectReasonTextArea, rejectReason);
		driver.safeClick(saveButtonInCaseForm);
		log.info("End of the method editRejectBulletinCase");
	}

    public void selectBulletinSubscriptions(String bulletinName) {
        log.info("Start of the method selectBulletinSubscriptions");
        driver.safeCheck(By.xpath(String.format(bulletinSubscriptionCheckbox, bulletinName)));
        log.info("End of the method selectBulletinSubscriptions");
    }

	public void unCheckBulletinSubscriptions() {
		log.info("Start of the method unCheckBulletinSubscriptions");
		List<WebElement> checkboxWebElementsList = driver.findElements(By.xpath("//label[starts-with(@id,'chk')]/preceding-sibling::input"));
		List<WebElement> checkboxWebElementsLabelList = driver.findElements(By.xpath("//label[starts-with(@id,'chk')]"));
		for(WebElement element : checkboxWebElementsList)
		{
			if(element.isSelected())
				checkboxWebElementsLabelList.get(checkboxWebElementsList.indexOf(element)).click();
		}
		log.info("End of the method unCheckBulletinSubscriptions");
	}

	public void clickOnSaveButton() {
		log.info("Start of the method clickOnSaveButton");
		driver.safeClick(saveButtonInBulletinSubscriptions);
		log.info("End of the method clickOnSaveButton");
	}

	/**
	 * Returns bulletin case id
	 * @return returns bulletinCaseId
	 */
	public String getBulletinCaseId() {
		log.info("Start of the method getBulletinCaseId");
		String bulletinCaseID = driver.safeGetText(bulletinCaseId);
		log.info("End of the method getBulletinCaseId");
		return bulletinCaseID;
	}

	/**
	 * Verifies whether record is displayed
	 * @param name : name of record in string format
	 * @return returns creation status
	 */
	public boolean isRecordDisplayed(String name) {
		log.info("Start of the method isRecordDisplayed");
		boolean isRecordDisplayed = driver.isPresentAndVisible(By.xpath(String.format(tableRow, name)));
		log.info("End of the method isRecordDisplayed");
		return isRecordDisplayed;
	}

	/**
	 * Verifies whether record is displayed
	 * @param name : name of record in string format
	 * @return returns creation status
	 */
	public boolean isRecordDisplayedInBulletins(String name) {
		log.info("Start of the method isRecordDisplayedInBulletins");
		boolean isRecordDisplayedInBulletins = driver.isPresentAndVisible(By.xpath(String.format(recordList, name)));
		log.info("End of the method isRecordDisplayedInBulletins");
		return isRecordDisplayedInBulletins;
	}

	/***
	 * Searches record by name
	 * @param name : name of record in string format
	 */
	public void searchRecordByName(String name) {
		log.info("Start of the method searchRecordByName");
		driver.safeType(searchTextBox, name);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(searchIconButton);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchRecordByName");
	}

	public void clearSearchTextField() {
		log.info("Start of the method clearSearchTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearSearchTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	/***
	 * Edits existing record status
	 * @param name : name of record in string format
	 * @param status : status of record in string format
	 */
	public void editRecordStatus(String name, String status) {
		log.info("Start of the method editRecordStatus");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.safeType(nameTextBox, name);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method editRecordStatus");
	}

	/***
	 * Deletes record
	 */
	public void deleteRecord() {
		log.info("Start of the method deleteRecord");
		driver.safeClick(verticalIconButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deleteRecord");
	}

	/**
	 * Verifies whether status column is displayed
	 */
	public boolean isStatusColumnsDisplayed() {
		log.info("Start of the method isStatusColumnsDisplayed");
		boolean isStatusColumnsDisplayed = driver.isPresentAndVisible(statusColumn);
		log.info("End of the method isStatusColumnsDisplayed");
		return isStatusColumnsDisplayed;
	}


	/**
	 * Verifies No Records Available Message
	 * @return returns message display status
	 */
	public boolean verifyNoRecordsMessage() {
		log.info("Start of the method verifyNoRecordsMessage");
		boolean isMessageDisplayed = driver.isPresentAndVisible(noRecordsAvailableTextMessage);
		log.info("End of the method verifyNoRecordsMessage");
		return isMessageDisplayed;
	}

	/**
	 * Verifies category drop down value
	 * @param name : category name in string format
	 */
	public boolean isCategoryDisplayedInCaseForm(String name) {
		log.info("Start of the method isCategoryDisplayedInCaseForm");
		boolean isCategoryDisplayedInCaseForm = false;
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.jSClick(categoryDropdown);
		List<String> categoryDropdownOptionsText = driver.getTextsFromAllElementOfALocator(By.cssSelector(String.format(categoryDropdownOptions)));
		isCategoryDisplayedInCaseForm = categoryDropdownOptionsText.contains(name);
		log.info("End of the method isCategoryDisplayedInCaseForm");
		return isCategoryDisplayedInCaseForm;
	}

	/**
	 * Verifies division drop down value
	 * @param name : division name in string format
	 */
	public boolean isDivisionDisplayedInCaseForm(String name) {
		log.info("Start of the method isDivisionDisplayedInCaseForm");
		boolean isDivisionDisplayedInCaseForm = false;
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.jSClick(divisionDropdown);
		List<String> divisionDropdownOptionsText = driver.getTextsFromAllElementOfALocator(By.cssSelector(String.format(divisionDropdownOptions)));
		isDivisionDisplayedInCaseForm = divisionDropdownOptionsText.contains(name);
		log.info("End of the method isDivisionDisplayedInCaseForm");
		return isDivisionDisplayedInCaseForm;
	}

	/**
	 * Verifies region drop down value
	 * @param name : region name in string format
	 */
	public boolean isRegionDisplayedInCaseForm(String name) {
		log.info("Start of the method isRegionDisplayedInCaseForm");
		boolean isRegionDisplayedInCaseForm = false;
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.jSClick(regionDropdown);
		List<String> regionDropdownOptionsText = driver.getTextsFromAllElementOfALocator(By.cssSelector(String.format(regionDropdownOptions)));
		isRegionDisplayedInCaseForm = regionDropdownOptionsText.contains(name);
		log.info("End of the method isRegionDisplayedInCaseForm");
		return isRegionDisplayedInCaseForm;
	}

	/***
	 * Used to navigate from case form to list view
	 * @param message : cancellation message in string format
	 */
	public void cancelCaseForm(String message) {
		log.info("Start of the method cancelCaseForm");
		driver.safeClick(cancelButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeType(confirmationMessageTextArea, message);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(confirmationSubmitButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		log.info("End of the method cancelCaseForm");
	}

	/**
	 * Verifies whether vertical icon, edit and delete is displayed
	 */
	public boolean isVerticalIconDisplayed() {
		log.info("Start of the method isVerticalIconDisplayed");
		boolean isVerticalIconDisplayed = driver.isPresentAndVisible(verticalIconInAttachments);
		log.info("End of the method isVerticalIconDisplayed");
		return isVerticalIconDisplayed;
	}

	/**
	 * Verifies whether download link is displayed
	 */
	public boolean isDownloadLinkDisplayed() {
		log.info("Start of the method isDownloadLinkDisplayed");
		boolean isDownloadLinkDisplayed = driver.isPresentAndVisible(downloadLink);
		log.info("End of the method isDownloadLinkDisplayed");
		return isDownloadLinkDisplayed;
	}

	/**
	 * Verifies whether the attachment is displayed
	 * @param fileName : file name in string format
	 * @return file displayed status
	 */
	public boolean isAttachmentDisplayedInBulletinCase(String fileName) {
		log.info("Start of the method isAttachmentDisplayedInBulletinCase");
		boolean isAttachmentDisplayedInBulletinCase = driver.isPresentAndVisible(By.xpath(String.format(fileNameTitle, fileName)));
		log.info("End of the method isAttachmentDisplayedInBulletinCase");
		return isAttachmentDisplayedInBulletinCase;
	}

}