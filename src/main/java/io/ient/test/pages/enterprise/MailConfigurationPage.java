package io.ient.test.pages.enterprise;


import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class MailConfigurationPage extends BasePage {

    private static final Logger logger = LogManager.getLogger(MailConfigurationPage.class);
    LogFactory log = new LogFactory(logger);

    private static final By mailConfigurationTab = By.cssSelector("div[id='mail-config-card']");
    private static final By editButton = By.cssSelector("button[id^='ient-buttton-']");
    private static final By hostTextBox = By.cssSelector("input[id='host']");
    private static final By portTextBox = By.cssSelector("input[id='port']");
    private static final By usernameTextBox = By.cssSelector("input[id='username']");
    private static final By passwordTextBox = By.cssSelector("input[id='password']");
    private static final By nameTextBox = By.cssSelector("input[id='fromName']");
    private static final By emailTextBox = By.cssSelector("input[id='fromEmail']");

    private static final By starttls = By.xpath("//span[text()='StartTLS']");
    private static final By auth = By.xpath("//span[text()='Auth']");
    private static final By ssl = By.xpath("//span[text()='SSL']");
    private static final By debug = By.xpath("//span[text()='DEBUG']");
//    private static final By discardButton = By.cssSelector("ient-button[label='Discard']");
    private static final By submitButton = By.xpath("//button[contains(text(),'Submit')]");

    public MailConfigurationPage(SeleniumDriver driver) {
        super(driver);
        log.info("In class constructor - MailConfigurationPage");
    }

    /***
     * Clicks on mail configuration tab
     */
    public void clickOnMailConfigurationTab() {
        log.info("Start of the method clickOnMailConfigurationTab");
        driver.safeClick(mailConfigurationTab);
        log.info("End of the method clickOnMailConfigurationTab");
    }

    /***
     * Edits a mail configuration
     * @param host : host in string format
     * @param port : port in string format
     * @param username : username in string format
     * @param password : password in string format
     * @param name : name in string format
     */
    public void editMailConfiguration(String host, String port, String username, String password, String name) {
        log.info("Start of the method editMailConfiguration");
        driver.safeClick(editButton);
        driver.safeType(hostTextBox, host);
        driver.safeType(portTextBox, port);
        driver.safeType(usernameTextBox, username);
        driver.safeType(passwordTextBox, password);
        driver.safeType(nameTextBox, name);
//        driver.safeCheck(starttls);
//        driver.safeCheck(auth);
//        driver.safeCheck(ssl);
        driver.sleep(WaitTime.XSMALL_WAIT);
        driver.safeClick(submitButton);
        log.info("End of the method editMailConfiguration");
    }

    /**
     * clicks edit button
     */
    public void clickEditButton() {
        driver.safeClick(editButton);
        log.info("Edit button clicked");
    }

    /**
     * Verifies whether Host TextBox is displayed
     */
    public boolean isHostTextBoxDisplayed() {
        log.info("Start of the method isHostTextBoxDisplayed");
        boolean isHostTextBoxDisplayed = driver.isPresentAndVisible(hostTextBox);
        log.info("End of the method isHostTextBoxDisplayed");
        return isHostTextBoxDisplayed;
    }

    /**
     * Verifies whether Port TextBox is displayed
     */
    public boolean isPortTextBoxDisplayed() {
        log.info("Start of the method isPortTextBoxDisplayed");
        boolean isPortTextBoxDisplayed = driver.isPresentAndVisible(portTextBox);
        log.info("End of the method isPortTextBoxDisplayed");
        return isPortTextBoxDisplayed;
    }

    /**
     * Verifies whether Username TextBox is displayed
     */
    public boolean isUsernameTextBoxDisplayed() {
        log.info("Start of the method isUsernameTextBoxDisplayed");
        boolean isUsernameTextBoxDisplayed = driver.isPresentAndVisible(usernameTextBox);
        log.info("End of the method isUsernameTextBoxDisplayed");
        return isUsernameTextBoxDisplayed;
    }

    /**
     * Verifies whether Password TextBox is displayed
     */
    public boolean isPasswordTextBoxDisplayed() {
        log.info("Start of the method isPasswordTextBoxDisplayed");
        boolean isPasswordTextBoxDisplayed = driver.isPresentAndVisible(passwordTextBox);
        log.info("End of the method isPasswordTextBoxDisplayed");
        return isPasswordTextBoxDisplayed;
    }

    /**
     * Verifies whether Name TextBox is displayed
     */
    public boolean isNameTextBoxDisplayed() {
        log.info("Start of the method isNameTextBoxDisplayed");
        boolean isNameTextBoxDisplayed = driver.isPresentAndVisible(nameTextBox);
        log.info("End of the method isNameTextBoxDisplayed");
        return isNameTextBoxDisplayed;
    }

    /**
     * Verifies whether Email TextBox is displayed
     */
    public boolean isEmailTextBoxDisplayed() {
        log.info("Start of the method isEmailTextBoxDisplayed");
        boolean isEmailTextBoxDisplayed = driver.isPresentAndVisible(emailTextBox);
        log.info("End of the method isEmailTextBoxDisplayed");
        return isEmailTextBoxDisplayed;
    }

    /**
     * Verifies whether Starttls TextBox is displayed
     */
    public boolean isStarttlsCheckBoxDisplayed() {
        log.info("Start of the method isStarttlsCheckBoxDisplayed");
        boolean isStarttlsCheckBoxDisplayed = driver.isPresentAndVisible(starttls);
        log.info("End of the method isEmailTextBoxDisplayed");
        return isStarttlsCheckBoxDisplayed;
    }

    /**
     * Verifies whether Auth TextBox is displayed
     */
    public boolean isAuthCheckBoxDisplayed() {
        log.info("Start of the method isAuthCheckBoxDisplayed");
        boolean isAuthCheckBoxDisplayed = driver.isPresentAndVisible(auth);
        log.info("End of the method isEmailTextBoxDisplayed");
        return isAuthCheckBoxDisplayed;
    }

    /**
     * Verifies whether SSL TextBox is displayed
     */
    public boolean isSslCheckBoxDisplayed() {
        log.info("Start of the method isSslCheckBoxDisplayed");
        boolean isSslCheckBoxDisplayed = driver.isPresentAndVisible(ssl);
        log.info("End of the method isEmailTextBoxDisplayed");
        return isSslCheckBoxDisplayed;
    }

    /**
     * Verifies whether Debug TextBox is displayed
     */
    public boolean isDebugCheckBoxDisplayed() {
        log.info("Start of the method isEmailTextBoxDisplayed");
        boolean isDebugCheckBoxDisplayed = driver.isPresentAndVisible(debug);
        log.info("End of the method isEmailTextBoxDisplayed");
        return isDebugCheckBoxDisplayed;
    }
}