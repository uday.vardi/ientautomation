package io.ient.test.pages.enterprise;


import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class SequenceManagementPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(SequenceManagementPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By sequenceManagementTab = By.cssSelector("div[id='seq-manage-card']");
	private static final By noRecordsAvailableText = By.xpath("//div[@class='total-count']/div");

	public SequenceManagementPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - SequenceManagementPage");
	}

	/***
	 * Clicks on sequence management tab
	 */
	public void clickOnSequenceManagementTab() {
		log.info("Start of the method clickOnSequenceManagementTab");
		driver.safeClick(sequenceManagementTab);
		log.info("End of the method clickOnSequenceManagementTab");
	}

	/**
	 * Verifies number of records displayed
	 * @param expectedRecords : username of user in integer format
	 */
	public boolean verifyNumberOfRecords(int expectedRecords) {
		log.info("Start of the method verifyNumberOfRecords");
		String numberOfRecordsText = driver.safeGetText(noRecordsAvailableText);
		numberOfRecordsText = numberOfRecordsText.replace("items","");
		int numberOfRecords = Integer.parseInt(numberOfRecordsText.trim());
		log.info("End of the method verifyNumberOfRecords");
		return numberOfRecords>=expectedRecords;
	}
}