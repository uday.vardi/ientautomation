package io.ient.test.pages.enterprise;


import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class EmailTemplatesPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(EmailTemplatesPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By emailTemplatesTab = By.cssSelector("div[id='email-template-card']");
	private static final By addButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static final By NameTextBox = By.cssSelector("input[name='data[name]']");
	private static final By subjectTextBox = By.cssSelector("input[name='data[subject]']");
	private static final By submitButton = By.cssSelector("button[name='data[submit]']");
	private static String tableRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]]";
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By verticalIconButtonFirstRow = By.cssSelector("i[id^='vertical-icon-0']");
	private static final By editLink = By.cssSelector("div[id='edit']");
	private static final By deleteLink = By.cssSelector("div[id='delete']");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By noRecordsAvailableTextMessage = By.xpath("//div[@class='ui red message ng-star-inserted' and text()=' No records available ']");
	private static final By bodyTextAreaDiv = By.xpath("//div[@class='ace_layer ace_cursor-layer ace_hidden-cursors']");
	private static final By bodyTextArea = By.xpath("//div[@class='ace_cursor']");
	private static final By fistRowEmailCheckbox = By.xpath("(//ient-checkbox[div[@class='ui checkbox ient-checkbox']])[1]");
	private static final By topVerticalIcon = By.cssSelector("div[class^='ui icon top left pointing dropdown button']>i[class^='ellipsis vertical icon']");
	private static final By exportToTenantOption = By.xpath("//div[contains(@class,'item') and span[text()='Export to Tenant']]");
	private static String targetTenantOption = "//div[contains(@class,'item') and @id='%s']";

	private static final By columnSelectionButton = By.cssSelector("button>i[class^='book open icon ient-button-icon']");
	private static final By filterButton = By.cssSelector("button>i[class^='filter icon ient-button-icon']");
	private static final By topLeftVerticalIcon = By.cssSelector("div[class^='ui icon top left pointing dropdown button']>i[class^='ellipsis vertical icon']");
	private static final By downloadOption = By.xpath("//div[contains(@class,'item') and span[text()='Download Options']]");
	private static final By downloadAsPdfOption = By.xpath("//div[contains(@class,'item') and text()=' Download as PDF ']");
	private static final By downloadAsCsvOption = By.xpath("//div[contains(@class,'item') and text()=' Download as CSV ']");
	private static final By paginationDiv = By.cssSelector("div[class='ient-pagination']");
	private static final By totalCountDiv = By.cssSelector("div[class='total-count']");
	private static String columnNameText = "//div[@class='ient-table-header-content-label' and text()=' %s ']";
	private static final By itemsPerPageDropdown = By.cssSelector("div[class='selection ui dropdown ient-items-per-page-dropdown']");
	private static final By itemsPerPageDropdownOption = By.xpath("//div[@class='menu transition visible']/div[text()='20']");
	private static final By fistRowEmailTemplateName = By.xpath("//table[@class='ui striped table actual-table']/tbody/tr[1]/td[3]/span");

	public EmailTemplatesPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - EmailTemplatesPage");
	}

	/***
	 * Clicks on email templates tab
	 */
	public void clickOnEmailTemplatesTab() {
		log.info("Start of the method clickOnEmailTemplatesTab");
		driver.safeClick(emailTemplatesTab);
		log.info("End of the method clickOnEmailTemplatesTab");
	}

	/***
	 * Creates a new email template
	 * @param name : name in string format
	 * @param subject : subject in string format
	 * @param body : body in string format
	 */
	public void createTemplate(String name, String subject, String body) {
		log.info("Start of the method createTemplate");
		driver.safeClick(addButton);
		driver.safeType(NameTextBox, name);
		driver.safeType(subjectTextBox, subject);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.jSClick(bodyTextAreaDiv);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClearAndTypeByActionsClass(bodyTextArea, body, false);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(submitButton);
		log.info("End of the method createTemplate");
	}

	/**
	 * Verifies whether template is displayed
	 * @param name : name in string format
	 * @return returns creation status
	 */
	public boolean isTemplateDisplayed(String name) {
		log.info("Start of the method isTemplateDisplayed");
		boolean isTemplateDisplayed = driver.isPresentAndVisible(By.xpath(String.format(tableRow, name)));
		log.info("End of the method isTemplateDisplayed");
		return isTemplateDisplayed;
	}

	/***
	 * Searches template by name
	 * @param name : name of template in string format
	 */
	public void searchTemplateByName(String name) {
		log.info("Start of the method searchTemplateByName");
		driver.safeType(searchTextBox, name);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchTemplateByName");
	}

	public void clearSearchTextField() {
		log.info("Start of the method clearSearchTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearSearchTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	/***
	 * Edits existing template
	 * @param name : name in string format
	 */
	public void editTemplate(String name) {
		log.info("Start of the method editTemplate");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.safeType(subjectTextBox, name);
		driver.safeClick(submitButton);
		log.info("End of the method editTemplate");
	}

	/***
	 * Deletes template
	 */
	public void deleteTemplate() {
		log.info("Start of the method deleteTemplate");
		driver.safeClick(verticalIconButton);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deleteTemplate");
	}

	/**
	 * Verifies No Records Available Message
	 * @return returns message display status
	 */
	public boolean verifyNoRecordsMessage() {
		log.info("Start of the method verifyNoRecordsMessage");
		boolean isMessageDisplayed = driver.isPresentAndVisible(noRecordsAvailableTextMessage);
		log.info("End of the method verifyNoRecordsMessage");
		return isMessageDisplayed;
	}

	/**
	 * Verifies whether SearchBar is displayed
	 */
	public boolean isSearchBarDisplayed() {
		log.info("Start of the method isSearchBarDisplayed");
		boolean isSearchBarDisplayed = driver.isPresentAndVisible(searchTextBox) && driver.isPresentAndVisible(searchIconButton);
		log.info("End of the method isSearchBarDisplayed");
		return isSearchBarDisplayed;
	}

	/**
	 * Verifies whether SelectColumns is displayed
	 */
	public boolean isSelectColumnsDisplayed() {
		log.info("Start of the method isSelectColumnsDisplayed");
		boolean isSelectColumnsDisplayed = driver.isPresentAndVisible(columnSelectionButton);
		log.info("End of the method isSelectColumnsDisplayed");
		return isSelectColumnsDisplayed;
	}

	/**
	 * Verifies whether FilterIcon is displayed
	 */
	public boolean isFilterIconDisplayed() {
		log.info("Start of the method isFilterIconDisplayed");
		boolean isFilterIconDisplayed = driver.isPresentAndVisible(filterButton);
		log.info("End of the method isFilterIconDisplayed");
		return isFilterIconDisplayed;
	}

	/**
	 * Verifies whether add new button is displayed
	 */
	public boolean isAddNewButtonDisplayed() {
		log.info("Start of the method isNewUserButtonDisplayed");
		boolean isAddNewButtonDisplayed = driver.isPresentAndVisible(addButton);
		log.info("End of the method isNewUserButtonDisplayed");
		return isAddNewButtonDisplayed;
	}

	/**
	 * Verifies whether DownloadAsPdf is displayed
	 */
	public boolean isDownloadAsPdfDisplayed() {
		log.info("Start of the method isDownloadAsPdfDisplayed");
		driver.safeMouseOver(downloadOption);
		boolean isDownloadAsPdfDisplayed = driver.isPresentAndVisible(downloadAsPdfOption);
		log.info("End of the method isDownloadAsPdfDisplayed");
		return isDownloadAsPdfDisplayed;
	}

	/**
	 * Verifies whether DownloadAsCsv is displayed
	 */
	public boolean isDownloadAsCsvDisplayed() {
		log.info("Start of the method isDownloadAsCsvDisplayed");
		driver.safeMouseOver(downloadOption);
		boolean isDownloadAsCsvDisplayed = driver.isPresentAndVisible(downloadAsCsvOption);
		log.info("End of the method isDownloadAsCsvDisplayed");
		return isDownloadAsCsvDisplayed;
	}

	/**
	 * Clicks on TopLeftVerticalIcon
	 */
	public void clickOnTopLeftVerticalIcon() {
		log.info("Start of the method clickOnTopLeftVerticalIcon");
		driver.safeClick(topLeftVerticalIcon);
		log.info("End of the method clickOnTopLeftVerticalIcon");
	}

	/**
	 * Verifies whether page column is displayed
	 * @param columnName : columnName in string format
	 */
	public boolean isPageColumnDisplayed(String columnName) {
		log.info("Start of the method isPageColumnDisplayed");
		boolean isPageColumnDisplayed = driver.isPresentAndVisible(By.xpath(String.format(columnNameText, columnName)));
		log.info("End of the method isPageColumnDisplayed");
		return isPageColumnDisplayed;
	}

	/**
	 * Verifies whether Edit And Delete is displayed
	 */
	public boolean isActionEditAndDeleteDisplayed() {
		log.info("Start of the method isActionEditAndDeleteDisplayed");
		driver.safeClick(verticalIconButtonFirstRow);
		boolean isActionEditAndDeleteDisplayed = driver.isPresentAndVisible(editLink) && driver.isPresentAndVisible(deleteLink);
		log.info("End of the method isActionEditAndDeleteDisplayed");
		return isActionEditAndDeleteDisplayed;
	}

	/**
	 * Verifies whether number of items per page is editable
	 */
	public boolean isNumberOfItemsPerPageEditable() {
		log.info("Start of the method isNumberOfItemsPerPageEditable");
		driver.safeClick(itemsPerPageDropdown);
		boolean isNumberOfItemsPerPageEditable = driver.isPresentAndVisible(itemsPerPageDropdownOption);
		log.info("End of the method isNumberOfItemsPerPageEditable");
		return isNumberOfItemsPerPageEditable;
	}

	/**
	 * Verifies whether pagination is displayed
	 */
	public boolean isPaginationDisplayed() {
		log.info("Start of the method isPaginationDisplayed");
		boolean isPaginationDisplayed = driver.isPresentAndVisible(paginationDiv);
		log.info("End of the method isPaginationDisplayed");
		return isPaginationDisplayed;
	}

	/***
	 * Exports tenants data
	 * @param tenantName : tenant name in string format
	 */
	public void exportToTenant(String tenantName)
	{
		log.info("Start of the method exportToTenant");
		driver.safeClick(fistRowEmailCheckbox);
		driver.safeClick(topVerticalIcon);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(exportToTenantOption);
		driver.safeClick(By.xpath(String.format(targetTenantOption, tenantName)));
		log.info("End of the method exportToTenant");
	}

	/**
	 * Returns first row email template name
	 * @return returns email template name
	 */
	public String getFirstRowEmailTemplateName() {
		log.info("Start of the method getFirstRowEmailTemplateName");
		String emailTemplateName = driver.safeGetText(fistRowEmailTemplateName);
		log.info("End of the method getFirstRowEmailTemplateName");
		return emailTemplateName;
	}
}