package io.ient.test.pages.enterprise;

import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class PartsPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(PartsPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By addButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static String partsRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]]";
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By editLink = By.cssSelector("div[id='edit']");
	private static final By deleteLink = By.cssSelector("div[id='delete']");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By noRecordsAvailableTextMessage = By.xpath("//div[@class='ui red message ng-star-inserted' and text()=' No records available ']");

	private static final By globalSettingsTab = By.cssSelector("div[id='global-settings-card']");
	private static final By partsTab = By.xpath("//span[text()='Parts']");
	private static final By nameTextBox = By.cssSelector("input[name='data[name]']");
	private static final By versionTextBox = By.cssSelector("input[id='partVersion']");

	private static final By supplierDropDown = By.xpath("//div[select[@name='data[supplier]']]");
	private static String supplierDropdownOption = "//div[contains(@id,'supplier') and @data-id='%s']";
	private static final By currencyDropdown = By.xpath("//div[div[label[contains(text(),'Currency')]]]/parent::div/descendant::div[@class='form-control ui fluid selection dropdown']");
	private static String currencyDropdownOption = "//div[@role='option' and span[contains(text(),'%s')]]/span";
	private static final By priceTextBox = By.cssSelector("input[id='price']");
	private static final By validatedRadioButton = By.xpath("//input[contains(@value,'true')]");
	private static final By submitButton = By.cssSelector("button[name='data[submit]']");


	public PartsPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - Global Settings Page");
	}

	/***
	 * Clicks on global settings tab
	 * Clicks on parts tab
	 */
	public void clickOnPartsTab() {
		log.info("Start of the method clickOnPartsTab");
		driver.safeClick(globalSettingsTab);
		driver.safeClick(partsTab);
		log.info("End of the method clickOnPartsTab");
	}

	/**
	 * Creates a new part
	 * @param partName : part name in string format
	 * @param versionName : version name in string format
	 * @param supplier : supplier in string format
	 * @param price : price in string format
	 * @param currency : currency in string format
	 */
	public void createPart(String partName, String versionName, String supplier, String price, String currency) {
		log.info("Start of the method createPart");
		driver.safeClick(addButton);
		driver.safeType(nameTextBox, partName);
		driver.safeType(versionTextBox, versionName);
		driver.safeClick(supplierDropDown);
		driver.safeClick(By.xpath(String.format(supplierDropdownOption, supplier)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeType(priceTextBox, price);
		driver.safeClick(currencyDropdown);
		driver.safeClick(By.xpath(String.format(currencyDropdownOption, currency)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.jSClick(validatedRadioButton);
		driver.safeClick(submitButton);
		log.info("End of the method createPart");
	}

	/**
	 * Verifies whether parts is displayed
	 * @param partName : part name in string format
	 */
	public boolean isPartDisplayed(String partName) {
		log.info("Start of the method isPartDisplayed");
		boolean isPartDisplayed = driver.isPresentAndVisible(By.xpath(String.format(partsRow, partName)));
		log.info("End of the method isPartDisplayed");
		return isPartDisplayed;
	}

	/***
	 * Searches part by name
	 * @param partName : part name in string format
	 */
	public void searchPartByName(String partName) {
		log.info("Start of the method searchPartByName");
		driver.safeType(searchTextBox, partName);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchPartByName");
	}

	public void clearPartTextField() {
		log.info("Start of the method clearPartTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearPartTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	/***
	 * Edits existing part
	 * @param partName : part name in string format
	 */
	public void editPart(String partName) {
		log.info("Start of the method editPart");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.safeType(nameTextBox, partName);
		driver.safeClick(submitButton);
		log.info("End of the method editPart");
	}

	/***
	 * Deletes part
	 */
	public void deletePart() {
		log.info("Start of the method deletePart");
		driver.safeClick(verticalIconButton);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deletePart");
	}

	/**
	 * Verifies No Records Available Message
	 * @return returns message display status
	 */
	public boolean verifyNoRecordsMessage() {
		log.info("Start of the method verifyNoRecordsMessage");
		boolean isMessageDisplayed = driver.isPresentAndVisible(noRecordsAvailableTextMessage);
		log.info("End of the method verifyNoRecordsMessage");
		return isMessageDisplayed;
	}
}