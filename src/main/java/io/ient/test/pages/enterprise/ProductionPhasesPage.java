package io.ient.test.pages.enterprise;

import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class ProductionPhasesPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(ProductionPhasesPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By addButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static final By statusDropdown = By.xpath("//div[select[@id='status']]");
	private static String statusDropdownOption = "//div[@role='option' and span[text()='%s']]";
	private static String globalSettingsRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]]";
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By editLink = By.cssSelector("div[id='edit']");
	private static final By deleteLink = By.cssSelector("div[id='delete']");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By noRecordsAvailableTextMessage = By.xpath("//div[@class='ui red message ng-star-inserted' and text()=' No records available ']");

	private static final By globalSettingsTab = By.cssSelector("div[id='global-settings-card']");
	private static final By productionPhasesTab = By.xpath("//span[text()='Production Phases']");
	private static final By nameTextBox = By.cssSelector("input[name='data[productionPhase]']");
	private static final By submitButton = By.cssSelector("button[name='data[submit]']");

	public ProductionPhasesPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - Global Settings Page");
	}

	/***
	 * Clicks on global settings tab
	 * Clicks on production phases tab
	 */
	public void clickOnProductionPhasesTab() {
		log.info("Start of the method clickOnProductionPhasesTab");
		driver.safeClick(globalSettingsTab);
		driver.safeClick(productionPhasesTab);
		log.info("End of the method clickOnProductionPhasesTab");
	}

	/**
	 * Creates a new production phases
	 * @param productionPhasesName : production phase name in string format
	 * @param status : status in string format
	 */
	public void createProductionPhases(String productionPhasesName, String status) {
		log.info("Start of the method createProductionPhases");
		driver.safeClick(addButton);
		driver.safeType(nameTextBox, productionPhasesName);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createProductionPhases");
	}

	/**
	 * Verifies whether production phases is displayed
	 * @param productionPhasesName : production phase name in string format
	 */
	public boolean isProductionPhasesDisplayed(String productionPhasesName) {
		log.info("Start of the method isProductionPhasesDisplayed");
		boolean isProductionPhasesDisplayed = driver.isPresentAndVisible(By.xpath(String.format(globalSettingsRow, productionPhasesName)));
		log.info("End of the method isProductionPhasesDisplayed");
		return isProductionPhasesDisplayed;
	}

	/***
	 * Searches production phases by name
	 * @param productionPhasesName : production phase name in string format
	 */
	public void searchProductionPhasesByName(String productionPhasesName) {
		log.info("Start of the method searchProductionPhasesByName");
		driver.safeType(searchTextBox, productionPhasesName);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(searchIconButton);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchProductionPhasesByName");
	}

	public void clearProductionPhasesTextField() {
		log.info("Start of the method clearProductionPhasesTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearProductionPhasesTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	/***
	 * Edits existing production phases
	 * @param productionPhasesName : production phase name in string format
	 */
	public void editProductionPhases(String productionPhasesName) {
		log.info("Start of the method editProductionPhases");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.safeType(nameTextBox, productionPhasesName);
		driver.safeClick(submitButton);
		log.info("End of the method editProductionPhases");
	}

	/***
	 * Deletes production phases
	 */
	public void deleteProductionPhases() {
		log.info("Start of the method deleteProductionPhases");
		driver.safeClick(verticalIconButton);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deleteProductionPhases");
	}

	/**
	 * Verifies No Records Available Message
	 * @return returns message display status
	 */
	public boolean verifyNoRecordsMessage() {
		log.info("Start of the method verifyNoRecordsMessage");
		boolean isMessageDisplayed = driver.isPresentAndVisible(noRecordsAvailableTextMessage);
		log.info("End of the method verifyNoRecordsMessage");
		return isMessageDisplayed;
	}

}