package io.ient.test.pages.enterprise;


import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class SupplierCapacityAppPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(SupplierCapacityAppPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By addButton = By.cssSelector("ient-button[icon='plus']>button");
	private static final By supplierCapacityPart = By.xpath("//div[select[@name='data[part]']]");
	private static String partDropDownOption = "div[id*='choices--datapart'][data-id='1']>span";
	private static final By supplierLocationDropDown = By.xpath("//div[select[@id='supplierLocation']]");
	private static String supplierLocationDropDownOption = "div[id*='supplierLocation-item-choice'][data-id='1']>span";
	private static final By locationTypeDropDown = By.xpath("//div[select[@id='locationType']]");
	private static String locationTypeDropDownOption = "div[id*='locationType-item-choice'][data-id='1']>span";
	private static final By supplierCapacity = By.cssSelector("input[id='capacity']");
	private static final By supplierCapacityUpside = By.cssSelector("input[id='upside']");
	private static final By caseStatusDropDown = By.xpath("//div[select[@id='caseStatus']]");
	private static String submitToBuyerDropDownOption = "div[id*='caseStatus-item-choice'][data-value='Submit to Buyer']>span";
	private static String saveAsDraftDropDownOption = "div[id*='caseStatus-item-choice'][data-value='Save As Draft']>span";
	private static String reworkDropDownOption = "div[id*='caseStatus-item-choice'][data-value='Rework']>span";
	private static String approvedDropDownOption = "div[id*='caseStatus-item-choice'][data-value='Approved']>span";
	private static String rejectedDropDownOption = "div[id*='caseStatus-item-choice'][data-value='Rejected']>span";
	private static final By verifyApproverDropdown = By.xpath("//div[select[@id='verifyApprover']]");
	private static String verifyApproverDropdownOption = "div[id*='verifyApprover-item-choice'][data-value='%s']>span";


	private static final By shortDescriptionTextArea = By.cssSelector("textarea[id='shortDescription']");
	private static final By saveButton = By.cssSelector("button[id='bottom-save-button']");

	private static final By filterButton = By.cssSelector("button[id='ient-table-filter-btn']");
	private static final By supplierFilter = By.xpath("//div[label[text()='Supplier']]/div/button");
	private static final By supplierFilterValue = By.xpath("//div[contains(@class,'drop-show-container')]/div[@class='drop-show']/label[contains(text(),' rrssi ')]/span");
	private static final By supplierLocationFilter = By.xpath("//div[label[text()='Supplier Location']]/div/button");
	private static final By supplierLocationFilterValue = By.xpath("//div[contains(@class,'drop-show-container')]/div[@class='drop-show']/label[contains(text(),' mpl ')]/span");
	private static final By locationTypeFilter = By.xpath("//div[label[text()='Location Type']]/div/button");
	private static final By locationTypeFilterValue = By.xpath("//div[contains(@class,'drop-show-container')]/div[@class='drop-show']/label[contains(text(),' slt ')]/span");
	private static final By partFilter = By.xpath("//div[label[text()='Part']]/div/button");
	private static final By partFilterValue = By.xpath("//div[contains(@class,'drop-show-container')]/div[@class='drop-show']/label[contains(text(),' pt ')]/span");
	private static final By submitFiltersButton = By.cssSelector("button[id = 'submit-bsc-filter']");

	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static String userRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']] and td[span[text()=' %s ']]]";

	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By viewButton = By.cssSelector("div[id='view']");
	private static final By editButtonInCaseForm = By.cssSelector("button[id='ient-btn-edit']");
	private static final By conversationTab = By.xpath("//span[text()='Conversations']");
	private static final By commentTextArea = By.cssSelector("textarea[placeholder='Comment'],textarea[placeholder='External Comment'],textarea[placeholder='Internal Comment']");
	private static final By submitComment = By.xpath("//span[text()='Submit']");
	private static final By detailsTab = By.xpath("//span[text()='Details']");


	public SupplierCapacityAppPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - SupplierCapacityAppPage");
	}

	/**
	 * Creates a new Supplier Capacity
	 * @param part        			  : part in string format
	 * @param supplierLocation        : supplier Location in string format
	 * @param locationType            : locationType in string format
	 * @param capacity 		  		  : capacity in string format
	 * @param upside                  : upside in string format
	 * @param caseStatus              : case Status in string format
	 * @param shortDescription        : short Description in string format
	 * @param approverName            : approver name in string format
	 **/
	public void createSupplierCapacity(String part, String supplierLocation, String locationType, String capacity, String upside, String approverName, String caseStatus, String shortDescription) {
		log.info("Start of the method createSupplierCapacity");
		driver.safeClick(addButton);
		driver.sleep(WaitTime.LONG_WAIT);
		driver.jSClick(supplierCapacityPart);
		driver.safeClick(By.cssSelector(String.format(partDropDownOption, part)));
		driver.jSClick(supplierLocationDropDown);
		driver.safeClick(By.cssSelector(String.format(supplierLocationDropDownOption, supplierLocation)));
		driver.jSClick(locationTypeDropDown);
		driver.safeClick(By.cssSelector(String.format(locationTypeDropDownOption, locationType)));
		driver.safeClearAndTypeByActionsClass(supplierCapacity, capacity,true);
		driver.safeClearAndTypeByActionsClass(supplierCapacityUpside, upside,true);
		driver.jSClick(verifyApproverDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.cssSelector(String.format(verifyApproverDropdownOption, approverName)));
		driver.jSClick(caseStatusDropDown);
		if(caseStatus == "Submit to Buyer"){
			driver.safeClick(By.cssSelector(String.format(submitToBuyerDropDownOption, caseStatus)));
		}else if(caseStatus == "Save As Draft"){
			driver.safeClick(By.cssSelector(String.format(saveAsDraftDropDownOption, caseStatus)));
		}
		driver.safeType(shortDescriptionTextArea, shortDescription);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(saveButton);
		log.info("End of the method createSupplierCapacity");
	}

	/***
	 * Searches created Supplier Capacity by Short Description
	 * @param description : description in string format
	 */
	public void searchByShortDescription(String description) {
		log.info("Start of the method searchByShortDescription");
		driver.safeType(searchTextBox, description);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchByShortDescription");
	}

	/**
	 * Verifies whether Supplier Capacity is displayed
	 */
	public boolean isSupplierCapacityDisplayed(String supplierLocation, String locationType) {
		log.info("Start of the method isSupplierCapacityDisplayed");
		boolean isSupplierCapacityDisplayed = driver.isPresentAndVisible(By.xpath(String.format(userRow, supplierLocation, locationType)));
		log.info("End of the method isSupplierCapacityDisplayed");
		return isSupplierCapacityDisplayed;
	}

	/**
	 * Verifies whether Supplier Capacity is displayed
	 */
	public void selectsRecordAndClicksEdit() {
		log.info("Start of the method selectsRecordAndClicksEdit");
		driver.safeClick(verticalIconButton);
		driver.safeClick(viewButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		boolean isEditButtonDisplayed = driver.isPresentAndVisible(editButtonInCaseForm);
		if(isEditButtonDisplayed){
			driver.safeClick(editButtonInCaseForm);
			driver.sleep(WaitTime.SMALL_WAIT);
		}
		log.info("End of the method selectsRecordAndClicksEdit");
	}

	/**
	 * Adds comments in conversation tab
	 */
	public void addComment(){
		log.info("Start of the method addComment");
		driver.safeClick(conversationTab);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeType(commentTextArea, "A-test: Rework needed");
		driver.safeClick(submitComment);
		driver.safeClick(detailsTab);
		log.info("End of the method addComment");
	}


	/***
	 * status change case
	 * @param caseStatus : next status in string format
	 */
	public void editStatus(String caseStatus) {
		log.info("Start of the method editStatus");
		driver.jSClick(caseStatusDropDown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		if(caseStatus == "Submit to Buyer"){
			driver.safeClick(By.cssSelector(String.format(submitToBuyerDropDownOption, caseStatus)));
		}else if(caseStatus == "Rework"){
			driver.safeClick(By.cssSelector(String.format(reworkDropDownOption, caseStatus)));
		}else if(caseStatus == "Save As Draft"){
			driver.safeClick(By.cssSelector(String.format(saveAsDraftDropDownOption, caseStatus)));
		}else if(caseStatus == "Approved"){
			driver.safeClick(By.cssSelector(String.format(approvedDropDownOption, caseStatus)));
		}else if(caseStatus == "Rejected"){
			driver.safeClick(By.cssSelector(String.format(rejectedDropDownOption, caseStatus)));
		}
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(saveButton);
		log.info("End of the method editStatus");
	}

	/**
	 * Clicks on Filter Icon
	 */
	public void clickFilterIcon() {
		log.info("Start of the method clickFilterIcon");
		driver.safeClick(filterButton);
		log.info("End of the method clickFilterIcon");
	}

	/**
	 * Select filters and values under for buyer admin
	 */
	public void buyerAdminFilters() {
		log.info("Start of the method buyerAdminFilters");
		driver.safeClick(supplierFilter);
		driver.safeClick(supplierFilterValue);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(supplierLocationFilter);
		driver.safeClick(supplierLocationFilterValue);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(locationTypeFilter);
		driver.safeClick(locationTypeFilterValue);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(submitFiltersButton);
		log.info("End of the method buyerAdminFilters");
	}

	/**
	 * Select filters and values under for supplier admin
	 */
	public void supplierAdminFilters() {
		log.info("Start of the method supplierAdminFilters");
		driver.safeClick(supplierLocationFilter);
		driver.safeClick(supplierLocationFilterValue);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(locationTypeFilter);
		driver.safeClick(locationTypeFilterValue);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(partFilter);
		driver.safeClick(partFilterValue);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(submitFiltersButton);
		log.info("End of the method supplierAdminFilters");
	}

}