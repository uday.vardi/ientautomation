package io.ient.test.pages.enterprise;


import io.ient.test.context.FrameworkTestBase;
import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class BusinessOrganizationsPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(FrameworkTestBase.class);
	LogFactory log = new LogFactory(logger);
	private static final By businessOrganizationTab = By.cssSelector("div[id='business-org-card']");
	private static final By addButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static final By departmentsAddButton = By.cssSelector("ient-tab-panel[header='Departments']   ient-button[icon='plus']>button");
	private static final By laborCodesAddButton = By.cssSelector("ient-tab-panel[header='Labor Codes']   ient-button[icon='plus']>button");
	private static final By productLinesAddButton = By.cssSelector("ient-tab-panel[header='Product Lines']   ient-button[icon='plus']>button");
	private static final By organizationNameTextBox = By.cssSelector("input[id='orgName']");
	private static final By dunsTextBox = By.cssSelector("input[id='duns']");
	private static final By costRecoveryThresholdTextBox = By.cssSelector("input[id='costThreshold']");
	private static final By defaultCurrencyDropdown = By.xpath("//div[div[label[contains(text(),'Default Currency')]]]/parent::div/descendant::div[@class='form-control ui fluid selection dropdown']");
	private static final By localTimeZoneDropdown = By.xpath("//div[div[label[contains(text(),'Local Time Zone')]]]/parent::div/descendant::div[@class='form-control ui fluid selection dropdown']");
	private static final By parentBusinessOrganizationDropdown = By.xpath("//div[div[label[contains(text(),'Parent Business Organization')]]]/parent::div/descendant::div[@class='form-control ui fluid selection dropdown']");
	private static String dropdownOption = "//div[@role='option' and span[contains(text(),'%s')]]/span";
	private static final By statusDropdown = By.xpath("//div[div[label[contains(text(),'Status')]]]/parent::div/descendant::div[@class='form-control ui fluid selection dropdown']");
	private static String statusDropdownOption = "//div[@role='option' and span[text()='%s']]";
	private static final By submitBusinessOrganizationButton = By.cssSelector("button[name='data[submit]']");
	private static String businessOrganizationRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']] and  td[span[text()=' %s ']]]";
	private static String businessOrganizationActiveRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']] and  td[span[text()=' %s ']] and td[ient-checkbox[div[@class='ui checkbox ient-checkbox slider checked']]]]";
	private static String businessOrganizationInactiveRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']] and  td[span[text()=' %s ']] and td[ient-checkbox[div[@class='ui checkbox ient-checkbox slider']]]]";
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By editLink = By.cssSelector("div[id='edit']");
	private static final By viewLink = By.cssSelector("div[class='menu left transition visible'] div[id='view']");
	private static final By deleteLink = By.cssSelector("div[id='delete']");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By departmentsTab = By.xpath("//a[span[text()='Departments']]");
	private static final By laborCodeTab = By.xpath("//a[span[text()='Labor Codes']]");
	private static final By productLinesTab = By.xpath("//a[span[text()='Product Lines']]");
	private static final By departmentsName = By.xpath("//input[@name='data[name]']");
	private static final By departmentsCode = By.cssSelector("input[id='code']");
	private static final By rateNumber = By.cssSelector("input[id='rate']");
	private static final By downRateNumber = By.cssSelector("input[id='downRate']");
	private static final By departmentStatusDropdown = By.xpath("//div[div[label[contains(text(),'Status')]]]/parent::div/descendant::div[@class='form-control ui fluid selection dropdown']");
	private static String departmentsDropdownOption = "//div[@role='option' and span[text()='%s']]";
	private static final By submitBusinessOrganizationDepartmentButton = By.cssSelector("button[name='data[submit]']");
	private static final By departmentsSearchTextBox = By.cssSelector("ient-tab-panel[header='Departments'] input[placeholder='Search keyword']");
	private static final By laborCodesSearchTextBox = By.cssSelector("ient-tab-panel[header='Labor Codes'] input[placeholder='Search keyword']");
	private static final By productLinesSearchTextBox = By.cssSelector("ient-tab-panel[header='Product Lines'] input[placeholder='Search keyword']");
	private static final By departmentsSearchIconButton = By.cssSelector("ient-tab-panel[header='Departments'] i[class='search icon link']");
	private static final By laborCodesSearchIconButton = By.cssSelector("ient-tab-panel[header='Labor Codes'] i[class='search icon link']");
	private static final By productLinesSearchIconButton = By.cssSelector("ient-tab-panel[header='Product Lines'] i[class='search icon link']");
	private static final By departmentsVerticalIconButton = By.cssSelector("ient-tab-panel[header='Departments'] i[id^='vertical-icon-']");
	private static final By laborCodesVerticalIconButton = By.cssSelector("ient-tab-panel[header='Labor Codes'] i[id^='vertical-icon-']");
	private static final By productLinesVerticalIconButton = By.cssSelector("ient-tab-panel[header='Product Lines'] i[id^='vertical-icon-']");
	private static final By departmentsEditLink = By.cssSelector("div[id='edit']");
	private static final By departmentsDeleteLink = By.cssSelector("div[id='delete']");
	private static final By departmentsDeleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By plantsTab = By.xpath("//a[span[text()='Plants']]");
	private static final By plantSelectSubmitButton = By.xpath("//button[text()='Submit']");
	private static final By noRecordsAvailableTextMessage = By.xpath("//div[@class='ui red message ng-star-inserted' and text()=' No records available ']");
	private static String businessOrganizationDepartmentRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']] and  td[span[text()=' %s ']]]";
	private static String expandOrganizationLink = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']] and  td[span[text()=' %s ']]]/td/i[contains(@class,'angle right icon')]";
	private static String organizationVerticalIconButton = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']] and  td[span[text()=' %s ']]]/td/div[contains(@class,'pointing item dropdown')]";
	private static String editLinkRow = "//tr[contains(@class,'ient-table-row') and td[span[contains(text(),'%s')]] and  td[span[contains(text(),'%s')]]]/td//div[text()=' Edit ']";
	private static String deleteLinkRow = "//tr[contains(@class,'ient-table-row') and td[span[contains(text(),'%s')]] and  td[span[contains(text(),'%s')]]]/td//div[text()=' Delete ']";
	private static String userRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']] and  td[span[text()=' %s ']]]";
	private static String userLatNameRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']] ]/td[5]/span";
	private static String roleUserRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']] and  td[span[contains(text(),' %s')]]]";
	private static final By columnSelectionButton = By.cssSelector("button[id='ient-table-col-selection-btn']");
	private static final By filterButton = By.cssSelector("button[id='ient-table-filter-btn']");
	private static final By createNewUserButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static final By topLeftVerticalIcon = By.cssSelector("i[id='ient-table-more-action-btn']");
	private static final By downloadOption = By.xpath("//span[text()='Download Options']");
	private static final By exportToTenantOption = By.xpath("//span[text()='Export to Tenant']");
	private static final By listRows = By.xpath("//td[contains(@class,'ient-table-body') and span[contains(text(),' USER:')]]");
	private static String columnNameText = "//div[@class='ient-table-header-content-label' and text()=' %s ']";
	private static final By itemsPerPageDropdown = By.cssSelector("div[id='items-per-page']");
	private static final By itemsPerPageDropdownOption = By.cssSelector("div[id='page-item-20']");
	private static String plantRowCheckbox = "//div[@id='chk1']";
	private static String plantRowCheckboxArea = "//tr[td[span[contains(text(),'%s ')]]]/td[contains(@class,'collapsing left aligned ient-table-body-f td tablerow-restrict-click-marker')]";
	private static String plantsRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]]";
	private static String boVerticalIconButton = "//tr[td[span[contains(text(),'%s')]]]/td/div[contains(@class,'pointing item dropdown')]/i[contains(@class,'ellipsis vertical icon')]";


	public BusinessOrganizationsPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - BusinessOrganizationsPage");
	}

	/***
	 * Clicks on business organizations tab
	 */
	public void clickOnBusinessOrganizationsTab() {
		log.info("Start of the method clickOnBusinessOrganizationsTab");
		driver.safeClick(businessOrganizationTab);
		log.info("End of the method clickOnBusinessOrganizationsTab");
	}

	/**
	 * Clicks on add organization button
	 */
	public void clickOnAddOrganization() {
		log.info("Start of the method clickOnAddOrganization");
		driver.safeClick(addButton);
		log.info("End of the method clickOnAddOrganization");
	}

	/**
	 * Clicks on add button
	 */
	public void clickOnAddButton() {
		log.info("Start of the method clickOnAddButton");
		driver.safeClick(addButton);
		log.info("End of the method clickOnAddButton");
	}

	/**
	 * Creates a new business organization
	 * @param businessOrganizationName : organization name in string format
	 * @param duns : duns string format
	 * @param costRecoveryThreshold : cost recovery threshold in string format
	 * @param defaultCurrency : default currency in string format
	 * @param localTimeZone : local time zone in string format
	 * @param status : status in string format
	 */
	public void createBusinessOrganization(String businessOrganizationName, String duns, String costRecoveryThreshold, String defaultCurrency, String localTimeZone, String status) {
		log.info("Start of the method createBusinessOrganization");
		driver.safeClick(addButton);
		driver.safeType(organizationNameTextBox, businessOrganizationName);
		driver.safeType(dunsTextBox, duns);
		driver.safeType(costRecoveryThresholdTextBox, costRecoveryThreshold);
		driver.safeClick(defaultCurrencyDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(dropdownOption, defaultCurrency)));
		driver.safeClick(localTimeZoneDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(dropdownOption, localTimeZone)));
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitBusinessOrganizationButton);
		log.info("End of the method createBusinessOrganization");
	}

	/**
	 * Creates a new business organization with parent business organization
	 * @param businessOrganizationName : organization name in string format
	 * @param duns : duns string format
	 * @param costRecoveryThreshold : cost recovery threshold in string format
	 * @param defaultCurrency : default currency in string format
	 * @param localTimeZone : local time zone in string format
	 * @param parentOrganizationName : parent organization name in string format
	 * @param status : status in string format
	 */
	public void createBusinessOrganizationWithParentOrganization(String businessOrganizationName, String duns, String costRecoveryThreshold, String defaultCurrency, String localTimeZone, String parentOrganizationName, String status) {
		log.info("Start of the method createBusinessOrganization");
		driver.safeClick(addButton);
		driver.safeType(organizationNameTextBox, businessOrganizationName);
		driver.safeType(dunsTextBox, duns);
		driver.safeType(costRecoveryThresholdTextBox, costRecoveryThreshold);
		driver.safeClick(defaultCurrencyDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(dropdownOption, defaultCurrency)));
		driver.safeClick(localTimeZoneDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(dropdownOption, localTimeZone)));
		driver.safeClick(parentBusinessOrganizationDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(dropdownOption, parentOrganizationName)));
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitBusinessOrganizationButton);
		log.info("End of the method createBusinessOrganization");
	}

	/**
	 * Verifies whether organization is displayed
	 * @param organizationName : organization name in string format
	 * @param duns : duns in string format
	 */
	public boolean isBusinessOrganizationDisplayed(String organizationName, String duns) {
		log.info("Start of the method isBusinessOrganizationDisplayed");
		boolean isOrganizationDisplayed = driver.isPresentAndVisible(By.xpath(String.format(businessOrganizationRow, organizationName, duns)));
		log.info("End of the method isBusinessOrganizationDisplayed");
		return isOrganizationDisplayed;
	}

	/**
	 * Verifies whether organization is active
	 * @param organizationName : organization name in string format
	 * @param duns : duns in string format
	 */
	public boolean isBusinessOrganizationIsActive(String organizationName, String duns) {
		log.info("Start of the method isBusinessOrganizationIsActive");
		boolean isBusinessOrganizationIsActive = driver.isPresentAndVisible(By.xpath(String.format(businessOrganizationActiveRow, organizationName, duns)));
		log.info("End of the method isBusinessOrganizationIsActive");
		return isBusinessOrganizationIsActive;
	}

	/**
	 * Verifies whether organization is inactive
	 * @param organizationName : organization name in string format
	 * @param duns : duns in string format
	 */
	public boolean isBusinessOrganizationIsInactive(String organizationName, String duns) {
		log.info("Start of the method isBusinessOrganizationIsInactive");
		boolean isBusinessOrganizationIsInactive = driver.isPresentAndVisible(By.xpath(String.format(businessOrganizationInactiveRow, organizationName, duns)));
		log.info("End of the method isBusinessOrganizationIsInactive");
		return isBusinessOrganizationIsInactive;
	}

	/***
	 * Searches organization by name
	 * @param organizationName : organization name in string format
	 */
	public void searchOrganizationByName(String organizationName) {
		log.info("Start of the method searchOrganizationByName");
		driver.safeType(searchTextBox, organizationName);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(searchIconButton);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchOrganizationByName");
	}

	public void clearOrganizationNameTextField() {
		log.info("Start of the method clearOrganizationNameTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearOrganizationNameTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	/**
	 * Clicks on view link
	 */
	public void clickOnViewLink() {
		log.info("Start of the method clickOnViewLink");
		driver.safeClick(verticalIconButton);
		driver.safeClick(viewLink);
		log.info("End of the method clickOnViewLink");
	}

	/**
	 * Clicks on view link
	 * @param organizationName : organization name in string format
	 */
	public void clickOnOrganizationViewLink(String organizationName) {
		log.info("Start of the method clickOnViewLink");
		driver.safeClick(By.xpath(String.format(boVerticalIconButton, organizationName)));
		driver.safeClick(viewLink);
		log.info("End of the method clickOnViewLink");
	}

	/**
	 * Clicks on departments tab
	 */
	public void clickOnDepartmentsTab() {
		log.info("Start of the method clickOnDepartmentsTab");
		driver.safeClick(departmentsTab);
		log.info("End of the method clickOnDepartmentsTab");
	}

	/**
	 * Clicks on plants tab
	 */
	public void clickOnPlantsTab() {
		log.info("Start of the method clickOnPlantsTab");
		driver.safeClick(plantsTab);
		log.info("End of the method clickOnPlantsTab");
	}

	/**
	 * Creates department
	 * @param name : name in string format
	 * @param code : code in string format
	 * @param status : status in string format
	 */
	public void createDepartment(String name, String code, String status) {
		log.info("Start of the method createDepartment");
		driver.safeClick(departmentsAddButton);
		driver.safeType(departmentsName, name);
		driver.safeType(departmentsCode, code);
		driver.safeClick(departmentStatusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(departmentsDropdownOption, status)));
		driver.safeClick(submitBusinessOrganizationDepartmentButton);
		log.info("End of the method createDepartment");
	}

	/**
	 * Edits department
	 * @param name : name in string format
	 */
	public void editDepartment(String name) {
		log.info("Start of the method createDepartment");
		driver.safeClick(departmentsVerticalIconButton);
		driver.safeClick(departmentsEditLink);
		driver.safeType(departmentsName, name);
		driver.safeClick(submitBusinessOrganizationDepartmentButton);
		log.info("End of the method createDepartment");
	}

	/**
	 * Clicks on labor codes tab
	 */
	public void clickOnLaborCodesTab() {
		log.info("Start of the method clickOnLaborCodesTab");
		driver.safeClick(laborCodeTab);
		log.info("End of the method clickOnLaborCodesTab");
	}

	/**
	 * Creates labor code
	 * @param name : name in string format
	 * @param code : code in string format
	 * @param rate : rate in string format
	 * @param status : status in string format
	 */
	public void createLaborCode(String name, String code, String rate, String status) {
		log.info("Start of the method createLaborCode");
		driver.safeClick(laborCodesAddButton);
		driver.safeType(departmentsName, name);
		driver.safeType(departmentsCode, code);
		driver.safeType(rateNumber, rate);
		driver.safeClick(departmentStatusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(departmentsDropdownOption, status)));
		driver.safeClick(submitBusinessOrganizationDepartmentButton);
		log.info("End of the method createLaborCode");
	}

	/**
	 * Edits Labor Code
	 * @param name : name in string format
	 */
	public void editLaborCode(String name) {
		log.info("Start of the method editLaborCode");
		driver.safeClick(laborCodesVerticalIconButton);
		driver.safeClick(departmentsEditLink);
		driver.safeType(departmentsName, name);
		driver.safeClick(submitBusinessOrganizationDepartmentButton);
		log.info("End of the method editLaborCode");
	}

	/**
	 * Clears labor codes search text field
	 */
	public void clearLaborCodesSearchTextField() {
		log.info("Start of the method clearLaborCodesSearchTextField");
		driver.safeClear(laborCodesSearchTextBox);
		log.info("End of the method clearLaborCodesSearchTextField");
	}

	/***
	 * Searches organization labor codes by name
	 * @param departmentName : labor codes name in string format
	 */
	public void searchOrganizationLaborCodesByName(String departmentName) {
		log.info("Start of the method searchOrganizationLaborCodesByName");
		driver.safeType(laborCodesSearchTextBox, departmentName);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(laborCodesSearchIconButton);
		driver.safeClick(laborCodesSearchIconButton);
		log.info("End of the method searchOrganizationLaborCodesByName");
	}

	/**
	 * Clicks on product lines tab
	 */
	public void clickOnProductLinesTab() {
		log.info("Start of the method clickOnProductLinesTab");
		driver.safeClick(productLinesTab);
		log.info("End of the method clickOnProductLinesTab");
	}

	/**
	 * Creates product line
	 * @param name : name in string format
	 * @param downRate : downRate in string format
	 * @param status : status in string format
	 */
	public void createProductLine(String name, String downRate, String status) {
		log.info("Start of the method createProductLine");
		driver.safeClick(productLinesAddButton);
		driver.safeType(departmentsName, name);
		driver.safeType(downRateNumber, downRate);
		driver.safeClick(departmentStatusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(departmentsDropdownOption, status)));
		driver.safeClick(submitBusinessOrganizationDepartmentButton);
		log.info("End of the method createProductLine");
	}

	/**
	 * Edits product line
	 * @param name : name in string format
	 */
	public void editProductLine(String name) {
		log.info("Start of the method editProductLine");
		driver.safeClick(productLinesVerticalIconButton);
		driver.safeClick(departmentsEditLink);
		driver.safeType(departmentsName, name);
		driver.safeClick(submitBusinessOrganizationDepartmentButton);
		log.info("End of the method editProductLine");
	}

	/**
	 * Clears product lines search text field
	 */
	public void clearProductLinesSearchTextField() {
		log.info("Start of the method clearProductLinesSearchTextField");
		driver.safeClear(productLinesSearchTextBox);
		log.info("End of the method clearProductLinesSearchTextField");
	}

	/***
	 * Searches organization product lines by name
	 * @param departmentName : product line name in string format
	 */
	public void searchOrganizationProductLinesByName(String departmentName) {
		log.info("Start of the method searchOrganizationProductLinesByName");
		driver.safeType(productLinesSearchTextBox, departmentName);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(productLinesSearchIconButton);
		driver.safeClick(productLinesSearchIconButton);
		log.info("End of the method searchOrganizationProductLinesByName");
	}

	/**
	 * Clears department search text field
	 */
	public void clearDepartmentSearchTextField() {
		log.info("Start of the method clearDepartmentSearchTextField");
		driver.safeClear(departmentsSearchTextBox);
		log.info("End of the method clearDepartmentSearchTextField");
	}

	/***
	 * Searches organization department by name
	 * @param departmentName : department name in string format
	 */
	public void searchOrganizationDepartmentByName(String departmentName) {
		log.info("Start of the method searchOrganizationDepartmentByName");
		driver.safeType(departmentsSearchTextBox, departmentName);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(departmentsSearchIconButton);
		driver.safeClick(departmentsSearchIconButton);
		log.info("End of the method searchOrganizationDepartmentByName");
	}


	/**
	 * Verifies whether organization department is displayed
	 * @param organizationDepartmentName : organization department name in string format
	 * @param code : organization department code in string format
	 */
	public boolean isBusinessOrganizationDepartmentDisplayed(String organizationDepartmentName, String code) {
		log.info("Start of the method isBusinessOrganizationDepartmentDisplayed");
		boolean isBusinessOrganizationDepartmentDisplayed = driver.isPresentAndVisible(By.xpath(String.format(businessOrganizationDepartmentRow, organizationDepartmentName, code)));
		log.info("End of the method isBusinessOrganizationDepartmentDisplayed");
		return isBusinessOrganizationDepartmentDisplayed;
	}

	/**
	 * Verifies whether organization product line is displayed
	 * @param organizationDepartmentName : organization product line name in string format
	 * @param downtimeRate : organization downtime rate in string format
	 */
	public boolean isBusinessOrganizationProductLineDisplayed(String organizationDepartmentName, String downtimeRate) {
		log.info("Start of the method isBusinessOrganizationProductLineDisplayed");
		boolean isBusinessOrganizationProductLineDisplayed = driver.isPresentAndVisible(By.xpath(String.format(businessOrganizationDepartmentRow, organizationDepartmentName, downtimeRate )));
		log.info("End of the method isBusinessOrganizationProductLineDisplayed");
		return isBusinessOrganizationProductLineDisplayed;
	}

	/***
	 * Edits existing organization
	 * @param organizationName : organization name in string format
	 *
	 */
	public void editOrganization(String organizationName) {
		log.info("Start of the method editOrganization");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.safeType(organizationNameTextBox, organizationName);
		driver.safeClick(submitBusinessOrganizationButton);
		log.info("End of the method editOrganization");
	}

	/***
	 * Edits existing organization
	 * @param organizationName : organization name in string format
	 * @param existingOrganizationName : existing organization name in string format
	 * @param organizationDuns : existing organization name in string format
	 *
	 */
	public void editOrganization(String organizationName, String existingOrganizationName, String organizationDuns) {
		log.info("Start of the method editOrganization");
		driver.safeClick(By.xpath(String.format(organizationVerticalIconButton, existingOrganizationName, organizationDuns)));
		driver.safeClick(By.xpath(String.format(editLinkRow, existingOrganizationName, organizationDuns)));
		driver.safeType(organizationNameTextBox, organizationName);
		driver.safeClick(submitBusinessOrganizationButton);
		log.info("End of the method editOrganization");
	}

	/***
	 * Deletes Organization
	 */
	public void deleteOrganization() {
		log.info("Start of the method deleteOrganization");
		driver.safeClick(verticalIconButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deleteOrganization");
	}

	/***
	 * Deletes department
	 */
	public void deleteDepartment() {
		log.info("Start of the method deleteDepartment");
		driver.safeClick(departmentsVerticalIconButton);
		driver.safeClick(departmentsDeleteLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(departmentsDeleteDialogYesButton);
		log.info("End of the method deleteDepartment");
	}

	/***
	 * Deletes labor code
	 */
	public void deleteLaborCode() {
		log.info("Start of the method deleteLaborCode");
		driver.safeClick(laborCodesVerticalIconButton);
		driver.safeClick(departmentsDeleteLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(departmentsDeleteDialogYesButton);
		log.info("End of the method deleteLaborCode");
	}

	/***
	 * Deletes product line
	 */
	public void deleteProductLine() {
		log.info("Start of the method deleteProductLine");
		driver.safeClick(productLinesVerticalIconButton);
		driver.safeClick(departmentsDeleteLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(departmentsDeleteDialogYesButton);
		log.info("End of the method deleteProductLine");
	}


	/**
	 * Verifies No Records Available Message
	 * @return returns message display status
	 */
	public boolean verifyNoRecordsMessage() {
		log.info("Start of the method verifyNoRecordsMessage");
		boolean isMessageDisplayed = driver.isPresentAndVisible(noRecordsAvailableTextMessage);
		log.info("End of the method verifyNoRecordsMessage");
		return isMessageDisplayed;
	}

	/**
	 * Expands business organization
	 * @param organizationName : organization name in string format
	 * @param duns : duns in string format
	 */
	public void expandBusinessOrganization(String organizationName, String duns) {
		log.info("Start of the method expandBusinessOrganization");
		driver.safeClick(By.xpath(String.format(expandOrganizationLink, organizationName, duns)));
		log.info("End of the method expandBusinessOrganization");
	}

	/***
	 * Deletes Organization
	 * @param organizationName : organization name in string format
	 * @param organizationDuns : existing organization name in string format
	 */
	public void deleteOrganization(String organizationName, String organizationDuns) {
		log.info("Start of the method deleteOrganization");
		driver.safeClick(By.xpath(String.format(organizationVerticalIconButton, organizationName, organizationDuns)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(deleteLinkRow, organizationName, organizationDuns)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deleteOrganization");
	}

	/**
	 * Verifies expand organization button
	 * @param organizationName : organization name in string format
	 * @param duns : duns in string format
	 * @return returns button display status
	 */
	public boolean verifyExpandButtonDisplayStatus(String organizationName, String duns) {
		log.info("Start of the method verifyExpandButtonDisplayStatus");
		boolean isButtonDisplayed = driver.isPresentAndVisible(By.xpath(String.format(expandOrganizationLink, organizationName, duns)));
		log.info("End of the method verifyExpandButtonDisplayStatus");
		return isButtonDisplayed;
	}

	/**
	 * Verifies whether submit button disabled
	 */
	public boolean isSubmitButtonDisabled() {
		log.info("Start of the method isSubmitButtonDisabled");
		String disabledText = driver.getAttribute(submitBusinessOrganizationButton, "disabled");
		boolean isSubmitButtonDisabled = disabledText.equals("true");
		log.info("End of the method isSubmitButtonDisabled");
		return isSubmitButtonDisabled;
	}

	/**
	 * Verifies whether SearchBar is displayed
	 */
	public boolean isSearchBarDisplayed() {
		log.info("Start of the method isSearchBarDisplayed");
		boolean isSearchBarDisplayed = driver.isPresentAndVisible(searchTextBox) && driver.isPresentAndVisible(searchIconButton);
		log.info("End of the method isSearchBarDisplayed");
		return isSearchBarDisplayed;
	}

	/**
	 * Verifies whether SelectColumns is displayed
	 */
	public boolean isSelectColumnsDisplayed() {
		log.info("Start of the method isSelectColumnsDisplayed");
		boolean isSelectColumnsDisplayed = driver.isPresentAndVisible(columnSelectionButton);
		log.info("End of the method isSelectColumnsDisplayed");
		return isSelectColumnsDisplayed;
	}

	/**
	 * Verifies whether FilterIcon is displayed
	 */
	public boolean isFilterIconDisplayed() {
		log.info("Start of the method isFilterIconDisplayed");
		boolean isFilterIconDisplayed = driver.isPresentAndVisible(filterButton);
		log.info("End of the method isFilterIconDisplayed");
		return isFilterIconDisplayed;
	}

	/**
	 * Verifies whether NewUser button is displayed
	 */
	public boolean isNewUserButtonDisplayed() {
		log.info("Start of the method isNewUserButtonDisplayed");
		boolean isNewUserDisplayed = driver.isPresentAndVisible(createNewUserButton);
		log.info("End of the method isNewUserButtonDisplayed");
		return isNewUserDisplayed;
	}

	/**
	 * Verifies whether downloadOption is displayed
	 */
	public boolean isDownloadOptionDisplayed() {
		log.info("Start of the method isDownloadOptionDisplayed");
		boolean isDownloadOptionDisplayed = driver.isPresentAndVisible(downloadOption);
		log.info("End of the method isDownloadOptionDisplayed");
		return isDownloadOptionDisplayed;
	}

	/**
	 * Clicks on TopLeftVerticalIcon
	 */
	public void clickOnTopLeftVerticalIcon() {
		log.info("Start of the method clickOnTopLeftVerticalIcon");
		driver.safeClick(topLeftVerticalIcon);
		log.info("End of the method clickOnTopLeftVerticalIcon");
	}

	/**
	 * Verifies whether ExportToTenantOption is displayed
	 */
	public boolean isExportToTenantOptionDisplayed() {
		log.info("Start of the method isExportToTenantOptionDisplayed");
		boolean isExportToTenantOptionDisplayed = driver.isPresentAndVisible(exportToTenantOption);
		log.info("End of the method isExportToTenantOptionDisplayed");
		return isExportToTenantOptionDisplayed;
	}

	/**
	 * Verifies business organization list
	 */
	public boolean verifyList() {
		log.info("Start of the method verifyList");
		boolean verifyList = driver.getXpathCount(listRows)>0;
		log.info("End of the method verifyList");
		return verifyList;
	}

	/**
	 * Verifies whether page column is displayed
	 * @param columnName : columnName in string format
	 */
	public boolean isPageColumnDisplayed(String columnName) {
		log.info("Start of the method isPageColumnDisplayed");
		boolean isPageColumnDisplayed = driver.isPresentAndVisible(By.xpath(String.format(columnNameText, columnName)));
		log.info("End of the method isPageColumnDisplayed");
		return isPageColumnDisplayed;
	}

	/**
	 * Verifies whether View And Edit And Delete is displayed
	 */
	public boolean isActionViewAndEditAndDeleteDisplayed() {
		log.info("Start of the method isActionViewAndEditAndDeleteDisplayed");
		driver.safeClick(verticalIconButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		boolean isActionViewAndEditAndDeleteDisplayed = driver.isPresentAndVisible(viewLink) && driver.isPresentAndVisible(editLink) && driver.isPresentAndVisible(deleteLink);
		log.info("End of the method isActionViewAndEditAndDeleteDisplayed");
		return isActionViewAndEditAndDeleteDisplayed;
	}

	/**
	 * Edits business organization status
	 * @param status : status of business org in string format
	 */
	public void editBusinessOrgStatus(String status) {
		log.info("Start of the method editBusinessOrgStatus");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(submitBusinessOrganizationButton);
		log.info("End of the method editBusinessOrgStatus");
	}

	/**
	 * Verifies whether number of items per page is editable
	 */
	public boolean isNumberOfItemsPerPageEditable() {
		log.info("Start of the method isNumberOfItemsPerPageEditable");
		driver.safeClick(itemsPerPageDropdown);
		boolean isNumberOfItemsPerPageEditable = driver.isPresentAndVisible(itemsPerPageDropdownOption);
		log.info("End of the method isNumberOfItemsPerPageEditable");
		return isNumberOfItemsPerPageEditable;
	}

	/***
	 * Searches plant by name
	 * @param locationName : location name in string format
	 */
	public void searchPlantByLocationName(String locationName) {
		log.info("Start of the method searchPlantByLocationName");
		driver.safeType(searchTextBox, locationName);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(searchIconButton);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchPlantByLocationName");
	}

	/**
	 * Selects plant
	 */
	public void selectPlant(String locationName) {
		log.info("Start of the method selectPlant");
		driver.safeMouseOver(By.xpath(String.format(plantRowCheckboxArea, locationName)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.jSClick(By.xpath(plantRowCheckbox));
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(plantSelectSubmitButton);
		driver.sleep(WaitTime.LONG_WAIT);
		log.info("End of the method selectPlant");
	}

	/**
	 * Verifies whether plant is displayed
	 * @param locationName : location name in string format
	 */
	public boolean isPlantDisplayed(String locationName) {
		log.info("Start of the method isPlantDisplayed");
		boolean isPlantDisplayed = driver.isPresentAndVisible(By.xpath(String.format(plantsRow, locationName)));
		log.info("End of the method isPlantDisplayed");
		return isPlantDisplayed;
	}

	/***
	 * Deletes plant
	 */
	public void deletePlant() {
		log.info("Start of the method deletePlant");
		driver.safeClick(verticalIconButton);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deletePlant");
	}

}