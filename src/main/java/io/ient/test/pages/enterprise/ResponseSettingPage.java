package io.ient.test.pages.enterprise;

import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;


public class ResponseSettingPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(ResponseSettingPage.class);
	LogFactory log = new LogFactory(logger);
	private static final By globalSettingsTab = By.xpath("//div[@class='header_name' and text()=' Global Settings ']");
	private static final By responseSettingTab = By.xpath("//span[text()='Response Setting']");
	private static final By editButtonInResponseSettingsTab = By.xpath("//span[text()='Edit']");
	private static final By allInputFieldsLocator = By.cssSelector("div[aria-hidden='false'] input[type='text']");
	private static final By allTextareaFieldsLocator = By.cssSelector("div[aria-hidden='false'] textarea[type='text']");
	private static final By allTextareaFieldLocator = By.cssSelector("div[ref='input']");
	private static final By submitButton = By.xpath("//button[contains(text(),'Submit')]");

	public ResponseSettingPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - Global Settings Page");
	}

	/***
	 * Clicks on global settings tab
	 * Clicks on response setting tab
	 */
	public void clickOnResponseSettingTab() {
		log.info("Start of the method clickOnResponseSettingTab");
		driver.safeClick(globalSettingsTab);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(responseSettingTab);
		log.info("End of the method clickOnResponseSettingTab");
	}

	/**
	 * Edits a response settings
	 * @param hours :  response setting hours in string format
	 */
	public void editResponseSettings(String hours) {
		log.info("Start of the method editResponseSettings");
		driver.jSClick(editButtonInResponseSettingsTab);
		driver.sleep(WaitTime.SMALL_WAIT);
		List<WebElement> allInputFieldElements = driver.findElements(allInputFieldsLocator);
		for (WebElement element : allInputFieldElements)
		{
			log.info(String.format("Entering Hours to %s field", element.getAttribute("name")));
			element.clear();
			element.sendKeys(hours);
		}
		List<WebElement> allTextAreaFieldElements = driver.findElements(allTextareaFieldsLocator);
		for (WebElement element : allTextAreaFieldElements)
		{
			log.info(String.format("Entering Hours to %s field", element.getAttribute("name")));
			element.clear();
			element.sendKeys(hours);
		}
		driver.safeClick(submitButton);
	}

	/**
	 * Verifies edited response settings
	 * @param hours :  response setting hours in string format
	 * @return returns updated status
	 */
	public boolean isResponseSettingsInputFieldsEdited(String hours) {
		log.info("Start of the method isResponseSettingsInputFieldsEdited");
		driver.sleep(WaitTime.SMALL_WAIT);
		boolean updatedStatus = false;
		List<WebElement> allInputFieldElements = driver.findElements(allInputFieldsLocator);
		for (WebElement element : allInputFieldElements) {
			String actualHoursValue = element.getAttribute("value");
			String elementName = element.getAttribute("name");
			log.info(String.format("Text Field %s Hours %s", elementName, actualHoursValue));
			if (hours.equals(actualHoursValue)) {
				updatedStatus = true;
				log.info(String.format("Actual hours are matched with expected hours for %s", elementName));
			} else {
				updatedStatus = false;
				log.info(String.format("Actual hours are not matched with expected hours for %s", elementName));
				break;
			}
		}
		return updatedStatus;
	}

	/**
	 * Verifies text area edited response settings
	 * @param hours :  response setting hours in string format
	 * @return returns updated status
	 */
	public boolean isResponseSettingsTextAreaFieldsEdited(String hours) {
		log.info("Start of the method isResponseSettingsTextAreaFieldsEdited");
		driver.sleep(WaitTime.SMALL_WAIT);
		boolean updatedStatus = false;
		List<WebElement> allTextAreaFieldElements = driver.findElements(allTextareaFieldLocator);
		for (WebElement element : allTextAreaFieldElements) {
			String actualHoursValue = element.getAttribute("value");
			String elementName = element.getAttribute("name");
			log.info(String.format("Text Field %s Hours %s", elementName, actualHoursValue));
			if (hours.equals(actualHoursValue)) {
				updatedStatus = true;
				log.info(String.format("Actual hours are matched with expected hours for %s", elementName));
			} else {
				updatedStatus = false;
				log.info(String.format("Actual hours are not matched with expected hours for %s", elementName));
				break;
			}
		}
		return updatedStatus;
	}
}