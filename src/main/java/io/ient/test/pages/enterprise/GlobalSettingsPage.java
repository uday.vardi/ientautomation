package io.ient.test.pages.enterprise;

import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class GlobalSettingsPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(GlobalSettingsPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By addButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static final By statusDropdown = By.xpath("//div[select[@id='status']]");
	private static String statusDropdownOption = "//div[@role='option' and span[text()='%s']]";
	private static String globalSettingsRow ="//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]]";
	private static String globalSettingsActiveRow ="//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]  and td[ient-checkbox[div[@class='ui checkbox ient-checkbox slider checked']]]]";
	private static String globalSettingsInactiveRow ="//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]  and td[ient-checkbox[div[@class='ui checkbox ient-checkbox slider']]]]";
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By verticalIconButtonFirstRow = By.cssSelector("i[id='vertical-icon-0']");
	private static final By editLink = By.cssSelector("div[id='edit']");
	private static final By deleteLink = By.cssSelector("div[id='delete']");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By noRecordsAvailableTextMessage = By.xpath("//div[@class='ui red message ng-star-inserted' and text()=' No records available ']");

	private static final By globalSettingsTab = By.cssSelector("div[id='global-settings-card']");
	private static final By nameTextBox = By.cssSelector("input[name='data[name]']");
	private static final By submitButton = By.cssSelector("button[name='data[submit]']");
	private static final By columnSelectionButton = By.cssSelector("button[id='ient-table-col-selection-btn']");
	private static final By filterButton = By.cssSelector("button[id='ient-table-filter-btn']");
	private static final By topLeftVerticalIcon = By.cssSelector("i[id='ient-table-more-action-btn']");
	private static final By downloadOption = By.xpath("//div[contains(@class,'item') and span[text()='Download Options']]");
	private static final By downloadAsPdfOption = By.xpath("//div[contains(@class,'item') and text()=' Download as PDF ']");
	private static final By downloadAsCsvOption = By.xpath("//div[contains(@class,'item') and text()=' Download as CSV ']");
	private static final By paginationDiv = By.cssSelector("div[class='ient-pagination']");
	private static final By totalCountDiv = By.cssSelector("div[id='ient-table-item-count']");
	private static String columnNameText = "//div[@class='ient-table-header-content-label' and text()=' %s ']";
	private static final By itemsPerPageDropdown = By.cssSelector("div[id='items-per-page']");
	private static final By itemsPerPageDropdownOption = By.cssSelector("div[id='page-item-20']");
	private static final By problemQualifiersListRows = By.xpath("//td[contains(@class,'ient-table-body') and span[contains(text(),' GPQ:')]]");

	private static final By problemQualifierTab = By.xpath("//span[text()='Problem Qualifiers']");
	private static final By customersTab = By.xpath("//span[text()='Customers']");
	private static final By problemTypeTab = By.xpath("//span[text()='Problem Types']");
	private static final By commodityGroupTab = By.xpath("//span[text()='Commodity Groups']");
	private static final By rejectReasonsTab = By.xpath("//span[text()='Reject Reasons']");
	private static final By partsTab = By.xpath("//span[text()='Parts']");
	private static final By productionPhaseTab = By.xpath("//span[text()='Production Phases']");
	private static final By disruptionStatusTab = By.xpath("//span[text()='Disruption Status']");
	private static final By suppliersTab = By.xpath("//span[text()='Suppliers']");
	private static final By tenantsManagementTab = By.cssSelector("div[id='tenant-management-card']");
	private static final By applicationApprovalTab = By.cssSelector("div[id='app-approval-card']");
	private static final By userManagementTab = By.cssSelector("div[id='user-manage-card']");


	public GlobalSettingsPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - Global Settings Page");
	}

	/***
	 * Clicks on global settings tab
	 */
	public void clickOnGlobalSettingsTab() {
		log.info("Start of the method clickOnGlobalSettingsTab");
		driver.safeClick(globalSettingsTab);
		log.info("End of the method clickOnGlobalSettingsTab");
	}

	/**
	 * Creates a new problem qualifier
	 * @param problemQualifierName : problem qualifier name in string format
	 * @param status : status in string format
	 */
	public void createProblemQualifier(String problemQualifierName, String status) {
		log.info("Start of the method createProblemQualifier");
		driver.safeClick(addButton);
		driver.safeType(nameTextBox, problemQualifierName);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createProblemQualifier");
	}

	/**
	 * Verifies whether problem qualifier is displayed
	 * @param problemQualifierName : problem qualifier name in string format
	 */
	public boolean isProblemQualifierDisplayed(String problemQualifierName) {
		log.info("Start of the method isProblemQualifierDisplayed");
		boolean isProblemQualifierDisplayed = driver.isPresentAndVisible(By.xpath(String.format(globalSettingsRow, problemQualifierName)));
		log.info("End of the method isProblemQualifierDisplayed");
		return isProblemQualifierDisplayed;
	}

	/***
	 * Searches problem qualifier by name
	 * @param problemQualifierName : problem qualifier name in string format
	 */
	public void searchProblemQualifierByName(String problemQualifierName) {
		log.info("Start of the method searchProblemQualifierByName");
		driver.safeType(searchTextBox, problemQualifierName);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(searchIconButton);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchProblemQualifierByName");
	}

	public void clearProblemQualifierTextField() {
		log.info("Start of the method clearProblemQualifierTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearProblemQualifierTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	/***
	 * Edits existing problem qualifier
	 * @param problemQualifierName : problem qualifier name in string format
	 */
	public void editProblemQualifier(String problemQualifierName) {
		log.info("Start of the method editProblemQualifier");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.safeType(nameTextBox, problemQualifierName);
		driver.safeClick(submitButton);
		log.info("End of the method editProblemQualifier");
	}

	/***
	 * Edits existing problem qualifier with status
	 * @param problemQualifierName : problem qualifier name in string format
	 * @param status : status in string format
	 */
	public void editProblemQualifierWithStatus(String problemQualifierName, String status) {
		log.info("Start of the method editProblemQualifier");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.safeType(nameTextBox, problemQualifierName);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method editProblemQualifier");
	}

	/***
	 * Deletes Problem Qualifier
	 */
	public void deleteProblemQualifier() {
		log.info("Start of the method deleteProblemQualifier");
		driver.safeClick(verticalIconButton);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deleteProblemQualifier");
	}

	/**
	 * Verifies No Records Available Message
	 * @return returns message display status
	 */
	public boolean verifyNoRecordsMessage() {
		log.info("Start of the method verifyNoRecordsMessage");
		boolean isMessageDisplayed = driver.isPresentAndVisible(noRecordsAvailableTextMessage);
		log.info("End of the method verifyNoRecordsMessage");
		return isMessageDisplayed;
	}

	/**
	 * Verifies whether SearchBar is displayed
	 */
	public boolean isSearchBarDisplayed() {
		log.info("Start of the method isSearchBarDisplayed");
		boolean isSearchBarDisplayed = driver.isPresentAndVisible(searchTextBox) && driver.isPresentAndVisible(searchIconButton);
		log.info("End of the method isSearchBarDisplayed");
		return isSearchBarDisplayed;
	}

	/**
	 * Verifies whether SelectColumns is displayed
	 */
	public boolean isSelectColumnsDisplayed() {
		log.info("Start of the method isSelectColumnsDisplayed");
		boolean isSelectColumnsDisplayed = driver.isPresentAndVisible(columnSelectionButton);
		log.info("End of the method isSelectColumnsDisplayed");
		return isSelectColumnsDisplayed;
	}

	/**
	 * Verifies whether FilterIcon is displayed
	 */
	public boolean isFilterIconDisplayed() {
		log.info("Start of the method isFilterIconDisplayed");
		boolean isFilterIconDisplayed = driver.isPresentAndVisible(filterButton);
		log.info("End of the method isFilterIconDisplayed");
		return isFilterIconDisplayed;
	}

	/**
	 * Verifies whether add new button is displayed
	 */
	public boolean isAddNewButtonDisplayed() {
		log.info("Start of the method isNewUserButtonDisplayed");
		boolean isAddNewButtonDisplayed = driver.isPresentAndVisible(addButton);
		log.info("End of the method isNewUserButtonDisplayed");
		return isAddNewButtonDisplayed;
	}

	/**
	 * Verifies whether DownloadAsPdf is displayed
	 */
	public boolean isDownloadAsPdfDisplayed() {
		log.info("Start of the method isDownloadAsPdfDisplayed");
		driver.safeMouseOver(downloadOption);
		boolean isDownloadAsPdfDisplayed = driver.isPresentAndVisible(downloadAsPdfOption);
		log.info("End of the method isDownloadAsPdfDisplayed");
		return isDownloadAsPdfDisplayed;
	}

	/**
	 * Verifies whether DownloadAsCsv is displayed
	 */
	public boolean isDownloadAsCsvDisplayed() {
		log.info("Start of the method isDownloadAsCsvDisplayed");
		driver.safeMouseOver(downloadOption);
		boolean isDownloadAsCsvDisplayed = driver.isPresentAndVisible(downloadAsCsvOption);
		log.info("End of the method isDownloadAsCsvDisplayed");
		return isDownloadAsCsvDisplayed;
	}

	/**
	 * Clicks on TopLeftVerticalIcon
	 */
	public void clickOnTopLeftVerticalIcon() {
		log.info("Start of the method clickOnTopLeftVerticalIcon");
		driver.safeClick(topLeftVerticalIcon);
		log.info("End of the method clickOnTopLeftVerticalIcon");
	}

	/**
	 * Verifies whether page column is displayed
	 * @param columnName : columnName in string format
	 */
	public boolean isPageColumnDisplayed(String columnName) {
		log.info("Start of the method isPageColumnDisplayed");
		boolean isPageColumnDisplayed = driver.isPresentAndVisible(By.xpath(String.format(columnNameText, columnName)));
		log.info("End of the method isPageColumnDisplayed");
		return isPageColumnDisplayed;
	}

	/**
	 * Verifies whether Edit And Delete is displayed
	 */
	public boolean isActionEditAndDeleteDisplayed() {
		log.info("Start of the method isActionEditAndDeleteDisplayed");
		driver.safeClick(verticalIconButtonFirstRow);
		boolean isActionEditAndDeleteDisplayed = driver.isPresentAndVisible(editLink) && driver.isPresentAndVisible(deleteLink);
		log.info("End of the method isActionEditAndDeleteDisplayed");
		return isActionEditAndDeleteDisplayed;
	}

	/**
	 * Verifies whether number of items per page is editable
	 */
	public boolean isNumberOfItemsPerPageEditable() {
		log.info("Start of the method isNumberOfItemsPerPageEditable");
		driver.safeClick(itemsPerPageDropdown);
		boolean isNumberOfItemsPerPageEditable = driver.isPresentAndVisible(itemsPerPageDropdownOption);
		log.info("End of the method isNumberOfItemsPerPageEditable");
		return isNumberOfItemsPerPageEditable;
	}

	/**
	 * Verifies whether pagination is displayed
	 */
	public boolean isPaginationDisplayed() {
		log.info("Start of the method isPaginationDisplayed");
		boolean isPaginationDisplayed = driver.isPresentAndVisible(paginationDiv);
		log.info("End of the method isPaginationDisplayed");
		return isPaginationDisplayed;
	}

	/**
	 * Verifies whether total count is displayed
	 */
	public boolean isTotalCountDisplayed() {
		log.info("Start of the method isTotalCountDisplayed");
		boolean isTotalCountDisplayed = driver.isPresentAndVisible(totalCountDiv);
		log.info("End of the method isTotalCountDisplayed");
		return isTotalCountDisplayed;
	}

	/**
	 * Verifies ProblemQualifiers list
	 */
	public boolean verifyProblemQualifiersList() {
		log.info("Start of the method verifyProblemQualifiersList");
		boolean problemQualifiersListStatus = driver.getXpathCount(problemQualifiersListRows)>0;
		log.info("End of the method verifyProblemQualifiersList");
		return problemQualifiersListStatus;
	}

	/**
	 * Verifies whether submit button disabled
	 */
	public boolean isSubmitButtonDisabled() {
		log.info("Start of the method isSubmitButtonDisabled");
		String disabledText = driver.getAttribute(submitButton, "disabled");
		boolean isSubmitButtonDisabled = disabledText.equals("disabled");
		log.info("End of the method isSubmitButtonDisabled");
		return isSubmitButtonDisabled;
	}

	/**
	 * Verifies whether problem qualifier is active
	 * @param problemQualifierName : problem qualifier name in string format
	 */
	public boolean isProblemQualifierIsActive(String problemQualifierName) {
		log.info("Start of the method isProblemQualifierIsActive");
		boolean isProblemQualifierIsActive = driver.isPresentAndVisible(By.xpath(String.format(globalSettingsActiveRow, problemQualifierName)));
		log.info("End of the method isProblemQualifierIsActive");
		return isProblemQualifierIsActive;
	}

	/**
	 * Verifies whether problem qualifier is inactive
	 * @param problemQualifierName : problem qualifier name in string format
	 */
	public boolean isProblemQualifierIsInactive(String problemQualifierName) {
		log.info("Start of the method isProblemQualifierIsInactive");
		boolean isProblemQualifierIsInactive = driver.isPresentAndVisible(By.xpath(String.format(globalSettingsInactiveRow, problemQualifierName)));
		log.info("End of the method isProblemQualifierIsInactive");
		return isProblemQualifierIsInactive;
	}

	/**
	 * Verifies whether problem qualifier tab is displayed
	 */
	public boolean isProblemQualifierTabDisplayed() {
		log.info("Start of the method isProblemQualifierTabDisplayed");
		boolean isProblemQualifierTabDisplayed = driver.isPresentAndVisible(problemQualifierTab);
		log.info("End of the method isProblemQualifierTabDisplayed");
		return isProblemQualifierTabDisplayed;
	}

	/**
	 * Verifies whether customers tab is displayed
	 */
	public boolean isCustomersTabDisplayed() {
		log.info("Start of the method isCustomersTabDisplayed");
		boolean isCustomersTabDisplayed = driver.isPresentAndVisible(customersTab);
		log.info("End of the method isCustomersTabDisplayed");
		return isCustomersTabDisplayed;
	}

	/**
	 * Verifies whether tenant management tab is displayed
	 */
	public boolean isTenantManagementTabDisplayed() {
		log.info("Start of the method isTenantManagementTabDisplayed");
		boolean isTenantManagementTabDisplayed = driver.isPresentAndVisible(tenantsManagementTab);
		log.info("End of the method isTenantManagementTabDisplayed");
		return isTenantManagementTabDisplayed;
	}

	/**
	 * Verifies whether application approval is displayed
	 */
	public boolean isApplicationApprovalDisplayed() {
		log.info("Start of the method isApplicationApprovalDisplayed");
		boolean isApplicationApprovalDisplayed = driver.isPresentAndVisible(applicationApprovalTab);
		log.info("End of the method isApplicationApprovalDisplayed");
		return isApplicationApprovalDisplayed;
	}

	/**
	 * Verifies whether user management tab is displayed
	 */
	public boolean isUserManagementTabDisplayed() {
		log.info("Start of the method isUserManagementTabDisplayed");
		boolean isUserManagementTabDisplayed = driver.isPresentAndVisible(userManagementTab);
		log.info("End of the method isUserManagementTabDisplayed");
		return isUserManagementTabDisplayed;
	}

	/**
	 * Verifies whether problem type tab is displayed
	 */
	public boolean isProblemTypeTabDisplayed() {
		log.info("Start of the method isProblemTypeTabDisplayed");
		boolean isProblemTypeTabDisplayed = driver.isPresentAndVisible(problemTypeTab);
		log.info("End of the method isProblemTypeTabDisplayed");
		return isProblemTypeTabDisplayed;
	}
	/**
	 * Verifies whether commodity groups tab is displayed
	 */
	public boolean isCommodityGroupsTabDisplayed() {
		log.info("Start of the method isCommodityGroupsTabDisplayed");
		boolean isCommodityGroupsTabDisplayed = driver.isPresentAndVisible(commodityGroupTab);
		log.info("End of the method isCommodityGroupsTabDisplayed");
		return isCommodityGroupsTabDisplayed;
	}
	/**
	 * Verifies whether production phase tab is displayed
	 */
	public boolean isProductionPhaseTabDisplayed() {
		log.info("Start of the method isProductionPhaseTabDisplayed");
		boolean isProductionPhaseTabDisplayed = driver.isPresentAndVisible(productionPhaseTab);
		log.info("End of the method isProductionPhaseTabDisplayed");
		return isProductionPhaseTabDisplayed;
	}
	/**
	 * Verifies whether parts tab is displayed
	 */
	public boolean isPartsTabDisplayed() {
		log.info("Start of the method isPartsTabDisplayed");
		boolean isPartsTabDisplayed = driver.isPresentAndVisible(partsTab);
		log.info("End of the method isPartsTabDisplayed");
		return isPartsTabDisplayed;
	}
	/**
	 * Verifies whether disruption status tab is displayed
	 */
	public boolean isDisruptionStatusTabDisplayed() {
		log.info("Start of the method isDisruptionStatusTabDisplayed");
		boolean isDisruptionStatusTabDisplayed = driver.isPresentAndVisible(disruptionStatusTab);
		log.info("End of the method isDisruptionStatusTabDisplayed");
		return isDisruptionStatusTabDisplayed;
	}
	/**
	 * Verifies whether reject reasons tab is displayed
	 */
	public boolean isRejectReasonsTabDisplayed() {
		log.info("Start of the method isRejectReasonsTabDisplayed");
		boolean isRejectReasonsTabDisplayed = driver.isPresentAndVisible(rejectReasonsTab);
		log.info("End of the method isRejectReasonsTabDisplayed");
		return isRejectReasonsTabDisplayed;
	}
	/**
	 * Verifies whether suppliers tab is displayed
	 */
	public boolean isSuppliersTabDisplayed() {
		log.info("Start of the method isSuppliersTabDisplayed");
		boolean isSuppliersTabDisplayed = driver.isPresentAndVisible(suppliersTab);
		log.info("End of the method isSuppliersTabDisplayed");
		return isSuppliersTabDisplayed;
	}

}