package io.ient.test.pages.enterprise;


import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;


public class UserManagementPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(UserManagementPage.class);
	LogFactory log = new LogFactory(logger);
	private static final By userManagementTab = By.cssSelector("div[id='user-manage-card']");
	private static final By addButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static final By userNameTextBox = By.cssSelector("input[name='data[username]']");
	private static final By firstNameTextBox = By.cssSelector("input[name='data[firstName]']");
	private static final By lastNameTextBox = By.cssSelector("input[name='data[lastName]']");
	private static final By supplierDropDown = By.xpath("//div[select[@id='supplier']]");
	private static String supplierDropDownOption = "div[id*='supplier-item-choice'][data-id='%s']>span";
	private static final By emailAddressTextField = By.cssSelector("input[name='data[email]']");
	private static final By titleTextField = By.cssSelector("input[name='data[title]']");
	private static final By roleTextBox = By.cssSelector("select[id='roles']+div+input");
	private static final By roleDropdown = By.xpath("//div[select[@id='roles']]");
	private static String roleDropdownOption = "//div[@role='option' and span[text()='%s']]/span";
	private static final By salutationDropdown = By.xpath("//select[@name='data[salutation]']/parent::div");
	private static String salutationDropdownOption = "//div[@role='option' and span[text()='%s']]/span";
	private static final By businessOrganizationDropdown = By.xpath("//select[@name='data[businessOrganization]']/parent::div");
	private static String businessOrganizationDropdownOption = "//div[@role='option' and span[text()='%s']]/span";
	private static final By plantDropdown = By.xpath("//select[@name='data[plant]']/parent::div");
	private static final By plantDropdownNoOptions = By.xpath("//div[contains(@class,'plant')]/descendant::div[contains(text(),'No choices to choose from')]");
	private static String plantDropdownOption = "//div[@role='option' and span[text()='%s']]/span";
	private static final By supplierDropdown = By.xpath("//select[@name='data[supplier]']/parent::div");
	private static String supplierDropdownOption = "//div[@role='option' and span[text()='%s']]/span";
	private static final By statusDropdown = By.xpath("//div[select[@id='active']]");
	private static String statusDropdownOption = "//div[@role='option' and span[text()='%s']]";
	private static final By submitButton = By.cssSelector("button[name='data[submit]']");
	private static String userRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']] and td[span[text()=' %s ']]]";
	private static String userLatNameRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']] ]/td[5]/span";
	private static String roleUserRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']] and td[span[contains(text(),' %s')]]]";
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static final By columnSelectionButton = By.cssSelector("button[id='ient-table-col-selection-btn']");
	private static final By filterButton = By.cssSelector("button[id='ient-table-filter-btn']");
	private static final By createNewUserButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static final By topLeftVerticalIcon = By.cssSelector("i[id='ient-table-more-action-btn']");
	private static final By importFromAribaOption = By.cssSelector("div[id='importFromAriba']");
	private static final By importFromLocalOption = By.cssSelector("div[id='importFromLocal']");
	private static final By usersListRows = By.xpath("//td[contains(@class,'ient-table-body') and span[contains(text(),' USER:')]]");
	private static String columnNameText = "//div[@class='ient-table-header-content-label' and text()=' %s ']";
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By editLink = By.cssSelector("div[id='edit']");
	private static final By deleteLink = By.cssSelector("div[id='delete']");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By noRecordsAvailableTextMessage = By.xpath("//div[@class='ui red message ng-star-inserted' and text()=' No records available ']");
	private static final By itemsPerPageDropdown = By.cssSelector("div[id='items-per-page']");
	private static final By maxItemsPerPage = By.cssSelector("div[id='page-item-20']");
	private static final By cancelButton = By.cssSelector("button[id='cancel']");
	private static final By confirmationMessageText = By.cssSelector("div[class='confirm-container']>div[class='message']");
	private static final By confirmationYesButton = By.xpath("//div[@class='btns']/button[text()='Yes']");

	private static String columnFieldSelection = "//div[label[contains(text(),'%s')]]/input|//div[label[contains(text(),'%s')]]";
	private static String columnFieldSelection1 = "//div[label[contains(text(),'%s')]]";
	private static String columnFieldSelection2 = "//div[label[contains(text(),'%s')] and contains(@class,'checked')]";
	private static final By columnSelectionCloseBtn = By.cssSelector("button[id='reorder-close-btn']");

	public UserManagementPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - UserManagementPage");
	}

	/***
	 * Clicks on user management tab
	 */
	public void clickOnUserManagementTab() {
		log.info("Start of the method clickOnUserManagementTab");
		driver.safeClick(userManagementTab);
		log.info("End of the method clickOnUserManagementTab");
	}

	/***
	 * Clicks on add button
	 */
	public void clickOnAddButton() {
		log.info("Start of the method clickOnAddButton");
		driver.safeClick(addButton);
		log.info("End of the method clickOnAddButton");
	}

	/**
	 * Verifies whether confirmation message is displayed
	 * @param userName : name of user in string format
	 */
	public boolean isConfirmationMessageDisplayed(String userName, String expectedConfirmationMessage) {
		log.info("Start of the method isConfirmationMessageDisplayed");
		driver.safeClick(addButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeType(userNameTextBox, userName);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(cancelButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		String actualConfirmationMessage = driver.safeGetText(confirmationMessageText);
		boolean isConfirmationMessageDisplayed = actualConfirmationMessage.equals(expectedConfirmationMessage);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(confirmationYesButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		log.info("End of the method isConfirmationMessageDisplayed");
		return isConfirmationMessageDisplayed;
	}

	/**
	 * Verifies whether submit button disabled
	 */
	public boolean isSubmitButtonDisabled() {
		log.info("Start of the method isSubmitButtonDisabled");
		String disabledText = driver.getAttribute(submitButton, "disabled");
		boolean isSubmitButtonDisabled = disabledText.equals("true");
		log.info("End of the method isSubmitButtonDisabled");
		return isSubmitButtonDisabled;
	}

	/***
	 * Creates a new user
	 * @param userName : name of user in string format
	 * @param firstName : firstname of a user in string format
	 * @param lastName : lastname of a user in string format
	 * @param emailAddress : emailAddress of a user in string format
	 * @param salutation : salutation in string format
	 * @param role : role in string format
	 * @param status : status in string format
	 */
	public void createUser(String userName, String firstName, String lastName, String emailAddress, String role, String salutation, String status) {
		log.info("Start of the method createUser");
		driver.safeClick(addButton);
		driver.safeType(userNameTextBox, userName);
		driver.safeType(firstNameTextBox, firstName);
		driver.safeType(lastNameTextBox, lastName);
		driver.safeType(emailAddressTextField, emailAddress);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(roleDropdown);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(By.xpath(String.format(roleDropdownOption, role)));
		driver.safeClick(salutationDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(salutationDropdownOption, salutation)));
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createUser");
	}

	/***
	 * Creates a new user
	 * @param userName : name of user in string format
	 * @param firstName : firstname of a user in string format
	 * @param lastName : lastname of a user in string format
	 * @param emailAddress : emailAddress of a user in string format
	 * @param salutation : salutation in string format
	 * @param role : role in string format
	 * @param status : status in string format
	 */
	public void createUser(String userName, String firstName, String lastName, String emailAddress, String role, String salutation, String status, String supplier) {
		log.info("Start of the method createUser");
		driver.safeClick(addButton);
		driver.safeType(userNameTextBox, userName);
		driver.safeType(firstNameTextBox, firstName);
		driver.safeType(lastNameTextBox, lastName);
		driver.safeType(emailAddressTextField, emailAddress);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.scrollToElementViaJavaScript(statusDropdown);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(roleTextBox);
		driver.safeClearAndTypeByActionsClassWithoutEnter(roleTextBox, role);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.getWrappedDriver().findElement(roleTextBox).sendKeys(Keys.ARROW_LEFT);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(roleDropdownOption, role)));
		driver.safeClick(salutationDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(salutationDropdownOption, salutation)));
		driver.safeClick(supplierDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(supplierDropdownOption, supplier)));
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createUser");
	}

	/***
	 * Creates a new user
	 * @param userName : name of user in string format
	 * @param firstName : firstname of a user in string format
	 * @param lastName : lastname of a user in string format
	 * @param emailAddress : emailAddress of a user in string format
	 * @param salutation : salutation in string format
	 * @param role : role in string format
	 * @param status : status in string format
	 * @param businessOrganization : business organization in string format
	 */
	public void createUser(String userName, String firstName, String lastName, String emailAddress, String role, String salutation, String status, String businessOrganization, String title) {
		log.info("Start of the method createUser");
		driver.safeClick(addButton);
		driver.safeType(userNameTextBox, userName);
		driver.safeType(firstNameTextBox, firstName);
		driver.safeType(lastNameTextBox, lastName);
		driver.safeType(titleTextField, title);
		driver.safeType(emailAddressTextField, emailAddress);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.scrollToElementViaJavaScript(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(roleDropdown);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(By.xpath(String.format(roleDropdownOption, role)));
		driver.safeClick(salutationDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(salutationDropdownOption, salutation)));
		driver.safeClick(businessOrganizationDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(businessOrganizationDropdownOption, businessOrganization)));
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createUser");
	}

	/***
	 * Creates a new user
	 * @param userName : name of user in string format
	 * @param firstName : firstname of a user in string format
	 * @param lastName : lastname of a user in string format
	 * @param emailAddress : emailAddress of a user in string format
	 * @param salutation : salutation in string format
	 * @param role : role in string format
	 * @param status : status in string format
	 * @param businessOrganization : business organization in string format
	 * @param title : title in string format
	 * @param plantName : plant name in string format
	 */
	public void createUser(String userName, String firstName, String lastName, String emailAddress, String role, String salutation, String status, String businessOrganization, String title, String plantName) {
		log.info("Start of the method createUser");
		driver.safeClick(addButton);
		driver.safeType(userNameTextBox, userName);
		driver.safeType(firstNameTextBox, firstName);
		driver.safeType(lastNameTextBox, lastName);
		driver.safeType(titleTextField, title);
		driver.safeType(emailAddressTextField, emailAddress);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.scrollToElementViaJavaScript(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(roleDropdown);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(By.xpath(String.format(roleDropdownOption, role)));
		driver.safeClick(salutationDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(salutationDropdownOption, salutation)));
		driver.safeClick(businessOrganizationDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(businessOrganizationDropdownOption, businessOrganization)));
		driver.safeClick(plantDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(plantDropdownOption, plantName)));
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createUser");
	}

	/**
	 * Verifies whether user is displayed
	 * @param userName : username of user in string format
	 */
	public boolean isUserDisplayed(String userName, String firstName) {
		log.info("Start of the method isUserDisplayed");
		boolean isUserDisplayed = driver.isPresentAndVisible(By.xpath(String.format(userRow, userName, firstName)));
		log.info("End of the method isUserDisplayed");
		return isUserDisplayed;
	}

	/***
	 * Searches user by name
	 * @param username : username of user in string format
	 */
	public void searchUserByUserName(String username) {
		log.info("Start of the method searchUserByUserName");
		driver.safeType(searchTextBox, username);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(searchIconButton);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchUserByUserName");
	}

	public void clearSearchTextField() {
		log.info("Start of the method clearSearchTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearSearchTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	public void clickOnImportFromAriba() {
		log.info("Start of the method clickOnImportFromAriba");
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(topLeftVerticalIcon);
		driver.safeClick(importFromAribaOption);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		log.info("End of the method clickOnImportFromAriba");
	}

	/***
	 * Edits existing user
	 * @param firstName : firstName of user in string format
	 *
	 */
	public void editUser(String firstName) {
		log.info("Start of the method editUser");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.safeType(firstNameTextBox, firstName);
		driver.safeClick(submitButton);
		log.info("End of the method editUser");
	}

	/***
	 * Deletes user
	 */
	public void deleteUser() {
		log.info("Start of the method deleteUser");
		driver.safeClick(verticalIconButton);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deleteUser");
	}

	/**
	 * Verifies No Records Available Message
	 * @return returns message display status
	 */
	public boolean verifyNoRecordsMessage() {
		log.info("Start of the method verifyNoRecordsMessage");
		boolean isMessageDisplayed = driver.isPresentAndVisible(noRecordsAvailableTextMessage);
		log.info("End of the method verifyNoRecordsMessage");
		return isMessageDisplayed;
	}

	/**
	 * Verifies whether impersonate user is displayed
	 * @param userName : username of user in string format
	 * @param role : role of user in string format
	 */
	public boolean isImpersonateUserDisplayed(String userName, String role) {
		log.info("Start of the method isImpersonateUserDisplayed");
		driver.sleep(WaitTime.SMALL_WAIT);
		boolean isUserDisplayed = driver.isPresentAndVisible(By.xpath(String.format(roleUserRow, userName, role)));
		log.info("End of the method isImpersonateUserDisplayed");
		return isUserDisplayed;
	}

	/***
	 * Edits existing user role
	 * @param role : role of user in string format
	 * @param roleStatus : role status of user in boolean format
	 *
	 */
	public void editUserRole(String role, boolean roleStatus) {
		log.info("Start of the method editUserRole");
		if (!roleStatus) {
			driver.safeClick(verticalIconButton);
			driver.safeClick(editLink);
			driver.safeType(roleTextBox, role);
			driver.sleep(WaitTime.XSMALL_WAIT);
			driver.getWrappedDriver().findElement(roleTextBox).sendKeys(Keys.ARROW_LEFT);
			driver.sleep(WaitTime.XSMALL_WAIT);
			driver.safeClick(By.xpath(String.format(roleDropdownOption, role)));
			driver.safeClick(submitButton);
		} else
			log.info(String.format("User already associated with %s", role));
		log.info("End of the method editUserRole");
	}

	/***
	 * returns user last name
	 * @param userName : username of user in string format
	 */
	public String getUserLastName(String userName) {
		log.info("Start of the method getUserLastName");
		String userLastName = driver.safeGetText(By.xpath(String.format(userLatNameRow, userName)));
		log.info("End of the method getUserLastName");
		return userLastName;
	}

	/**
	 * Verifies whether SearchBar is displayed
	 */
	public boolean isSearchBarDisplayed() {
		log.info("Start of the method isSearchBarDisplayed");
		boolean isSearchBarDisplayed = driver.isPresentAndVisible(searchTextBox) && driver.isPresentAndVisible(searchIconButton);
		log.info("End of the method isSearchBarDisplayed");
		return isSearchBarDisplayed;
	}

	/**
	 * Verifies whether SelectColumns is displayed
	 */
	public boolean isSelectColumnsDisplayed() {
		log.info("Start of the method isSelectColumnsDisplayed");
		boolean isSelectColumnsDisplayed = driver.isPresentAndVisible(columnSelectionButton);
		log.info("End of the method isSelectColumnsDisplayed");
		return isSelectColumnsDisplayed;
	}

	/**
	 * Clicks on column selection icon
	 */
	public void clickOnColumnSelectionIcon() {
		log.info("Start of the method clickOnColumnSelectionIcon");
		driver.safeClick(columnSelectionButton);
		log.info("End of the method clickOnColumnSelectionIcon");
	}


	/**
	 * Verifies whether FilterIcon is displayed
	 */
	public boolean isFilterIconDisplayed() {
		log.info("Start of the method isFilterIconDisplayed");
		boolean isFilterIconDisplayed = driver.isPresentAndVisible(filterButton);
		log.info("End of the method isFilterIconDisplayed");
		return isFilterIconDisplayed;
	}

	/**
	 * Verifies whether NewUser button is displayed
	 */
	public boolean isNewUserButtonDisplayed() {
		log.info("Start of the method isNewUserButtonDisplayed");
		boolean isNewUserDisplayed = driver.isPresentAndVisible(createNewUserButton);
		log.info("End of the method isNewUserButtonDisplayed");
		return isNewUserDisplayed;
	}

	/**
	 * Verifies whether ImportFromAriba is displayed
	 */
	public boolean isImportFromAribaDisplayed() {
		log.info("Start of the method isImportFromAribaDisplayed");
		boolean isImportFromAribaDisplayed = driver.isPresentAndVisible(importFromAribaOption);
		log.info("End of the method isImportFromAribaDisplayed");
		return isImportFromAribaDisplayed;
	}

	/**
	 * Clicks on TopLeftVerticalIcon
	 */
	public void clickOnTopLeftVerticalIcon() {
		log.info("Start of the method clickOnTopLeftVerticalIcon");
		driver.safeClick(topLeftVerticalIcon);
		log.info("End of the method clickOnTopLeftVerticalIcon");
	}

	/**
	 * Verifies whether ImportFromLocal is displayed
	 */
	public boolean isImportFromLocalDisplayed() {
		log.info("Start of the method isImportFromLocalDisplayed");
		boolean isImportFromLocalDisplayed = driver.isPresentAndVisible(importFromLocalOption);
		log.info("End of the method isImportFromLocalDisplayed");
		return isImportFromLocalDisplayed;
	}

	/**
	 * Verifies users list
	 */
	public boolean verifyUsersList() {
		log.info("Start of the method verifyUsersList");
		boolean verifyUsersList = driver.getXpathCount(usersListRows)>0;
		log.info("End of the method verifyUsersList");
		return verifyUsersList;
	}

	/**
	 * Verifies whether page column is displayed
	 * @param columnName : columnName in string format
	 */
	public boolean isPageColumnDisplayed(String columnName) {
		log.info("Start of the method isPageColumnDisplayed");
		boolean isPageColumnDisplayed = driver.isPresentAndVisible(By.xpath(String.format(columnNameText, columnName)));
		log.info("End of the method isPageColumnDisplayed");
		return isPageColumnDisplayed;
	}

	/**
	 * Verifies whether Edit And Delete is displayed
	 */
	public boolean isActionEditAndDeleteDisplayed() {
		log.info("Start of the method isActionEditAndDeleteDisplayed");
		driver.safeClick(verticalIconButton);
		boolean isActionEditAndDeleteDisplayed = driver.isPresentAndVisible(editLink) && driver.isPresentAndVisible(deleteLink);
		log.info("End of the method isActionEditAndDeleteDisplayed");
		return isActionEditAndDeleteDisplayed;
	}

	/**
	 * Edits user status
	 * @param status : status of user in string format
	 */
	public void editUserStatus(String status) {
		log.info("Start of the method editUserStatus");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(submitButton);
		log.info("End of the method editUserStatus");
	}

	/**
	 * Edits user supplier
	 * @param supplier : supplier user in string format
	 */
	public void editUserSupplier(String supplier) {
		log.info("Start of the method editUserSupplier");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.safeClick(supplierDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(supplierDropdownOption, supplier)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(submitButton);
		log.info("End of the method editUserSupplier");
	}

	/**
	 * Verifies whether number of items per page is editable
	 */
	public boolean isNumberOfItemsPerPageEditable() {
		log.info("Start of the method isNumberOfItemsPerPageEditable");
		driver.safeClick(itemsPerPageDropdown);
		boolean isNumberOfItemsPerPageEditable = driver.isPresentAndVisible(maxItemsPerPage);
		log.info("End of the method isNumberOfItemsPerPageEditable");
		return isNumberOfItemsPerPageEditable;
	}

	/***
	 * Creates a new supplier user
	 * @param userName : name of user in string format
	 * @param firstName : firstname of a user in string format
	 * @param lastName : lastname of a user in string format
	 * @param emailAddress : emailAddress of a user in string format
	 * @param salutation : salutation in string format
	 * @param role : role in string format
	 * @param supplier : supplier in string format
	 * @param status : status in string format
	 */
	public void createSupplierUser(String userName, String firstName, String lastName, String emailAddress, String role, String salutation, String supplier, String status) {
		log.info("Start of the method createSupplierUser");
		driver.safeClick(addButton);
		driver.safeType(userNameTextBox, userName);
		driver.safeType(firstNameTextBox, firstName);
		driver.safeType(lastNameTextBox, lastName);
		driver.safeType(emailAddressTextField, emailAddress);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(roleDropdown);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(roleDropdownOption, role)));
		driver.safeClick(salutationDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(salutationDropdownOption, salutation)));
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.jSClick(supplierDropDown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.cssSelector(String.format(supplierDropDownOption, supplier)));
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createSupplierUser");
	}

	/***
	 * Creates a new supplier user
	 * @param userName : name of user in string format
	 * @param firstName : firstname of a user in string format
	 * @param lastName : lastname of a user in string format
	 * @param emailAddress : emailAddress of a user in string format
	 * @param salutation : salutation in string format
	 * @param role : role in string format
	 * @param supplier : supplier in string format
	 * @param status : status in string format
	 * @param businessOrganization : business organization in string format
	 * @param title : title in string format
	 * @param plantName : plant name in string format
	 */
	public void createSupplierUser(String userName, String firstName, String lastName, String emailAddress, String role, String salutation, String supplier, String status, String businessOrganization, String title, String plantName) {
		log.info("Start of the method createSupplierUser");
		driver.safeClick(addButton);
		driver.safeType(userNameTextBox, userName);
		driver.safeType(firstNameTextBox, firstName);
		driver.safeType(lastNameTextBox, lastName);
		driver.safeType(emailAddressTextField, emailAddress);
		driver.safeType(titleTextField, title);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(roleDropdown);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(roleDropdownOption, role)));
		driver.safeClick(salutationDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(salutationDropdownOption, salutation)));
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.jSClick(supplierDropDown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.cssSelector(String.format(supplierDropDownOption, supplier)));
		driver.safeClick(businessOrganizationDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(businessOrganizationDropdownOption, businessOrganization)));
		if(!plantName.equals("None")) {
			driver.safeClick(plantDropdown);
			driver.sleep(WaitTime.XSMALL_WAIT);
			driver.safeClick(By.xpath(String.format(plantDropdownOption, plantName)));
		}
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createSupplierUser");
	}

	/***
	 * Verifies plant dropdown options
	 * @param userName : name of user in string format
	 * @param firstName : firstname of a user in string format
	 * @param lastName : lastname of a user in string format
	 * @param emailAddress : emailAddress of a user in string format
	 * @param salutation : salutation in string format
	 * @param role : role in string format
	 * @param supplier : supplier in string format
	 * @param status : status in string format
	 * @param businessOrganization : business organization in string format
	 * @param title : title in string format
	 * @param plantName : plant name in string format
	 */
	public boolean verifyPlantDropdownOptions(String userName, String firstName, String lastName, String emailAddress, String role, String salutation, String supplier, String status, String businessOrganization, String title, String plantName) {
		log.info("Start of the method verifyPlantDropdownOptions");
		boolean noOptionsDisplayStatus = false;
		driver.safeType(userNameTextBox, userName);
		driver.safeType(firstNameTextBox, firstName);
		driver.safeType(lastNameTextBox, lastName);
		driver.safeType(emailAddressTextField, emailAddress);
		driver.safeType(titleTextField, title);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(roleDropdown);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(roleDropdownOption, role)));
		driver.safeClick(salutationDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(salutationDropdownOption, salutation)));
		driver.sleep(WaitTime.MEDIUM_WAIT);
		if(!supplier.equals("None")) {
			driver.jSClick(supplierDropDown);
			driver.sleep(WaitTime.XSMALL_WAIT);
			driver.safeClick(By.cssSelector(String.format(supplierDropDownOption, supplier)));
		}
		driver.safeClick(businessOrganizationDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(businessOrganizationDropdownOption, businessOrganization)));
		driver.safeClick(plantDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		noOptionsDisplayStatus = driver.isPresentAndVisible(plantDropdownNoOptions);
		log.info("End of the method verifyPlantDropdownOptions");
		return noOptionsDisplayStatus;
	}

	/***
	 * Edits existing user from profile
	 * @param firstName : firstName of user in string format
	 *
	 */
	public void editUserProfile(String firstName) {
		log.info("Start of the method editUser");
		driver.safeType(firstNameTextBox, firstName);
		driver.safeClick(submitButton);
		log.info("End of the method editUser");
	}

	public void selectColumnFields(String columnField) {
		log.info("Start of the method selectColumnFields");
		driver.safeToggleCheckBox(By.xpath(String.format(columnFieldSelection, columnField, columnField)));
		log.info("End of the method selectColumnFields");
	}

	public void selectColumnFields1(String columnField) {
		log.info("Start of the method selectColumnFields");

		if(!driver.isPresentAndVisible(By.xpath(String.format(columnFieldSelection2, columnField)))) {
			driver.jSClick(By.xpath(String.format(columnFieldSelection1, columnField)));
		}
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method selectColumnFields");
	}

	public void clickOnColumnSelectionCloseButton() {
		log.info("Start of the method clickOnColumnSelectionCloseButton");
		driver.safeClick(columnSelectionCloseBtn);
		log.info("End of the method clickOnColumnSelectionCloseButton");
	}

	public Integer getColumnNumber(String columnName)
	{
		log.info("Start of the method getColumnNumber");
		int columnNumber = driver.columnNumFromColumnHeading(columnName, By.xpath("//table[@class='ui striped table actual-table']/thead/tr"));
		log.info("End of the method getColumnNumber");
		return columnNumber;
	}

}