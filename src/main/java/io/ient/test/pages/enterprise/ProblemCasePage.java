package io.ient.test.pages.enterprise;

import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.framework.utils.extentreports.FileUploadUtils;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;


public class ProblemCasePage extends BasePage {

	private static final Logger logger = LogManager.getLogger(ProblemCasePage.class);
	FileUploadUtils fileUploadUtils = new FileUploadUtils(driver);
	LogFactory log = new LogFactory(logger);

	private static final By addButton = By.cssSelector("ient-button[icon='plus']>button");
	private static String problemCaseRow = "//tr[contains(@class,'ient-table-row') and td[a[contains(text(),'%s')]] and td[a[text()=' %s ']]]";
	private static String problemCaseVersionRow = "//tr[contains(@class,'ient-table-row') and td[a[text()=' %s ']] and td[a[text()=' %s ']] and td[span[text()=' %s ']]]";
	private static String problemCaseVersionRowText = "//tr[contains(@class,'ient-table-row') and td[a[text()=' %s ']] and td[a[text()=' %s ']]]/td[3]/span";
	private static String problemCaseDueDateRow = "//tr[contains(@class,'ient-table-row') and td[a[contains(text(),'%s')]] and td[a[text()=' %s ']]]";
	private static String problemCaseOwnerAssignee = "//p[span[contains(text(),'Owner')] and span[a[text()='%s']]]";
	private static String problemCaseSupplierAssignee = "//p[span[contains(text(),'Supplier Owner')] and span[a[text()='%s']]]";
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static final By searchUserCancelButton = By.xpath("//button[span[text()='Cancel']]");
	private static final By cancelButton = By.xpath("//button[@id='cancel-button']");
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By viewButton = By.cssSelector("div[id='view']");
	private static final By appButton = By.xpath("//*[@id='cdk-drop-list-3']/div/div");
	private static final By attachmentsTab = By.xpath("//a[span[text()='Attachments']]");
	private static final By noAttachmentMessageText = By.xpath("//div[@class='attachments-card__empty-list__message']");
	private static final By addAttachmentLink = By.cssSelector("div[class^='attachments-card__empty-list__link']");
	private static final By addAttachmentDisabledButton = By.cssSelector("div[class='add-attachment__footer']>ient-button[label='Add']>button[disabled]");
	private static final By pdfLogoText = By.cssSelector("div[class='logo']>i[class*='pdf']");
	private static final By addAttachmentCloseButton = By.cssSelector("button[class*='ient-dialog-header-icon']");
	private static final By attachmentLargeSizeMessage = By.xpath("//div[contains(text(),'File size is too large')]");
	private static final By historyTab = By.xpath("//a[span[text()='History']]");
	private static final By addDocumentButton = By.cssSelector("button[id^='ient-buttton-']");
	private static final By addAttachmentLabel = By.xpath("//div[contains(@class,'ient-dialog-content')]/descendant::ient-button[@label='Add']");
	private static final By uploadButton = By.cssSelector("input[type='file'][multiple]");
	private static final By attachmentTagDropdown = By.cssSelector("div[class='selected-container']>div>i[class='icon chevron down']");
	private static final By addVersionButton = By.xpath("//div[contains(@class,' active visible')]/descendant::div[text()=' Add version ']");
	private static final By attachmentDownloadButton = By.xpath("//a[text()='Download']");
	private static final By viewVersionButton = By.xpath("//div[contains(@class,' active visible')]/descendant::div[text()=' View versions ']");
	private static final By editVersionButton = By.xpath("//div[contains(@class,' active visible')]/descendant::div[text()=' Edit ']");
	private static final By deleteVersionButton = By.xpath("//div[contains(@class,' active visible')]/descendant::div[text()=' Delete ']");
	private static final By fileNameTextBox = By.xpath("//label[text()='File name']/following-sibling::input");
	private static final By editAttachmentTagDropdown = By.cssSelector("div[class='selected-container']");
	private static String editAttachmentTagDropdownOption = "//p[text()='%s']";
	private static String versionVerticalIcon = "//tr[td[div[div[@class='version__title' and span[text()='Version'] and contains(text(),'%s')]]]]/td/div/ient-button[@icon='ellipsis vertical']";
	private static final By versionDeleteButton = By.xpath("//div[contains(@class,'active selected')]/span[text()='Delete']");
	private static final By versionDeletePopUpYesButton = By.xpath("//span[text()='Yes']");
	private static final By deletePopUpYesButton = By.xpath("//span[text()='Yes']");
	private static final By versionDeleteDoneButton = By.xpath("//span[text()='Done']");
	private static final By editAttachmentSaveButton = By.cssSelector("ient-button[label='Save']");
	private static String attachmentTagDropdownOption = "//p[contains(text(),'%s')]";
	private static String attachmentWithVersionOnTop = "//table/tr[1]/descendant::div[div[@class='headings__filename' and text()='%s']]";
	private static String attachmentWithTag = "//tr/td/descendant::div[div[@class='headings__filename' and text()='%s']]/following::td/div/div[@class='headings__tag' and text()=' %s ']";
	private static String attachmentWithoutTag = "//tr/td/descendant::div[div[@class='headings__filename' and text()='%s']]";
	private static String attachmentVerticalIcon = "//div[@class='headings__filename' and text()='%s']/ancestor::tr/descendant::td[@class='actions']/div/i[@class='ellipsis vertical icon']";
	private static String attachmentTagHistoryText = "//p[text()='Added attachments (%s) by %s']";
	private static String removeAttachmentTagHistoryText = "//p[text()='Deleted attachment %s and all versions by %s']";
	private static String statusUpdateHistoryText = "//p[text()='%s completed Open Case']";
	private static String statusHistoryTextForAssignToSelf = "//p[text()='%s assigned to Self.']";
	private static final By attachmentSubmitButton = By.xpath("//button[text()='Submit']");
	private static String attachmentNameAndTagText = "//div[@class='headings__filename' and text()='%s']/ancestor::td/following-sibling::td/div/div[@class='headings__tag' and text()=' %s ']";
	private static String attachmentNameText = "//div[@class='headings__filename' and text()='%s']";
	private static String attachmentRemoveButton = "//div[contains(@class,'description') and text()='%s']/ancestor::div[@class='ng-star-inserted']/descendant::div/div[text()=' Remove ']";
	private static final By conversationsTagDropdown = By.cssSelector("div[class='selected-container']");
	private static String conversationsTagDropdownOption = "//p[text()='%s']";
	private static String conversationsTagsText = "//div[contains(@class,'comment-list-box')]/descendant::div[contains(@class,'comment-body') and text()=' %s ']/preceding-sibling::div/div[@class='comment-header-right']/div[text()='%s']";
	private static String conversationsTagsTextWithoutTag = "//div[contains(@class,'comment-list-box')]/descendant::div[contains(@class,'comment-body') and text()=' %s ']";
	private static String conversationsDraftFirstCommentText = "(//div[@class='comment-header-right']/div[text()='Draft']/ancestor::div[@class='comment'])[1]";
	private static String conversationsCommentEditButton = "//div[@class='comment' and div[contains(@class,'comment-body') and text()=' %s ']]/div/span[text()='Edit']";
	private static String conversationsTagsTextUsers = "//div[contains(@class,'comment-list-box')]/descendant::div[contains(@class,'comment-body') and text()=' %s ']";
	private static String conversationsTagsTextDeleteButton = "//div[contains(@class,'comment-list-box')]/descendant::div[contains(@class,'comment-body') and text()=' %s ']/following-sibling::div/span[text()='Delete']";
	private static String finalResponseDueDateText = "//div[@ref='component']/p[@ref='html' and contains(text(),'Final Response Due: ') and b[contains(text(),'%s')]]";
	private static String initialResponseDueDateText = "//div[@ref='component']/p[@ref='html' and contains(text(),'Initial Response Due: ') and b[contains(text(),'%s')]]";

	private static final By problemTypeDropDown = By.xpath("//div[select[@id='problemType']]");
	private static String problemTypeDropDownOption = "div[id*='problemType-item-choice'][data-id='%s']>span";
	private static String problemTypeDropDownOptionWithText = "//div[contains(@id,'problemType-item-choice')]/span[text()='%s']";
	private static String problemTypeDropDownOptions = "div[id*='problemType-item-choice']>span";
	private static final By primaryNonConformanceDropDown = By.xpath("//div[select[@id='primaryNonconformance']]");
	private static String primaryNonConformanceDropDownOption = "div[id*='primaryNonconformance-item-choice'][data-id='%s']>span";
	private static String primaryNonConformanceDropDownOptionWithText = "//div[contains(@id,'primaryNonconformance-item-choice')]/span[text()='%s']";
	private static String primaryNonConformanceDropDownOptions = "div[id*='primaryNonconformance-item-choice']>span";
	private static final By secondNonConformanceDropDown = By.xpath("//div[select[@id='secondaryNonconformance']]");
	private static String secondNonConformanceDropDownOption = "div[id*='secondaryNonconformance-item-choice'][data-id='%s']>span";
	private static String secondNonConformanceDropDownOptionWithText = "//div[contains(@id,'secondaryNonconformance-item-choice')]/span[text()='%s']";
	private static String secondNonConformanceDropDownOptions = "div[id*='secondaryNonconformance-item-choice']>span";
	private static final By partDropDown = By.xpath("//div[select[@name='data[part]']]");
	private static String partDropDownOption = "div[id*='choices--datapart'][data-id='%s']>span";
	private static final By buyerLocationDropDown = By.xpath("//div[select[@id='buyerLocation']]");
	private static String buyerLocationDropDownOption = "div[id*='buyerLocation-item-choice'][data-id='%s']>span";
	private static final By nextStateDropDown = By.xpath("//div[select[@id='caseStatus']]");
	private static String nextStateDropDownOption = "div[id*='caseStatus-item-choice'][data-value='%s']>span";
	private static final By priorityDropDown = By.xpath("//div[select[@id='priority']]");
	private static String priorityDropDownOption = "div[id*='priority-item-choice'][data-value='%s']>span";
	private static final By supplierDropDown = By.xpath("//div[select[@id='supplierId']]");
	private static String supplierDropDownOption = "div[id*='supplierId-item-choice'][data-id='1']>span";
	private static final By supplierLocationDropDown = By.xpath("//div[select[@id='supplierLocation']]");
	private static String supplierLocationDropDownOption = "div[id*='supplierLocation-item-choice'][data-id='%s']>span";
	private static final By shortDescriptionTextArea = By.cssSelector("textarea[id='shortDescription']");
	private static final By submitButton = By.xpath("//button[contains(text(),'Submit')]|//button[@type='submit']|//button[@type='button' and text()='Save']");
	private static final By problemCaseText = By.xpath("//div[@ref='nested-editProblemCase']/div/descendant::span[@class='formio-details__value' and contains(text(),'SCIM')]");
	private static final By editButton = By.cssSelector("button[id='ient-btn-edit']");
	private static final By editAttachmentButton = By.xpath("//div[text()=' Edit ']");
	private static final By partText = By.xpath("//div[div[select[@name='data[part]']]]/descendant::div[@data-item]/span");
	private static final By supplierText = By.xpath("//div[div[select[@name='data[supplierId]']]]/descendant::div[@data-item]/span");
	private static final By supplierLocationText = By.cssSelector("a[id='supplier_location_qv']");
	private static final By nextStateEditDropDown = By.xpath("//div[select[@id='caseStatus']]");
	private static String nextStateEditDropDownOption = "//select[contains(@name,'data[caseStatus]')]/option/span[text()='%s']";
	private static String nextStateEditDropDownOptionByText = "//span[text()='%s']";
	private static String nextStateEditDropDownOptionValue = "//div[span[text()='%s']]";
	private static final By problemDescriptionTextArea = By.cssSelector("textarea[name='data[problemDescription]']");
	private static final By productionPhaseDropDown = By.xpath("//div[select[@name='data[productionPhase]']]");
	private static String productionPhaseDropDownOption = "div[id*='productionPhase-item-choice'][data-id='%s']>span";
	private static final By problemQualifierDropDown = By.xpath("//div[select[@name='data[problemQualifier]']]");
	private static String problemQualifierDropDownOption = "div[id*='problemQualifier-item-choice'][data-id='%s']>span";
	private static final By conversationTab = By.xpath("//span[text()='Conversations']");
	private static final By commentTextArea = By.cssSelector("textarea[placeholder='Comment'],textarea[placeholder='External Comment'],textarea[placeholder='Internal Comment']");
	private static final By submitComment = By.xpath("//span[text()='Submit']");
	private static final By editDraftCommentSubmitButton = By.xpath("//div[@class='draft-toolbar']/following-sibling::div/descendant::button[span[text()='Submit']]");
	private static final By saveAsDraftButton = By.xpath("//button[span[text()='Save as draft']]");
	private static final By detailsTab = By.xpath("//span[text()='Details']");
	private static final By caseFormVerticalIconButton = By.xpath("//button[@id='action-menu-btn']");
	private static final By caseFormUpdateBuyerButton = By.xpath("//a[@id='delegate']");
	private static final By caseFormAssignSelfButton = By.xpath("//a[@id='claim']");
	private static final By caseFormExtendDueDateButton = By.xpath("//a[span[text()='Extend Due Date']]");
	private static final By caseFormUpdateSupplierButton = By.xpath("//a[@id='assignee']");
	private static final By userSearchTextField = By.cssSelector("input[placeholder*='Search users']");
	private static final By userSearchIcon = By.cssSelector("i[class='inverted circular search link icon']");
	private static final By userNameLink = By.xpath("(//div[@class='header'])[1]");
	private static final By assignUserSubmitButton = By.xpath("//button[span[contains(text(),'Submit')]]");
	private static final By dueDateTextField = By.cssSelector("input[placeholder='Start']");
	private static final By caseFormPopUpExtendDueDateButton = By.xpath("//button[span[text()='Extend Due Date']]");
	private static String extendDueDateDateText = "//table[@class='ui celled center aligned unstackable table day seven column']/tbody/tr/td[@class='link' and text()='%s']";
	private static String extendDueDateHourText = "//table[@class='ui celled center aligned unstackable table hour four column']/tbody/tr/td[@class='link' and text()='%s']";
	private static String extendDueDateMinuteText = "//table[@class='ui celled center aligned unstackable table minute three column']/tbody/tr/td[@class='link' and text()='%s']";
	private static final By columnSelectionButton = By.cssSelector("button[id='ient-table-col-selection-btn']");
	private static final By filterButton = By.cssSelector("button[id='ient-table-filter-btn']");
	private static final By addNewButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static final By topLeftVerticalIcon = By.cssSelector("i[id='ient-table-more-action-btn']");
	private static final By downloadOption = By.xpath("//div[contains(@class,'item') and span[text()='Download Options']]");
	private static final By downloadAsPdfOption = By.xpath("//div[contains(@class,'item') and text()=' Download as PDF ']");
	private static final By downloadAsCsvOption = By.xpath("//div[contains(@class,'item') and text()=' Download as CSV ']");
	private static final By paginationDiv = By.cssSelector("div[class='ient-pagination']");
	private static final By totalCountDiv = By.cssSelector("div[class='total-count']");
	private static String columnNameText = "//div[@class='ient-table-header-content-label' and text()=' %s ']";
	private static final By itemsPerPageDropdown = By.cssSelector("div[id='items-per-page']");
	private static final By itemsPerPageDropdownOption = By.cssSelector("div[id='page-item-20']");
	private static final By editLink = By.xpath("div[id='edit']");
	private static final By deleteLink = By.xpath("div[id='delete']");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By tempPartCheckBox = By.cssSelector("input[name='data[tempPart]']");
	private static final By tempPartCheckBoxDiv = By.cssSelector("div[class*='tempPart']>div[class*='checkbox']");
	private static String commentTextAreaPlaceHolder = "textarea[placeholder='%s']";
	private static final By draftToggleLInk = By.cssSelector("div[class^='draft-toolbar-action']");
	private static final By conversationCommentSortOption = By.xpath("//option[text()='Newest to oldest']");


	public ProblemCasePage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - Problem Case Page");
	}

	/***
	 * Clicks on problem case tab
	 */
	public void clickOnProblemCaseTab() {
		log.info("Start of the method clickOnProblemCaseTab");
		driver.safeClick(appButton);
		log.info("End of the method clickOnProblemCaseTab");
	}

	/***
	 * Clicks on add button
	 */
	public void clickOnAddButton() {
		log.info("Start of the method clickOnAddButton");
		driver.safeClick(addButton);
		log.info("End of the method clickOnAddButton");
	}

	/**
	 * Creates a new problem case
	 * @param problemType : problem Type in string format
	 * @param primaryNonConformance : primary Nonconformance in string format
	 * @param secondaryNonConformance : secondary Nonconformance in string format
	 * @param part : part in string format
	 * @param buyerLocation : buyer Location in string format
	 * @param caseStatus : case Status in string format
	 * @param priority : priority in string format
	 * @param supplierLocation : supplier Location in string format
	 * @param shortDescription : short Description in string format
	 * @param supplier : supplier in string format
	 */
	public void createProblemCase(String problemType, String primaryNonConformance, String secondaryNonConformance, String part, String buyerLocation, String caseStatus, String priority, String supplierLocation, String shortDescription, String supplier) {
		log.info("Start of the method createProblemCase");
		driver.sleep(WaitTime.LONG_WAIT);
		driver.safeClick(addButton);
		driver.sleep(WaitTime.LONG_WAIT);
		driver.jSClick(problemTypeDropDown);
		driver.safeClick(By.cssSelector(String.format(problemTypeDropDownOption, problemType)));
		driver.jSClick(primaryNonConformanceDropDown);
		driver.safeClick(By.cssSelector(String.format(primaryNonConformanceDropDownOption, primaryNonConformance)));
		driver.jSClick(secondNonConformanceDropDown);
		driver.safeClick(By.cssSelector(String.format(secondNonConformanceDropDownOption, secondaryNonConformance)));
		driver.jSClick(partDropDown);
		driver.safeClick(By.cssSelector(String.format(partDropDownOption, part)));
		driver.jSClick(buyerLocationDropDown);
		driver.safeClick(By.cssSelector(String.format(buyerLocationDropDownOption, buyerLocation)));
		driver.jSClick(nextStateDropDown);
		driver.safeClick(By.cssSelector(String.format(nextStateDropDownOption, caseStatus)));
		driver.jSClick(priorityDropDown);
		driver.safeClick(By.cssSelector(String.format(priorityDropDownOption, priority)));
		driver.sleep(WaitTime.XSMALL_WAIT);
//		driver.jSClick(supplierDropDown);
//		driver.sleep(WaitTime.XSMALL_WAIT);
//      driver.safeClick(By.cssSelector(String.format(supplierDropDownOption, supplier)));
		driver.jSClick(supplierLocationDropDown);
		driver.safeClick(By.cssSelector(String.format(supplierLocationDropDownOption, supplierLocation)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeType(shortDescriptionTextArea, shortDescription);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(submitButton);
		log.info("End of the method createProblemCase");
	}

	/**
	 * Returns problem case id
	 * @return returns problemCaseId
	 */
	public String getProblemCaseId() {
		log.info("Start of the method getProblemCaseId");
		String problemCaseID = driver.safeGetText(problemCaseText);
		log.info("End of the method getProblemCaseId");
		return problemCaseID;
	}

	/**
	 * Returns problem case selected partproblemCaseSelectedPart
	 * @return returns problemCaseSelectedPart
	 */
	public String getProblemCaseSelectedPart() {
		log.info("Start of the method getProblemCaseSelectedPart");
		String problemCaseSelectedPart = driver.safeGetText(partText);
		log.info("End of the method getProblemCaseSelectedPart");
		return problemCaseSelectedPart;
	}
	/**
	 * Returns problem case selected supplier
	 * @return returns problemCaseSelectedSupplier
	 */
	public String getProblemCaseSelectedSupplier() {
		log.info("Start of the method getProblemCaseSelectedSupplier");
		String problemCaseSelectedSupplier = driver.safeGetText(supplierText);
		log.info("End of the method getProblemCaseSelectedSupplier");
		return problemCaseSelectedSupplier;
	}
	/**
	 * Returns problem case selected supplier location
	 * @return returns problemCaseSelectedSupplierLocation
	 */
	public String getProblemCaseSelectedSupplierLocation() {
		log.info("Start of the method getProblemCaseSelectedSupplierLocation");
		String problemCaseSelectedSupplierLocation = driver.safeGetText(supplierLocationText);
		log.info("End of the method getProblemCaseSelectedSupplierLocation");
		return problemCaseSelectedSupplierLocation;
	}

	/***
	 * Assign existing problem case to Supplier Admin
	 * @param conversationComment : conversation comment in string format
	 * @param problemDescription : problem description in string format
	 * @param productionPhase : production phase in string format
	 * @param problemQualifier : problem qualifier in string format
	 * @param nextState : next state in string format
	 */
	public void editProblemCase(String conversationComment, String problemDescription, String productionPhase, String problemQualifier, String nextState) {
		log.info("Start of the method editProblemCase");
		driver.safeClick(editButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(conversationTab);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeType(commentTextArea, conversationComment);
		driver.safeClick(submitComment);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(detailsTab);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeType(problemDescriptionTextArea, problemDescription);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.jSClick(productionPhaseDropDown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.cssSelector(String.format(productionPhaseDropDownOption, productionPhase)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.jSClick(problemQualifierDropDown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.cssSelector(String.format(problemQualifierDropDownOption, problemQualifier)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.jSClick(nextStateEditDropDown);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(nextStateEditDropDownOptionValue, nextState)));
		driver.safeClick(submitButton);
		driver.sleep(WaitTime.LONG_WAIT);
		log.info("End of the method editProblemCase");
	}

	/***
	 * Verifies whether problem case editable
	 */
	public boolean isProblemCaseEditable() {
		log.info("Start of the method isProblemCaseEditable");
		driver.safeClick(editButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		boolean isEditable = driver.isPresentAndVisible(submitButton) || driver.isPresentAndVisible(cancelButton);
		log.info("End of the method isProblemCaseEditable");
		return isEditable;
	}

	/**
	 * Verifies whether save and cancel button displayed
	 */
	public boolean isSaveAndCancelButtonDisplayed() {
		log.info("Start of the method isSaveAndCancelButtonDisplayed");
		boolean isSaveAndCancelButtonDisplayed = driver.isPresentAndVisible(submitButton) || driver.isPresentAndVisible(cancelButton);
		log.info("End of the method isSaveAndCancelButtonDisplayed");
		return isSaveAndCancelButtonDisplayed;
	}

	/***
	 * navigates to edit view of problem case
	 */
	public void goToEditView() {
		log.info("Start of the method goToEditView");
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(editButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method goToEditView");
	}

	/***
	 * Assign existing problem case to Supplier Admin
	 * @param conversationComment : conversation comment in string format
	 * @param nextState : next state in string format
	 */
	public void editProblemCase(String conversationComment, String nextState) {
		log.info("Start of the method editProblemCase");
		driver.safeClick(editButton);
		driver.safeClick(conversationTab);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeType(commentTextArea, conversationComment);
		driver.safeClick(submitComment);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(detailsTab);
		driver.jSClick(nextStateEditDropDown);
		if(driver.isPresentAndVisible(By.xpath(String.format(nextStateEditDropDownOption, nextState)))) {
			driver.safeClick(By.xpath(String.format(nextStateEditDropDownOption, nextState)));
		} else {
			driver.safeClick(By.xpath(String.format(nextStateEditDropDownOptionByText, nextState)));
		}
		driver.safeClick(submitButton);
		log.info("End of the method editProblemCase");
	}

	/**
	 * Returns problem case version
	 * @param problemCaseId : problem case id in string format
	 * @param status : status in string format
	 * @return returns problemCase version
	 */
	public String getProblemCaseVersion(String problemCaseId, String status) {
		log.info("Start of the method getProblemCaseVersion");
		String problemCaseVersion = driver.safeGetText(By.xpath(String.format(problemCaseVersionRowText, problemCaseId, status)));
		log.info("End of the method getProblemCaseVersion");
		return problemCaseVersion;
	}

	/**
	 * Verifies whether problem case is displayed
	 * @param problemCaseId : problem case id in string format
	 */
	public boolean isProblemCaseDisplayed(String problemCaseId, String status) {
		log.info("Start of the method isProblemCaseDisplayed");
		boolean isProblemCaseDisplayed = driver.isPresentAndVisible(By.xpath(String.format(problemCaseRow, problemCaseId, status)));
		log.info("End of the method isProblemCaseDisplayed");
		return isProblemCaseDisplayed;
	}

	/**
	 * Verifies whether problem case temp part checkbox is displayed
	 */
	public boolean isProblemCaseTempPartCheckboxDisplayed() {
		log.info("Start of the method isProblemCaseTempPartCheckboxDisplayed");
		boolean isProblemCaseTempPartCheckboxDisplayed = driver.isPresentAndVisible(tempPartCheckBox);
		log.info("End of the method isProblemCaseTempPartCheckboxDisplayed");
		return isProblemCaseTempPartCheckboxDisplayed;
	}

	/**
	 * Verifies whether problem case temp part checkbox div is displayed
	 */
	public boolean isProblemCaseTempPartCheckboxDivDisplayed() {
		log.info("Start of the method isProblemCaseTempPartCheckboxDivDisplayed");
		boolean isProblemCaseTempPartCheckboxDivDisplayed = driver.isPresentAndVisible(tempPartCheckBoxDiv);
		log.info("End of the method isProblemCaseTempPartCheckboxDivDisplayed");
		return isProblemCaseTempPartCheckboxDivDisplayed;
	}

	/**
	 * Verifies whether problem case is displayed
	 * @param problemCaseId : problem case id in string format
	 * @param status : status in string format
	 * @param version : version in string format
	 */
	public boolean isProblemCaseDisplayed(String problemCaseId, String status, String version) {
		log.info("Start of the method isProblemCaseDisplayed");
		boolean isProblemCaseDisplayed = driver.isPresentAndVisible(By.xpath(String.format(problemCaseVersionRow, problemCaseId, status, version)));
		log.info("End of the method isProblemCaseDisplayed");
		return isProblemCaseDisplayed;
	}

	/**
	 * Verifies whether problem case due date is displayed
	 * @param problemCaseId : problem case id in string format
	 */
	public boolean isProblemCaseDueDateDisplayed(String problemCaseId, String date) {
		log.info("Start of the method isProblemCaseDueDateDisplayed");
		boolean isProblemCaseDisplayed = driver.isPresentAndVisible(By.xpath(String.format(problemCaseDueDateRow, problemCaseId, date)));
		log.info("End of the method isProblemCaseDueDateDisplayed");
		return isProblemCaseDisplayed;
	}

	/***
	 * Searches problem case by id
	 * @param problemCaseID : problem case id in string format
	 */
	public void searchProblemCaseByID(String problemCaseID) {
		log.info("Start of the method searchProblemCaseByID");
		driver.safeClear(searchTextBox);
		driver.safeType(searchTextBox, problemCaseID);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(searchIconButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchProblemCaseByID");
	}

	/**
	 * Clears problem case text field
	 */
	public void clearProblemCaseTextField() {
		log.info("Start of the method clearProblemCaseTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearProblemCaseTextField");
	}

	/**
	 * Clicks on search icon
	 */
	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	/***
	 * View problem case id
	 */
	public void viewProblemCase() {
		log.info("Start of the method viewProblemCase");
		driver.safeClick(verticalIconButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(viewButton);
		log.info("End of the method viewProblemCase");
	}

	/**
	 * Clicks on case form vertical button
	 */
	public void clickOnCaseFormVerticalButton() {
		log.info("Start of the method clickOnCaseFormVerticalButton");
		driver.safeClick(caseFormVerticalIconButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		log.info("End of the method clickOnCaseFormVerticalButton");
	}

	/**
	 * Clicks on update buyer
	 */
	public void updateBuyer() {
		log.info("Start of the method updateBuyer");
		driver.safeClick(caseFormUpdateBuyerButton);
		log.info("End of the method updateBuyer");
	}

    /**
	 * Clicks on assign to self
	 */
	public void assignToSelf() {
		log.info("Start of the method assignToSelf");
		driver.safeClick(caseFormAssignSelfButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		log.info("End of the method assignToSelf");
	}

	/**
	 * Clicks on update supplier
	 */
	public void updateSupplier() {
		log.info("Start of the method updateSupplier");
		driver.safeClick(caseFormUpdateSupplierButton);
		log.info("End of the method updateSupplier");
	}

	/**
	 * Clicks on extend due date
	 */
	public void extendDueDate() {
		log.info("Start of the method extendDueDate");
		driver.safeClick(caseFormExtendDueDateButton);
		log.info("End of the method extendDueDate");
	}

	/***
	 * Searches user
	 * @param userName : name of user in string format
	 */
	public void searchUser(String userName) {
		log.info("Start of the method searchUser");
		driver.safeType(userSearchTextField, userName);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(userSearchIcon);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(userNameLink);
		driver.safeClick(assignUserSubmitButton);
		log.info("End of the method searchUser");
	}

	/***
	 * Searches user
	 * @param userName : name of user in string format
	 */
	public boolean isUserSearched(String userName) {
		log.info("Start of the method isUserSearched");
		boolean isUserSearched = false;
		driver.safeType(userSearchTextField, userName);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(userSearchIcon);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		isUserSearched = driver.isPresentAndVisible(userNameLink);
		log.info("End of the method isUserSearched");
		return isUserSearched;
	}

	/**
	 * Clicks on search user cancel button
	 */
	public void clickOnSearchUserCancelButton() {
		log.info("Start of the method clickOnSearchUserCancelButton");
		driver.safeClick(searchUserCancelButton);
		log.info("End of the method clickOnSearchUserCancelButton");
	}

	/**
	 * Clicks on submit button
	 */
	public void clickOnSubmitButton() {
		log.info("Start of the method clickOnSubmitButton");
		driver.safeClick(attachmentSubmitButton);
		log.info("End of the method clickOnSubmitButton");
	}

	/**
	 * Verifies whether owner assignee is displayed
	 * @param assigneeUserName : assignee userName case id in string format
	 */
	public boolean isOwnerAssigned(String assigneeUserName) {
		log.info("Start of the method isOwnerAssigned");
		boolean isAssigneeDisplayed = driver.isPresentAndVisible(By.xpath(String.format(problemCaseOwnerAssignee, assigneeUserName)));
		log.info("End of the method isOwnerAssigned");
		return isAssigneeDisplayed;
	}

	/**
	 * Verifies whether supplier assignee is displayed
	 * @param assigneeUserName : assignee userName case id in string format
	 */
	public boolean isSupplierAssigned(String assigneeUserName) {
		log.info("Start of the method isSupplierAssigned");
		boolean isAssigneeDisplayed = driver.isPresentAndVisible(By.xpath(String.format(problemCaseSupplierAssignee, assigneeUserName)));
		log.info("End of the method isSupplierAssigned");
		return isAssigneeDisplayed;
	}

	/**
	 * extendDueDate : Extends due date
	 * @param date : Date in string format
	 * @param hour : hour string format
	 * @param minute : minute in string format
	 */
	public void extendDueDate(String date, String hour, String minute) {
		log.info("Start of the method extendDueDate");
		driver.safeClick(dueDateTextField);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(extendDueDateDateText, date)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(extendDueDateHourText, hour)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(extendDueDateMinuteText, minute)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(caseFormPopUpExtendDueDateButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		log.info("End of the method extendDueDate");
	}

	/**
	 * Clicks on attachments tab
	 */
	public void clickOnAttachmentsTab() {
		log.info("Start of the method clickOnAttachmentsTab");
		driver.safeClick(attachmentsTab);
		log.info("End of the method clickOnAttachmentsTab");
	}

	/**
	 * Clicks on history tab
	 */
	public void clickOnHistoryTab() {
		log.info("Start of the method clickOnHistoryTab");
		driver.safeClick(historyTab);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method clickOnHistoryTab");
	}

	/**
	 * Clicks on conversations tab
	 */
	public void clickOnConversationsTab() {
		log.info("Start of the method clickOnConversationsTab");
		driver.safeClick(conversationTab);
		log.info("End of the method clickOnConversationsTab");
	}

	/**
	 * Adds problem case attachments
	 * @param tagName : tag name in string format
	 * @param attachmentName : attachment name in string format
	 */
	public void addProblemCaseAttachments(String tagName, String attachmentName) {
		log.info("Start of the method addProblemCaseAttachments");
		driver.safeClick(addDocumentButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		fileUploadUtils.uploadFile(uploadButton, attachmentName);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(attachmentTagDropdown);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(attachmentTagDropdownOption, tagName)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(addAttachmentLabel);
		//driver.safeClick(attachmentSubmitButton);
		log.info("End of the method addProblemCaseAttachments");
	}

	/**
	 * Adds problem case attachments for supplier admin
	 * @param tagName : tag name in string format
	 * @param attachmentName : attachment name in string format
	 */
	public void addProblemCaseAttachmentsForSupplierAdmin(String tagName, String attachmentName) {
		log.info("Start of the method addProblemCaseAttachmentsForSupplierAdmin");
		driver.safeClick(addDocumentButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		fileUploadUtils.uploadFile(uploadButton, attachmentName);
		driver.sleep(WaitTime.MEDIUM_WAIT);
//		driver.safeClick(attachmentTagDropdown);
//		driver.sleep(WaitTime.SMALL_WAIT);
//		driver.safeClick(By.xpath(String.format(attachmentTagDropdownOption, tagName)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(addAttachmentLabel);
		//driver.safeClick(attachmentSubmitButton);
		log.info("End of the method addProblemCaseAttachmentsForSupplierAdmin");
	}

     /**
	 * Adds problem case attachments
	 * @param attachmentName : attachment name in string format
	 */
	public void addProblemCaseAttachmentsWithoutTag(String attachmentName) {
		log.info("Start of the method addProblemCaseAttachmentsWithoutTag");
		driver.safeClick(addDocumentButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		fileUploadUtils.uploadFile(uploadButton, attachmentName);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(addAttachmentLabel);
		log.info("End of the method addProblemCaseAttachmentsWithoutTag");
	}

	/**
	 * navigates to Add problem case attachments popup
	 * @param attachmentName : attachment name in string format
	 */
	public void goToAddAttachmentPopup(String attachmentName) {
		log.info("Start of the method goToAddAttachmentPopup");
		driver.safeClick(addDocumentButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		fileUploadUtils.uploadFile(uploadButton, attachmentName);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method goToAddAttachmentPopup");
	}

	/**
	 * Is tag attachment history displayed
	 * @param attachmentName : attachment name in string format
	 * @param userName : username in string format
	 */
	public boolean isProblemCaseTagAttachmentHistoryDisplayed(String attachmentName, String userName) {
		log.info("Start of the method isProblemCaseTagAttachmentHistoryDisplayed");
		boolean isProblemCaseTagAttachmentHistoryDisplayed = driver.isPresentAndVisible(By.xpath(String.format(attachmentTagHistoryText, attachmentName, userName)));
		log.info("End of the method isProblemCaseTagAttachmentHistoryDisplayed");
		return isProblemCaseTagAttachmentHistoryDisplayed;
	}

	/**
	 * Is tag attachment remove history displayed
	 * @param attachmentName : attachment name in string format
	 * @param userName : username in string format
	 */
	public boolean isProblemCaseTagAttachmentRemoveHistoryDisplayed( String attachmentName, String userName) {
		log.info("Start of the method isProblemCaseTagAttachmentRemoveHistoryDisplayed");
		boolean isProblemCaseTagAttachmentRemoveHistoryDisplayed = driver.isPresentAndVisible(By.xpath(String.format(removeAttachmentTagHistoryText, attachmentName, userName)));
		log.info("End of the method isProblemCaseTagAttachmentRemoveHistoryDisplayed");
		return isProblemCaseTagAttachmentRemoveHistoryDisplayed;
	}

	/**
	 * Is problem case update history displayed
	 * @param userName : username in string format
	 */
	public boolean isProblemCaseUpdateHistoryDisplayed(String userName) {
		log.info("Start of the method isProblemCaseUpdateHistoryDisplayed");
		boolean isProblemCaseUpdateHistoryDisplayed = driver.isPresentAndVisible(By.xpath(String.format(statusUpdateHistoryText, userName)));
		log.info("End of the method isProblemCaseUpdateHistoryDisplayed");
		return isProblemCaseUpdateHistoryDisplayed;
	}

/**
	 * Is problem case history displayed for assign to self
	 * @param userName : username in string format
	 */
	public boolean isProblemCaseHistoryDisplayedForAssignToSelf(String userName) {
		log.info("Start of the method isProblemCaseHistoryDisplayedForAssignToSelf");
		boolean isProblemCaseHistoryDisplayedForAssignToSelf = driver.isPresentAndVisible(By.xpath(String.format(statusHistoryTextForAssignToSelf, userName)));
		log.info("End of the method isProblemCaseHistoryDisplayedForAssignToSelf");
		return isProblemCaseHistoryDisplayedForAssignToSelf;
	}

	/**
	 * Verifies whether problem case attachment is displayed
	 * @param tagName : tag name in string format
	 * @param attachmentName : attachment name in string format
	 * @return returns problem case attachment status
	 */
	public boolean isProblemCaseAttachmentDisplayed(String tagName, String attachmentName) {
		log.info("Start of the method isProblemCaseAttachmentDisplayed");
		boolean isProblemCaseAttachmentDisplayed = driver.isPresentAndVisible(By.xpath(String.format(attachmentNameAndTagText, attachmentName, tagName)));
		log.info("End of the method isProblemCaseAttachmentDisplayed");
		return isProblemCaseAttachmentDisplayed;
	}

	/**
	 * Verifies whether problem case attachment is displayed
	 * @param attachmentName : attachment name in string format
	 * @return returns problem case attachment status
	 */
	public boolean isProblemCaseAttachmentDisplayedWithoutTag(String attachmentName) {
		log.info("Start of the method isProblemCaseAttachmentDisplayedWithoutTag");
		boolean isProblemCaseAttachmentDisplayedWithoutTag = driver.isPresentAndVisible(By.xpath(String.format(attachmentNameText, attachmentName)));
		log.info("End of the method isProblemCaseAttachmentDisplayedWithoutTag");
		return isProblemCaseAttachmentDisplayedWithoutTag;
	}

	/**
	 * Verifies whether tags dropdown is displayed
	 */
	public boolean isTagsDropdownDisplayed() {
		log.info("Start of the method isTagsDropdownDisplayed");
		boolean isTagsDropdownDisplayed = driver.isPresentAndVisible(attachmentTagDropdown);
		log.info("End of the method isTagsDropdownDisplayed");
		return isTagsDropdownDisplayed;
	}


	/**
	 * Adds problem case conversations
	 * @param tagName : tag name in string format
	 * @param conversationComment : conversation comment in string format
	 */
	public void addProblemCaseConversations(String tagName, String conversationComment) {
		log.info("Start of the method addProblemCaseConversations");
		driver.safeClick(conversationTab);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(conversationsTagDropdown);
		driver.safeClick(By.xpath(String.format(conversationsTagDropdownOption, tagName)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeType(commentTextArea, conversationComment);
		driver.safeClick(submitComment);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method addProblemCaseConversations");
	}

	/**
	 * Adds problem case conversations
	 * @param conversationComment : conversation comment in string format
	 */
	public void addProblemCaseConversationsWithoutTag(String conversationComment) {
		log.info("Start of the method addProblemCaseConversations");
		driver.safeClick(conversationTab);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeType(commentTextArea, conversationComment);
		driver.safeClick(submitComment);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method addProblemCaseConversations");
	}

	/**
	 * Adds problem case conversation draft
	 * @param tagName : tag name in string format
	 * @param conversationComment : conversation comment in string format
	 */
	public void addProblemCaseConversationDraft(String tagName, String conversationComment) {
		log.info("Start of the method addProblemCaseConversationDraft");
		driver.safeClick(conversationTab);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(conversationsTagDropdown);
		driver.safeClick(By.xpath(String.format(conversationsTagDropdownOption, tagName)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeType(commentTextArea, conversationComment);
		driver.safeClick(saveAsDraftButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method addProblemCaseConversationDraft");
	}

	/**
	 * Verifies whether problem case conversation comment is displayed
	 * @param tagName : tag name in string format
	 * @param conversationComment : conversation comment in string format
	 * @return returns problem case conversation comment displayed status
	 */
	public boolean isProblemCaseConversationCommentDisplayed(String tagName, String conversationComment) {
		log.info("Start of the method isProblemCaseConversationCommentDisplayed");
		boolean isProblemCaseConversationDisplayed = driver.isPresentAndVisible(By.xpath(String.format(conversationsTagsText, conversationComment, tagName)));
		log.info("End of the method isProblemCaseConversationCommentDisplayed");
		return isProblemCaseConversationDisplayed;
	}

	/**
	 * Verifies whether problem case conversation comment without tag is displayed
	 * @param conversationComment : conversation comment in string format
	 * @return returns problem case conversation comment displayed status
	 */
	public boolean isProblemCaseConversationCommentWithoutTagDisplayed(String conversationComment) {
		log.info("Start of the method isProblemCaseConversationCommentWithoutTagDisplayed");
		boolean isProblemCaseConversationDisplayed = driver.isPresentAndVisible(By.xpath(String.format(conversationsTagsTextWithoutTag, conversationComment)));
		log.info("End of the method isProblemCaseConversationCommentWithoutTagDisplayed");
		return isProblemCaseConversationDisplayed;
	}

	/**
	 * Returns problem case first conversation draft comment text
	 * @return returns problem case first conversation draft comment text
	 */
	public String getProblemCaseFirstConversationDraftCommentText() {
		log.info("Start of the method getProblemCaseFirstConversationDraftCommentText");
		String problemCaseFirstConversationDraftCommentText = driver.safeGetText(By.xpath(conversationsDraftFirstCommentText));
		log.info("End of the method getProblemCaseFirstConversationDraftCommentText");
		return problemCaseFirstConversationDraftCommentText;
	}

	/**
	 * Verifies whether problem case conversation comment edit is displayed
	 * @param conversationComment : conversation comment in string format
	 * @return returns problem case conversation comment edit button displayed status
	 */
	public boolean isProblemCaseConversationCommentEditDisplayed(String conversationComment) {
		log.info("Start of the method isProblemCaseConversationCommentEditDisplayed");
		boolean isProblemCaseConversationCommentEditDisplayed = driver.isPresentAndVisible(By.xpath(String.format(conversationsCommentEditButton, conversationComment)));
		log.info("End of the method isProblemCaseConversationCommentEditDisplayed");
		return isProblemCaseConversationCommentEditDisplayed;
	}

	/**
	 * Edits problem case conversation comment
	 * @param conversationComment : conversation comment in string format
	 * @param newConversationComment : new conversation comment in string format
	 * @return returns problem case conversation comment edit button displayed status
	 */
	public void editProblemCaseConversationComment(String conversationComment, String newConversationComment) {
		log.info("Start of the method editProblemCaseConversationComment");
		driver.safeClick(By.xpath(String.format(conversationsCommentEditButton, conversationComment)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(editDraftCommentSubmitButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		log.info("End of the method editProblemCaseConversationComment");
	}

	/**
	 * Deletes problem case conversations
	 * @param conversationComment : conversation comment in string format
	 */
	public void deleteConversationComment(String conversationComment) {
		log.info("Start of the method deleteConversationComment");
		driver.safeClick(By.xpath(String.format(conversationsTagsTextDeleteButton, conversationComment)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		log.info("End of the method deleteConversationComment");
	}

	/**
	 * Verifies whether problem case conversation comment is displayed
	 * @param conversationComment : conversation comment in string format
	 * @return returns problem case conversation comment displayed status
	 */
	public boolean isProblemCaseConversationCommentDisplayed_Users(String conversationComment) {
		log.info("Start of the method isProblemCaseConversationCommentDisplayed");
		boolean isProblemCaseConversationDisplayed = driver.isPresentAndVisible(By.xpath(String.format(conversationsTagsTextUsers, conversationComment)));
		log.info("End of the method isProblemCaseConversationCommentDisplayed");
		return isProblemCaseConversationDisplayed;
	}

	/**
	 * Verifies whether final response due date is displayed
	 * @param dateAndHour : date and hour in string format
	 * @return returns final response due date displayed status
	 */
	public boolean isFinalResponseDueDateAndTimeDisplayed(String dateAndHour) {
		log.info("Start of the method isFinalResponseDueDateAndTimeDisplayed");
		boolean isFinalResponseDueDateAndTimeDisplayed = driver.isPresentAndVisible(By.xpath(String.format(finalResponseDueDateText, dateAndHour)));
		log.info("End of the method isFinalResponseDueDateAndTimeDisplayed");
		return isFinalResponseDueDateAndTimeDisplayed;
	}

	/**
	 * Verifies whether initial response due date is displayed
	 * @param dateAndHour : date and hour in string format
	 * @return returns final response due date displayed status
	 */
	public boolean isInitialResponseDueDateAndTimeDisplayed(String dateAndHour) {
		log.info("Start of the method isInitialResponseDueDateAndTimeDisplayed");
		boolean isInitialResponseDueDateAndTimeDisplayed = driver.isPresentAndVisible(By.xpath(String.format(initialResponseDueDateText, dateAndHour)));
		log.info("End of the method isInitialResponseDueDateAndTimeDisplayed");
		return isInitialResponseDueDateAndTimeDisplayed;
	}

	/**
	 * Verifies whether SearchBar is displayed
	 */
	public boolean isSearchBarDisplayed() {
		log.info("Start of the method isSearchBarDisplayed");
		boolean isSearchBarDisplayed = driver.isPresentAndVisible(searchTextBox) && driver.isPresentAndVisible(searchIconButton);
		log.info("End of the method isSearchBarDisplayed");
		return isSearchBarDisplayed;
	}

	/**
	 * Verifies whether SelectColumns is displayed
	 */
	public boolean isSelectColumnsDisplayed() {
		log.info("Start of the method isSelectColumnsDisplayed");
		boolean isSelectColumnsDisplayed = driver.isPresentAndVisible(columnSelectionButton);
		log.info("End of the method isSelectColumnsDisplayed");
		return isSelectColumnsDisplayed;
	}

	/**
	 * Verifies whether FilterIcon is displayed
	 */
	public boolean isFilterIconDisplayed() {
		log.info("Start of the method isFilterIconDisplayed");
		boolean isFilterIconDisplayed = driver.isPresentAndVisible(filterButton);
		log.info("End of the method isFilterIconDisplayed");
		return isFilterIconDisplayed;
	}

	/**
	 * Verifies whether add new button is displayed
	 */
	public boolean isAddNewButtonDisplayed() {
		log.info("Start of the method isNewUserButtonDisplayed");
		boolean isAddNewButtonDisplayed = driver.isPresentAndVisible(addNewButton);
		log.info("End of the method isNewUserButtonDisplayed");
		return isAddNewButtonDisplayed;
	}

	/**
	 * Verifies whether DownloadAsPdf is displayed
	 */
	public boolean isDownloadAsPdfDisplayed() {
		log.info("Start of the method isDownloadAsPdfDisplayed");
		driver.safeMouseOver(downloadOption);
		boolean isDownloadAsPdfDisplayed = driver.isPresentAndVisible(downloadAsPdfOption);
		log.info("End of the method isDownloadAsPdfDisplayed");
		return isDownloadAsPdfDisplayed;
	}

	/**
	 * Verifies whether DownloadAsCsv is displayed
	 */
	public boolean isDownloadAsCsvDisplayed() {
		log.info("Start of the method isDownloadAsCsvDisplayed");
		driver.safeMouseOver(downloadOption);
		boolean isDownloadAsCsvDisplayed = driver.isPresentAndVisible(downloadAsCsvOption);
		log.info("End of the method isDownloadAsCsvDisplayed");
		return isDownloadAsCsvDisplayed;
	}

	/**
	 * Clicks on TopLeftVerticalIcon
	 */
	public void clickOnTopLeftVerticalIcon() {
		log.info("Start of the method clickOnTopLeftVerticalIcon");
		driver.safeClick(topLeftVerticalIcon);
		log.info("End of the method clickOnTopLeftVerticalIcon");
	}

	/**
	 * Verifies whether page column is displayed
	 * @param columnName : columnName in string format
	 */
	public boolean isPageColumnDisplayed(String columnName) {
		log.info("Start of the method isPageColumnDisplayed");
		boolean isPageColumnDisplayed = driver.isPresentAndVisible(By.xpath(String.format(columnNameText, columnName)));
		log.info("End of the method isPageColumnDisplayed");
		return isPageColumnDisplayed;
	}

	/**
	 * Verifies whether Edit And Delete is displayed
	 */
	public boolean isActionEditAndDeleteDisplayed() {
		log.info("Start of the method isActionEditAndDeleteDisplayed");
		driver.safeClick(verticalIconButton);
		boolean isActionEditAndDeleteDisplayed = driver.isPresentAndVisible(editLink) && driver.isPresentAndVisible(deleteLink);
		log.info("End of the method isActionEditAndDeleteDisplayed");
		return isActionEditAndDeleteDisplayed;
	}

	/**
	 * Verifies whether number of items per page is editable
	 */
	public boolean isNumberOfItemsPerPageEditable() {
		log.info("Start of the method isNumberOfItemsPerPageEditable");
		driver.safeClick(itemsPerPageDropdown);
		boolean isNumberOfItemsPerPageEditable = driver.isPresentAndVisible(itemsPerPageDropdownOption);
		log.info("End of the method isNumberOfItemsPerPageEditable");
		return isNumberOfItemsPerPageEditable;
	}

	/**
	 * Verifies whether pagination is displayed
	 */
	public boolean isPaginationDisplayed() {
		log.info("Start of the method isPaginationDisplayed");
		boolean isPaginationDisplayed = driver.isPresentAndVisible(paginationDiv);
		log.info("End of the method isPaginationDisplayed");
		return isPaginationDisplayed;
	}

	/**
	 * Verifies Primary problem type
	 * @param problemType : problem Type in string format
	 */
	public boolean isProblemTypeDisplayed(String problemType) {
		log.info("Start of the method isProblemTypeDisplayed");
		boolean isProblemTypesDisplayed = false;
		driver.sleep(WaitTime.LONG_WAIT);
		driver.jSClick(problemTypeDropDown);
		List<String> problemTypeDropdownOptionsText = driver.getTextsFromAllElementOfALocator(By.cssSelector(String.format(problemTypeDropDownOptions)));
		driver.safeClick(By.xpath(String.format(problemTypeDropDownOptionWithText, problemType)));
		isProblemTypesDisplayed = problemTypeDropdownOptionsText.contains(problemType);
		log.info("End of the method isProblemTypeDisplayed");
		return isProblemTypesDisplayed;
	}

	/**
	 * Verifies primaryNonConformance problem type
	 * @param primaryNonConformance : primary Nonconformance in string format
	 */
	public boolean isPrimaryNonConformanceProblemTypesDisplayed(String primaryNonConformance) {
		log.info("Start of the method isPrimaryNonConformanceProblemTypesDisplayed");
		boolean isPrimaryNonConformanceProblemTypesDisplayed = false;
		driver.jSClick(primaryNonConformanceDropDown);
		List<String> problemTypeDropdownOptionsText = driver.getTextsFromAllElementOfALocator(By.cssSelector(String.format(primaryNonConformanceDropDownOptions)));
		driver.safeClick(By.xpath(String.format(primaryNonConformanceDropDownOptionWithText, primaryNonConformance)));
		isPrimaryNonConformanceProblemTypesDisplayed = problemTypeDropdownOptionsText.contains(primaryNonConformance);
		log.info("End of the method isPrimaryNonConformanceProblemTypesDisplayed");
		return isPrimaryNonConformanceProblemTypesDisplayed;
	}

	/**
	 * Verifies secondaryNonConformance problem type
	 * @param secondaryNonConformance : secondary NonConformance in string format
	 */
	public boolean isSecondaryNonConformanceProblemTypesDisplayed(String secondaryNonConformance) {
		log.info("Start of the method isSecondaryNonConformanceProblemTypesDisplayed");
		boolean isSecondaryNonConformanceProblemTypesDisplayed = false;
		driver.jSClick(secondNonConformanceDropDown);
		List<String> problemTypeDropdownOptionsText = driver.getTextsFromAllElementOfALocator(By.cssSelector(String.format(secondNonConformanceDropDownOptions)));
		driver.safeClick(By.xpath(String.format(secondNonConformanceDropDownOptionWithText, secondaryNonConformance)));
		isSecondaryNonConformanceProblemTypesDisplayed = problemTypeDropdownOptionsText.contains(secondaryNonConformance);
		log.info("End of the method isSecondaryNonConformanceProblemTypesDisplayed");
		return isSecondaryNonConformanceProblemTypesDisplayed;
	}

	/**
	 * Removes tag
	 * @param tagName : tag name in string format
	 */
	public void removeTag(String tagName) {
		log.info("Start of the method removeTag");
		driver.safeClick(By.xpath(String.format(attachmentRemoveButton, tagName)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		log.info("End of the method removeTag");
	}

	/**
	 * Verifies whether remove button is displayed
	 * @param tagName : tag name in string format
	 */
	public boolean isRemoveButtonDisplayed(String tagName) {
		log.info("Start of the method isRemoveButtonDisplayed");
		boolean isRemoveButtonDisplayed = driver.isPresentAndVisible(By.xpath(String.format(attachmentRemoveButton, tagName)));
		log.info("End of the method isRemoveButtonDisplayed");
		return isRemoveButtonDisplayed;
	}

	/**
	 * Verifies presence of delegation feature in case form
	 * @return delegation feature status
	 */
	public boolean isDelegationFeatureDisplayed() {
		log.info("Start of the method isDelegationFeatureDisplayed");
		boolean isDelegationFeatureDisplayed = driver.isPresentAndVisible(caseFormVerticalIconButton);
		log.info("End of the method isDelegationFeatureDisplayed");
		return isDelegationFeatureDisplayed;
	}

	/**
	 * Verifies whether problem case conversation comment is displayed
	 * @param tagName : tag name in string format
	 * @param conversationComment : conversation comment in string format
	 * @return returns problem case conversation comment displayed status
	 */
	public String getProblemCaseConversationCommentTagColor(String tagName, String conversationComment) {
		log.info("Start of the method getProblemCaseConversationCommentTagColor");
		String getProblemCaseConversationCommentTagColor = driver.getCssValue(By.xpath(String.format(conversationsTagsText, conversationComment, tagName)), "background-color");
		log.info("End of the method getProblemCaseConversationCommentTagColor");
		return getProblemCaseConversationCommentTagColor;
	}

	/**
	 * Verifies whether problem case conversation comment box placeholder
	 * @param tagName : tag name in string format
	 * @param commentBoxPlaceholder : conversation comment box placeholder in string format
	 * @return returns problem case conversation comment displayed status
	 */
	public boolean isProblemCaseConversationCommentBoxPlaceholderDisplayed(String tagName, String commentBoxPlaceholder) {
		log.info("Start of the method isProblemCaseConversationCommentBoxPlaceholderDisplayed");
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(conversationsTagDropdown);
		driver.safeClick(By.xpath(String.format(conversationsTagDropdownOption, tagName)));
		driver.sleep(WaitTime.SMALL_WAIT);
		boolean idProblemCaseConversationCommentBoxPlaceholderDisplayed = driver.isPresentAndVisible(By.cssSelector(String.format(commentTextAreaPlaceHolder, commentBoxPlaceholder)));
		log.info("End of the method isProblemCaseConversationCommentBoxPlaceholderDisplayed");
		return idProblemCaseConversationCommentBoxPlaceholderDisplayed;
	}

	/**
	 * Verifies whether comment box placeholder
	 * @param commentBoxPlaceholder : conversation comment box placeholder in string format
	 * @return returns problem case conversation comment displayed status
	 */
	public boolean isCommentBoxPlaceholderDisplayed(String commentBoxPlaceholder) {
		log.info("Start of the method isCommentBoxPlaceholderDisplayed");
		driver.sleep(WaitTime.SMALL_WAIT);
		boolean isCommentBoxPlaceholderDisplayed = driver.isPresentAndVisible(By.cssSelector(String.format(commentTextAreaPlaceHolder, commentBoxPlaceholder)));
		log.info("End of the method isCommentBoxPlaceholderDisplayed");
		return isCommentBoxPlaceholderDisplayed;
	}

	/**
	 * Verifies problem case conversation comment sort option
	 * @return  returns sort option status
	 */
	public boolean isProblemCaseConversationCommentSortOptionDisplayed() {
		log.info("Start of the method isProblemCaseConversationCommentSortOptionDisplayed");
		driver.sleep(WaitTime.SMALL_WAIT);
		boolean isProblemCaseConversationCommentSortOptionDisplayed = driver.isPresentAndVisible(conversationCommentSortOption);
		log.info("End of the method isProblemCaseConversationCommentSortOptionDisplayed");
		return isProblemCaseConversationCommentSortOptionDisplayed;
	}

	/**
	 * Clicks on draft toggle link
	 */
	public void clickOnDraftToggleLink() {
		log.info("Start of the method clickOnDraftToggleLink");
		driver.safeClick(draftToggleLInk);
		log.info("End of the method clickOnDraftToggleLink");
	}

	/**
	 * Verifies whether no attachments message displayed
	 * @return returns no attachment message displayed status
	 */
	public boolean isNoAttachmentsMessageDisplayed() {
		log.info("Start of the method isNoAttachmentsMessageDisplayed");
		driver.sleep(WaitTime.SMALL_WAIT);
		boolean isNoAttachmentsMessageDisplayed = driver.isPresentAndVisible(noAttachmentMessageText);
		log.info("End of the method isNoAttachmentsMessageDisplayed");
		return isNoAttachmentsMessageDisplayed;
	}

	/**
	 * Verifies whether add attachments link displayed
	 * @return returns add attachment link displayed status
	 */
	public boolean isAddAttachmentsLinkDisplayed() {
		log.info("Start of the method isAddAttachmentsLinkDisplayed");
		driver.sleep(WaitTime.SMALL_WAIT);
		boolean isAddAttachmentsLinkDisplayed = driver.isPresentAndVisible(addAttachmentLink);
		log.info("End of the method isAddAttachmentsLinkDisplayed");
		return isAddAttachmentsLinkDisplayed;
	}

	/**
	 * Clicks on add attachment link
	 */
	public void clickOnAddAttachmentLink() {
		log.info("Start of the method clickOnAddAttachmentLink");
		driver.safeClick(addDocumentButton);
		log.info("End of the method clickOnAddAttachmentLink");
	}


	/**
	 * Verifies whether add attachments button disabled
	 * @return returns add attachment button disabled status
	 */
	public boolean isAddAttachmentsButtonDisabled() {
		log.info("Start of the method isAddAttachmentsButtonDisabled");
		driver.sleep(WaitTime.SMALL_WAIT);
		boolean isAddAttachmentsButtonDisabled = driver.isPresentAndVisible(addAttachmentDisabledButton);
		log.info("End of the method isAddAttachmentsButtonDisabled");
		return isAddAttachmentsButtonDisabled;
	}

	/**
	 * Verifies whether attachments logo text displayed
	 * @param attachmentName : attachment name in string format
	 * @return returns attachment logo text displayed status
	 */
	public boolean isAttachmentsLogoTextDisplayed(String attachmentName) {
		log.info("Start of the method isAttachmentsLogoTextDisplayed");
		driver.sleep(WaitTime.MEDIUM_WAIT);
		fileUploadUtils.uploadFile(uploadButton, attachmentName);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		boolean isAttachmentsLogoTextDisplayed = driver.isPresentAndVisible(pdfLogoText);
		log.info("End of the method isAttachmentsLogoTextDisplayed");
		return isAttachmentsLogoTextDisplayed;
	}

	/**
	 * Clicks on add attachment close button
	 */
	public void clickOnAddAttachmentCloseButton() {
		log.info("Start of the method clickOnAddAttachmentCloseButton");
		driver.safeClick(addAttachmentCloseButton);
		log.info("End of the method clickOnAddAttachmentCloseButton");
	}

	/**
	 * Clicks on attachment vertical icon
	 * @param attachmentName : attachment name in string format
	 */
	public void clickOnAttachmentVerticalIcon(String attachmentName) {
		log.info("Start of the method clickOnAttachmentVerticalIcon");
		driver.safeClick(By.xpath(String.format(attachmentVerticalIcon, attachmentName)));
		log.info("End of the method clickOnAttachmentVerticalIcon");
	}

	/**
	 * Adds problem case attachments versions
	 * @param attachmentName : attachment name in string format
	 */
	public void addProblemCaseAttachmentsVersion(String attachmentName) {
		log.info("Start of the method addProblemCaseAttachmentsVersion");
		driver.sleep(WaitTime.LONG_WAIT);
		//driver.safeClick(addVersionButton);
		FileUploadUtils fileUploadUtils = new FileUploadUtils(driver);
		String uploadsPath = fileUploadUtils.getUploadsPath(attachmentName);
		//driver.getWrappedDriver().findElement(addVersionButton).click();
		WebElement element = driver.getWrappedDriver().findElement(addVersionButton);
		//element.click();
		element.sendKeys(uploadsPath);
		driver.sleep(WaitTime.LONG_WAIT);
		//fileUploadUtils.uploadFile(uploadButton, attachmentName);
		driver.sleep(WaitTime.LONG_WAIT);
		log.info("End of the method addProblemCaseAttachmentsVersion");
	}

	/**
	 * Verifies whether attachments attachment displayed on top
	 * @param attachmentName : attachment name in string format
	 * @return returns attachment displayed status
	 */
	public boolean isLatestVersionAttachmentsDisplayedOnTop(String attachmentName) {
		log.info("Start of the method isLatestVersionAttachmentsDisplayedOnTop");
		driver.sleep(WaitTime.SMALL_WAIT);
		boolean isLatestVersionAttachmentsDisplayedOnTop = driver.isPresentAndVisible(By.xpath(String.format(attachmentWithVersionOnTop, attachmentName)));
		log.info("End of the method isLatestVersionAttachmentsDisplayedOnTop");
		return isLatestVersionAttachmentsDisplayedOnTop;
	}

	/**
	 * Verifies whether view version button displayed
	 * @return returns attachment view version button displayed status
	 */
	public boolean isViewVersionButtonDisplayed() {
		log.info("Start of the method isViewVersionButtonDisplayed");
		driver.sleep(WaitTime.SMALL_WAIT);
		boolean isViewVersionButtonDisplayed = driver.isPresentAndVisible(viewVersionButton);
		log.info("End of the method isViewVersionButtonDisplayed");
		return isViewVersionButtonDisplayed;
	}

	/**
	 * Verifies whether edit version button displayed
	 * @return returns attachment edit version button displayed status
	 */
	public boolean isEditVersionButtonDisplayed() {
		log.info("Start of the method isEditVersionButtonDisplayed");
		driver.sleep(WaitTime.SMALL_WAIT);
		boolean isEditVersionButtonDisplayed = driver.isPresentAndVisible(editVersionButton);
		log.info("End of the method isEditVersionButtonDisplayed");
		return isEditVersionButtonDisplayed;
	}

	/**
	 * Verifies whether delete version button displayed
	 * @return returns attachment delete version button displayed status
	 */
	public boolean isDeleteVersionButtonDisplayed() {
		log.info("Start of the method isDeleteVersionButtonDisplayed");
		driver.sleep(WaitTime.SMALL_WAIT);
		boolean isDeleteVersionButtonDisplayed = driver.isPresentAndVisible(deleteVersionButton);
		log.info("End of the method isDeleteVersionButtonDisplayed");
		return isDeleteVersionButtonDisplayed;
	}

	public void editAttachment(String attachmentName, String tagName) {
		log.info("Start of the method editAttachment");
		driver.safeClick(editAttachmentButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeType(fileNameTextBox, attachmentName);
		driver.safeClick(editAttachmentTagDropdown);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(editAttachmentTagDropdownOption, tagName)));
		driver.safeClick(editAttachmentSaveButton);
		log.info("End of the method editAttachment");
	}

	public void editAttachmentForSupplierAdmin(String attachmentName, String tagName) {
		log.info("Start of the method editAttachmentForSupplierAdmin");
		driver.safeClick(editAttachmentButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeType(fileNameTextBox, attachmentName);
//		driver.safeClick(editAttachmentTagDropdown);
//		driver.sleep(WaitTime.SMALL_WAIT);
//		driver.safeClick(By.xpath(String.format(editAttachmentTagDropdownOption, tagName)));
		driver.safeClick(editAttachmentSaveButton);
		log.info("End of the method editAttachmentForSupplierAdmin");
	}

	/**
	 * Verifies whether attachments with tag displayed on top
	 * @param attachmentName : attachment name in string format
	 * @param tagName : tag name in string format
	 * @return returns attachment displayed status
	 */
	public boolean isAttachmentsWithTagDisplayed(String attachmentName, String tagName) {
		log.info("Start of the method isAttachmentsWithTagDisplayed");
		driver.sleep(WaitTime.SMALL_WAIT);
		boolean isAttachmentsWithTagDisplayed = driver.isPresentAndVisible(By.xpath(String.format(attachmentWithTag, attachmentName, tagName)));
		log.info("End of the method isAttachmentsWithTagDisplayed");
		return isAttachmentsWithTagDisplayed;
	}

	/**
	 * Verifies whether attachments without tag displayed on top
	 * @param attachmentName : attachment name in string format
	 * @return returns attachment displayed status
	 */
	public boolean isAttachmentsWithoutTagDisplayed(String attachmentName) {
		log.info("Start of the method isAttachmentsWithoutTagDisplayed");
		driver.sleep(WaitTime.SMALL_WAIT);
		boolean isAttachmentsWithoutTagDisplayed = driver.isPresentAndVisible(By.xpath(String.format(attachmentWithoutTag, attachmentName)));
		log.info("End of the method isAttachmentsWithoutTagDisplayed");
		return isAttachmentsWithoutTagDisplayed;
	}

	/**
	 * Verifies whether attachments download button displayed
	 * @return attachments download button display status
	 */
	public boolean isAttachmentsDownloadButtonDisplayed() {
		log.info("Start of the method isAttachmentsDownloadButtonDisplayed");
		driver.sleep(WaitTime.SMALL_WAIT);
		boolean isAttachmentsDownloadButtonDisplayed = driver.isPresentAndVisible(attachmentDownloadButton);
		log.info("End of the method isAttachmentsDownloadButtonDisplayed");
		return isAttachmentsDownloadButtonDisplayed;
	}

	/**
	 * Verifies whether attachments download button displayed
	 * @return attachments download button display status
	 */
	public boolean isAttachmentsVerticalIconDisplayed(String attachmentName) {
		log.info("Start of the method isAttachmentsVerticalIconDisplayed");
		driver.sleep(WaitTime.SMALL_WAIT);
		boolean isAttachmentsVerticalIconDisplayed = driver.isPresentAndVisible(By.xpath(String.format(attachmentVerticalIcon, attachmentName)));
		log.info("End of the method isAttachmentsVerticalIconDisplayed");
		return isAttachmentsVerticalIconDisplayed;
	}

	/**
	 * Deletes attachment versions
	 * @param versionNumber : version number in string format
	 */
	public void deleteAttachmentVersion(String versionNumber) {
		log.info("Start of the method clickOnDeleteAttachment");
		driver.safeClick(viewVersionButton);
		driver.safeClick(By.xpath(String.format(versionVerticalIcon, versionNumber)));
		driver.safeClick(versionDeleteButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(versionDeletePopUpYesButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(versionDeleteDoneButton);
		log.info("End of the method clickOnDeleteAttachment");
	}

	/**
	 * Clicks on attachment delete icon
	 */
	public void clickOnAttachmentDeleteIcon() {
		log.info("Start of the method clickOnAttachmentDeleteIcon");
		driver.safeClick(deleteVersionButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method clickOnAttachmentDeleteIcon");
	}

	/**
	 * Verifies whether attachments large size error message displayed
	 * @param attachmentName : attachment name in string format
	 * @return returns attachment logo text displayed status
	 */
	public boolean isAttachmentsLargeSizeMessageDisplayed(String attachmentName) {
		log.info("Start of the method isAttachmentsLargeSizeMessageDisplayed");
		driver.sleep(WaitTime.MEDIUM_WAIT);
		fileUploadUtils.uploadFile(uploadButton, attachmentName);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		boolean isAttachmentsLargeSizeMessageDisplayed = driver.isPresentAndVisible(attachmentLargeSizeMessage);
		log.info("End of the method isAttachmentsLargeSizeMessageDisplayed");
		return isAttachmentsLargeSizeMessageDisplayed;
	}

}