package io.ient.test.pages.enterprise;


import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class GroupManagementPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(GroupManagementPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By addButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static final By statusDropdown = By.xpath("//div[select[@id='status']]");
	private static String statusDropdownOption = "//div[@role='option' and span[text()='%s']]";
	private static String roleRow = "//tr[contains(@class,'ient-table-row') and td[span[contains(text(),'%s')]]]";
	private static final By fistRowGroupCheckbox = By.xpath("(//ient-checkbox[div[@class='ui checkbox ient-checkbox']])[1]");
	private static final By fistRowGroupName = By.xpath("//table[@class='ui striped table actual-table']/tbody/tr[1]/td[4]/span");
	private static final By submitButton = By.cssSelector("button[name='data[submit]']");
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By editLink = By.cssSelector("div[id='edit']");
	private static final By deleteLink = By.cssSelector("div[id='delete']");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By noRecordsAvailableTextMessage = By.xpath("//div[@class='ui red message ng-star-inserted' and text()=' No records available ']");
	private static final By topVerticalIcon = By.cssSelector("div[class^='ui icon top left pointing dropdown button']>i[class^='ellipsis vertical icon']");
	private static final By exportToTenantOption = By.xpath("//div[contains(@class,'item') and span[text()='Export to Tenant']]");
	private static String targetTenantOption = "//div[contains(@class,'item') and @id='%s']";

	private static final By groupManagementTab = By.cssSelector("div[id='group-manage-card']");
	private static final By groupNameTextBox = By.cssSelector("input[name='data[name]']");
	private static final By uniqueNameTextBox = By.cssSelector("input[id='uniqueName']");


	public GroupManagementPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - GroupManagementPage");
	}

	/***
	 * Clicks on group management tab
	 */
	public void clickOnGroupManagementTab() {
		log.info("Start of the method clickOnGroupManagementTab");
		driver.safeClick(groupManagementTab);
		log.info("End of the method clickOnGroupManagementTab");
	}

	/***
	 * Creates a new group
	 * @param uniqueName : unique name of group in string format
	 * @param name : name of group in string format
	 * @param status : status in string format
	 */
	public void createGroup(String uniqueName, String name, String status) {
		log.info("Start of the method createGroup");
		driver.safeClick(addButton);
		driver.safeType(uniqueNameTextBox, uniqueName);
		driver.safeType(groupNameTextBox, name);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createGroup");
	}

	/**
	 * Verifies whether group is displayed
	 * @param name : name of group in string format
	 * @return returns creation status
	 */
	public boolean isGroupDisplayed(String name) {
		log.info("Start of the method isGroupDisplayed");
		boolean isGroupDisplayed = driver.isPresentAndVisible(By.xpath(String.format(roleRow, name)));
		log.info("End of the method isGroupDisplayed");
		return isGroupDisplayed;
	}

	/***
	 * Searches group by name
	 * @param Name : name of group in string format
	 */
	public void searchGroupByName(String Name) {
		log.info("Start of the method searchGroupByName");
		driver.safeType(searchTextBox, Name);
		driver.safeClick(searchIconButton);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchGroupByName");
	}

	public void clearSearchTextField() {
		log.info("Start of the method clearSearchTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearSearchTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	/***
	 * Edits existing group
	 * @param name : name of group in string format
	 * @param status : status in string format
	 */
	public void editGroup(String name, String status) {
		log.info("Start of the method editGroup");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.safeType(groupNameTextBox, name);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method editGroup");
	}

	/***
	 * Deletes group
	 */
	public void deleteGroup() {
		log.info("Start of the method deleteGroup");
		driver.safeClick(verticalIconButton);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deleteGroup");
	}

	/**
	 * Verifies No Records Available Message
	 * @return returns message display status
	 */
	public boolean verifyNoRecordsMessage() {
		log.info("Start of the method verifyNoRecordsMessage");
		boolean isMessageDisplayed = driver.isPresentAndVisible(noRecordsAvailableTextMessage);
		log.info("End of the method verifyNoRecordsMessage");
		return isMessageDisplayed;
	}

	/***
	 * Exports tenants data
	 * @param tenantName : tenant name in string format
	 */
	public void exportToTenant(String tenantName)
	{
		log.info("Start of the method exportToTenant");
		driver.safeClick(fistRowGroupCheckbox);
		driver.safeClick(topVerticalIcon);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(exportToTenantOption);
		driver.safeClick(By.xpath(String.format(targetTenantOption, tenantName)));
		log.info("End of the method exportToTenant");
	}

	/**
	 * Returns first row group name
	 * @return returns group name
	 */
	public String getFirstRowGroupName() {
		log.info("Start of the method getFirstRowGroupName");
		String groupName = driver.safeGetText(fistRowGroupName);
		log.info("End of the method getFirstRowGroupName");
		return groupName;
	}
}