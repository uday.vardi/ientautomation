package io.ient.test.pages.enterprise;

import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class CommodityGroupsPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(CommodityGroupsPage.class);
	LogFactory log = new LogFactory(logger);
	private static final By addButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static final By statusDropdown = By.xpath("//div[select[@id='status']]");
	private static String statusDropdownOption = "//div[@role='option' and span[text()='%s']]";
	private static String globalSettingsRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]]";
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By editLink = By.cssSelector("div[id='edit']");
	private static final By deleteLink = By.cssSelector("div[id='delete']");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By noRecordsAvailableTextMessage = By.xpath("//div[@class='ui red message ng-star-inserted' and text()=' No records available ']");

	private static final By globalSettingsTab = By.cssSelector("div[id='global-settings-card']");
	private static final By nameTextBox = By.cssSelector("input[name='data[name]']");
	private static final By submitButton = By.cssSelector("button[name='data[submit]']");

	private static final By commodityGroupsTab = By.xpath("//span[text()='Commodity Groups']");


	public CommodityGroupsPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - Global Settings Page");
	}

	/***
	 * Clicks on global settings tab
	 * Clicks on commodity groups tab
	 */
	public void clickOnCommodityGroupTab() {
		log.info("Start of the method clickOnCommodityGroupsTab");
		driver.safeClick(globalSettingsTab);
		driver.safeClick(commodityGroupsTab);
		log.info("End of the method clickOnCommodityGroupsTab");
	}

	/**
	 * Creates a new commodity group
	 * @param commodityGroupName : commodity group name in string format
	 * @param status : status in string format
	 */
	public void createCommodityGroup(String commodityGroupName, String status) {
		log.info("Start of the method createCommodityGroup");
		driver.safeClick(addButton);
		driver.safeType(nameTextBox, commodityGroupName);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createCommodityGroup");
	}

	/**
	 * Verifies whether commodity group is displayed
	 * @param commodityGroupName : commodity group name in string format
	 */
	public boolean isCommodityGroupDisplayed(String commodityGroupName) {
		log.info("Start of the method isCommodityGroupDisplayed");
		boolean isCommodityGroupDisplayed = driver.isPresentAndVisible(By.xpath(String.format(globalSettingsRow, commodityGroupName)));
		log.info("End of the method isCommodityGroupDisplayed");
		return isCommodityGroupDisplayed;
	}

	/***
	 * Searches commodity group by name
	 * @param commodityGroupName : commodity group name in string format
	 */
	public void searchCommodityGroupByName(String commodityGroupName) {
		log.info("Start of the method searchCommodityGroupByName");
		driver.safeType(searchTextBox, commodityGroupName);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(searchIconButton);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchCommodityGroupByName");
	}

	public void clearCommodityGroupTextField() {
		log.info("Start of the method clearCommodityGroupTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearCommodityGroupTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	/***
	 * Edits existing commodity group
	 * @param commodityGroupName : commodity group name in string format
	 */
	public void editCommodityGroup(String commodityGroupName) {
		log.info("Start of the method editCommodityGroup");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.safeType(nameTextBox, commodityGroupName);
		driver.safeClick(submitButton);
		log.info("End of the method editCommodityGroup");
	}

	/***
	 * Deletes commodity group
	 */
	public void deleteCommodityGroup() {
		log.info("Start of the method deleteCommodityGroup");
		driver.safeClick(verticalIconButton);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deleteCommodityGroup");
	}

	/**
	 * Verifies No Records Available Message
	 * @return returns message display status
	 */
	public boolean verifyNoRecordsMessage() {
		log.info("Start of the method verifyNoRecordsMessage");
		boolean isMessageDisplayed = driver.isPresentAndVisible(noRecordsAvailableTextMessage);
		log.info("End of the method verifyNoRecordsMessage");
		return isMessageDisplayed;
	}

}