package io.ient.test.pages.enterprise;

import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import java.util.List;


public class SuppliersPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(SuppliersPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By addButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static String suppliersRow ="//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]]";
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By editLink = By.cssSelector("button[id^='ient-buttton-']");
	private static final By viewLink = By.cssSelector("div[id='view']");
	private static final By deleteLink = By.cssSelector("div[id='delete']");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By noRecordsAvailableTextMessage = By.xpath("//div[@class='ui red message ng-star-inserted' and text()=' No records available ']");

	private static final By globalSettingsTab = By.cssSelector("div[id='global-settings-card']");
	private static final By submitButton = By.cssSelector("button[name='data[submit]']");

	private static final By suppliersTab = By.xpath("//span[text()='Suppliers']");
	private static final By supplierSubTab = By.xpath("//span[text()='Supplier']");
	private static final By supplierLocationTab = By.xpath("//span[text()='Supplier Locations']");
	private static final By supplierLocationTypesTab = By.xpath("//span[text()='Supplier Location Types']");
	private static final By registeredBusinessNameTextBox = By.cssSelector("input[name='data[registeredBusinessName]']");
	private static final By registeredTradeNamesTextBox = By.cssSelector("input[name='data[registeredTradeNames]']");
	private static final By businessWebsiteTextBox = By.cssSelector("input[name='data[businessWebsite]']");
	private static final By isThisBusinessMoreThan51OwnedByWomenRadioButton = By.xpath("//input[contains(@name,'isThisBusinessMoreThan51OwnedByWomen') and @value='true']");
	private static final By whichOfTheseLanguagesAreYouDoingBusinessInTextBox = By.cssSelector("input[name='data[whichOfTheseLanguagesAreYouDoingBusinessIn]']");
	private static final By whatIsYourGovernmentIssuesBusinessRegistrationNumberTextBox = By.cssSelector("input[name='data[whatIsYourGovernmentIssuesBusinessRegistrationNumber]']");
	private static final By ifYouHaveDunBradstreetNumberDunsPleaseIncludeItHereTextBox = By.cssSelector("input[name='data[ifYouHaveDunBradstreetNumberDunsPleaseIncludeItHere]']");
	private static final By inWhatYearWasTheBusinessEstablishedTextBox = By.cssSelector("input[name='data[inWhatYearWasTheBusinessEstablished]']");
	private static final By ifYouAreNotTheOwnerWhoEstablishedTheBusinessInWhatYearDidYouAcquireItTextBox = By.cssSelector("input[name='data[ifYouAreNotTheOwnerWhoEstablishedTheBusinessInWhatYearDidYouAcquireIt]']");
	private static final By pleaseProvideAShortDescriptionOfYourBusinessTextBox = By.cssSelector("input[name='data[pleaseProvideAShortDescriptionOfYourBusiness]']");
	private static final By pleaseProvideADescriptionOfYourBusinessProductsAndServicesTextBox = By.cssSelector("input[name='data[pleaseProvideADescriptionOfYourBusinessProductsAndServices]']");
	private static final By primaryProductServiceCategoryTextBox = By.cssSelector("input[name='data[primaryProductServiceCategory]']");
	private static final By secondaryProductServiceCategoryTextBox = By.cssSelector("input[name='data[secondaryProductServiceCategory]']");
	private static final By numberOfFullTimeEmployeesTextBox = By.cssSelector("input[name='data[numberOfFullTimeEmployees]']");
	private static final By whereDoYouCurrentlyFocusOnSellingYourProductsServicesTextBox = By.cssSelector("input[name='data[whereDoYouCurrentlyFocusOnSellingYourProductsServices]']");
	private static final By whatAreYourBusinessCurrentAndFutureTargetMarketsTextBox = By.cssSelector("input[name='data[whatAreYourBusinessCurrentAndFutureTargetMarkets]']");
	private static final By otherRelevantBusinessCertificationSForExampleIsoDiversityEnvironmentalEtcTextBox = By.cssSelector("input[name='data[otherRelevantBusinessCertificationSForExampleIsoDiversityEnvironmentalEtc]']");
	private static final By inWhichCountriesDoesYourBusinessCurrentlyProvideProductsOrServicesDistributionAreaTextBox = By.cssSelector("input[name='data[inWhichCountriesDoesYourBusinessCurrentlyProvideProductsOrServicesDistributionArea]']");
	private static final By whatIsYourBusinessCurrentLegalStructureTextBox = By.cssSelector("input[name='data[whatIsYourBusinessCurrentLegalStructure]']");
	private static final By whereIsYourPrincipalPlaceOfBusinessTextBox = By.cssSelector("input[name='data[whereIsYourPrincipalPlaceOfBusiness]']");
	private static final By address1TextBox = By.cssSelector("textarea[name='data[address1]']");
	private static final By address2TextBox = By.cssSelector("textarea[name='data[address2]']");
	private static final By townTextBox = By.cssSelector("input[name='data[town]']");
	private static final By stateProvinceTextBox = By.cssSelector("input[name='data[stateProvince]']");
	private static final By zipPostalCodeTextBox = By.cssSelector("input[name='data[zipPostalCode]']");
	private static final By countryTextBox = By.cssSelector("input[name='data[country]']");
	private static final By useDifferentBillingAddressTextBox = By.cssSelector("input[name='data[useDifferentBillingAddress]']");
	private static final By ownerManagerFirstNameTextBox = By.cssSelector("input[name='data[ownerManagerFirstName]']");
	private static final By ownerManagerLastNameTextBox = By.cssSelector("input[name='data[ownerManagerLastName]']");
	private static final By ownerManagerGenderTextBox = By.cssSelector("input[name='data[ownerManagerGender]']");
	private static final By ownerManagerEmailTextBox = By.cssSelector("input[name='data[ownerManagerEmail]']");
	private static final By ownerManagerOfficeNumberTextBox = By.cssSelector("input[name='data[ownerManagerOfficeNumber]']");
	private static final By ownerManagerMobileNumberTextBox = By.cssSelector("input[name='data[ownerManagerMobileNumber]']");
	private static final By ownerManagerFaxNumberTextBox = By.cssSelector("input[name='data[ownerManagerFaxNumber]']");
	private static final By whatIsTheOwnerManagersJobTitleTextBox = By.cssSelector("input[name='data[whatIsTheOwnerManagersJobTitle]']");
	private static final By whatWereYourBusinessRevenuesIn2020PleaseEnterInUsDollarsTextBox = By.cssSelector("input[name='data[whatWereYourBusinessRevenuesIn2020PleaseEnterInUsDollars]']");
	private static final By whatWereYourBusinessRevenuesIn2019PleaseEnterInUsDollarsTextBox = By.cssSelector("input[name='data[whatWereYourBusinessRevenuesIn2019PleaseEnterInUsDollars]']");
	private static final By supplierUserDropdown = By.xpath("//select[@name='data[supplierUser]']/parent::div[@class='form-control ui fluid selection dropdown']");
	private static final By supplierUserDropdownOptions = By.cssSelector("div[id*='supplierUser']>span");
	private static String supplierUserDropdownOption = "//div[@role='option' and span[text()='%s']]/span";
	private static final By globalSettingsLinkInBreadcrumb = By.xpath("//a[text()='Global Settings']");


	public SuppliersPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - Global Settings Page");
	}

	/***
	 * Clicks on global settings tab
	 * Clicks on suppliers tab
	 */
	public void clickOnSuppliersTab() {
		log.info("Start of the method clickOnSuppliersTab");
		driver.safeClick(globalSettingsTab);
		driver.safeClick(suppliersTab);
		log.info("End of the method clickOnSuppliersTab");
	}

	/**
	 * creates a suppliers
	 * @param registeredBusinessName :  registeredBusinessName in string format
	 * @param registeredTradeName : registeredTradeName in string format
	 * @param businessWebsite : businessWebsite in string format
	 * @param whichOfTheseLanguagesAreYouDoingBusinessIn : whichOfTheseLanguagesAreYouDoingBusinessIn in string format
	 * @param whatIsYourGovernmentIssuesBusinessRegistrationNumber : whatIsYourGovernmentIssuesBusinessRegistrationNumber in string format
	 * @param ifYouHaveDunBradstreetNumberDunsPleaseIncludeItHere : ifYouHaveDunBradstreetNumberDunsPleaseIncludeItHere in string format
	 * @param inWhatYearWasTheBusinessEstablished : inWhatYearWasTheBusinessEstablished in string format
	 * @param ifYouAreNotTheOwnerWhoEstablishedTheBusinessInWhatYearDidYouAcquireIt : ifYouAreNotTheOwnerWhoEstablishedTheBusinessInWhatYearDidYouAcquireIt in string format
	 * @param pleaseProvideAShortDescriptionOfYourBusiness : pleaseProvideAShortDescriptionOfYourBusiness in string format
	 * @param pleaseProvideADescriptionOfYourBusinessProductsAndServices : pleaseProvideADescriptionOfYourBusinessProductsAndServices in string format
	 * @param primaryProductServiceCategory : primaryProductServiceCategory in string format
	 * @param secondaryProductServiceCategory : secondaryProductServiceCategory in string format
	 * @param numberOfFullTimeEmployees : numberOfFullTimeEmployees in string format
	 * @param whereDoYouCurrentlyFocusOnSellingYourProductsServices : whereDoYouCurrentlyFocusOnSellingYourProductsServices in string format
	 * @param whatAreYourBusinessCurrentAndFutureTargetMarkets : whatAreYourBusinessCurrentAndFutureTargetMarkets in string format
	 * @param otherRelevantBusinessCertificationSForExampleIsoDiversityEnvironmentalEtc : otherRelevantBusinessCertificationSForExampleIsoDiversityEnvironmentalEtc in string format
	 * @param inWhichCountriesDoesYourBusinessCurrentlyProvideProductsOrServicesDistributionArea : inWhichCountriesDoesYourBusinessCurrentlyProvideProductsOrServicesDistributionArea in string format
	 * @param whatIsYourBusinessCurrentLegalStructure : whatIsYourBusinessCurrentLegalStructure in string format
	 * @param whereIsYourPrincipalPlaceOfBusiness : whereIsYourPrincipalPlaceOfBusiness in string format
	 * @param address1 : address1 in string format
	 * @param address2 : address2 in string format
	 * @param town : town in string format
	 * @param stateProvince : stateProvince in string format
	 * @param zipPostalCode : zipPostalCode in string format
	 * @param country : country in string format
	 * @param useDifferentBillingAddress : useDifferentBillingAddress in string format
	 * @param ownerManagerFirstName : ownerManagerFirstName in string format
	 * @param ownerManagerLastName : ownerManagerLastName in string format
	 * @param ownerManagerGender : ownerManagerGender in string format
	 * @param ownerManagerEmail : ownerManagerEmail in string format
	 * @param ownerManagerOfficeNumber : ownerManagerOfficeNumber in string format
	 * @param ownerManagerMobileNumber : ownerManagerMobileNumber in string format
	 * @param ownerManagerFaxNumber : ownerManagerFaxNumber in string format
	 * @param whatIsTheOwnerManagersJobTitle : whatIsTheOwnerManagersJobTitle in string format
	 * @param whatWereYourBusinessRevenuesIn2020PleaseEnterInUsDollars : whatWereYourBusinessRevenuesIn2020PleaseEnterInUsDollars in string format
	 * @param whatWereYourBusinessRevenuesIn2019PleaseEnterInUsDollars : whatWereYourBusinessRevenuesIn2019PleaseEnterInUsDollars in string format
	 */
	public void createSuppliers(String registeredBusinessName, String registeredTradeName, String businessWebsite, String whichOfTheseLanguagesAreYouDoingBusinessIn, String whatIsYourGovernmentIssuesBusinessRegistrationNumber, String ifYouHaveDunBradstreetNumberDunsPleaseIncludeItHere, String inWhatYearWasTheBusinessEstablished, String ifYouAreNotTheOwnerWhoEstablishedTheBusinessInWhatYearDidYouAcquireIt, String pleaseProvideAShortDescriptionOfYourBusiness, String pleaseProvideADescriptionOfYourBusinessProductsAndServices, String primaryProductServiceCategory, String secondaryProductServiceCategory, String numberOfFullTimeEmployees, String whereDoYouCurrentlyFocusOnSellingYourProductsServices, String whatAreYourBusinessCurrentAndFutureTargetMarkets, String otherRelevantBusinessCertificationSForExampleIsoDiversityEnvironmentalEtc, String inWhichCountriesDoesYourBusinessCurrentlyProvideProductsOrServicesDistributionArea, String whatIsYourBusinessCurrentLegalStructure, String whereIsYourPrincipalPlaceOfBusiness, String address1, String address2, String town, String stateProvince, String zipPostalCode, String country, String useDifferentBillingAddress, String ownerManagerFirstName, String ownerManagerLastName, String ownerManagerGender, String ownerManagerEmail, String ownerManagerOfficeNumber, String ownerManagerMobileNumber, String ownerManagerFaxNumber, String whatIsTheOwnerManagersJobTitle, String whatWereYourBusinessRevenuesIn2020PleaseEnterInUsDollars, String whatWereYourBusinessRevenuesIn2019PleaseEnterInUsDollars) {
		log.info("Start of the method createSuppliers");
		driver.safeClick(addButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeType(registeredBusinessNameTextBox, registeredBusinessName);
		driver.safeType(registeredTradeNamesTextBox, registeredTradeName);
		driver.safeType(businessWebsiteTextBox, businessWebsite);
		driver.jSClick(isThisBusinessMoreThan51OwnedByWomenRadioButton);
		driver.safeType(whichOfTheseLanguagesAreYouDoingBusinessInTextBox, whichOfTheseLanguagesAreYouDoingBusinessIn);
		driver.safeType(whatIsYourGovernmentIssuesBusinessRegistrationNumberTextBox, whatIsYourGovernmentIssuesBusinessRegistrationNumber);
		driver.safeType(ifYouHaveDunBradstreetNumberDunsPleaseIncludeItHereTextBox, ifYouHaveDunBradstreetNumberDunsPleaseIncludeItHere);
		driver.safeType(inWhatYearWasTheBusinessEstablishedTextBox, inWhatYearWasTheBusinessEstablished);
		driver.safeType(ifYouAreNotTheOwnerWhoEstablishedTheBusinessInWhatYearDidYouAcquireItTextBox, ifYouAreNotTheOwnerWhoEstablishedTheBusinessInWhatYearDidYouAcquireIt);
		driver.safeType(pleaseProvideAShortDescriptionOfYourBusinessTextBox, pleaseProvideAShortDescriptionOfYourBusiness);
		driver.safeType(pleaseProvideADescriptionOfYourBusinessProductsAndServicesTextBox, pleaseProvideADescriptionOfYourBusinessProductsAndServices);
		driver.safeType(primaryProductServiceCategoryTextBox, primaryProductServiceCategory);
		driver.safeType(secondaryProductServiceCategoryTextBox, secondaryProductServiceCategory);
		driver.safeType(numberOfFullTimeEmployeesTextBox, numberOfFullTimeEmployees);
		driver.safeType(whereDoYouCurrentlyFocusOnSellingYourProductsServicesTextBox, whereDoYouCurrentlyFocusOnSellingYourProductsServices);
		driver.safeType(whatAreYourBusinessCurrentAndFutureTargetMarketsTextBox, whatAreYourBusinessCurrentAndFutureTargetMarkets);
		driver.safeType(otherRelevantBusinessCertificationSForExampleIsoDiversityEnvironmentalEtcTextBox, otherRelevantBusinessCertificationSForExampleIsoDiversityEnvironmentalEtc);
		driver.safeType(inWhichCountriesDoesYourBusinessCurrentlyProvideProductsOrServicesDistributionAreaTextBox, inWhichCountriesDoesYourBusinessCurrentlyProvideProductsOrServicesDistributionArea);
		driver.safeType(whatIsYourBusinessCurrentLegalStructureTextBox, whatIsYourBusinessCurrentLegalStructure);
		driver.safeType(whereIsYourPrincipalPlaceOfBusinessTextBox, whereIsYourPrincipalPlaceOfBusiness);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeType(address1TextBox, address1);
		driver.safeType(address2TextBox, address2);
		driver.safeType(townTextBox, town);
		driver.safeType(stateProvinceTextBox, stateProvince);
		driver.safeType(zipPostalCodeTextBox, zipPostalCode);
		driver.safeType(countryTextBox, country);
		driver.safeType(useDifferentBillingAddressTextBox, useDifferentBillingAddress);
		driver.safeType(ownerManagerFirstNameTextBox, ownerManagerFirstName);
		driver.safeType(ownerManagerLastNameTextBox, ownerManagerLastName);
		driver.safeType(ownerManagerGenderTextBox, ownerManagerGender);
		driver.safeType(ownerManagerEmailTextBox, ownerManagerEmail);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClearAndTypeByActionsClass(ownerManagerOfficeNumberTextBox, ownerManagerOfficeNumber, false);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClearAndTypeByActionsClass(ownerManagerMobileNumberTextBox, ownerManagerMobileNumber, false);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeType(ownerManagerFaxNumberTextBox, ownerManagerFaxNumber);
		driver.safeType(whatIsTheOwnerManagersJobTitleTextBox, whatIsTheOwnerManagersJobTitle);
		driver.safeType(whatWereYourBusinessRevenuesIn2020PleaseEnterInUsDollarsTextBox, whatWereYourBusinessRevenuesIn2020PleaseEnterInUsDollars);
		driver.safeType(whatWereYourBusinessRevenuesIn2019PleaseEnterInUsDollarsTextBox, whatWereYourBusinessRevenuesIn2019PleaseEnterInUsDollars);
		driver.safeClick(submitButton);
		log.info("End of the method createSuppliers");
	}

	/**
	 * Verifies whether supplier is displayed
	 * @param supplierName : supplier name in string format
	 */
	public boolean isSupplierDisplayed(String supplierName) {
		log.info("Start of the method isSupplierDisplayed");
		boolean isSupplierDisplayed = driver.isPresentAndVisible(By.xpath(String.format(suppliersRow, supplierName)));
		log.info("End of the method isSupplierDisplayed");
		return isSupplierDisplayed;
	}

	/***
	 * Searches supplier by name
	 * @param supplierName : supplier name in string format
	 */
	public void searchSupplierByName(String supplierName) {
		log.info("Start of the method searchSupplierByName");
		driver.safeType(searchTextBox, supplierName);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(searchIconButton);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchSupplierByName");
	}

	public void clearSupplierTextField() {
		log.info("Start of the method clearSupplierTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearSupplierTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	public void clickOnGlobalSettingsThroughBreadcrumb() {
		log.info("Start of the method clickOnGlobalSettingsThroughBreadcrumb");
		driver.safeClick(globalSettingsLinkInBreadcrumb);
		driver.sleep(WaitTime.XSMALL_WAIT);
		log.info("End of the method clickOnGlobalSettingsThroughBreadcrumb");
	}

	/***
	 * Edits existing supplier
	 * @param registeredBusinessName : registered business name in string format
	 */
	public void editSupplier(String registeredBusinessName) {
		log.info("Start of the method editSupplier");
		driver.safeClick(verticalIconButton);
		driver.safeClick(viewLink);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(editLink);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeType(registeredBusinessNameTextBox, registeredBusinessName);
		driver.safeClick(submitButton);
		log.info("End of the method editSupplier");
	}

	/***
	 * Edits existing supplier
	 * @param registeredBusinessName : registered business name in string format
	 */
	public void viewSupplier(String registeredBusinessName) {
		log.info("Start of the method viewSupplier");
		driver.safeClick(verticalIconButton);
		driver.safeClick(viewLink);
		log.info("End of the method viewSupplier");
	}

	/***
	 * Edits existing supplier with supplier user
	 * @param registeredBusinessName : registered business name in string format
	 * @param supplierUser : supplierUser in string format
	 */
	public void editSupplierWithSupplierUser(String registeredBusinessName, String supplierUser) {
		log.info("Start of the method editSupplierWithSupplierUser");
		driver.safeClick(verticalIconButton);
		driver.safeClick(viewLink);
		driver.safeClick(editLink);
		driver.safeType(registeredBusinessNameTextBox, registeredBusinessName);
		driver.safeClick(supplierUserDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(supplierUserDropdownOption, supplierUser)));
		driver.safeClick(submitButton);
		log.info("End of the method editSupplierWithSupplierUser");
	}

	/**
	 * returns suppliers users list
	 * @
	 */
	public List<String> getSupplierUsersList() {
		log.info("Start of the method getSupplierUsersList");
		driver.safeClick(verticalIconButton);
		driver.safeClick(viewLink);
		driver.safeClick(editLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(supplierUserDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		List<String> usersList = driver.getTextsFromAllElementOfALocator(supplierUserDropdownOptions);
		driver.safeClick(submitButton);
		log.info("End of the method getSupplierUsersList");
		return usersList;
	}

	/**
	 * returns suppliers user text
	 * @
	 */
	public String getSupplierUserText() {
		log.info("Start of the method getSupplierUserText");
		driver.safeClick(verticalIconButton);
		driver.safeClick(viewLink);
		driver.safeClick(editLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(supplierUserDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		String userName = driver.getText(supplierUserDropdown);
		driver.safeClick(submitButton);
		log.info("End of the method getSupplierUserText");
		return userName;
	}

	/***
	 * Deletes supplier
	 */
	public void deleteSupplier() {
		log.info("Start of the method deleteSupplier");
		driver.safeClick(verticalIconButton);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deleteSupplier");
	}

	/**
	 * Verifies No Records Available Message
	 * @return returns message display status
	 */
	public boolean verifyNoRecordsMessage() {
		log.info("Start of the method verifyNoRecordsMessage");
		boolean isMessageDisplayed = driver.isPresentAndVisible(noRecordsAvailableTextMessage);
		log.info("End of the method verifyNoRecordsMessage");
		return isMessageDisplayed;
	}

	/**
	 * Verifies whether supplier sub tab is displayed
	 */
	public boolean isSupplierSubTabDisplayed() {
		log.info("Start of the method isSupplierSubTabDisplayed");
		boolean isSupplierSubTabDisplayed = driver.isPresentAndVisible(supplierSubTab);
		log.info("End of the method isSupplierSubTabDisplayed");
		return isSupplierSubTabDisplayed;
	}

	/**
	 * Verifies whether supplier location tab is displayed
	 */
	public boolean isSupplierLocationTabDisplayed() {
		log.info("Start of the method isSupplierLocationTabDisplayed");
		boolean isSupplierLocationTabDisplayed = driver.isPresentAndVisible(supplierLocationTab);
		log.info("End of the method isSupplierLocationTabDisplayed");
		return isSupplierLocationTabDisplayed;
	}

	/**
	 * Verifies whether supplier location types tab is displayed
	 */
	public boolean isSupplierLocationTypesTabDisplayed() {
		log.info("Start of the method isSupplierLocationTypesTabDisplayed");
		boolean isSupplierLocationTypesTabDisplayed = driver.isPresentAndVisible(supplierLocationTypesTab);
		log.info("End of the method isSupplierLocationTypesTabDisplayed");
		return isSupplierLocationTypesTabDisplayed;
	}

}