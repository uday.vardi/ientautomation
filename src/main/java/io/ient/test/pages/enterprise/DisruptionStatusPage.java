package io.ient.test.pages.enterprise;

import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class DisruptionStatusPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(DisruptionStatusPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By addButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static final By statusDropdown = By.xpath("//div[select[@id='status']]");
	private static String statusDropdownOption = "//div[@role='option' and span[text()='%s']]";
	private static String globalSettingsRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]]";
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By editLink = By.cssSelector("div[id='edit']");
	private static final By deleteLink = By.cssSelector("div[id='delete']");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By noRecordsAvailableTextMessage = By.xpath("//div[@class='ui red message ng-star-inserted' and text()=' No records available ']");

	private static final By globalSettingsTab = By.cssSelector("div[id='global-settings-card']");
	private static final By nameTextBox = By.cssSelector("input[name='data[name]']");
	private static final By submitButton = By.cssSelector("button[name='data[submit]']");
	private static final By DisruptionStatusTab = By.xpath("//span[text()='Disruption Status']");

	public DisruptionStatusPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - Global Settings Page");
	}

	/***
	 * Clicks on global settings tab
	 * Clicks on disruption status tab
	 */
	public void clickOnDisruptionStatusTab() {
		log.info("Start of the method clickOnDisruptionStatusTab");
		driver.safeClick(globalSettingsTab);
		driver.safeClick(DisruptionStatusTab);
		log.info("End of the method clickOnDisruptionStatusTab");
	}

	/**
	 * Creates a new disruption status
	 * @param disruptionStatusName : disruption status name in string format
	 * @param status : status in string format
	 */
	public void createDisruptionStatus(String disruptionStatusName, String status) {
		log.info("Start of the method createDisruptionStatus");
		driver.safeClick(addButton);
		driver.safeType(nameTextBox, disruptionStatusName);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createDisruptionStatus");
	}

	/**
	 * Verifies whether disruption status is displayed
	 * @param disruptionStatusName : disruption status name in string format
	 */
	public boolean isDisruptionStatusDisplayed(String disruptionStatusName) {
		log.info("Start of the method isDisruptionStatusDisplayed");
		boolean isDisruptionStatusDisplayed = driver.isPresentAndVisible(By.xpath(String.format(globalSettingsRow, disruptionStatusName)));
		log.info("End of the method isDisruptionStatusDisplayed");
		return isDisruptionStatusDisplayed;
	}

	/***
	 * Searches disruption status by name
	 * @param disruptionStatusName : disruption status name in string format
	 */
	public void searchDisruptionStatusByName(String disruptionStatusName) {
		log.info("Start of the method searchDisruptionStatusByName");
		driver.safeType(searchTextBox, disruptionStatusName);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(searchIconButton);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchDisruptionStatusByName");
	}

	public void clearDisruptionStatusTextField() {
		log.info("Start of the method clearDisruptionStatusTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearDisruptionStatusTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	/***
	 * Edits existing disruption status
	 * @param disruptionStatusName : disruption status name in string format
	 */
	public void editDisruptionStatus(String disruptionStatusName) {
		log.info("Start of the method editDisruptionStatus");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.safeType(nameTextBox, disruptionStatusName);
		driver.safeClick(submitButton);
		log.info("End of the method editDisruptionStatus");
	}

	/***
	 * Deletes disruption status
	 */
	public void deleteDisruptionStatus() {
		log.info("Start of the method deleteDisruptionStatus");
		driver.safeClick(verticalIconButton);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deleteDisruptionStatus");
	}

	/**
	 * Verifies No Records Available Message
	 * @return returns message display status
	 */
	public boolean verifyNoRecordsMessage() {
		log.info("Start of the method verifyNoRecordsMessage");
		boolean isMessageDisplayed = driver.isPresentAndVisible(noRecordsAvailableTextMessage);
		log.info("End of the method verifyNoRecordsMessage");
		return isMessageDisplayed;
	}

}