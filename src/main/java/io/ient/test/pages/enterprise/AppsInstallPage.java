package io.ient.test.pages.enterprise;


import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import java.util.List;


public class AppsInstallPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(AppsInstallPage.class);
	LogFactory log = new LogFactory(logger);

	//Common locators for all apps installation
	private static final By commentTextArea = By.cssSelector("textarea[placeholder='Comment']");
	private static final By commentTextAreaApproveButton = By.xpath("//button[text()='Approve']");
	private static final By appInstallRequestsTab = By.xpath("//div[@class='header_name' and text()=' App Install Requests ']");
	private static final By approvedTab = By.xpath("//a[span[text()='Approved']]");
	private static final By appSaveButton = By.xpath("//button[span[text()='Save']]");
	private static final By deployConfigTab = By.xpath("//a[@role='tab' and span[text()='Deploy Config']]");
	private static final By deployWorkflowButton = By.cssSelector("input[value='Deploy Workflow']");
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");

	//Bulletins App Installation Locators
	private static final By bulletinsInfoText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Bulletins ']]]/descendant::div[text()=' Info ']");
	private static final By bulletinsAppCard = By.xpath("//div[text()=' Bulletins ']");
	private static final By bulletinsRequestAccessText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Bulletins ']]]/descendant::span[text()='Request access']");
	private static final By bulletinsAccessRequestedText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Bulletins ']]]/descendant::span[text()='Access requested']");
	private static final By bulletinsInstallText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Bulletins ']]]/descendant::span[text()='Approved for install']");
	private static final By bulletinsRequestAccessIcon = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Bulletins ']]]/descendant::i[@class='arrow down circle icon bigicon']");
	private static final By bulletinsInstallIcon = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Bulletins ']]]/descendant::i[@class='download icon bigicon']");
	private static final By bulletinsInstallIconYesButton = By.xpath("//div[@class='confirm-container']/descendant::button[text()='Yes']");
	private static String bulletinsAppInstallRequestsVerticalIcon = "//tr[contains(@class,'ient-table-row') and td[span[contains(text(),' Bulletins ')]] and td[span[text()=' bmw ']]]";
	private static final By bulletinsAppCardInProjects = By.xpath("//div[@class='card project-card ng-star-inserted' and div[div[@class='header' and text()='Bulletins']]]");
	//	private static final By bulletinsInstalledAppText = By.xpath("//div[h2[text()='Installed Apps']]/descendant::div[text()=' Bulletins ']");

	//Shipping Calendar App Installation Locators
	private static final By shippingCalendarInfoText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Shipping Calendar ']]]/descendant::div[text()=' Info ']");
	private static final By shippingCalendarAppCard = By.xpath("//div[text()=' Shipping Calendar ']");
	private static final By shippingCalendarAppCardInProjects = By.xpath("//div[@class='card project-card ng-star-inserted' and div[div[@class='header' and text()='Shipping Calendar']]]");
	private static final By shippingCalendarRequestAccessText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Shipping Calendar ']]]/descendant::span[text()='Request access']");
	private static final By shippingCalendarRequestAccessIcon = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Shipping Calendar ']]]/descendant::i[@class='arrow down circle icon bigicon']");
	private static final By shippingCalendarAccessRequestedText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Shipping Calendar ']]]/descendant::span[text()='Access requested']");
	private static final By shippingCalendarInstallText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Shipping Calendar ']]]/descendant::span[text()='Approved for install']");
	private static String shippingCalendarInstallRequestsVerticalIcon = "//tr[contains(@class,'ient-table-row') and td[span[contains(text(),' Shipping Calendar ')]] and td[span[text()=' bmw ']]]";
	private static final By shippingCalendarInstallIcon = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Shipping Calendar ']]]/descendant::i[@class='download icon bigicon']");
	private static final By shippingCalendarInstallIconYesButton = By.xpath("//div[@class='confirm-container']/descendant::button[text()='Yes']");
	//	private static final By shippingCalendarInstalledAppText = By.xpath("//div[h2[text()='Installed Apps']]/descendant::div[text()=' Shipping Calendar ']");

	//Shipping Lanes App Installation Locators
	private static final By shippingLanesInfoText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Shipping Lanes ']]]/descendant::div[text()=' Info ']");
	private static final By shippingLanesAppCard = By.xpath("//div[text()=' Shipping Lanes ']");
	private static final By shippingLanesAppCardInProjects = By.xpath("//div[@class='card project-card ng-star-inserted' and div[div[@class='header' and text()='Shipping Lanes']]]");
	private static final By shippingLanesRequestAccessText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Shipping Lanes ']]]/descendant::span[text()='Request access']");
	private static final By shippingLanesRequestAccessIcon = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Shipping Lanes ']]]/descendant::i[@class='arrow down circle icon bigicon']");
	private static final By shippingLanesAccessRequestedText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Shipping Lanes ']]]/descendant::span[text()='Access requested']");
	private static final By shippingLanesInstallText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Shipping Lanes ']]]/descendant::span[text()='Approved for install']");
	private static String shippingLanesInstallRequestsVerticalIcon = "//tr[contains(@class,'ient-table-row') and td[span[contains(text(),' Shipping Lanes ')]] and td[span[text()=' bmw ']]]";
	private static final By shippingLanesInstallIcon = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Shipping Lanes ']]]/descendant::i[@class='download icon bigicon']");
	private static final By shippingLanesInstallIconYesButton = By.xpath("//div[@class='confirm-container']/descendant::button[text()='Yes']");
	//	private static final By shippingLanesInstalledAppText = By.xpath("//div[h2[text()='Installed Apps']]/descendant::div[text()=' Shipping Lanes ']");

	//Part Sourcing Attributes App Installation Locators
	private static final By psaInfoText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Part Sourcing Attributes ']]]/descendant::div[text()=' Info ']");
	private static final By psaAppCard = By.xpath("//div[text()=' Part Sourcing Attributes ']");
	private static final By psaAppCardInProjects = By.xpath("//div[@class='card project-card ng-star-inserted' and div[div[@class='header' and text()='Part Sourcing Attributes']]]");
	private static final By psaRequestAccessText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Part Sourcing Attributes ']]]/descendant::span[text()='Request access']");
	private static final By psaRequestAccessIcon = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Part Sourcing Attributes ']]]/descendant::i[@class='arrow down circle icon bigicon']");
	private static final By psaAccessRequestedText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Part Sourcing Attributes ']]]/descendant::span[text()='Access requested']");
	private static final By psaInstallText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Part Sourcing Attributes ']]]/descendant::span[text()='Approved for install']");
	private static String psaInstallRequestsVerticalIcon = "//tr[contains(@class,'ient-table-row') and td[span[contains(text(),' Part Sourcing Attributes ')]] and td[span[text()=' bmw ']]]";
	private static final By psaInstallIcon = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Part Sourcing Attributes ']]]/descendant::i[@class='download icon bigicon']");
	private static final By psaInstallIconYesButton = By.xpath("//div[@class='confirm-container']/descendant::button[text()='Yes']");
	//	private static final By psaInstalledAppText = By.xpath("//div[h2[text()='Installed Apps']]/descendant::div[text()=' Part Sourcing Attributes ']");

	//Supplier Capacity App Installation Locators
	private static final By supplierCapacityInfoText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Supplier Capacity ']]]/descendant::div[text()=' Info ']");
	private static final By supplierCapacityAppCard = By.xpath("//div[text()=' Supplier Capacity ']");
	private static final By supplierCapacityAppCardInProjects = By.xpath("//div[@class='card project-card ng-star-inserted' and div[div[@class='header' and text()='Supplier Capacity']]]");
	private static final By supplierCapacityRequestAccessText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Supplier Capacity ']]]/descendant::span[text()='Request access']");
	private static final By supplierCapacityRequestAccessIcon = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Supplier Capacity ']]]/descendant::i[@class='arrow down circle icon bigicon']");
	private static final By supplierCapacityAccessRequestedText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Supplier Capacity ']]]/descendant::span[text()='Access requested']");
	private static final By supplierCapacityInstallText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Supplier Capacity ']]]/descendant::span[text()='Approved for install']");
	private static String supplierCapacityInstallRequestsVerticalIcon = "//tr[contains(@class,'ient-table-row') and td[span[contains(text(),' Supplier Capacity ')]] and td[span[text()=' bmw ']]]";
	private static final By supplierCapacityInstallIcon = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Supplier Capacity ']]]/descendant::i[@class='download icon bigicon']");
	private static final By supplierCapacityInstallIconYesButton = By.xpath("//div[@class='confirm-container']/descendant::button[text()='Yes']");
	//	private static final By supplierCapacityInstalledAppText = By.xpath("//div[h2[text()='Installed Apps']]/descendant::div[text()=' Supplier Capacity ']");

	//Product Transitions App Installation Locators
	private static final By productTransitionsInfoText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Product Transitions ']]]/descendant::div[text()=' Info ']");
	private static final By productTransitionsAppCard = By.xpath("//div[text()=' Product Transitions ']");
	private static final By productTransitionsAppCardInProjects = By.xpath("//div[@class='card project-card ng-star-inserted' and div[div[@class='header' and text()='Product Transitions']]]");
	private static final By productTransitionsRequestAccessText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Product Transitions ']]]/descendant::span[text()='Request access']");
	private static final By productTransitionsRequestAccessIcon = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Product Transitions ']]]/descendant::i[@class='arrow down circle icon bigicon']");
	private static final By productTransitionsAccessRequestedText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Product Transitions ']]]/descendant::span[text()='Access requested']");
	private static final By productTransitionsInstallText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Product Transitions ']]]/descendant::span[text()='Approved for install']");
	private static String productTransitionsInstallRequestsVerticalIcon = "//tr[contains(@class,'ient-table-row') and td[span[contains(text(),' Product Transitions ')]] and td[span[text()=' bmw ']]]";
	private static final By productTransitionsInstallIcon = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Product Transitions ']]]/descendant::i[@class='download icon bigicon']");
	private static final By productTransitionsInstallIconYesButton = By.xpath("//div[@class='confirm-container']/descendant::button[text()='Yes']");
	//	private static final By productTransitionsInstalledAppText = By.xpath("//div[h2[text()='Installed Apps']]/descendant::div[text()=' Product Transitions ']");

	//Holiday Calendar  App Installation Locators
	private static final By holidayCalendarInfoText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Holiday Calendar ']]]/descendant::div[text()=' Info ']");
	private static final By holidayCalendarAppCard = By.xpath("//div[text()=' Holiday Calendar ']");
	private static final By holidayCalendarAppCardInProjects = By.xpath("//div[@class='card project-card ng-star-inserted' and div[div[@class='header' and text()='Holiday Calendar']]]");
	private static final By holidayCalendarRequestAccessText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Holiday Calendar ']]]/descendant::span[text()='Request access']");
	private static final By holidayCalendarRequestAccessIcon = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Holiday Calendar ']]]/descendant::i[@class='arrow down circle icon bigicon']");
	private static final By holidayCalendarAccessRequestedText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Holiday Calendar ']]]/descendant::span[text()='Access requested']");
	private static final By holidayCalendarInstallText = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Holiday Calendar ']]]/descendant::span[text()='Approved for install']");
	private static String holidayCalendarInstallRequestsVerticalIcon = "//tr[contains(@class,'ient-table-row') and td[span[contains(text(),' Holiday Calendar ')]] and td[span[text()=' bmw ']]]";
	private static final By holidayCalendarInstallIcon = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Holiday Calendar ']]]/descendant::i[@class='download icon bigicon']");
	private static final By holidayCalendarInstallIconYesButton = By.xpath("//div[@class='confirm-container']/descendant::button[text()='Yes']");
	//	private static final By holidayCalendarInstalledAppText = By.xpath("//div[h2[text()='Installed Apps']]/descendant::div[text()=' Holiday Calendar ']");

	public AppsInstallPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - AppInstallRequestsPage");
	}

	/***
	 * Clicks on app install requests tab
	 */
	public void clickOnAppInstallRequestsTab() {
		log.info("Start of the method clickOnAppInstallRequestsTab");
		driver.safeClick(appInstallRequestsTab);
		log.info("End of the method clickOnAppInstallRequestsTab");
	}

	/***
	 * Clicks on approve tab
	 */
	public void clickOnApproveTab() {
		log.info("Start of the method clickOnApproveTab");
		driver.safeClick(approvedTab);
		log.info("End of the method clickOnApproveTab");
	}

	/**
	 * Deploys Installed app after saving minor version
	 */
	public void deployInstalledApp() {
		log.info("Start of the method deployInstalledApp");
		driver.safeClick(appSaveButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(deployConfigTab);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		boolean isAppDeployedAlready = driver.isPresentAndVisible(deployWorkflowButton);
		if (isAppDeployedAlready == false) {
			log.info("App is already deployed");
		} else {
			driver.safeClick(deployWorkflowButton);
		}
		log.info("End of the method deployInstalledApp");
	}

	/***
	 * Searches by Application name
	 * @param appName : name in string format
	 */
	public void searchByApplicationName(String appName) {
		log.info("Start of the method searchByName");
		driver.safeType(searchTextBox, appName);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchByName");
	}

	/**
	 * Verifies whether Application already installed or not
	 */
	boolean isAppInstalled;
	public boolean isAppDisplayedUnderInstalledApps(String appName) {
		log.info("Start of the method isAppDisplayedUnderInstalledApps");
		if(appName == "Shipping Calendar"){
			isAppInstalled = driver.isPresentAndVisible(shippingCalendarInfoText);
		}else if(appName == "Bulletins"){
			isAppInstalled = driver.isPresentAndVisible(bulletinsInfoText);
		}else if(appName == "Shipping Lanes"){
			isAppInstalled = driver.isPresentAndVisible(shippingLanesInfoText);
		}else if(appName == "Part Sourcing Attributes"){
			isAppInstalled = driver.isPresentAndVisible(psaInfoText);
		}else if(appName == "Supplier Capacity"){
			isAppInstalled = driver.isPresentAndVisible(supplierCapacityInfoText);
		}else if(appName == "Product Transitions"){
			isAppInstalled = driver.isPresentAndVisible(productTransitionsInfoText);
		}else if(appName == "Holiday Calendar"){
			isAppInstalled = driver.isPresentAndVisible(holidayCalendarInfoText);
		}

		log.info("End of the method isAppDisplayedUnderInstalledApps");
		return isAppInstalled;
	}

	/**
	 * Verifies whether Application is displayed
	 */
	boolean isApplicationDisplayed;
	public boolean isAppDisplayed(String appName) {
		log.info("Start of the method isAppDisplayed");
		if(appName == "Shipping Calendar"){
			isApplicationDisplayed = driver.isPresentAndVisible(shippingCalendarAppCard);
		}else if(appName == "Bulletins"){
			isApplicationDisplayed = driver.isPresentAndVisible(bulletinsAppCard);
		}else if(appName == "Shipping Lanes"){
			isApplicationDisplayed = driver.isPresentAndVisible(shippingLanesAppCard);
		}else if(appName == "Part Sourcing Attributes"){
			isApplicationDisplayed = driver.isPresentAndVisible(psaAppCard);
		}else if(appName == "Supplier Capacity"){
			isApplicationDisplayed = driver.isPresentAndVisible(supplierCapacityAppCard);
		}else if(appName == "Product Transitions"){
			isApplicationDisplayed = driver.isPresentAndVisible(productTransitionsAppCard);
		}else if(appName == "Holiday Calendar"){
			isApplicationDisplayed = driver.isPresentAndVisible(holidayCalendarAppCard);
		}
		log.info("End of the method isAppDisplayed");
		return isApplicationDisplayed;
	}

	/**
	 * Verifies whether Application is displayed in Projects if it is displayed clicks on it
	 */
	boolean isAppDisplayedInProjects;
	public boolean isApplicationDisplayedInProjects(String appName) {
		log.info("Start of the method isApplicationDisplayedInProjects");
		if(appName == "Shipping Calendar"){
			isAppDisplayedInProjects = driver.isPresentAndVisible(shippingCalendarAppCardInProjects);
			if (isAppDisplayedInProjects) {
				driver.safeClick(shippingCalendarAppCardInProjects);
			}
		}else if(appName == "Bulletins"){
			isAppDisplayedInProjects = driver.isPresentAndVisible(bulletinsAppCardInProjects);
			if (isAppDisplayedInProjects) {
				driver.safeClick(bulletinsAppCardInProjects);
			}
		}else if(appName == "Shipping Lanes"){
			isAppDisplayedInProjects = driver.isPresentAndVisible(shippingLanesAppCardInProjects);
			if (isAppDisplayedInProjects) {
				driver.safeClick(shippingLanesAppCardInProjects);
			}
		}else if(appName == "Part Sourcing Attributes"){
			isAppDisplayedInProjects = driver.isPresentAndVisible(psaAppCardInProjects);
			if (isAppDisplayedInProjects) {
				driver.safeClick(psaAppCardInProjects);
			}
		}else if(appName == "Supplier Capacity"){
			isAppDisplayedInProjects = driver.isPresentAndVisible(supplierCapacityAppCardInProjects);
			if (isAppDisplayedInProjects) {
				driver.safeClick(supplierCapacityAppCardInProjects);
			}
		}else if(appName == "Product Transitions"){
			isAppDisplayedInProjects = driver.isPresentAndVisible(productTransitionsAppCardInProjects);
			if (isAppDisplayedInProjects) {
				driver.safeClick(productTransitionsAppCardInProjects);
			}
		}else if(appName == "Holiday Calendar"){
			isAppDisplayedInProjects = driver.isPresentAndVisible(holidayCalendarAppCardInProjects);
			if (isAppDisplayedInProjects) {
				driver.safeClick(holidayCalendarAppCardInProjects);
			}
		}
		log.info("End of the method isApplicationDisplayedInProjects");
		return isAppDisplayedInProjects;
	}

	/**
	 * Verifies Request Access is displayed under Application Card
	 */
	boolean isRequestAccessDisplayed;
	public boolean isApplicationRequestAccessTextDisplayed(String appName) {
		log.info("START OF the method isApplicationRequestAccessTextDisplayed");
		if(appName == "Shipping Calendar"){
			isRequestAccessDisplayed = driver.isPresentAndVisible(shippingCalendarRequestAccessText);
		}else if(appName == "Bulletins"){
			isRequestAccessDisplayed = driver.isPresentAndVisible(bulletinsRequestAccessText);
		}else if(appName == "Shipping Lanes"){
			isRequestAccessDisplayed = driver.isPresentAndVisible(shippingLanesRequestAccessText);
		}else if(appName == "Part Sourcing Attributes"){
			isRequestAccessDisplayed = driver.isPresentAndVisible(psaRequestAccessText);
		}else if(appName == "Supplier Capacity"){
			isRequestAccessDisplayed = driver.isPresentAndVisible(supplierCapacityRequestAccessText);
		}
		else if(appName == "Product Transitions"){
			isRequestAccessDisplayed = driver.isPresentAndVisible(productTransitionsRequestAccessText);
		}else if(appName == "Holiday Calendar"){
			isRequestAccessDisplayed = driver.isPresentAndVisible(holidayCalendarRequestAccessText);
		}
		log.info("END OF the method isApplicationRequestAccessTextDisplayed");
		return isRequestAccessDisplayed;
	}

	/***
	 * Clicks on Application Request Access Icon
	 */
	public void clickOnApplicationRequestAccessIcon(String appName) {
		log.info("Start of the method clickOnApplicationRequestAccessIcon");
		if(appName == "Shipping Calendar"){
			driver.safeClick(shippingCalendarRequestAccessIcon);
		}else if(appName == "Bulletins"){
			driver.safeClick(bulletinsRequestAccessIcon);
		}else if(appName == "Shipping Lanes"){
			driver.safeClick(shippingLanesRequestAccessIcon);
		}else if(appName == "Part Sourcing Attributes"){
			driver.safeClick(psaRequestAccessIcon);
		}else if(appName == "Supplier Capacity"){
			driver.safeClick(supplierCapacityRequestAccessIcon);
		}else if(appName == "Product Transitions"){
			driver.safeClick(productTransitionsRequestAccessIcon);
		}else if(appName == "Holiday Calendar"){
			driver.safeClick(holidayCalendarRequestAccessIcon);
		}
		log.info("End of the method clickOnApplicationRequestAccessIcon");
	}

	/**
	 * Verifies Access Requested is displayed under Application Card
	 */
	boolean isAccessRequestedDisplayed;
	public boolean isApplicationAccessRequestedTextDisplayed(String appName) {
		log.info("Start of the method isApplicationAccessRequestedTextDisplayed");
		if(appName == "Shipping Calendar"){
			isAccessRequestedDisplayed = driver.isPresentAndVisible(shippingCalendarAccessRequestedText);
		}else if(appName == "Bulletins"){
			isAccessRequestedDisplayed = driver.isPresentAndVisible(bulletinsAccessRequestedText);
		}else if(appName == "Shipping Lanes"){
			isAccessRequestedDisplayed = driver.isPresentAndVisible(shippingLanesAccessRequestedText);
		}else if(appName == "Part Sourcing Attributes"){
			isAccessRequestedDisplayed = driver.isPresentAndVisible(psaAccessRequestedText);
		}else if(appName == "Supplier Capacity"){
			isAccessRequestedDisplayed = driver.isPresentAndVisible(supplierCapacityAccessRequestedText);
		}else if(appName == "Product Transitions"){
			isAccessRequestedDisplayed = driver.isPresentAndVisible(productTransitionsAccessRequestedText);
		}else if(appName == "Holiday Calendar"){
			isAccessRequestedDisplayed = driver.isPresentAndVisible(holidayCalendarAccessRequestedText);
		}
		log.info("End of the method isApplicationAccessRequestedTextDisplayed");
		return isAccessRequestedDisplayed;
	}

	/**
	 * Verifies Install is displayed under Application Card
	 */
	boolean isInstallDisplayed;
	public boolean isApplicationInstallTextDisplayed(String appName) {
		log.info("Start of the method isApplicationInstallTextDisplayed");
		if(appName == "Shipping Calendar"){
			isInstallDisplayed = driver.isPresentAndVisible(shippingCalendarInstallText);
		}else if(appName == "Bulletins"){
			isInstallDisplayed = driver.isPresentAndVisible(bulletinsInstallText);
		}else if(appName == "Shipping Lanes"){
			isInstallDisplayed = driver.isPresentAndVisible(shippingLanesInstallText);
		}else if(appName == "Part Sourcing Attributes"){
			isInstallDisplayed = driver.isPresentAndVisible(psaInstallText);
		}else if(appName == "Supplier Capacity"){
			isInstallDisplayed = driver.isPresentAndVisible(supplierCapacityInstallText);
		}else if(appName == "Product Transitions"){
			isInstallDisplayed = driver.isPresentAndVisible(productTransitionsInstallText);
		}else if(appName == "Holiday Calendar"){
			isInstallDisplayed = driver.isPresentAndVisible(holidayCalendarInstallText);
		}
		log.info("End of the method isApplicationInstallTextDisplayed");
		return isInstallDisplayed;
	}

	/**
	 * Verifies whether Application install request row is displayed in the table
	 * @param requesterName : name of role in string format
	 * @return returns display status
	 */
	boolean isAppInstallRequestDisplayed;
	public boolean isApplicationInstallRequestRowDisplayed(String requesterName,String appName) {
		log.info("Start of the method isApplicationInstallRequestRowDisplayed");
		if(appName == "Shipping Calendar"){
			isAppInstallRequestDisplayed = driver.isPresentAndVisible(By.xpath(String.format(shippingCalendarInstallRequestsVerticalIcon, requesterName)));
		}else if(appName == "Bulletins"){
			isAppInstallRequestDisplayed = driver.isPresentAndVisible(By.xpath(String.format(bulletinsAppInstallRequestsVerticalIcon, requesterName)));
		}else if(appName == "Shipping Lanes"){
			isAppInstallRequestDisplayed = driver.isPresentAndVisible(By.xpath(String.format(shippingLanesInstallRequestsVerticalIcon, requesterName)));
		}else if(appName == "Part Sourcing Attributes"){
			isAppInstallRequestDisplayed = driver.isPresentAndVisible(By.xpath(String.format(psaInstallRequestsVerticalIcon, requesterName)));
		}else if(appName == "Supplier Capacity"){
			isAppInstallRequestDisplayed = driver.isPresentAndVisible(By.xpath(String.format(supplierCapacityInstallRequestsVerticalIcon, requesterName)));
		}else if(appName == "Product Transitions"){
			isAppInstallRequestDisplayed = driver.isPresentAndVisible(By.xpath(String.format(productTransitionsInstallRequestsVerticalIcon, requesterName)));
		}else if(appName == "Holiday Calendar"){
			isAppInstallRequestDisplayed = driver.isPresentAndVisible(By.xpath(String.format(holidayCalendarInstallRequestsVerticalIcon, requesterName)));
		}
		log.info("End of the method isApplicationInstallRequestRowDisplayed");
		return isAppInstallRequestDisplayed;
	}

	/**
	 * Approves Application installation request
	 * @param requesterName      : requester name in string format
	 * @param approveCommentText : comment in string format
	 */
	public void approveApplicationInstallationRequest(String requesterName, String approveCommentText,String appName) {
		log.info("Start of the method approveApplicationInstallationRequest");
		if(appName == "Shipping Calendar"){
			driver.safeClick(By.xpath(String.format(shippingCalendarInstallRequestsVerticalIcon, requesterName)));
		}else if(appName == "Bulletins"){
			driver.safeClick(By.xpath(String.format(bulletinsAppInstallRequestsVerticalIcon, requesterName)));
		}else if(appName == "Shipping Lanes"){
			driver.safeClick(By.xpath(String.format(shippingLanesInstallRequestsVerticalIcon, requesterName)));
		}else if(appName == "Part Sourcing Attributes"){
			driver.safeClick(By.xpath(String.format(psaInstallRequestsVerticalIcon, requesterName)));
		}else if(appName == "Supplier Capacity"){
			driver.safeClick(By.xpath(String.format(supplierCapacityInstallRequestsVerticalIcon, requesterName)));
		}else if(appName == "Product Transitions"){
			driver.safeClick(By.xpath(String.format(productTransitionsInstallRequestsVerticalIcon, requesterName)));
		}else if(appName == "Holiday Calendar"){
			driver.safeClick(By.xpath(String.format(holidayCalendarInstallRequestsVerticalIcon, requesterName)));
		}
//		driver.safeClick(appInstallRequestsApproveLink);
		driver.safeType(commentTextArea, approveCommentText);
		driver.safeClick(commentTextAreaApproveButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		log.info("End of the method approveApplicationInstallationRequest");
	}

	/***
	 * Installs Application
	 */
	public void installApplication(String appName) {
		log.info("Start of the method installApplication");
		if(appName == "Shipping Calendar"){
			driver.safeClick(shippingCalendarInstallIcon);
			driver.safeClick(shippingCalendarInstallIconYesButton);
		}else if(appName == "Bulletins"){
			driver.safeClick(bulletinsInstallIcon);
			driver.safeClick(bulletinsInstallIconYesButton);
		}else if(appName == "Shipping Lanes"){
			driver.safeClick(shippingLanesInstallIcon);
			driver.safeClick(shippingLanesInstallIconYesButton);
		}else if(appName == "Part Sourcing Attributes"){
			driver.safeClick(psaInstallIcon);
			driver.safeClick(psaInstallIconYesButton);
		}else if(appName == "Supplier Capacity"){
			driver.safeClick(supplierCapacityInstallIcon);
			driver.safeClick(supplierCapacityInstallIconYesButton);
		}else if(appName == "Product Transitions"){
			driver.safeClick(productTransitionsInstallIcon);
			driver.safeClick(productTransitionsInstallIconYesButton);
		}else if(appName == "Holiday Calendar"){
			driver.safeClick(holidayCalendarInstallIcon);
			driver.safeClick(holidayCalendarInstallIconYesButton);
		}
		log.info("End of the method installApplication");
	}

}