package io.ient.test.pages.enterprise;


import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class SupplierProductTransitionsPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(SupplierProductTransitionsPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By addButton = By.cssSelector("ient-button[icon='plus']>button");
	private static final By ptPart = By.xpath("//div[select[@name='data[part]']]");
	private static String partDropDownOption = "div[id*='choices--datapart'][data-id='1']>span";
	private static final By ptManufacturer = By.cssSelector("input[id='manufacturer']");
	private static final By ptLifecycleStatus = By.xpath("//div[select[@id='lifecycleStatus']]");
	private static String lifecycleStatusDropDownOption = "div[id*='lifecycleStatus-item-choice'][data-id='2']>span";
	private static final By ptLastTimeBuyDate = By.cssSelector("input[id='lastTimeBuyDate']+input");
	private static final By ptEstimatedEOLDate = By.cssSelector("input[id='estimatedEndOfLifeDate']+input");
	private static String datePicker = "//div[contains(@class,'open')]/descendant::span[@class='flatpickr-day ' and text()='%s']";
	private static final By verifyApproverDropdown = By.xpath("//div[select[@id='verifyApprover']]");
	private static String verifyApproverDropdownOption = "div[id*='verifyApprover-item-choice'][data-value='%s']>span";

	private static final By caseStatusDropDown = By.xpath("//div[select[@id='caseStatus']]");
	private static String submitToBuyerDropDownOption = "div[id*='caseStatus-item-choice'][data-id='2']>span";
	private static String saveAsDraftDropDownOption = "div[id*='caseStatus-item-choice'][data-value='Save as Draft']>span";
	private static String reworkDropDownOption = "div[id*='caseStatus-item-choice'][data-value='Rework']>span";
	private static String approvedDropDownOption = "div[id*='caseStatus-item-choice'][data-value='Approved']>span";
	private static String rejectedDropDownOption = "div[id*='caseStatus-item-choice'][data-value='Rejected']>span";
	private static final By ptAlternateNewPart = By.cssSelector("input[id='alternateNewPart']");
	private static final By ptNewPartAvailableDate = By.cssSelector("input[id='dateTime']+input");
	private static final By shortDescriptionTextArea = By.cssSelector("textarea[id='shortDescription']");
	private static final By saveButton = By.cssSelector("button[id='bottom-save-button']");

	private static final By filterButton = By.cssSelector("button[id='ient-table-filter-btn']");
	private static final By supplierFilter = By.xpath("//div[label[text()='Supplier']]/div/button");
	private static final By supplierFilterValue = By.xpath("//div[contains(@class,'drop-show-container')]/div[@class='drop-show']/label[contains(text(),' rrssi ')]/span");
	private static final By partFilter = By.xpath("//div[label[text()='Part']]/div/button");
	private static final By partFilterValue = By.xpath("//div[contains(@class,'drop-show-container')]/div[@class='drop-show']/label[contains(text(),' pt ')]/span");
	private static final By submitFiltersButton = By.cssSelector("button[id = 'submit-bsc-filter']");

	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static String userRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]]";

	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By viewButton = By.cssSelector("div[id='view']");
	private static final By editButtonInCaseForm = By.cssSelector("button[id='ient-btn-edit']");
	private static final By conversationTab = By.xpath("//span[text()='Conversations']");
	private static final By commentTextArea = By.cssSelector("textarea[placeholder='Comment'],textarea[placeholder='External Comment'],textarea[placeholder='Internal Comment']");
	private static final By submitComment = By.xpath("//span[text()='Submit']");
	private static final By detailsTab = By.xpath("//span[text()='Details']");


	public SupplierProductTransitionsPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - SupplierProductTransitionsPage");
	}

	/**
	 * Creates a new Product Transitions
	 * @param part        			  : part in string format
	 * @param manufacturer        	  : manufacturer in string format
	 * @param lifecycleStatus         : lifecycleStatus in string format
	 * @param lastTimeBuyDate 		  : lastTimeBuyDate in string format
	 * @param estimatedEOLDate        : estimatedEOLDate in string format
	 * @param caseStatus              : case Status in string format
	 * @param approverName            : approver name in string format
	 * @param alternateNewPart        : alternateNewPart in string format
	 * @param newPartAvailableDate    : newPartAvailableDate in string format
	 * @param shortDescription        : short Description in string format
	 **/
	public void createProductTransitions(String part, String manufacturer, String lifecycleStatus, String lastTimeBuyDate, String estimatedEOLDate, String caseStatus, String approverName, String alternateNewPart,String newPartAvailableDate, String shortDescription) {
		log.info("Start of the method createProductTransitions");
		driver.safeClick(addButton);
		driver.sleep(WaitTime.LONG_WAIT);
		driver.jSClick(ptPart);
		driver.safeClick(By.cssSelector(String.format(partDropDownOption, part)));
		driver.safeType(ptManufacturer, manufacturer);
		driver.jSClick(ptLifecycleStatus);
		driver.safeClick(By.cssSelector(String.format(lifecycleStatusDropDownOption, lifecycleStatus)));
		driver.jSClick(ptLastTimeBuyDate);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(datePicker, lastTimeBuyDate)));
		driver.jSClick(ptEstimatedEOLDate);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(datePicker, estimatedEOLDate)));
		driver.jSClick(caseStatusDropDown);
		if(caseStatus == "Submit to Buyer"){
			driver.safeClick(By.cssSelector(String.format(submitToBuyerDropDownOption, caseStatus)));
		}else if(caseStatus == "Save as Draft"){
			driver.safeClick(By.cssSelector(String.format(saveAsDraftDropDownOption, caseStatus)));
		}
		driver.jSClick(verifyApproverDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.cssSelector(String.format(verifyApproverDropdownOption, approverName)));
		driver.safeType(ptAlternateNewPart, alternateNewPart);
		driver.jSClick(ptNewPartAvailableDate);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(datePicker, newPartAvailableDate)));
		driver.safeType(shortDescriptionTextArea, shortDescription);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(saveButton);
		log.info("End of the method createProductTransitions");
	}

	/***
	 * Searches created Product Transitions by Short Description
	 * @param manufacturer : description in string format
	 */
	public void searchByManufacturer(String manufacturer) {
		log.info("Start of the method searchByManufacturer");
		driver.safeType(searchTextBox, manufacturer);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchByManufacturer");
	}

	/**
	 * Verifies whether Product Transitions is displayed
	 */
	public boolean isProductTransitionsDisplayed(String part) {
		log.info("Start of the method isProductTransitionsDisplayed");
		boolean isProductTransitionsDisplayed = driver.isPresentAndVisible(By.xpath(String.format(userRow, part)));
		log.info("End of the method isProductTransitionsDisplayed");
		return isProductTransitionsDisplayed;
	}

	/**
	 * Verifies whether Product Transitions is displayed
	 */
	public void selectsRecordAndClicksEdit() {
		log.info("Start of the method selectsRecordAndClicksEdit");
		driver.safeClick(verticalIconButton);
		driver.safeClick(viewButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		boolean isEditButtonDisplayed = driver.isPresentAndVisible(editButtonInCaseForm);
		if(isEditButtonDisplayed){
			driver.safeClick(editButtonInCaseForm);
			driver.sleep(WaitTime.SMALL_WAIT);
		}
		log.info("End of the method selectsRecordAndClicksEdit");
	}

	/**
	 * Adds comments in conversation tab
	 */
	public void addComment(){
		log.info("Start of the method addComment");
		driver.safeClick(conversationTab);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeType(commentTextArea, "A-test: Rework needed");
		driver.safeClick(submitComment);
		driver.safeClick(detailsTab);
		log.info("End of the method addComment");
	}


	/***
	 * status change case
	 * @param caseStatus : next status in string format
	 */
	public void editStatus(String caseStatus) {
		log.info("Start of the method editStatus");
		driver.jSClick(caseStatusDropDown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		if(caseStatus == "Submit to Buyer"){
			driver.safeClick(By.cssSelector(String.format(submitToBuyerDropDownOption, caseStatus)));
		}else if(caseStatus == "Rework"){
			driver.safeClick(By.cssSelector(String.format(reworkDropDownOption, caseStatus)));
		}else if(caseStatus == "Save as Draft"){
			driver.safeClick(By.cssSelector(String.format(saveAsDraftDropDownOption, caseStatus)));
		}else if(caseStatus == "Approved"){
			driver.safeClick(By.cssSelector(String.format(approvedDropDownOption, caseStatus)));
		}else if(caseStatus == "Rejected"){
			driver.safeClick(By.cssSelector(String.format(rejectedDropDownOption, caseStatus)));
		}
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(saveButton);
		log.info("End of the method editStatus");
	}

	/**
	 * Clicks on Filter Icon
	 */
	public void clickFilterIcon() {
		log.info("Start of the method clickFilterIcon");
		driver.safeClick(filterButton);
		log.info("End of the method clickFilterIcon");
	}

	/**
	 * Select filters and values under for buyer admin
	 */
	public void buyerAdminFilters() {
		log.info("Start of the method buyerAdminFilters");
		driver.safeClick(supplierFilter);
		driver.safeClick(supplierFilterValue);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(partFilter);
		driver.safeClick(partFilterValue);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(submitFiltersButton);
		log.info("End of the method buyerAdminFilters");
	}

	/**
	 * Select filters and values under for supplier admin
	 */
	public void supplierAdminFilters() {
		log.info("Start of the method supplierAdminFilters");
		driver.safeClick(partFilter);
		driver.safeClick(partFilterValue);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(submitFiltersButton);
		log.info("End of the method supplierAdminFilters");
	}

}