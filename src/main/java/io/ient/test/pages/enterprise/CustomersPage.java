package io.ient.test.pages.enterprise;

import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class CustomersPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(CustomersPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By addButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static final By statusDropdown = By.xpath("//div[select[@id='status']]");
	private static String statusDropdownOption = "//div[@role='option' and span[text()='%s']]";
	private static String globalSettingsRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]]";
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By editLink = By.cssSelector("div[id='edit']");
	private static final By deleteLink = By.cssSelector("div[id='delete']");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By noRecordsAvailableTextMessage = By.xpath("//div[@class='ui red message ng-star-inserted' and text()=' No records available ']");

	private static final By globalSettingsTab = By.cssSelector("div[id='global-settings-card']");
	private static final By nameTextBox = By.cssSelector("input[name='data[name]']");
	private static final By submitButton = By.cssSelector("button[name='data[submit]']");

	private static final By customersTab = By.xpath("//span[text()='Customers']");


	public CustomersPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - Global Settings Page");
	}

	/***
	 * Clicks on global settings tab
	 * Clicks on customers tab
	 */
	public void clickOnCustomersTab() {
		log.info("Start of the method clickOnCustomersTab");
		driver.safeClick(globalSettingsTab);
		driver.safeClick(customersTab);
		log.info("End of the method clickOnCustomersTab");
	}

	/**
	 * Creates a new customer
	 * @param customerName : customer name in string format
	 * @param status : status in string format
	 */
	public void createCustomer(String customerName, String status) {
		log.info("Start of the method createCustomer");
		driver.safeClick(addButton);
		driver.safeType(nameTextBox, customerName);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createCustomer");
	}

	/**
	 * Verifies whether customer is displayed
	 * @param customerName : customer name in string format
	 */
	public boolean isCustomerDisplayed(String customerName) {
		log.info("Start of the method isCustomerDisplayed");
		boolean isCustomerDisplayed = driver.isPresentAndVisible(By.xpath(String.format(globalSettingsRow, customerName)));
		log.info("End of the method isCustomerDisplayed");
		return isCustomerDisplayed;
	}

	/***
	 * Searches customer by name
	 * @param customerName : customer name in string format
	 */
	public void searchCustomerByName(String customerName) {
		log.info("Start of the method searchCustomerByName");
		driver.safeType(searchTextBox, customerName);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(searchIconButton);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchCustomerByName");
	}

	public void clearCustomerTextField() {
		log.info("Start of the method clearCustomerTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearCustomerTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	/***
	 * Edits existing customer
	 * @param customerName : customer name in string format
	 */
	public void editCustomer(String customerName) {
		log.info("Start of the method editCustomer");
		driver.safeClick(verticalIconButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(editLink);
		driver.safeType(nameTextBox, customerName);
		driver.safeClick(submitButton);
		log.info("End of the method editCustomer");
	}

	/***
	 * Deletes customer
	 */
	public void deleteCustomer() {
		log.info("Start of the method deleteCustomer");
		driver.safeClick(verticalIconButton);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deleteCustomer");
	}

	/**
	 * Verifies No Records Available Message
	 * @return returns message display status
	 */
	public boolean verifyNoRecordsMessage() {
		log.info("Start of the method verifyNoRecordsMessage");
		boolean isMessageDisplayed = driver.isPresentAndVisible(noRecordsAvailableTextMessage);
		log.info("End of the method verifyNoRecordsMessage");
		return isMessageDisplayed;
	}

}