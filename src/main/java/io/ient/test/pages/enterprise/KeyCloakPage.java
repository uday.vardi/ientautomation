package io.ient.test.pages.enterprise;


import io.ient.test.framework.exception.FrameworkException;
import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class KeyCloakPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(KeyCloakPage.class);
	LogFactory log = new LogFactory(logger);
    private static final By profileUserIcon  = By.xpath("//a[@class='dropdown-toggle ng-binding' and span[@class='pficon pficon-user']]");
	private static final By selectRealm  = By.cssSelector("h2[data-ng-hide='current.realm']");
    private static String realmTenant  = "//li/a[text()='%s']";
	private static final By addRealm  = By.xpath("//a[text()='Add realm']");
	private static final By nameTextBox = By.cssSelector("input[name='name']");
	private static final By createButton  = By.xpath("//button[@type='submit' and text()='Create']");
	private static final By clientsButton  = By.xpath("//a[text()=' Clients']");
	private static final By createClientButton  = By.xpath("//a[@id='createClient']");
	private static final By clientId  = By.xpath("//input[@id='clientId']");
	private static final By saveButton  = By.xpath("//button[text()='Save']");
	private static final By implicitFlowEnabled  = By.xpath("//div[input[@id='implicitFlowEnabled' and contains(@class,'ng-empty')]]");
	private static final By newRedirectUri  = By.xpath("//input[@id='newRedirectUri']");
	private static final By addNewRedirectUri  = By.xpath("//button[contains(@data-ng-click,'addRedirectUri')]");
    private static final By webOriginTextBox  = By.cssSelector("input#newWebOrigin");
	private static final By advancedSettingsFilter  = By.xpath("//span[text()='Advanced Settings']");
	private static final By accessTokenLifespan  = By.xpath("//input[@id='accessTokenLifespan']");
	private static final By clientSessionIdleTimeout  = By.xpath("//input[@id='clientSessionIdleTimeout']");
	private static final By clientSessionMaxLifespan  = By.xpath("//input[@id='clientSessionMaxLifespan']");
	private static final By clientOfflineSessionIdleTimeout  = By.xpath("//input[@id='clientOfflineSessionIdleTimeout']");
	private static final By clientOfflineSessionMaxLifespan  = By.xpath("//input[@id='clientOfflineSessionMaxLifespan']");
	private static final By usersButton  = By.xpath("//a[text()=' Users']");
	private static final By addUserButton  = By.xpath("//a[@id='createUser']");
	private static final By viewAllUsersButton  = By.xpath("//button[@id='viewAllUsers']");
	private static final By usernameTextBox  = By.xpath("//input[@id='username']");
	private static final By emailTextBox  = By.xpath("//input[@id='email']");
	private static final By usersSaveButton  = By.xpath("//button[text()='Save' and @data-ng-show]");
	private static final By credentialsTab  = By.xpath("//a[text()='Credentials']");
	private static final By newPasswordTextBox  = By.xpath("//input[@id='newPas']");
	private static final By confirmPasswordTextBox  = By.xpath("//input[@id='confirmPas']");
    private static final By temporaryDisabled  = By.xpath("//div[input[@id='temporaryPassword' and contains(@class,'ng-not-empty')]]");
	private static final By setPasswordButton  = By.xpath("//button[text()='Set Password']");
	private static final By setPasswordInPopup  = By.xpath("//button[text()='Set password' and @class='ng-binding btn btn-danger']");
    private static String tenantNameLink  = "//a[text()='%s' and contains(@href, '#/realm')]";
    private static final By deleteIcon  = By.cssSelector("i#removeRealm");
    private static final By deleteButtonOnPopup  = By.xpath("//button[text()='Delete']");
    private static final By adminDropdownLink = By.xpath("//a[@aria-expanded='false']");
    private static final By signOutLink = By.xpath("//a[text()='Sign Out']");
    private static final By searchUserTextBox = By.cssSelector("input[placeholder='Search...']");
    private static final By searchUserIconButton = By.cssSelector("i[id='userSearch']");
    private static String userEditLink  = "//tr[td[text()='%s']]/td[text()='Edit']";
    private static final By reSetPasswordButton  = By.xpath("//button[text()='Reset Password']");
    private static final By reSetPasswordInPopup  = By.xpath("//button[text()='Reset password' and @class='ng-binding btn btn-danger']");


	public KeyCloakPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - KeyCloakPage");
	}

    /**
     * Verifies Keycloak page
     * @return returns page open status
     */
    public boolean isKeyCloakPage() {
        log.info("Start of method - isKeyCloakPage");
        boolean KeyCloakPageStatus = false;
        try {
            KeyCloakPageStatus = driver.isPresentAndVisible(profileUserIcon);
        } catch (Exception e) {
            log.error("Failed to load isKeyCloakPage");
            throw new FrameworkException(e);
        }
        return KeyCloakPageStatus;
    }

    /**
     * Creates client
     * @param tenantName : tenant name in string format
     * @param redirectUrl1 : redirectUrl1 in string format
     * @param redirectUrl2: redirectUrl2 in string format
     * @param redirectUrl3: redirectUrl3 in string format
     * @param accessTokenLifeSpanText: accessTokenLifeSpanText in string format
     * @param clientSessionIdleText: clientSessionIdleText in string format
     * @param clientSessionMaxText: clientSessionMaxText in string format
     * @param clientOfflineSessionIdleText: clientOfflineSessionIdleText in string format
     * @param clientOfflineSessionMaxText: clientOfflineSessionMaxText in string format
     */
    public void createClient(String tenantName, String redirectUrl1, String redirectUrl2, String redirectUrl3, String accessTokenLifeSpanText, String clientSessionIdleText, String clientSessionMaxText, String clientOfflineSessionIdleText, String clientOfflineSessionMaxText) {
        log.info("Start of the method createClient");
        driver.safeMouseOver(selectRealm);
        //driver.safeClick(By.xpath(String.format(realmTenant, tenantName)));
        driver.safeClick(By.xpath(String.format(tenantNameLink, tenantName)));
//        driver.safeClick(addRealm);
//        driver.safeType(nameTextBox, tenantName);
//        driver.safeClick(createButton);
//        driver.sleep(WaitTime.LONG_WAIT);
//        driver.safeClick(clientsButton);
//        driver.safeClick(createClientButton);
//        String clientIdText = tenantName + "-app";
//        driver.safeType(clientId, clientIdText);
//        driver.safeClick(saveButton);
//        driver.safeClick(implicitFlowEnabled);
//        driver.safeType(newRedirectUri, redirectUrl1);
//        driver.safeClick(addNewRedirectUri);
//        driver.safeType(newRedirectUri, redirectUrl2);
//        driver.safeClick(addNewRedirectUri);
//        driver.safeType(newRedirectUri, redirectUrl3);
//        driver.sleep(WaitTime.XSMALL_WAIT);
//        driver.safeType(webOriginTextBox, "*");
//        driver.safeClick(advancedSettingsFilter);
//        driver.safeType(accessTokenLifespan, accessTokenLifeSpanText);
//        driver.safeType(clientSessionIdleTimeout, clientSessionIdleText);
//        driver.safeType(clientSessionMaxLifespan, clientSessionMaxText);
//        driver.safeType(clientOfflineSessionIdleTimeout, clientOfflineSessionIdleText);
//        driver.safeType(clientOfflineSessionMaxLifespan, clientOfflineSessionMaxText);
//        driver.safeClick(saveButton);
        log.info("End of the method createClient");
    }

    /**
     * Creates users
     * @param userName : user name in string format
     * @param emailAddress : email address in string format
     * @param password : password in string format
     */
    public void createUsers(String userName, String emailAddress, String password) {
        log.info("Start of the method createUsers");
        driver.safeClick(usersButton);
        driver.safeClick(addUserButton);
        driver.safeType(usernameTextBox, userName);
        driver.safeType(emailTextBox, emailAddress);
        driver.safeClick(usersSaveButton);
        driver.safeClick(credentialsTab);
        driver.safeType(newPasswordTextBox, password);
        driver.safeType(confirmPasswordTextBox, password);
        driver.safeClick(temporaryDisabled);
        driver.safeClick(setPasswordButton);
        driver.sleep(WaitTime.SMALL_WAIT);
        driver.safeClick(setPasswordInPopup);
        log.info("End of the method createUsers");
    }

    /**
     * Clicks on view all users link
     */
    public void clickOnViewAllUsers() {
        log.info("Start of the method clickOnViewAllUsers");
        driver.safeClick(viewAllUsersButton);
        log.info("End of the method clickOnViewAllUsers");
    }

    /**
     * Click on users link
     */
    public void clickOnUsers() {
        log.info("Start of the method clickOnUsers");
        driver.safeClick(usersButton);
        log.info("End of the method clickOnUsers");
    }

    /***
     * Searches user by name
     * @param userName : name of user in string format
     */
    public void searchUserByName(String userName) {
        log.info("Start of the method searchUserByName");
        driver.safeType(searchUserTextBox, userName);
        driver.sleep(WaitTime.SMALL_WAIT);
        driver.safeClick(searchUserIconButton);
        driver.safeClick(searchUserIconButton);
        log.info("End of the method searchUserByName");
    }

    /**
     * Update user password
     * @param userName : user name in string format
     * @param password : password in string format
     */
    public void updateUserPassword(String userName, String password) {
        log.info("Start of the method updateUserPassword");
        driver.safeClick(By.xpath(String.format(userEditLink, userName)));
        driver.safeClick(credentialsTab);
        driver.safeType(newPasswordTextBox, password);
        driver.safeType(confirmPasswordTextBox, password);
        driver.safeClick(temporaryDisabled);
        driver.safeClick(reSetPasswordButton);
        driver.sleep(WaitTime.SMALL_WAIT);
        driver.safeClick(reSetPasswordInPopup);
        log.info("End of the method updateUserPassword");
    }

    /**
     * Deletes tenant
     * @param tenantName : tenant name in string format
     */
    public void deleteTenant(String tenantName) {
        log.info("Start of the method deleteTenant");
        driver.safeClick(By.xpath(String.format(tenantNameLink, tenantName)));
        driver.safeClick(deleteIcon);
        driver.sleep(WaitTime.SMALL_WAIT);
        driver.safeClick(deleteButtonOnPopup);
        driver.sleep(WaitTime.SMALL_WAIT);
        log.info("End of the method deleteTenant");
    }

    /**
     * Click on tenant name link
     * @param tenantName : tenant name in string format
     */
    public void clickOnTenantName(String tenantName) {
        log.info("Start of the method clickOnTenantName");
        driver.safeClick(By.xpath(String.format(tenantNameLink, tenantName)));
        log.info("End of the method clickOnTenantName");
    }

    /**
     * Clicks on admin dropdown link
     */
    public void clickOnAdminDropDownLink()
    {
        log.info("Start of the method clickOnAdminDropDownLink");
        driver.safeClick(adminDropdownLink);
        log.info("End of the method clickOnAdminDropDownLink");
    }

    /**
     * Click on signout link
     */
    public void clickOnSignOutLink()
    {
        log.info("Start of the method clickOnSignOutLink");
        driver.safeClick(signOutLink);
        log.info("End of the method clickOnSignOutLink");
    }

    /**
     * Click on tenant link
     * @param tenantName : tenant name in string format
     */
    public void clickOnTenantLink(String tenantName)
    {
        log.info("Start of the method clickOnTenantLink");
        driver.safeClick(By.xpath(String.format(tenantNameLink, tenantName)));
        log.info("End of the method clickOnTenantLink");
    }

    /**
     * Verifies no tenant
     * @param tenantName : tenant name in string format
     * @return returns tenant display status
     */
    public boolean verifyNoTenant(String tenantName) {
        log.info("Start of the method verifyNoTenant");
        boolean isTeanantDisplayed = driver.isPresentAndVisible(By.xpath(String.format(tenantNameLink, tenantName)));
        log.info("End of the method verifyNoTenant");
        return isTeanantDisplayed;
    }
}