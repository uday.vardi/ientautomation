package io.ient.test.pages.enterprise;


import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class PlantDetailsPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(PlantDetailsPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By addButton = By.cssSelector("ient-button[icon='plus']>button");
	private static String dropdownOption = "//div[@role='option' and span[contains(text(),'%s')]]/span";
	private static final By statusDropdown = By.xpath("//div[div[label[contains(text(),'Status')]]]/parent::div/descendant::div[@class='form-control ui fluid selection dropdown']");
	private static String statusDropdownOption = "//div[@role='option' and span[text()='%s']]";
	private static final By submitButton = By.cssSelector("button[name='data[submit]']");
	private static String departmentNameRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]]";
	private static final By searchTextBox = By.cssSelector("input[placeholder='Search keyword']");
	private static final By searchIconButton = By.cssSelector("i[class='search icon link']");
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By viewLink = By.cssSelector("div[id='view']");
	private static final By editLink = By.cssSelector("div[id='edit']");
	private static final By deleteLink = By.cssSelector("div[id='delete']");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By noRecordsAvailableTextMessage = By.xpath("//div[@aria-hidden='false']/descendant::div[@class='ui red message ng-star-inserted' and text()=' No records available ']");
	private static final By departmentsTab = By.xpath("//span[contains(text(),'Departments')]");
	private static final By plantLineTab = By.xpath("//span[contains(text(),'Plant Lines')]");
	private static final By laborCodeTab = By.xpath("//span[contains(text(),'Labor Codes')]");

	private static final By searchTextBoxOnDetails = By.cssSelector("div[aria-hidden='false'] input[placeholder='Search keyword']");
	private static final By searchIconButtonOnDetails = By.cssSelector("div[aria-hidden='false'] input[placeholder='Search keyword']+i");

	private static final By verticalIconButtonOnDetails = By.cssSelector("ient-tab-panel[header='Departments'] i[id^='vertical-icon-']");
	private static final By verticalIconButtonOnDetailsInPlantLine = By.cssSelector("ient-tab-panel[header='Plant Lines'] i[id^='vertical-icon-']");
	private static final By verticalIconButtonOnDetailsInLaborCode = By.cssSelector("ient-tab-panel[header='Labor Codes'] i[id^='vertical-icon-']");

	private static final By addButtonInPlantDetails = By.cssSelector("div[aria-hidden='false'] ient-button[icon='plus']>button");

	private static final By nameTextBox = By.cssSelector("input[name='data[name]']");
	private static final By codeTextBox = By.cssSelector("input[name='data[code]']");
	private static final By downRateTextBox = By.cssSelector("input[name='data[downRate]']");
	private static final By rateTextBox = By.cssSelector("input[name='data[rate]']");


	public PlantDetailsPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - PlantsPage");
	}

	/***
	 * Clicks on plant details tab
	 */
	public void clickOnPlantDetailsTab() {
		log.info("Start of the method clickOnPlantDetailsTab");
		driver.safeClick(verticalIconButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(viewLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method clickOnPlantDetailsTab");
	}

	/***
	 * Creates a new department
	 * @param name : name of department in string format
	 * @param code : code of a department in string format
	 * @param status : status in string format
	 */

	public void createDepartment(String name, String code, String status) {
		log.info("Start of the method createDepartment");
		driver.safeClick(departmentsTab);
		driver.safeClick(addButton);
		driver.safeType(nameTextBox, name);
		driver.safeType(codeTextBox, code);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createDepartment");
	}

	/**
	 /**
	 * Verifies whether department is displayed
	 * @param name : name of department in string format
	 */

	public boolean isDepartmentDisplayed(String name) {
		log.info("Start of the method isDepartmentDisplayed");
		boolean isDepartmentDisplayed = driver.isPresentAndVisible(By.xpath(String.format(departmentNameRow, name)));
		log.info("End of the method isDepartmentDisplayed");
		return isDepartmentDisplayed;
	}

	/***
	 * Searches department by name
	 * @param name : name in string format
	 */
	public void searchDepartmentByName(String name) {
		log.info("Start of the method searchDepartmentByName");
		driver.safeType(searchTextBoxOnDetails, name);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(searchIconButtonOnDetails);
		driver.safeClick(searchIconButtonOnDetails);
		log.info("End of the method searchDepartmentByName");
	}
	public void clearNameTextField() {
		log.info("Start of the method clearNameTextField");
		driver.safeClear(searchTextBoxOnDetails);
		log.info("End of the method clearNameTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButtonOnDetails);
		log.info("End of the method clickOnSearchIcon");
	}

	/***
	 * Edits existing department
	 * @param name : name in string format
	 *
	 */
	public void editDepartment(String name) {
		log.info("Start of the method editDepartment");
		driver.safeClick(verticalIconButtonOnDetails);
		driver.safeClick(editLink);
		driver.safeType(nameTextBox, name);
		driver.safeClick(submitButton);
		log.info("End of the method editDepartment");
	}

	/***
	 * Edits existing plant line
	 * @param name : name in string format
	 *
	 */
	public void editPlantLine(String name) {
		log.info("Start of the method editPlantLine");
		driver.safeClick(verticalIconButtonOnDetailsInPlantLine);
		driver.safeClick(editLink);
		driver.safeType(nameTextBox, name);
		driver.safeClick(submitButton);
		log.info("End of the method editPlantLine");
	}
	/***
	 * Edits existing labor codes
	 * @param name : name in string format
	 *
	 */
	public void editLaborCodes(String name) {
		log.info("Start of the method editLaborCodes");
		driver.safeClick(verticalIconButtonOnDetailsInLaborCode);
		driver.safeClick(editLink);
		driver.safeType(nameTextBox, name);
		driver.safeClick(submitButton);
		log.info("End of the method editLaborCodes");
	}

	/***
	 * Deletes department
	 */
	public void deleteDepartment() {
		log.info("Start of the method deleteDepartment");
		driver.safeClick(verticalIconButtonOnDetails);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deleteDepartment");
	}
	/***
	 * Deletes plant line
	 */
	public void deletePlantLine() {
		log.info("Start of the method deletePlantLine");
		driver.safeClick(verticalIconButtonOnDetailsInPlantLine);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deletePlantLine");
	}
	/***
	 * Deletes Labor Codes
	 */
	public void deleteLaborCodes() {
		log.info("Start of the method deleteLaborCodes");
		driver.safeClick(verticalIconButtonOnDetailsInLaborCode);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deleteLaborCodes");
	}

	/**
	 * Verifies No Records Available Message
	 * @return returns message display status
	 */
	public boolean verifyNoRecordsMessage() {
		log.info("Start of the method verifyNoRecordsMessage");
		boolean isMessageDisplayed = driver.isPresentAndVisible(noRecordsAvailableTextMessage);
		log.info("End of the method verifyNoRecordsMessage");
		return isMessageDisplayed;
	}

	/***
	 * Creates a new plant line
	 * @param name : name of plant line in string format
	 * @param downRate : code of a plant line in string format
	 * @param status : status in string format
	 */

	public void createPlantLIne(String name, String downRate, String status) {
		log.info("Start of the method createPlantLIne");
		driver.safeClick(plantLineTab);
		driver.safeClick(addButtonInPlantDetails);
		driver.safeType(nameTextBox, name);
		driver.safeType(downRateTextBox, downRate);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createPlantLIne");
	}

	/***
	 * Creates a new labor code
	 * @param name : name of labor code in string format
	 * @param code : code of labor code in string format
	 * @param rate : rate of a labor code in string format
	 * @param status : status in string format
	 */

	public void createLaborCode(String name, String code, String rate, String status) {
		log.info("Start of the method createLaborCode");
		driver.safeClick(laborCodeTab);
		driver.safeClick(addButtonInPlantDetails);
		driver.safeType(nameTextBox, name);
		driver.safeType(codeTextBox, code);
		driver.safeType(rateTextBox, rate);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createLaborCode");
	}

}