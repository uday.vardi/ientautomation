package io.ient.test.pages.enterprise;


import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class PortalConfigurationPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(PortalConfigurationPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By portalConfigurationTab = By.cssSelector("div[id='portal-config-card']");

	private static final By organizationTextBox = By.cssSelector("input[name='data[headerWhiteLabel]']");
	private static final By bodyTextAreaDiv = By.xpath("//div[@class='ace_layer ace_cursor-layer ace_hidden-cursors']");
	private static final By bodyImageField = By.cssSelector("div[label='Body Image']");
	private static final By logoImageField = By.cssSelector("div[label='Logo Image']");
	private static final By bodyColorField = By.cssSelector("div[label='Body Color']");
	private static final By clearButton = By.xpath("//button[contains(text(),'Clear')]");
	private static final By cancelButton = By.xpath("//button[contains(text(),'Cancel')]");
	private static final By submitButton = By.xpath("//button[contains(text(),'Submit')]");

	private static String organizationHeaderNameText = "//div[@class='org-name' and text()='%s']";
	private static final By bodyTextArea = By.xpath("//div[@class='ace_cursor']");
	private static final By editButton = By.cssSelector("ient-button[label='Edit']");
	private static final By discardButton = By.cssSelector("ient-button[label='Discard']");

	public PortalConfigurationPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - PortalConfigurationPage");
	}

	/***
	 * Clicks on portal configuration tab
	 */
	public void clickOnPortalConfigurationTab() {
		log.info("Start of the method clickOnPortalConfigurationTab");
		driver.safeClick(portalConfigurationTab);
		log.info("End of the method clickOnPortalConfigurationTab");
	}

	/**
	 * clicks edit button
	 */
	public void clickEditButton() {
		driver.safeClick(editButton);
		log.info("Edit button clicked");
	}

	/***
	 * Edits a portal configuration
	 * @param name : name of portal configuration in string format
	 * @param body : body text of portal configuration in string format
	 */
	public void editPortalConfiguration(String name, String body) {
		log.info("Start of the method editPortalConfiguration");
		driver.safeClick(editButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeType(organizationTextBox, name);
		driver.jSClick(bodyTextAreaDiv);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClearAndTypeByActionsClass(bodyTextArea, body, false);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(submitButton);
		log.info("End of the method editPortalConfiguration");
	}

	/**
	 * Verifies Header Text Message
	 * organizationName : organization name in string format
	 * @return returns message display status
	 */
	public boolean verifyHeaderName(String  organizationName) {
		log.info("Start of the method verifyHeaderName");
		boolean isHeaderMatched = driver.isPresentAndVisible(By.xpath(String.format(organizationHeaderNameText, organizationName)));
		log.info("End of the method verifyHeaderName");
		return isHeaderMatched;
	}

	/**
	 * Verifies whether organizationName TextBox is displayed
	 */
	public boolean isOrganizationNameTextBoxDisplayed() {
		log.info("Start of the method isOrganizationNameTextBoxDisplayed");
		boolean isOrganizationNameTextBoxDisplayed = driver.isPresentAndVisible(organizationTextBox);
		log.info("End of the method isOrganizationNameTextBoxDisplayed");
		return isOrganizationNameTextBoxDisplayed;
	}

	/**
	 * Verifies whether bodyTextAreaDiv TextBox is displayed
	 */
	public boolean isBodyContentTextAreaDivDisplayed() {
		log.info("Start of the method isBodyContentTextAreaDivDisplayed");
		boolean isBodyContentTextAreaDivDisplayed = driver.isPresentAndVisible(bodyTextAreaDiv);
		log.info("End of the method isBodyContentTextAreaDivDisplayed");
		return isBodyContentTextAreaDivDisplayed;
	}

	/**
	 * Verifies whether bodyImageField is displayed
	 */
	public boolean isBodyImageFieldDisplayed() {
		log.info("Start of the method isBodyImageFieldDisplayed");
		boolean isBodyImageFieldDisplayed = driver.isPresentAndVisible(bodyImageField);
		log.info("End of the method isBodyImageFieldDisplayed");
		return isBodyImageFieldDisplayed;
	}

	/**
	 * Verifies whether logoImageField is displayed
	 */
	public boolean isLogoImageFieldDisplayed() {
		log.info("Start of the method isLogoImageFieldDisplayed");
		boolean isLogoImageFieldDisplayed = driver.isPresentAndVisible(logoImageField);
		log.info("End of the method isLogoImageFieldDisplayed");
		return isLogoImageFieldDisplayed;
	}

	/**
	 * Verifies whether bodyColorField is displayed
	 */
	public boolean isBodyColorFieldDisplayed() {
		log.info("Start of the method isBodyColorFieldDisplayed");
		boolean isBodyColorFieldDisplayed = driver.isPresentAndVisible(bodyColorField);
		log.info("End of the method isBodyColorFieldDisplayed");
		return isBodyColorFieldDisplayed;
	}

	/**
	 * Verifies whether clearButton is displayed
	 */
	public boolean isClearButtonDisplayed() {
		log.info("Start of the method isClearButtonDisplayed");
		boolean isClearButtonDisplayed = driver.isPresentAndVisible(clearButton);
		log.info("End of the method isClearButtonDisplayed");
		return isClearButtonDisplayed;
	}

	/**
	 * Verifies whether cancelButton is displayed
	 */
	public boolean isCancelButtonDisplayed() {
		log.info("Start of the method isCancelButtonDisplayed");
		boolean isCancelButtonDisplayed = driver.isPresentAndVisible(cancelButton);
		log.info("End of the method isCancelButtonDisplayed");
		return isCancelButtonDisplayed;
	}

	/**
	 * Verifies whether submitButton is displayed
	 */
	public boolean isSubmitButtonDisplayed() {
		log.info("Start of the method isSubmitButtonDisplayed");
		boolean isSubmitButtonDisplayed = driver.isPresentAndVisible(submitButton);
		log.info("End of the method isSubmitButtonDisplayed");
		return isSubmitButtonDisplayed;
	}
}