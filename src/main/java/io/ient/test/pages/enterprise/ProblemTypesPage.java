package io.ient.test.pages.enterprise;

import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class ProblemTypesPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(ProblemTypesPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By addButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static final By statusDropdown = By.xpath("//div[select[@id='status']]");
	private static String statusDropdownOption = "//div[@role='option' and span[text()='%s']]";
	private static final By problemTypeDropdown = By.xpath("//div[select[@name='data[parentId]']]");
	private static String problemTypeDropdownOption = "//div[@role='option' and span[text()='%s']]";
	private static String globalSettingsTabRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]]";
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By editLink = By.cssSelector("div[id='edit']");
	private static final By deleteLink = By.cssSelector("div[id='delete']");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By noRecordsAvailableTextMessage = By.xpath("//div[@class='ui red message ng-star-inserted' and text()=' No records available ']");
	private static String expandProblemTypeLink = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]]/td/i[contains(@class,'angle right icon')]";
	private static String problemTypeVerticalIcon = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]]/td/div[@class='ui icon top left pointing item dropdown']/i[contains(@class,'ellipsis vertical icon')]";

	private static final By globalSettingsTab = By.cssSelector("div[id='global-settings-card']");
	private static final By nameTextBox = By.cssSelector("input[name='data[name]']");
	private static final By submitButton = By.cssSelector("button[name='data[submit]']");

	private static final By problemTypesTab = By.xpath("//span[text()='Problem Types']");


	public ProblemTypesPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - Global Settings Page");
	}

	/***
	 * Clicks on global settings tab
	 * Clicks on problem types tab
	 */
	public void clickOnProblemTypesTab() {
		log.info("Start of the method clickOnProblemTypesTab");
		driver.safeClick(globalSettingsTab);
		driver.safeClick(problemTypesTab);
		log.info("End of the method clickOnProblemTypesTab");
	}

	/**
	 * Creates a new problem type
	 * @param problemTypeName : problem type name in string format
	 * @param status : status in string format
	 */
	public void createProblemType(String problemTypeName, String status) {
		log.info("Start of the method createProblemType");
		driver.safeClick(addButton);
		driver.safeType(nameTextBox, problemTypeName);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createProblemType");
	}

	/**
	 * Creates a new problem type
	 * @param problemTypeName : problem type name in string format
	 * @param status : status in string format
	 * @param parentProblemType : parentProblemType in string format
	 */
	public void createProblemType(String problemTypeName, String status, String parentProblemType) {
		log.info("Start of the method createProblemType");
		driver.safeClick(addButton);
		driver.safeType(nameTextBox, problemTypeName);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(problemTypeDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(problemTypeDropdownOption, parentProblemType)));
		driver.safeClick(submitButton);
		log.info("End of the method createProblemType");
	}

	/**
	 * Verifies whether problem type is displayed
	 * @param problemTypeName : problem type name in string format
	 */
	public boolean isProblemTypeDisplayed(String problemTypeName) {
		log.info("Start of the method isProblemTypeDisplayed");
		boolean isProblemTypeDisplayed = driver.isPresentAndVisible(By.xpath(String.format(globalSettingsTabRow, problemTypeName)));
		log.info("End of the method isProblemTypeDisplayed");
		return isProblemTypeDisplayed;
	}

	/***
	 * Searches problem type by name
	 * @param problemTypeName : problem type name in string format
	 */
	public void searchProblemTypeByName(String problemTypeName) {
		log.info("Start of the method searchProblemTypeByName");
		driver.safeType(searchTextBox, problemTypeName);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(searchIconButton);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchProblemTypeByName");
	}

	public void clearProblemTypeTextField() {
		log.info("Start of the method clearProblemTypeTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearProblemTypeTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	/***
	 * Edits existing problem type
	 * @param problemTypeName : problem type name in string format
	 */
	public void editProblemType(String problemTypeName) {
		log.info("Start of the method editProblemType");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.safeType(nameTextBox, problemTypeName);
		driver.safeClick(submitButton);
		log.info("End of the method editProblemType");
	}

	/***
	 * Deletes problem type
	 */
	public void deleteProblemType() {
		log.info("Start of the method deleteProblemType");
		driver.safeClick(verticalIconButton);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deleteProblemType");
	}

	/***
	 * Deletes problem type
	 * @param problemTypeName : problem type name in string format
	 */
	public void deleteProblemType(String problemTypeName) {
		log.info("Start of the method deleteProblemType");
		driver.safeClick(By.xpath(String.format(problemTypeVerticalIcon, problemTypeName)));
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deleteProblemType");
	}

	/**
	 * Verifies No Records Available Message
	 * @return returns message display status
	 */
	public boolean verifyNoRecordsMessage() {
		log.info("Start of the method verifyNoRecordsMessage");
		boolean isMessageDisplayed = driver.isPresentAndVisible(noRecordsAvailableTextMessage);
		log.info("End of the method verifyNoRecordsMessage");
		return isMessageDisplayed;
	}

	/**
	 * Expands problem type
	 * @param problemTypeName : problem type name in string format
	 */
	public void expandProblemType(String problemTypeName) {
		log.info("Start of the method expandProblemType");
		driver.safeClick(By.xpath(String.format(expandProblemTypeLink, problemTypeName)));
		log.info("End of the method expandProblemType");
	}

	/***
	 * Edits existing problem type
	 * @param problemTypeName : problem type name in string format
	 */
	public void editProblemTypeBySearch(String problemTypeName, String newProblemTypeName) {
		log.info("Start of the method editProblemTypeBySearch");
		driver.safeClick(By.xpath(String.format(problemTypeVerticalIcon, problemTypeName)));
		driver.safeClick(editLink);
		driver.safeType(nameTextBox, newProblemTypeName);
		driver.safeClick(submitButton);
		log.info("End of the method editProblemTypeBySearch");
	}


}