package io.ient.test.pages.enterprise;

import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class SupplierLocationsPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(SupplierLocationsPage.class);
	LogFactory log = new LogFactory(logger);
	private static final By addButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static String locationRow ="//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]]";
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By editLink = By.cssSelector("div[id='edit']");
	private static final By deleteLink = By.cssSelector("div[id='delete']");
	private static final By viewLink = By.cssSelector("div[id='view']");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By noRecordsAvailableTextMessage = By.xpath("//div[@class='ui red message ng-star-inserted' and text()=' No records available ']");
	private static final By submitButton = By.xpath("//button[contains(text(),'Submit')]");

	private static final By supplierLocationTab = By.xpath("//span[contains(text(),'Supplier Locations')]");
	private static final By locationNameTextBox = By.cssSelector("input[name='data[locationName]']");
	private static final By businessNameTextBox = By.cssSelector("input[name='data[businessName]']");
	private static final By streetTextBox = By.cssSelector("input[name='data[street]']");
	private static final By postalCodeTextBox = By.cssSelector("input[name='data[postalCode]']");
	private static final By cityTextBox = By.cssSelector("input[name='data[city]']");
	private static final By countryTextBox = By.cssSelector("input[name='data[country]']");
	private static final By phoneNumberTextBox = By.cssSelector("input[name='data[phoneNumber]']");
	private static final By faxTextBox = By.cssSelector("input[name='data[fax]']");
	private static final By dunsTextBox = By.cssSelector("input[name='data[duns]']");
	private static final By countryOfBusinessHqTextBox = By.cssSelector("input[name='data[countryOfBusinessHq]']");
	private static final By statusDropdown = By.xpath("//div[select[@id='status']]");
	private static String statusDropdownOption = "//div[@role='option' and span[text()='%s']]";

	public SupplierLocationsPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - Global Settings Page");
	}

	/***
	 * Clicks on supplier locations tab
	 */
	public void clickOnSupplierLocationsTab() {
		log.info("Start of the method clickOnSupplierLocationsTab");
		driver.safeClick(verticalIconButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(viewLink);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(supplierLocationTab);
		log.info("End of the method clickOnSupplierLocationsTab");
	}

	/**
	 * creates a supplier location
	 * @param locationName :  location name in string format
	 * @param businessName : business name in string format
	 * @param street : street in string format
	 * @param postalCode : postal code in string format
	 * @param city : city in string format
	 * @param country : country in string format
	 * @param phoneNumber : phone number in string format
	 * @param fax : fax in string format
	 * @param duns : duns in string format
	 * @param countryOfBusinessHq : country of business Hq in string format
	 * @param status : status in string format
	 */
	public void createSupplierLocation(String locationName, String businessName, String street, String postalCode, String city, String country, String phoneNumber, String fax, String duns, String countryOfBusinessHq, String status) {
		log.info("Start of the method createSupplierLocation");
		driver.safeClick(addButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeType(locationNameTextBox, locationName);
		driver.safeType(businessNameTextBox, businessName);
		driver.safeType(streetTextBox, street);
		driver.safeType(postalCodeTextBox, postalCode);
		driver.safeType(cityTextBox, city);
		driver.safeType(countryTextBox, country);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClearAndTypeByActionsClass(phoneNumberTextBox, phoneNumber, false);
		driver.safeClearAndTypeByActionsClass(faxTextBox, fax, false);
		driver.safeClearAndTypeByActionsClass(dunsTextBox, duns,false);
		driver.safeType(countryOfBusinessHqTextBox, countryOfBusinessHq);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createSupplierLocation");
	}

	/**
	 * Verifies whether location is displayed
	 * @param locationName : location name in string format
	 */
	public boolean isLocationDisplayed(String locationName) {
		log.info("Start of the method isLocationDisplayed");
		boolean isLocationDisplayed = driver.isPresentAndVisible(By.xpath(String.format(locationRow, locationName)));
		log.info("End of the method isLocationDisplayed");
		return isLocationDisplayed;
	}

	/***
	 * Searches location by name
	 * @param locationName : location name in string format
	 */
	public void searchLocationByName(String locationName) {
		log.info("Start of the method searchLocationByName");
		driver.safeType(searchTextBox, locationName);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(searchIconButton);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchLocationByName");
	}

	public void clearLocationTextField() {
		log.info("Start of the method clearLocationTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearLocationTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	/***
	 * Edits existing location
	 * @param locationName : location name in string format
	 */
	public void editLocation(String locationName) {
		log.info("Start of the method editLocation");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeType(locationNameTextBox, locationName);
		driver.safeClick(submitButton);
		log.info("End of the method editLocation");
	}

	/***
	 * Deletes location
	 */
	public void deleteLocation() {
		log.info("Start of the method deleteLocation");
		driver.safeClick(verticalIconButton);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deleteLocation");
	}

	/**
	 * Verifies No Records Available Message
	 * @return returns message display status
	 */
	public boolean verifyNoRecordsMessage() {
		log.info("Start of the method verifyNoRecordsMessage");
		boolean isMessageDisplayed = driver.isPresentAndVisible(noRecordsAvailableTextMessage);
		log.info("End of the method verifyNoRecordsMessage");
		return isMessageDisplayed;
	}

}