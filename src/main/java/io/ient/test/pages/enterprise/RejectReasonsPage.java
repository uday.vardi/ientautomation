package io.ient.test.pages.enterprise;

import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class RejectReasonsPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(RejectReasonsPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By addButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static final By statusDropdown = By.xpath("//div[select[@id='status']]");
	private static String statusDropdownOption = "//div[@role='option' and span[text()='%s']]";
	private static String globalSettingsRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]]";
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By editLink = By.cssSelector("div[id='edit']");
	private static final By deleteLink = By.cssSelector("div[id='delete']");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By noRecordsAvailableTextMessage = By.xpath("//div[@class='ui red message ng-star-inserted' and text()=' No records available ']");

	private static final By globalSettingsTab = By.cssSelector("div[id='global-settings-card']");
	private static final By rejectReasonsTab = By.xpath("//span[text()='Reject Reasons']");
	private static final By nameTextBox = By.cssSelector("input[name='data[name]']");
	private static final By submitButton = By.cssSelector("button[name='data[submit]']");


	public RejectReasonsPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - Global Settings Page");
	}

	/***
	 * Clicks on global settings tab
	 * Clicks on reject reasons tab
	 */
	public void clickOnRejectReasonsTab() {
		log.info("Start of the method clickOnRejectReasonsTab");
		driver.safeClick(globalSettingsTab);
		driver.safeClick(rejectReasonsTab);
		log.info("End of the method clickOnRejectReasonsTab");
	}

	/**
	 * Creates a new reject reasons
	 * @param rejectReasonsName : reject reason name in string format
	 * @param status : status in string format
	 */
	public void createRejectReasons(String rejectReasonsName, String status) {
		log.info("Start of the method createRejectReasons");
		driver.safeClick(addButton);
		driver.safeType(nameTextBox, rejectReasonsName);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createRejectReasons");
	}

	/**
	 * Verifies whether reject reasons is displayed
	 * @param rejectReasonName : reject reason name in string format
	 */
	public boolean isRejectReasonDisplayed(String rejectReasonName) {
		log.info("Start of the method isRejectReasonDisplayed");
		boolean isRejectReasonDisplayed = driver.isPresentAndVisible(By.xpath(String.format(globalSettingsRow, rejectReasonName)));
		log.info("End of the method isRejectReasonDisplayed");
		return isRejectReasonDisplayed;
	}

	/***
	 * Searches reject reason by name
	 * @param rejectReasonName : reject reason name in string format
	 */
	public void searchRejectReasonByName(String rejectReasonName) {
		log.info("Start of the method searchRejectReasonByName");
		driver.safeType(searchTextBox, rejectReasonName);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(searchIconButton);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchRejectReasonByName");
	}

	public void clearRejectReasonTextField() {
		log.info("Start of the method clearRejectReasonTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearRejectReasonTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	/***
	 * Edits existing reject reason
	 * @param rejectReasonName : reject reason name in string format
	 */
	public void editRejectReason(String rejectReasonName) {
		log.info("Start of the method editRejectReason");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.safeType(nameTextBox, rejectReasonName);
		driver.safeClick(submitButton);
		log.info("End of the method editRejectReason");
	}

	/***
	 * Deletes reject reason
	 */
	public void deleteRejectReason() {
		log.info("Start of the method deleteRejectReason");
		driver.safeClick(verticalIconButton);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deleteRejectReason");
	}

	/**
	 * Verifies No Records Available Message
	 * @return returns message display status
	 */
	public boolean verifyNoRecordsMessage() {
		log.info("Start of the method verifyNoRecordsMessage");
		boolean isMessageDisplayed = driver.isPresentAndVisible(noRecordsAvailableTextMessage);
		log.info("End of the method verifyNoRecordsMessage");
		return isMessageDisplayed;
	}

}