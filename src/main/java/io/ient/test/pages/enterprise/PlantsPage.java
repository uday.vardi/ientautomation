package io.ient.test.pages.enterprise;


import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class PlantsPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(PlantsPage.class);
	LogFactory log = new LogFactory(logger);
	private static final By addButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static final By dunsTextBox = By.cssSelector("input[id='duns']");
	private static final By costRecoveryThresholdTextBox = By.cssSelector("input[id='costRecoveryThreshold']");
	private static final By defaultCurrencyDropdown = By.xpath("//div[select[@id='currency']]");
	private static final By localTimeZoneDropdown = By.xpath("//div[select[@id='timeZone']]");
	private static String dropdownOption = "//div[@role='option' and span[contains(text(),'%s')]]/span";
	private static final By statusDropdown = By.xpath("//div[select[@id='status']]");
	private static String statusDropdownOption = "//div[@role='option' and span[text()='%s']]";
	private static final By businessOrganizationDropdown = By.xpath("//div[select[@id='parentBusinessOrgId']]");
	private static String businessOrganizationDropdownOption = "//div[@role='option' and span[text()='%s']]";
	private static final By submitButton = By.cssSelector("button[name='data[submit]']");
	private static String plantsRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]]";
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By editLink = By.cssSelector("div[id='edit']");
	private static final By deleteLink = By.cssSelector("div[id='delete']");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By noRecordsAvailableTextMessage = By.xpath("//div[@class='ui red message ng-star-inserted' and text()=' No records available ']");
	private static final By plantsTab = By.cssSelector("div[id='plants-card']");
	private static final By locationNameTextBox = By.cssSelector("input[id='locationName']");
	private static final By cancelButton = By.cssSelector("button[id='cancel']");
	private static final By confirmationMessageText = By.cssSelector("div[class='confirm-container']>div[class='message']");
	private static final By confirmationYesButton = By.xpath("//div[@class='btns']/button[text()='Yes']");
	private static String plantActiveRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']] and  td[span[text()=' %s ']] and td[ient-checkbox[div[@class='ui checkbox ient-checkbox slider checked']]]]";
	private static String plantInactiveRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']] and  td[span[text()=' %s ']] and td[ient-checkbox[div[@class='ui checkbox ient-checkbox slider']]]]";

	public PlantsPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - PlantsPage");
	}

	/***
	 * Clicks on plants tab
	 */
	public void clickOnPlantsTab() {
		log.info("Start of the method clickOnPlantsTab");
		driver.safeClick(plantsTab);
		log.info("End of the method clickOnPlantsTab");
	}

	/**
	 * Creates a new plant
	 * @param locationName : location name in string format
	 * @param duns : duns string format
	 * @param costRecoveryThreshold : cost recovery threshold in string format
	 * @param defaultCurrency : default currency in string format
	 * @param localTimeZone : local time zone in string format
	 * @param status : status in string format
	 */
	public void createPlant(String locationName, String duns, String costRecoveryThreshold, String defaultCurrency, String localTimeZone, String status) {
		log.info("Start of the method createPlant");
		driver.safeClick(addButton);
		driver.safeType(locationNameTextBox, locationName);
		driver.safeType(dunsTextBox, duns);
		driver.safeType(costRecoveryThresholdTextBox, costRecoveryThreshold);
		driver.safeClick(defaultCurrencyDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(dropdownOption, defaultCurrency)));
		driver.safeClick(localTimeZoneDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(dropdownOption, localTimeZone)));
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createPlant");
	}

	/**
	 * Creates a new plant
	 * @param locationName : location name in string format
	 * @param duns : duns string format
	 * @param costRecoveryThreshold : cost recovery threshold in string format
	 * @param defaultCurrency : default currency in string format
	 * @param localTimeZone : local time zone in string format
	 * @param status : status in string format
	 */
	public void createPlant(String locationName, String duns, String costRecoveryThreshold, String defaultCurrency, String localTimeZone, String status, String businessOrganization) {
		log.info("Start of the method createPlant");
		driver.safeClick(addButton);
		driver.safeType(locationNameTextBox, locationName);
		driver.safeType(dunsTextBox, duns);
		driver.safeType(costRecoveryThresholdTextBox, costRecoveryThreshold);
		driver.safeClick(defaultCurrencyDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(dropdownOption, defaultCurrency)));
		driver.safeClick(localTimeZoneDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(dropdownOption, localTimeZone)));
		driver.safeClick(businessOrganizationDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(businessOrganizationDropdownOption, businessOrganization)));
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method createPlant");
	}

	/**
	 * Verifies whether plant is displayed
	 * @param locationName : location name in string format
	 */
	public boolean isPlantDisplayed(String locationName) {
		log.info("Start of the method isPlantDisplayed");
		boolean isPlantDisplayed = driver.isPresentAndVisible(By.xpath(String.format(plantsRow, locationName)));
		log.info("End of the method isPlantDisplayed");
		return isPlantDisplayed;
	}

	/***
	 * Searches plant by name
	 * @param locationName : location name in string format
	 */
	public void searchPlantByLocationName(String locationName) {
		log.info("Start of the method searchPlantByLocationName");
		driver.safeType(searchTextBox, locationName);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(searchIconButton);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchPlantByLocationName");
	}

	public void clearLocationNameTextField() {
		log.info("Start of the method clearLocationNameTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearLocationNameTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	/***
	 * Edits existing plant
	 * @param locationName : location name in string format
	 *
	 */
	public void editPlant(String locationName) {
		log.info("Start of the method editPlant");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.safeType(locationNameTextBox, locationName);
		driver.safeClick(submitButton);
		log.info("End of the method editPlant");
	}

	/***
	 * Edits existing plant
	 * @param locationName : location name in string format
	 * @param status : status name in string format
	 *
	 */
	public void editPlant(String locationName, String status) {
		log.info("Start of the method editPlant");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.safeType(locationNameTextBox, locationName);
		driver.safeClick(statusDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(statusDropdownOption, status)));
		driver.safeClick(submitButton);
		log.info("End of the method editPlant");
	}

	/***
	 * Deletes plant
	 */
	public void deletePlant() {
		log.info("Start of the method deletePlant");
		driver.safeClick(verticalIconButton);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deletePlant");
	}

	/**
	 * Verifies No Records Available Message
	 * @return returns message display status
	 */
	public boolean verifyNoRecordsMessage() {
		log.info("Start of the method verifyNoRecordsMessage");
		boolean isMessageDisplayed = driver.isPresentAndVisible(noRecordsAvailableTextMessage);
		log.info("End of the method verifyNoRecordsMessage");
		return isMessageDisplayed;
	}

	/**
	 * Verifies whether submit button disabled
	 */
	public boolean isSubmitButtonDisabled() {
		log.info("Start of the method isSubmitButtonDisabled");
		String disabledText = driver.getAttribute(submitButton, "disabled");
		boolean isSubmitButtonDisabled = disabledText.equals("true");
		log.info("End of the method isSubmitButtonDisabled");
		return isSubmitButtonDisabled;
	}

	/***
	 * Clicks on add button
	 */
	public void clickOnAddButton() {
		log.info("Start of the method clickOnAddButton");
		driver.safeClick(addButton);
		log.info("End of the method clickOnAddButton");
	}

	/**
	 * Verifies whether confirmation message is displayed
	 * @param locationName : location name in string format
	 */
	public boolean isConfirmationMessageDisplayed(String locationName, String expectedConfirmationMessage) {
		log.info("Start of the method isConfirmationMessageDisplayed");
		driver.safeClick(addButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeType(locationNameTextBox, locationName);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(cancelButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		String actualConfirmationMessage = driver.safeGetText(confirmationMessageText);
		boolean isConfirmationMessageDisplayed = actualConfirmationMessage.equals(expectedConfirmationMessage);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(confirmationYesButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		log.info("End of the method isConfirmationMessageDisplayed");
		return isConfirmationMessageDisplayed;
	}

	/**
	 * Verifies whether plant is active
	 * @param plantLocationName : plant location name in string format
	 * @param duns : duns in string format
	 */
	public boolean isPlantIsActive(String plantLocationName, String duns) {
		log.info("Start of the method isPlantIsActive");
		boolean isPlantIsActive = driver.isPresentAndVisible(By.xpath(String.format(plantActiveRow, plantLocationName, duns)));
		log.info("End of the method isPlantIsActive");
		return isPlantIsActive;
	}

	/**
	 * Verifies whether plant is inactive
	 * @param plantName : plant name in string format
	 * @param duns : duns in string format
	 */
	public boolean isPlantIsInactive(String plantName, String duns) {
		log.info("Start of the method isPlantIsInactive");
		boolean isPlantIsInactive = driver.isPresentAndVisible(By.xpath(String.format(plantInactiveRow, plantName, duns)));
		log.info("End of the method isPlantIsInactive");
		return isPlantIsInactive;
	}

}