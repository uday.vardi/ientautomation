package io.ient.test.pages.enterprise;

import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import java.awt.*;


public class SupplierLocationTypesPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(SupplierLocationTypesPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By addButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static String locationTypeRow ="//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]]";
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By editLink = By.cssSelector("div[id='edit']");
	private static final By deleteLink = By.cssSelector("div[id='delete']");
	private static final By viewLink = By.cssSelector("div[id='view']");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By noRecordsAvailableTextMessage = By.xpath("//div[@class='ui red message ng-star-inserted' and text()=' No records available ']");
	private static final By supplierLocationTypesTab = By.xpath("//span[contains(text(),'Supplier Location Types')]");
	private static final By nameTextBox = By.cssSelector("input[name='data[name]']");
	private static final By descriptionTextBox = By.cssSelector("textarea[name='data[description]']");
	private static final By submitButton = By.xpath("//button[contains(text(),'Submit')]");
	private static final By cancelButton = By.xpath("//button[contains(text(),'Cancel')]");

	public SupplierLocationTypesPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - Global Settings Page");
	}

	/***
	 * Clicks on supplier location types tab
	 */
	public void clickOnSupplierLocationTypesTab() {
		log.info("Start of the method clickOnSupplierLocationTypesTab");
		driver.safeClick(verticalIconButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(viewLink);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(supplierLocationTypesTab);
		log.info("End of the method clickOnSupplierLocationTypesTab");
	}

	/**
	 * creates a supplier location type
	 * @param name : name in string format
	 * @param description : description in string format
	 */
	public void createSupplierLocationType(String name, String description) {
		log.info("Start of the method createSupplierLocationType");
		driver.safeClick(addButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeType(nameTextBox, name);
		driver.safeType(descriptionTextBox, description);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(submitButton);
		log.info("End of the method createSupplierLocationType");
	}

	/**
	 * Verifies whether location type is displayed
	 * @param name : name in string format
	 */
	public boolean isNameDisplayed(String name) {
		log.info("Start of the method isNameDisplayed");
		boolean isNameDisplayed = driver.isPresentAndVisible(By.xpath(String.format(locationTypeRow, name)));
		log.info("End of the method isNameDisplayed");
		return isNameDisplayed;
	}

	/***
	 * Searches by name
	 * @param name : name in string format
	 */
	public void searchByName(String name) {
		log.info("Start of the method searchByName");
		driver.safeType(searchTextBox, name);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(searchIconButton);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchByName");
	}

	public void clearNameTextField() {
		log.info("Start of the method clearNameTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearNameTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	/***
	 * Edits existing location type
	 * @param name : name in string format
	 */
	public void editLocationType(String name) {
		log.info("Start of the method editLocationType");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeType(nameTextBox, name);
		driver.safeClick(submitButton);
		log.info("End of the method editLocationType");
	}

	/***
	 * Deletes location type
	 */
	public void deleteLocationType() {
		log.info("Start of the method deleteLocationType");
		driver.safeClick(verticalIconButton);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method deleteLocationType");
	}

	/**
	 * Verifies No Records Available Message
	 * @return returns message display status
	 */
	public boolean verifyNoRecordsMessage() {
		log.info("Start of the method verifyNoRecordsMessage");
		boolean isMessageDisplayed = driver.isPresentAndVisible(noRecordsAvailableTextMessage);
		log.info("End of the method verifyNoRecordsMessage");
		return isMessageDisplayed;
	}

}