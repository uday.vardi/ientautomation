package io.ient.test.pages.enterprise;


import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class SupplierShippingLanesPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(SupplierShippingLanesPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By addButton = By.cssSelector("ient-button[icon='plus']>button");
	private static final By shippingLanesPart = By.xpath("//div[select[@name='data[part]']]");
	private static String partDropDownOption = "div[id*='choices--datapart'][data-id='1']>span";
	private static final By shippingLanesSource = By.xpath("//div[select[@id='select']]");
	private static String sourceDropDownOption = "div[id*='select-item-choice'][data-id='1']>span";
	private static final By shippingLanesDestination = By.xpath("//div[select[@id='destination']]");
	private static String destinationDropDownOption = "div[id*='destination-item-choice'][data-id='1']>span";
	private static final By shippingLanesTransitTime = By.xpath("//div[input[@id='transitTime']]");
	private static final By shippingLanesTransitMode = By.xpath("//div[select[@id='transitMode']]");
	private static final String transitModeDropDownOption = "div[id*='transitMode-item-choice'][data-id='1']>span";
	private static final By caseStatusDropDown = By.xpath("//div[select[@id='caseStatus']]");
	private static String submitToBuyerDropDownOption = "div[id*='caseStatus-item-choice'][data-value='Submit to Buyer']>span";
	private static String saveAsDraftDropDownOption = "div[id*='caseStatus-item-choice'][data-value='Save as Draft']>span";
	private static String reworkDropDownOption = "div[id*='caseStatus-item-choice'][data-value='Rework']>span";
	private static String approvedDropDownOption = "div[id*='caseStatus-item-choice'][data-value='Approved']>span";
	private static String rejectedDropDownOption = "div[id*='caseStatus-item-choice'][data-value='Rejected']>span";
	private static final By shortDescriptionTextArea = By.cssSelector("textarea[id='shortDescription']");
	private static final By saveButton = By.cssSelector("button[id='bottom-save-button']");
	private static final By filterButton = By.cssSelector("button[id='ient-table-filter-btn']");
	private static final By supplierFilter = By.xpath("//div[label[text()='Supplier']]/div/button");
	private static final By supplierFilterValue = By.xpath("//div[contains(@class,'drop-show-container')]/div[@class='drop-show']/label[contains(text(),' rrssi ')]/span");
	private static final By partFilter = By.xpath("//div[label[text()='Part']]/div/button");
	private static final By partFilterValue = By.xpath("//div[contains(@class,'drop-show-container')]/div[@class='drop-show']/label[contains(text(),' pt ')]/span");
	private static final By submitFiltersButton = By.cssSelector("button[id = 'submit-bsc-filter']");
	private static final By verifyApproverDropdown = By.xpath("//div[select[@id='verifyApprover']]");
	private static String verifyApproverDropdownOption = "div[id*='verifyApprover-item-choice'][data-value='%s']>span";

	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static String userRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]]";

	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By viewButton = By.cssSelector("div[id='view']");
	private static final By editButtonInCaseForm = By.cssSelector("button[id='ient-btn-edit']");
	private static final By conversationTab = By.xpath("//span[text()='Conversations']");
	private static final By commentTextArea = By.cssSelector("textarea[placeholder='Comment'],textarea[placeholder='External Comment'],textarea[placeholder='Internal Comment']");
	private static final By submitComment = By.xpath("//span[text()='Submit']");
	private static final By detailsTab = By.xpath("//span[text()='Details']");


	public SupplierShippingLanesPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - SupplierShippingLanesPage");
	}

	/**
	 * Creates a new problem case
	 * @param part        			  : part in string format
	 * @param source            	  : source in string format
	 * @param destination 			  : destination in string format
	 * @param transitTime             : transitTime in string format
	 * @param transitMode             : transitMode in string format
	 * @param caseStatus              : case Status in string format
	 * @param approverName            : approverName in string format
	 * @param shortDescription        : short Description in string format
	 **/
	public void createShippingLanes(String part, String source, String destination,String transitTime, String transitMode, String caseStatus, String approverName, String shortDescription) {
		log.info("Start of the method createShippingLanes");
		driver.safeClick(addButton);
		driver.sleep(WaitTime.LONG_WAIT);
		driver.jSClick(shippingLanesPart);
		driver.safeClick(By.cssSelector(String.format(partDropDownOption, part)));
		driver.jSClick(shippingLanesSource);
		driver.safeClick(By.cssSelector(String.format(sourceDropDownOption, source)));
		driver.jSClick(shippingLanesDestination);
		driver.safeClick(By.cssSelector(String.format(destinationDropDownOption, destination)));
		driver.safeClearAndTypeByActionsClass(shippingLanesTransitTime, transitTime,true);
		driver.jSClick(shippingLanesTransitMode);
		driver.safeClick(By.cssSelector(String.format(transitModeDropDownOption, transitMode)));
		driver.jSClick(caseStatusDropDown);
		if(caseStatus == "Submit to Buyer"){
			driver.safeClick(By.cssSelector(String.format(submitToBuyerDropDownOption, caseStatus)));
		}else if(caseStatus == "Save as Draft"){
			driver.safeClick(By.cssSelector(String.format(saveAsDraftDropDownOption, caseStatus)));
		}
		driver.jSClick(verifyApproverDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.cssSelector(String.format(verifyApproverDropdownOption, approverName)));
		driver.safeType(shortDescriptionTextArea, shortDescription);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(saveButton);
		log.info("End of the method createShippingLanes");
	}

	/***
	 * Searches created shipping lanes by Short Description
	 * @param description : description in string format
	 */
	public void searchByShortDescription(String description) {
		log.info("Start of the method searchByShortDescription");
		driver.safeType(searchTextBox, description);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchByShortDescription");
	}

	/**
	 * Verifies whether Shipping Lanes is displayed
	 */
	public boolean isShippingLanesDisplayed(String part) {
		log.info("Start of the method isShippingLanesDisplayed");
		boolean isShippingLanesDisplayed = driver.isPresentAndVisible(By.xpath(String.format(userRow, part)));
		log.info("End of the method isShippingLanesDisplayed");
		return isShippingLanesDisplayed;
	}

	/**
	 * Verifies whether Shipping Lanes is displayed
	 */
	public void selectsRecordAndClicksEdit() {
		log.info("Start of the method selectsRecordAndClicksEdit");
		driver.safeClick(verticalIconButton);
		driver.safeClick(viewButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		boolean isEditButtonDisplayed = driver.isPresentAndVisible(editButtonInCaseForm);
		if(isEditButtonDisplayed){
			driver.safeClick(editButtonInCaseForm);
			driver.sleep(WaitTime.SMALL_WAIT);
		}
		log.info("End of the method selectsRecordAndClicksEdit");
	}

	/**
	 * Adds comments in conversation tab
	 */
	public void addComment(){
		log.info("Start of the method addComment");
		driver.safeClick(conversationTab);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeType(commentTextArea, "A-test: Rework needed");
		driver.safeClick(submitComment);
		driver.safeClick(detailsTab);
		log.info("End of the method addComment");
	}


	/***
	 * status change case
	 * @param caseStatus : next status in string format
	 */
	public void editStatus(String caseStatus) {
		log.info("Start of the method editStatus");
		driver.jSClick(caseStatusDropDown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		if(caseStatus == "Submit to Buyer"){
			driver.safeClick(By.cssSelector(String.format(submitToBuyerDropDownOption, caseStatus)));
		}else if(caseStatus == "Rework"){
			driver.safeClick(By.cssSelector(String.format(reworkDropDownOption, caseStatus)));
		}else if(caseStatus == "Save as Draft"){
			driver.safeClick(By.cssSelector(String.format(saveAsDraftDropDownOption, caseStatus)));
		}else if(caseStatus == "Approved"){
			driver.safeClick(By.cssSelector(String.format(approvedDropDownOption, caseStatus)));
		}else if(caseStatus == "Rejected"){
			driver.safeClick(By.cssSelector(String.format(rejectedDropDownOption, caseStatus)));
		}
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(saveButton);
		log.info("End of the method editStatus");
	}

	/**
	 * Clicks on Filter Icon
	 */
	public void clickFilterIcon() {
		log.info("Start of the method clickFilterIcon");
		driver.safeClick(filterButton);
		log.info("End of the method clickFilterIcon");
	}

	/**
	 * Select filters and values under for buyer admin
	 */
	public void buyerAdminFilters() {
		log.info("Start of the method buyerAdminFilters");
		driver.safeClick(supplierFilter);
		driver.safeClick(supplierFilterValue);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(partFilter);
		driver.safeClick(partFilterValue);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(submitFiltersButton);
		log.info("End of the method buyerAdminFilters");
	}

	/**
	 * Select filters and values under for supplier admin
	 */
	public void supplierAdminFilters() {
		log.info("Start of the method supplierAdminFilters");
		driver.safeClick(partFilter);
		driver.safeClick(partFilterValue);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(submitFiltersButton);
		log.info("End of the method supplierAdminFilters");
	}

}