package io.ient.test.pages.enterprise;


import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;


public class SupplierInvoicesPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(SupplierInvoicesPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By addButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By viewButton = By.cssSelector("div[id='view']");
	private static final By editButtonInCaseForm = By.cssSelector("button[id='ient-btn-edit']");
	private static String startDatePicker = "//div[contains(@class,'open')]/descendant::span[@class='flatpickr-day ' and text()='%s']";
	private static String endDatePicker = "//div[contains(@class,'open')]/descendant::span[@class='flatpickr-day ' and text()='%s']";

	private static final By poNumberTextBox = By.cssSelector("input[id='poNumber']");
	private static final By sapReferenceNumberTextBox = By.cssSelector("input[id='sapReferenceNumber']");
	private static final By startDateInPurchaseOrder = By.cssSelector("input[id='startDate']+input");
	private static final By endDateInPurchaseOrder = By.cssSelector("input[id='endDate']+input");
	private static final By startDateInSupplierInvoice = By.cssSelector("input[id='ServicePeriodStart']+input");
	private static final By endDateInSupplierInvoice = By.cssSelector("input[id='ServicePeriodEnd']+input");
	private static String caseFormDropdownOption = "//div[@role='option' and span[text()='%s']]/span";
	private static final By currencyPreferenceDropdown = By.xpath("//div[select[@id='currencyPreference']]");
	private static final By nextStateDropdown = By.xpath("//div[select[@id='caseStatus']]");
	private static final By shortDescriptionTextArea = By.cssSelector("textarea[id='shortDescription']");
	private static final By supplierIdDropdown = By.xpath("//div[select[@id='supplierId']]");
	private static final By supplierNameDropdown = By.xpath("//div[select[@id='supplierName']]");
	private static final By monthDropdown = By.xpath("//div[select[@id='servicePeriod-month']]");
	private static String monthDropdownOption = "select[id='servicePeriod-month']>option[value='%s']";
	private static final By yearDropdown = By.xpath("//div[select[@id='servicePeriod-year']]");
	private static String yearDropdownOption = "select[id='servicePeriod-year']>option[value='%s']";
	private static final By saveButtonInCaseForm = By.cssSelector("button[id='bottom-save-button']");
	private static String supplierIdDropdownOption = "div[id*='supplierId-item-choice'][data-id='%s']>span";
	private static String currencyPreferenceDropdownOption = "div[id*='currencyPreference-item-choice'][data-id='%s']>span";
	private static String supplierNameDropdownOption = "div[id*='supplierName-item-choice'][data-value='%s']";
	private static final By conversationTab = By.xpath("//span[text()='Conversations']");
	private static final By commentTextArea = By.cssSelector("textarea[placeholder='Comment'],textarea[placeholder='External Comment'],textarea[placeholder='Internal Comment']");
	private static final By detailsTab = By.xpath("//span[text()='Details']");
	private static final By submitComment = By.xpath("//span[text()='Submit']");

	private static final By purchaseOrderNumberDropdown = By.xpath("//div[select[@id='purchaseOrderNumber']]");
	private static String purchaseOrderNumberDropdownOption = "div[id*='purchaseOrderNumber-item-choice'][data-id='%s']>span";
	private static final By verifyApproverDropdown = By.xpath("//div[select[@id='verifyApprover']]");
	private static String verifyApproverDropdownOption = "div[id*='verifyApprover-item-choice'][data-value='%s']>span";
	private static final By invoiceNumberTextBox = By.cssSelector("input[id='invoiceNumber']");
	private static final By invoiceGrossAmountTextBox = By.cssSelector("input[id='invoiceGrossAmount']");
	private static final By reasonTextArea = By.cssSelector("textarea[id='reason']");


	public SupplierInvoicesPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - SupplierInvoicesPage");
	}

	/***
	 * Creates a new purchase order
	 * @param purchaseNumber : number in numeric format
	 * @param sapReferenceNumber : sap reference number in string format
	 * @param startDate : start date in string format
	 * @param endDate : end date in string format
	 * @param currencyPreference : currency preference in string format
	 * @param nextState : next state in string format
	 * @param shortDescription : short description in string format
	 * @param supplierId : supplier id in string format
	 * @param supplierName : supplier name in string format
	 * @param month : month in string format
	 * @param year : year in string format
	 */
	public void createPurchaseOrder(String purchaseNumber, String sapReferenceNumber, String startDate, String endDate, String currencyPreference, String nextState, String shortDescription, String supplierId, String supplierName, String month, String year) {
		log.info("Start of the method createPurchaseOrder");
		driver.safeClick(addButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeType(poNumberTextBox, purchaseNumber);
		driver.safeType(sapReferenceNumberTextBox, sapReferenceNumber);
		driver.jSClick(startDateInPurchaseOrder);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(startDatePicker, startDate)));
		driver.jSClick(endDateInPurchaseOrder);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(endDatePicker, endDate)));
		driver.jSClick(currencyPreferenceDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.cssSelector(String.format(currencyPreferenceDropdownOption, currencyPreference)));
		driver.jSClick(nextStateDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(caseFormDropdownOption, nextState)));
		driver.safeType(shortDescriptionTextArea, shortDescription);
		driver.jSClick(supplierIdDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.cssSelector(String.format(supplierIdDropdownOption, supplierId)));
		driver.jSClick(supplierNameDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.cssSelector(String.format(supplierNameDropdownOption, supplierName)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.jSClick(monthDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.cssSelector(String.format(monthDropdownOption, month)));
		driver.jSClick(yearDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.cssSelector(String.format(yearDropdownOption, year)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(saveButtonInCaseForm);
		log.info("End of the method createPurchaseOrder");
	}
	/***
	 * Creates new supplier invoice
	 * @param startDate : start date in string format
	 * @param endDate : end date in string format
	 * @param purchaseOrderNumber : purchase order number in string format
	 * @param invoiceNumber : invoice number in string format
	 * @param invoiceGrossAmount : invoice gross amount in string format
	 * @param approverName : approver name in string format
	 */
	public void createSupplierInvoice(String startDate, String endDate, String purchaseOrderNumber, String invoiceNumber, String invoiceGrossAmount, String approverName) {
		log.info("Start of the method createSupplierInvoice");
		driver.safeClick(addButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.jSClick(startDateInSupplierInvoice);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(startDatePicker, startDate)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.jSClick(endDateInSupplierInvoice);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(endDatePicker, endDate)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.jSClick(purchaseOrderNumberDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.cssSelector(String.format(purchaseOrderNumberDropdownOption, purchaseOrderNumber)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeType(invoiceNumberTextBox, invoiceNumber);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeType(invoiceGrossAmountTextBox, invoiceGrossAmount);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.jSClick(verifyApproverDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.cssSelector(String.format(verifyApproverDropdownOption, approverName)));
		driver.safeClick(saveButtonInCaseForm);
		log.info("End of the method createSupplierInvoice");
	}

	/***
	 * edit case
	 * @param conversationComment : conversation comment in string format
	 * @param status : next state in string format
	 */
	public void editCase(String conversationComment, String status) {
		log.info("Start of the method editCase");
		driver.safeClick(verticalIconButton);
		driver.safeClick(viewButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(editButtonInCaseForm);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(conversationTab);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeType(commentTextArea, conversationComment);
		driver.safeClick(submitComment);
		driver.safeClick(detailsTab);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.jSClick(nextStateDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(caseFormDropdownOption, status)));
		driver.safeClick(saveButtonInCaseForm);
		log.info("End of the method editCase");
	}

	/***
	 * status change case
	 * @param status : next state in string format
	 * @param rejectReason :  reject reason in string format
	 */
	public void editStatus(String status, String rejectReason) {
		log.info("Start of the method editStatus");
		driver.safeClick(verticalIconButton);
		driver.safeClick(viewButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(editButtonInCaseForm);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.jSClick(nextStateDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(caseFormDropdownOption, status)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeType(reasonTextArea, rejectReason);
		driver.safeClick(saveButtonInCaseForm);
		log.info("End of the method editStatus");
	}


}