package io.ient.test.pages.enterprise;


import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.framework.utils.extentreports.FileUploadUtils;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.List;


public class SupplyChainIssueMgmtPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(SupplyChainIssueMgmtPage.class);
	FileUploadUtils fileUploadUtils = new FileUploadUtils(driver);
	LogFactory log = new LogFactory(logger);
	private static final By supplyChainIssueMgmtInstallIcon = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Supply Chain Issue Mgmt ']]]/descendant::i[@class='arrow down circle icon bigicon']");
	private static final By supplyChainIssueMgmtDownloadIcon = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Supply Chain Issue Mgmt ']]]/descendant::i[@class='download icon bigicon']");
	private static final By sendInstallRequestYesButton = By.xpath("//div[@class='confirm-container']/descendant::button[text()='Yes']");
	private static final By sendInstallRequestAccessRequestedStatus = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Supply Chain Issue Mgmt ']]]/descendant::span[text()='Access requested']");
	private static final By supplyChainIssueMgmtUnderReview = By.xpath("//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' Supply Chain Issue Mgmt ']]]/descendant::span[text()='Under Review']");
	private static final By supplyChainIssueMgmtInstalledAppText = By.xpath("//div[h2[text()='Installed Apps']]/descendant::div[text()=' Supply Chain Issue Mgmt ']");
	private static final By supplyChainIssueMgmtInstalledAppVersionText = By.xpath("(//div[h2[text()='Installed Apps']]/descendant::div[text()=' Supply Chain Issue Mgmt ']/ancestor::div/div[@class='right floated']/div/input/following-sibling::div)[1]");
	private static final By appSettingsTab = By.xpath("//a[@role='tab' and span[text()='App Settings']]");
	private static final By tagsTab = By.xpath("//a[span[text()='Tags']]");
	private static final By slaDefinitionTab = By.xpath("//a[span[text()='SLA Definition']]");
	private static final By deployConfigTab = By.xpath("//a[span[text()='Deploy Config']]");
	private static final By deployConfigVersion = By.cssSelector("input[formcontrolname='version']");
	private static final By versionTab = By.xpath("//a[span[text()='Versions']]");
	private static final By versionTabRowVersion = By.xpath("(//table[@class='ui striped table']/tbody/tr/td[3])[1]/span");
	private static final By workflowTab = By.xpath("//a[span[contains(text(),'Workflow')]]");
	private static final By uploadBPMNButton = By.xpath("//div[contains(@class,'bottom-buttons ng')]/descendant::input[@accept='.bpmn']");
	private static final By workflowSaveChangesButton = By.xpath("//button[i[@class='save icon']]");
	private static final By workflowPopUpYesButton = By.cssSelector("button[id='popup-yes-btn']");
	private static final By workflowDoneButton = By.cssSelector("button#done");
	private static final By initialResponsePlusIcon = By.xpath("//div[@formarrayname='initialResponseDue']/div/div/i[@class='plus circle icon action-btn']");
	private static final By initialResponseSelectFormDropdown = By.xpath("//div[@formarrayname='initialResponseDue']/div/div/div[@class='form-control-col']/select[@class='ng-untouched ng-pristine ng-valid']");
	private static final By initialResponseSelectFormFieldDropdown = By.xpath("//div[@formarrayname='initialResponseDue']/div/div/div[@class='form-control-col']/select[@formcontrolname='field']");
	private static final By initialResponseOperatorDropdown = By.xpath("//div[@formarrayname='initialResponseDue']/div/div/div[@class='form-control-col']/select[@formcontrolname='operator']");
	private static final By initialResponseValueInput = By.xpath("//div[@formarrayname='initialResponseDue']/div/div/div[@class='form-control-col']/input[@formcontrolname='value']");
	private static final By initialResponseHourInput = By.xpath("//div[@formarrayname='initialResponseDue']/div/div/div[@class='form-control-col']/input[@formcontrolname='hour']");
	private static final By finalResponseDuePlusIcon = By.xpath("//div[@formarrayname='finalResponseDue']/div/div/i[@class='plus circle icon action-btn']");
	private static final By finalResponseDueSelectFormDropdown = By.xpath("//div[@formarrayname='finalResponseDue']/div/div/div[@class='form-control-col']/select[@class='ng-untouched ng-pristine ng-valid']");
	private static final By finalResponseDueSelectFormFieldDropdown = By.xpath("//div[@formarrayname='finalResponseDue']/div/div/div[@class='form-control-col']/select[@formcontrolname='field']");
	private static final By finalResponseDueOperatorDropdown = By.xpath("//div[@formarrayname='finalResponseDue']/div/div/div[@class='form-control-col']/select[@formcontrolname='operator']");
	private static final By finalResponseDueValueInput = By.xpath("//div[@formarrayname='finalResponseDue']/div/div/div[@class='form-control-col']/input[@formcontrolname='value']");
	private static final By finalResponseDueHourInput = By.xpath("//div[@formarrayname='finalResponseDue']/div/div/div[@class='form-control-col']/input[@formcontrolname='hour']");
	private static final By saveButton = By.xpath("//button[span[contains(text(),'Save')]]");
	private static final By createButton = By.xpath("//button[contains(text(),'Create')]");
	private static final By publishButtonInProject = By.cssSelector("button[id='publish-btn']");
	private static final By publishButtonInMarketplace = By.cssSelector("//button[span[text()='Publish']]");
	private static final By appsDeployedButtonInMarketplace = By.xpath("//button[span[text()='Apps Published']]");
	private static final By availableAppsTextInMarketplace = By.xpath("//h2[text()='Available Apps']");
	private static final By installedAppsTextInMarketplace = By.xpath("//h2[text()='Installed Apps']");
	private static String installedAppWithVersion = "//h2[@class='ui header'  and text()='Installed Apps']/following-sibling::div/descendant::div[@class='content' and div[@class='header' and text()=' %s ']]/descendant::div[not(@class)]/div[text()='%s']";
	private static String availableAppWithVersion = "//h2[@class='ui header my-header'  and text()='Available Apps']/following-sibling::div/descendant::div[@class='content' and div[@class='header' and text()=' %s ']]/descendant::div[not(@class)]/div[text()='%s']";
	private static String availableAppDownloadIcon = "//h2[@class='ui header my-header'  and text()='Available Apps']/following-sibling::div/descendant::div[@class='content' and div[@class='header' and text()=' %s ']]/following-sibling::div/descendant::i[@class='download icon bigicon']";
	private static final By availableAppDownloadMessage = By.xpath("//div[text()='Your local app is going to be upgraded with the market place app version.']");
	private static final By installDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By saveMenuDropDownButton = By.xpath("//div[@class='ui dropdown']/ient-button/button");
	private static final By saveMajorVersionButton = By.xpath("//div[i[@class='plus icon'] and text()='Save Major Ver']");
	private static final By saveMinorVersionButton = By.xpath("//div[i[@class='minus icon'] and text()='Save Minor Ver']");
	private static final By versionCommentTextArea = By.cssSelector("textarea[placeholder='Comment']");
	private static final By versionCommentSaveMajorButton = By.xpath("//button[text()='Save Major']");
	private static final By versionCommentSaveMinorButton = By.xpath("//button[text()='Save Minor']");
	private static final By deployWorkflowButton = By.cssSelector("input[value='Deploy Workflow']");
	private static final By appInstallRequestsTab = By.xpath("//div[@class='header_name' and text()=' App Install Requests ']");
	private static String appInstallRequestsVerticalIcon = "//tr[td[span[text()=' %s ']]]/td/div[@class='ui icon top left pointing item dropdown']";
	private static final By appInstallRequestsApproveLink = By.xpath("//div[@class='menu left transition visible']/div[text()=' Approve ']");
	private static final By commentTextArea = By.cssSelector("textarea[placeholder='Comment']");
	private static final By commentTextAreaApproveButton = By.xpath("//button[text()='Approve']");
	private static final By approvedTab = By.xpath("//a[span[text()='Approved']]");
	private static final By searchTextBox = By.cssSelector("input[placeholder='Search keyword']");
	private static final By searchIconButton = By.cssSelector("i[class='search icon link']");
	private static final By tempPartCheckbox = By.xpath("//div[@ref='component' and div/input[@name='data[tempPart]']]");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By deleteDialogDeleteButton = By.xpath("//button[text()='Delete']");
	private static String appInstallApprovedRow = "//tr[td[span[text()=' %s ']]]";
	private static String installedAppVersionVerticalIcon = "//table[@class='ui striped table actual-table']/tbody/tr[td[span[contains(text(),' %s ')]]]/td/div/i[contains(@class,'ellipsis vertical icon')]";
	private static String appDeleteIconInMarketplace = "//div[@class='card ng-star-inserted' and div[div[@class='header' and text()=' %s ']]]/descendant::i[@class='trash icon bigicon']";
	private static final By noRecordsAvailableTextMessage = By.xpath("//div[@class='ui red message ng-star-inserted' and text()=' No records available ']");
	private static final By appLocalVersionText = By.xpath("//table[@class='ui striped table actual-table']/tbody/tr[1]/td[3]/span");
	private static final By appVersionCommentText = By.xpath("//table[@class='ui striped table actual-table']/tbody/tr[1]/td[5]/span");
	private static final By appVersionLoadButton = By.xpath("//div[@class='menu left transition visible']/div[text()=' Load ']");
	private static final By appVersionDeleteButton = By.xpath("//div[@class='menu left transition visible']/div[text()=' Delete ']");
	private static final By appVersionExportTenantButton = By.xpath("//div[@class='menu left transition visible']/div[text()=' Export To Tenant ']");
	private static final By selectTargetTenantDiv = By.xpath("//div[@class='date-box__value']");
	private static final By selectTargetTenantDivSubmitButton = By.xpath("//button[text()='Submit']");
	private static final By supplyIssueManagementTitleVersionText = By.xpath("//div[@class='heading']/h2[contains(text(),'v')]");
	private static String appVersionText = "(//div[@class='header' and text()=' %s ']/ancestor::div[@class='content']/descendant::div[not(@class) and contains(text(),'v')])[1]";
	private static String columnNameText = "//div[@class='ient-table-header-content-label' and text()=' %s ']";
	private static String selectTargetTenant = "//div[span[contains(text(),'%s')]]";
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By viewLink = By.cssSelector("div[id='view']");
	private static final By editLink = By.cssSelector("button[id='ient-btn-edit']");
	private static final By approveRadioButton = By.cssSelector("input[id*='approved']");
	private static final By rejectRadioButton = By.cssSelector("input[id*='rejected']");
	private static final By rejectReasonTextArea = By.cssSelector("textarea[name='data[reason]']");
	private static final By saveButtonInApplicationApproval = By.cssSelector("button[id='save-button']");
	private static final By taskInitiatorRoles = By.cssSelector("div[id='mdrop']");
	private static final By taskInitiatorRoleBuyerUser = By.cssSelector("div[data-value='BUYER_USER']");
	private static final By taskInitiatorRoleSupplierUser = By.cssSelector("div[data-value='SUPPLIER_USER']");
	private static final By tableAddButton = By.cssSelector("ient-button[icon='plus']>button");

	public SupplyChainIssueMgmtPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - AppInstallRequestsPage");
	}

	/***
	 * Clicks on request access button
	 */
	public void clickOnRequestAccessButton() {
		log.info("Start of the method clickOnRequestAccessButton");
		driver.safeClick(supplyChainIssueMgmtInstallIcon);
		driver.safeClick(sendInstallRequestYesButton);
		log.info("End of the method clickOnRequestAccessButton");
	}

	/**
	 * Verifies whether install access requested
	 * @return returns install access requested status
	 */
	public boolean isInstallAccessRequested() {
		log.info("Start of the method isInstallAccessRequested");
		boolean isInstallAccessRequested = driver.isPresentAndVisible(sendInstallRequestAccessRequestedStatus);
		log.info("End of the method isInstallAccessRequested");
		return isInstallAccessRequested;
	}

	/***
	 * Clicks on app install requests tab
	 */
	public void clickOnAppInstallRequestsTab() {
		log.info("Start of the method clickOnAppInstallRequestsTab");
		driver.safeClick(appInstallRequestsTab);
		log.info("End of the method clickOnAppInstallRequestsTab");
	}

	/***
	 * Clicks on approve tab
	 */
	public void clickOnApproveTab() {
		log.info("Start of the method clickOnApproveTab");
		driver.safeClick(approvedTab);
		log.info("End of the method clickOnApproveTab");
	}

	/**
	 * Verifies whether app install request is displayed
	 * @param requesterName : name of role in string format
	 * @return returns display status
	 */
	public boolean isAppInstallRequestDisplayed(String requesterName) {
		log.info("Start of the method isAppInstallRequestDisplayed");
		boolean isAppInstallRequestDisplayed = driver.isPresentAndVisible(By.xpath(String.format(appInstallRequestsVerticalIcon, requesterName)));
		log.info("End of the method isAppInstallRequestDisplayed");
		return isAppInstallRequestDisplayed;
	}

	/**
	 * Approves app installation request
	 * @param requesterName : requester name in string format
	 * @param approveCommentText : comment in string format
	 */
	public void approveAppInstallationRequest(String requesterName, String approveCommentText) {
		log.info("Start of the method approveAppInstallationRequest");
		driver.safeClick(By.xpath(String.format(appInstallRequestsVerticalIcon, requesterName)));
		driver.safeClick(appInstallRequestsApproveLink);
		driver.safeType(commentTextArea, approveCommentText);
		driver.safeClick(commentTextAreaApproveButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		log.info("End of the method approveAppInstallationRequest");
	}

	/**
	 * Clicks on delete app button
	 * @param deleteApp : delete app in string format
	 * @param approveCommentText : comment in string format
	 */
	public void clickOnDeleteApplication(String deleteApp, String approveCommentText) {
		log.info("Start of the method clickOnDeleteApplication");
		driver.safeClick(By.xpath(String.format(appDeleteIconInMarketplace, deleteApp)));
		driver.safeType(commentTextArea, approveCommentText);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(deleteDialogDeleteButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		log.info("End of the method clickOnDeleteApplication");
	}

	/**
	 * Verifies whether app install request is approved
	 * @param requesterName : name of role in string format
	 * @return returns display status
	 */
	public boolean isAppInstallRequestApproved(String requesterName) {
		log.info("Start of the method isAppInstallRequestApproved");
		boolean isAppInstallRequestApproved = driver.isPresentAndVisible(By.xpath(String.format(appInstallApprovedRow, requesterName)));
		log.info("End of the method isAppInstallRequestApproved");
		return isAppInstallRequestApproved;
	}

	/***
	 * Installs approved app
	 */
	public void installApprovedApp() {
		log.info("Start of the method installApprovedApp");
		driver.safeClick(supplyChainIssueMgmtDownloadIcon);
		driver.safeClick(sendInstallRequestYesButton);
		log.info("End of the method installApprovedApp");
	}

	/**
	 * Verifies whether app installed or not
	 * @return returns installation status
	 */
	public boolean isAppInstalled() {
		log.info("Start of the method isAppInstalled");
		boolean isAppInstalled = driver.isPresentAndVisible(supplyChainIssueMgmtInstalledAppText);
		log.info("End of the method isAppInstalled");
		return isAppInstalled;
	}

	/**
	 * Returns installed app version
	 * @return returns app version
	 */
	public String getInstalledAppVersion() {
		log.info("Start of the method getInstalledAppVersion");
		String roleId = driver.safeGetText(supplyChainIssueMgmtInstalledAppVersionText);
		log.info("End of the method getInstalledAppVersion");
		return roleId;
	}

	/***
	 * Clicks on sla definition tab
	 */
	public void clickOnSLADefinitionTab() {
			log.info("Start of the method clickOnSLADefinitionTab");
			driver.safeClick(slaDefinitionTab);
			log.info("End of the method clickOnSLADefinitionTab");
	}

	/***
	 * Saves major version
	 *  @param versionComment : version comment in string format
	 */
	public void saveMajorVersion(String versionComment) {
		log.info("Start of the method saveMajorVersion");
		driver.safeClick(saveMenuDropDownButton);
		driver.safeClick(saveMajorVersionButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeType(versionCommentTextArea, versionComment);
		driver.safeClick(versionCommentSaveMajorButton);
		log.info("End of the method saveMajorVersion");
	}

	/***
	 * Saves major version
	 *  @param versionComment : version comment in string format
	 */
	public void saveMajorVersionWithoutClickOnDropdown(String versionComment) {
		log.info("Start of the method saveMajorVersionWithoutClickOnDropdown");
		driver.safeClick(saveMajorVersionButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeType(versionCommentTextArea, versionComment);
		driver.safeClick(versionCommentSaveMajorButton);
		log.info("End of the method saveMajorVersionWithoutClickOnDropdown");
	}

	/***
	 * Saves minor version
	 *  @param versionComment : version comment in string format
	 */
	public void saveMinorVersion(String versionComment) {
		log.info("Start of the method saveMinorVersion");
		driver.safeClick(saveMenuDropDownButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(saveMinorVersionButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeType(versionCommentTextArea, versionComment);
		driver.safeClick(versionCommentSaveMinorButton);
		log.info("End of the method saveMinorVersion");
	}

	/***
	 * returns save major version status
	 */
	public boolean isSaveMajorVersionDisplayed() {
		log.info("Start of the method isSaveMajorVersionDisplayed");
		driver.safeClick(saveMenuDropDownButton);
		boolean isSaveMajorVersionDisplayed = driver.isPresentAndVisible(saveMajorVersionButton);
		log.info("End of the method isSaveMajorVersionDisplayed");
		return isSaveMajorVersionDisplayed;
	}

	/***
	 * returns save minor version status
	 */
	public boolean isSaveMinorVersionDisplayed() {
		log.info("Start of the method isSaveMajorVersionDisplayed");
		driver.safeClick(saveMenuDropDownButton);
		boolean isSaveMinorVersionDisplayed = driver.isPresentAndVisible(saveMinorVersionButton);
		log.info("End of the method isSaveMajorVersionDisplayed");
		return isSaveMinorVersionDisplayed;
	}

	/***
	 * Clicks on save button
	 */
	public void clickOnSaveButton() {
		log.info("Start of the method clickOnSaveButton");
		driver.safeClick(saveButton);
		log.info("End of the method clickOnSaveButton");
	}

	/***
	 * Clicks on create button
	 */
	public void clickOnCreateButton() {
		log.info("Start of the method clickOnSaveButton");
		driver.safeClick(createButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method clickOnSaveButton");
	}

	/**
	 * Returns deployed version
	 * @return returns deploy version
	 */
	public String getAppVersionText(String deployedVersion) {
		log.info("Start of the method getAppVersionText");
		driver.sleep(WaitTime.XSMALL_WAIT);
		String deployedVersionText = driver.safeGetText(By.xpath(String.format(appVersionText, deployedVersion)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		log.info("End of the method getAppVersionText");
		return deployedVersionText;
	}

	/***
	 * Clicks on deploy config tab
	 */
	public void clickOnDeployConfigTab() {
		log.info("Start of the method clickOnDeployConfigTab");
		driver.safeClick(deployConfigTab);
		log.info("End of the method clickOnDeployConfigTab");
	}

	/**
	 * Returns deploy config version
	 * @return returns deploy config version
	 */
	public String getDeployConfigVersion() {
		log.info("Start of the method getDeployConfigVersion");
		String deployConfigVersionText = driver.safeGetAttribute(deployConfigVersion, "value");
		log.info("End of the method getDeployConfigVersion");
		return deployConfigVersionText;
	}

	/**
	 * Clicks on workflow tab
	 */
	public void clickOnWorkflowTab() {
		log.info("Start of the method clickOnWorkflowTab");
		driver.safeClick(workflowTab);
		log.info("End of the method clickOnWorkflowTab");
	}

	/**
	 * Returns version tab version
	 * @return returns version tab version
	 */
	public String getVersionTabVersion() {
		log.info("Start of the method getVersionTabVersion");
		String versionText = driver.safeGetText(versionTabRowVersion);
		log.info("End of the method getVersionTabVersion");
		return versionText;
	}

	/**
	 * Clicks on version tab
	 */
	public void clickOnVersionTab() {
		log.info("Start of the method clickOnVersionTab");
		driver.safeClick(versionTab);
		log.info("End of the method clickOnVersionTab");
	}

	public void uploadBpmnFile(String fileName) {
		log.info("Start of the method uploadBpmnFile");
		fileUploadUtils.uploadFile(uploadBPMNButton, fileName);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeClick(workflowPopUpYesButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(workflowSaveChangesButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(workflowDoneButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(sendInstallRequestYesButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		log.info("End of the method uploadBpmnFile");
	}

	/***
	 * Clicks on deploy workflow button
	 */
	public void clickOnDeployWorkflowButton() {
		log.info("Start of the method clickOnDeployWorkflowButton");
		driver.safeClick(deployWorkflowButton);
		log.info("End of the method clickOnDeployWorkflowButton");
	}

	public void clearSearchTextField() {
		log.info("Start of the method clearSearchTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearSearchTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	/***
	 * Searches application by name
	 * @param appName : app name in string format
	 */
	public void searchApplicationByName(String appName) {
		log.info("Start of the method searchApplicationByName");
		driver.safeType(searchTextBox, appName);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(searchIconButton);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchApplicationByName");
	}

	/***
	 * view application approval in super admin
	 */
	public void viewApplicationApprovals() {
		log.info("Start of the method viewApplicationApprovals");
		driver.safeClick(verticalIconButton);
		driver.safeClick(viewLink);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(editLink);
		log.info("End of the method viewApplicationApprovals");
	}

	/***
	 * Clicks on approve radio button in super admin
	 */
	public void clickOnApproveRadioButton() {
		log.info("Start of the method clickOnApproveRadioButton");
		driver.jSClick(approveRadioButton);
		driver.safeClick(saveButtonInApplicationApproval);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method clickOnApproveRadioButton");
	}

	/***
	 * Clicks on reject radio button in super admin
	 * @param rejectReason : Enters reason for rejection in string format
	 */
	public void clickOnRejectRadioButton(String rejectReason) {
		log.info("Start of the method clickOnRejectRadioButton");
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.jSClick(rejectRadioButton);
//		driver.safeType(rejectReasonTextArea, rejectReason);
		driver.safeClick(saveButtonInApplicationApproval);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method clickOnRejectRadioButton");
	}


	/**
	 * Verifies No Records Available Message
	 * @return returns message display status
	 */
	public boolean verifyNoRecordsMessage() {
		log.info("Start of the method verifyNoRecordsMessage");
		boolean isMessageDisplayed = driver.isPresentAndVisible(noRecordsAvailableTextMessage);
		log.info("End of the method verifyNoRecordsMessage");
		return isMessageDisplayed;
	}

	/**
	 * Verifies whether temp part checkbox displayed
	 * @return returns temp part checkbox display status
	 */
	public boolean isTempPartCheckboxDisplayed() {
		log.info("Start of the method isTempPartCheckboxDisplayed");
		boolean isTempPartCheckboxDisplayed = driver.isPresentAndVisible(tempPartCheckbox);
		log.info("End of the method isTempPartCheckboxDisplayed");
		return isTempPartCheckboxDisplayed;
	}

	/**
	 * addInitialResponseDueFormDetails : fills initial response due form details
	 * @param formName : form name in string format
	 * @param fieldName : field name in string format
	 * @param operatorName : operator name in string format
	 * @param inputValue : input value in string format
	 * @param hours : hours in string format
	 */
	public void addInitialResponseDueFormDetails(String formName, String fieldName, String operatorName, String inputValue, String hours) {
		log.info("Start of the method addInitialResponseDueFormDetails");
		driver.safeSelect(initialResponseSelectFormDropdown, formName);
		driver.safeSelect(initialResponseSelectFormFieldDropdown, fieldName);
		driver.safeSelect(initialResponseOperatorDropdown, operatorName);
		driver.safeType(initialResponseValueInput, inputValue);
		driver.safeType(initialResponseHourInput, hours);
		log.info("End of the method addInitialResponseDueFormDetails");
	}

	/**
	 * addFinalResponseDueFormDetails : fills final response due form details
	 * @param formName : form name in string format
	 * @param fieldName : field name in string format
	 * @param operatorName : operator name in string format
	 * @param inputValue : input value in string format
	 * @param hours : hours in string format
	 */
	public void addFinalResponseDueFormDetails(String formName, String fieldName, String operatorName, String inputValue, String hours) {
		log.info("Start of the method addFinalResponseDueFormDetails");
		driver.safeSelect(finalResponseDueSelectFormDropdown, formName);
		driver.safeSelect(finalResponseDueSelectFormFieldDropdown, fieldName);
		driver.safeSelect(finalResponseDueOperatorDropdown, operatorName);
		driver.safeType(finalResponseDueValueInput, inputValue);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeType(finalResponseDueHourInput, hours);
		log.info("End of the method addFinalResponseDueFormDetails");
	}

	/**
	 * Returns app local version
	 * @return returns app local version
	 */
	public String getAppLocalVersion() {
		log.info("Start of the method getAppLocalVersion");
		String localVersion = driver.safeGetText(appLocalVersionText);
		log.info("End of the method getAppLocalVersion");
		return localVersion;
	}

	/**
	 * Returns app version comment
	 * @return returns app version comment
	 */
	public String getVersionComment() {
		log.info("Start of the method getVersionComment");
		String versionComment = driver.safeGetText(appVersionCommentText);
		log.info("End of the method getVersionComment");
		return versionComment;
	}

	/**
	 * Returns app version title
	 * @return returns app local version
	 */
	public String getAppVersionTitle() {
		log.info("Start of the method getAppVersionTitle");
		String appVersionTitle = driver.safeGetText(supplyIssueManagementTitleVersionText);
		log.info("End of the method getAppVersionTitle");
		return appVersionTitle;
	}

	/**
	 * Verifies whether page column is displayed
	 * @param columnName : columnName in string format
	 */
	public boolean isPageColumnDisplayed(String columnName) {
		log.info("Start of the method isPageColumnDisplayed");
		boolean isPageColumnDisplayed = driver.isPresentAndVisible(By.xpath(String.format(columnNameText, columnName)));
		log.info("End of the method isPageColumnDisplayed");
		return isPageColumnDisplayed;
	}

	/**
	 * Clicks on load button
	 * @param localVersion : local version in string format
	 */
	public void clickOnLoadButton(String localVersion) {
		log.info("Start of the method clickOnLoadButton");
		driver.safeClick(By.xpath(String.format(installedAppVersionVerticalIcon, localVersion)));
		driver.safeClick(appVersionLoadButton);
		log.info("End of the method clickOnLoadButton");
	}

	/**
	 * Clicks on export tenant button
	 * @param localVersion : local version in string format
	 */
	public void clickOnExportTenantButton(String localVersion) {
		log.info("Start of the method clickOnExportTenantButton");
		driver.safeClick(By.xpath(String.format(installedAppVersionVerticalIcon, localVersion)));
		driver.safeClick(appVersionExportTenantButton);
		log.info("End of the method clickOnExportTenantButton");
	}

	/**
	 * Selects target tenant
	 * @param tenantName : tenant name in string format
	 */
	public void selectTargetTenant(String tenantName) {
		log.info("Start of the method selectTargetTenant");
		driver.safeClick(selectTargetTenantDiv);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(selectTargetTenant, tenantName)));
		driver.safeClick(selectTargetTenantDivSubmitButton);
		log.info("End of the method selectTargetTenant");
	}

	/**
	 * Clicks on delete button
	 * @param localVersion : local version in string format
	 */
	public void clickOnDeleteButton(String localVersion) {
		log.info("Start of the method clickOnDeleteButton");
		driver.safeClick(By.xpath(String.format(installedAppVersionVerticalIcon, localVersion)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(appVersionDeleteButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		log.info("End of the method clickOnDeleteButton");
	}

	/***
	 * returns save minor version status
	 * @param localVersion : local version in string format
	 */
	public boolean isAppVersionDisplayed(String localVersion) {
		log.info("Start of the method isAppVersionDisplayed");
		boolean isAppVersionDisplayed = driver.isPresentAndVisible(By.xpath(String.format(installedAppVersionVerticalIcon, localVersion)));
		log.info("End of the method isAppVersionDisplayed");
		return isAppVersionDisplayed;
	}

	/***
	 * Clicks on delete dialog yes button
	 */
	public void clickOnDeleteDialogYesButton() {
		log.info("Start of the method clickOnDeleteDialogYesButton");
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		log.info("End of the method clickOnDeleteDialogYesButton");
	}

	/***
	 * Clicks on apps deployed button in marketplace
	 */
	public void clickOnAppsPublishedButton() {
		log.info("Start of the method clickOnAppsPublishedButton");
		driver.safeClick(appsDeployedButtonInMarketplace);
		log.info("End of the method clickOnAppsPublishedButton");
	}

	/***
	 * returns publish button status
	 */
	public boolean isPublishButtonInProjectDisplayed() {
		log.info("Start of the method isPublishButtonDisplayed");
		boolean isPublishButtonDisplayed = driver.isPresentAndVisible(publishButtonInProject);
		log.info("End of the method isPublishButtonDisplayed");
		return isPublishButtonDisplayed;
	}

	/***
	 * returns publish button display status
	 */
	public boolean isPublishButtonDisplayedInMarketplace() {
		log.info("Start of the method isPublishButtonDisplayedInMarketplace");
		boolean isPublishButtonDisplayedInMarketplace = driver.isPresentAndVisible(publishButtonInMarketplace);
		log.info("End of the method isPublishButtonDisplayedInMarketplace");
		return isPublishButtonDisplayedInMarketplace;
	}

	/***
	 * returns apps deployed button display status
	 */
	public boolean isAppsDeployedButtonDisplayed() {
		log.info("Start of the method isAppsDeployedButtonDisplayed");
		boolean isAppsDeployedButtonDisplayed = driver.isPresentAndVisible(appsDeployedButtonInMarketplace);
		log.info("End of the method isAppsDeployedButtonDisplayed");
		return isAppsDeployedButtonDisplayed;
	}

	/***
	 * returns installed apps display status
	 */
	public boolean isInstalledAppsDisplayed() {
		log.info("Start of the method isInstalledAppsDisplayed");
		boolean isInstalledAppsDisplayed = driver.isPresentAndVisible(installedAppsTextInMarketplace);
		log.info("End of the method isInstalledAppsDisplayed");
		return isInstalledAppsDisplayed;
	}

	/***
	 * Verifies whether plus button is present or not
	 */
	public boolean isPlusButtonDisplayed() {
		log.info("Start of the method isPlusButtonDisplayed");
		boolean isPlusButtonDisplayed = driver.isPresentAndVisible(tableAddButton);
		log.info("End of the method isPlusButtonDisplayed");
		return isPlusButtonDisplayed;
	}

	/***
	 * returns available apps display status
	 */
	public boolean isAvailableAppsDisplayed() {
		log.info("Start of the method isAvailableAppsDisplayed");
		boolean isAvailableAppsDisplayed = driver.isPresentAndVisible(availableAppsTextInMarketplace);
		log.info("End of the method isAvailableAppsDisplayed");
		return isAvailableAppsDisplayed;
	}

	/***
	 * Clicks on App Settings Tab
	 */
	public void clickOnAppSettingsTab() {
		log.info("Start of the method clickOnAppSettingsTab");
		driver.safeClick(appSettingsTab);
		log.info("End of the method clickOnAppSettingsTab");
	}

	/***
	 * Selects one Task Initiator Role Field
	 */
	public void selectOneTaskInitiatorRole() {
		log.info("Start of the method clickOnTaskInitiatorRoles");
		driver.safeClick(taskInitiatorRoles);
		driver.safeClick(taskInitiatorRoleBuyerUser);
		log.info("End of the method clickOnTaskInitiatorRoles");
	}

	/***
	 * Selects multiple Task Initiator Roles
	 */
	public void selectMultipleTaskInitiatorRoles() {
		log.info("Start of the method clickOnTaskInitiatorRoles");
		driver.safeClick(taskInitiatorRoles);
		driver.safeClick(taskInitiatorRoleBuyerUser);
		driver.safeClick(taskInitiatorRoleSupplierUser);
		log.info("End of the method clickOnTaskInitiatorRoles");
	}

	/***
	 * returns installed apps display status
	 */
	public boolean isInstalledAppsDisplayedWithVersion(String appName, String version) {
		log.info("Start of the method isInstalledAppsDisplayedWithVersion");
		boolean isInstalledAppsDisplayedWithVersion = driver.isPresentAndVisible(By.xpath(String.format(installedAppWithVersion, appName, version)));
		log.info("End of the method isInstalledAppsDisplayedWithVersion");
		return isInstalledAppsDisplayedWithVersion;
	}

	/***
	 * returns available apps display status
	 */
	public boolean isAvailableAppsDisplayedWithVersion(String appName, String version) {
		log.info("Start of the method isAvailableAppsDisplayedWithVersion");
		boolean isAvailableAppsDisplayedWithVersion = driver.isPresentAndVisible(By.xpath(String.format(availableAppWithVersion, appName, version)));
		log.info("End of the method isAvailableAppsDisplayedWithVersion");
		return isAvailableAppsDisplayedWithVersion;
	}

	/***
	 * returns available apps display status
	 */
	public boolean isAvailableAppsDownloadMessageDisplayed(String appName) {
		log.info("Start of the method isAvailableAppsDownloadMessageDisplayed");
		driver.safeClick(By.xpath(String.format(availableAppDownloadIcon, appName)));
		driver.sleep(WaitTime.SMALL_WAIT);
		boolean isAvailableAppsDownloadMessageDisplayed = driver.isPresentAndVisible(availableAppDownloadMessage);
		driver.safeClick(installDialogYesButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method isAvailableAppsDownloadMessageDisplayed");
		return isAvailableAppsDownloadMessageDisplayed;
	}

}