package io.ient.test.pages.enterprise;

import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class AnalyticsPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(AnalyticsPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By selectAllCheckBox = By.xpath("//div[@id='chk1']/input[@id='chk']");
	private static final By topVerticalIcon = By.cssSelector("div[class^='ui icon top left pointing dropdown button']>i[class^='ellipsis vertical icon']");
	private static final By exportToTenantOption = By.xpath("//div[contains(@class,'item') and span[text()='Export to Tenant']]");
	private static final By firstRowAnalyticsName = By.xpath("//table[@class='ui striped table actual-table']/tbody/tr[1]/td[3]/a");
	private static final By firstRowText = By.xpath("(//table[@class='ui striped table actual-table']/tbody/tr/td[3]/a)[1]");
	private static final By configurationExpandButton = By.cssSelector("i[data-title='Collapse Panel']");
	private static final By fromDateTextField = By.cssSelector("input#fromDate");
	private static final By toDateTextField = By.cssSelector("input#toDate");
	private static String targetTenantOption = "//div[contains(@class,'item') and @id='%s']";


	public AnalyticsPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - AuditLogPage");
	}

	/***
	 * Clicks on select all checkbox
	 */
	public void clickSelectAllCheckbox() {
		log.info("Start of the method clickSelectAllCheckbox");
		driver.safeClick(selectAllCheckBox);
		log.info("End of the method clickSelectAllCheckbox");
	}

	/***
	 * Exports tenants data
	 * @param tenantName : tenant name in string format
	 */
	public void exportToTenant(String tenantName)
	{
		log.info("Start of the method exportToTenant");
		driver.safeClick(topVerticalIcon);
		driver.safeClick(exportToTenantOption);
		driver.safeClick(By.xpath(String.format(targetTenantOption, tenantName)));
		log.info("End of the method exportToTenant");
	}

	/**
	 * Returns first row analytics name
	 * @return returns app local version
	 */
	public String getFirstRowAnalyticsName() {
		log.info("Start of the method getFirstRowAnalyticsName");
		String analyticsName = driver.safeGetText(firstRowAnalyticsName);
		log.info("End of the method getFirstRowAnalyticsName");
		return analyticsName;
	}

	/**
	 * Clicks on analytics first row
	 */
	public void clickOnAnalyticsFirstRow() {
		log.info("Start of the method clickOnAnalyticsFirstRow");
		driver.safeClick(firstRowText);
		log.info("End of the method clickOnAnalyticsFirstRow");
	}

	/**
	 * Clicks on configuration expand button
	 */
	public void clickOnConfigurationExpandButton() {
		log.info("Start of the method clickOnConfigurationExpandButton");
		driver.safeClick(configurationExpandButton);
		log.info("End of the method clickOnConfigurationExpandButton");
	}

	/**
	 * Returns from date text
	 * @return returns from date
	 */
	public String getFromDateText() {
		log.info("Start of the method getFromDateText");
		String fromDate = driver.safeGetAttribute(fromDateTextField, "value");
		fromDate = fromDate.replace("T", " ");
		log.info("End of the method getFromDateText");
		return fromDate;
	}

	/**
	 * Returns to date text
	 * @return returns from date
	 */
	public String getToDateText() {
		log.info("Start of the method getToDateText");
		String toDate = driver.safeGetAttribute(toDateTextField, "value");
		toDate = toDate.replace("T", " ");
		log.info("End of the method getToDateText");
		return toDate;
	}
}