package io.ient.test.pages.enterprise;


import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class TenantsPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(TenantsPage.class);
	LogFactory log = new LogFactory(logger);
	private static final By tenantsTab = By.cssSelector("div[id='tenants-card']");
	private static final By tenantsManagementTab = By.cssSelector("div[id='tenant-management-card']");
	private static final By addButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static final By organizationNameTextBox = By.cssSelector("input[id='orgName']");
	private static final By phoneNumberTextBox = By.cssSelector("input[id='orgPhone']");
	private static final By dataSourceRegion = By.xpath("//div[select[@id='datasourceRegion']]");
	private static String dataSourceRegionDropdownOption = "//div[contains(@id,'datasourceRegion') and @data-id='%s']";
	private static final By preferredContextPathTextBox = By.cssSelector("input[id='preferredContextPath']");
	private static final By address1TextBox = By.cssSelector("input[id='address1']");
	private static final By cityTextBox = By.cssSelector("input[id='city']");
	private static final By stateTextBox = By.cssSelector("input[id='state']");
	private static final By countryTextBox = By.cssSelector("input[id='country']");
	private static final By zipCodeTextBox = By.cssSelector("input[id='zipCode']");
	private static final By userNameTextBox = By.cssSelector("input[id='username']");
	private static final By salutationDropdown = By.xpath("//div[select[@id='salutation']]");
	private static final By salutationDropdownOption = By.cssSelector("div[data-value='Mr']");
	private static final By firstNameTextBox = By.cssSelector("input[id='firstName']");
	private static final By lastNameTextBox = By.cssSelector("input[id='lastName']");
	private static final By emailTextBox = By.cssSelector("input[id='email']");
	private static final By submitButton = By.cssSelector("button[name='data[submit]']");
	private static final By cancelButton = By.cssSelector("button[id='cancel']");
	private static final By confirmationMessageText = By.cssSelector("div[class='confirm-container']>div[class='message']");
	private static final By confirmationYesButton = By.xpath("//div[@class='btns']/button[text()='Yes']");
	private static String tenantRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']]]";
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static final By verticalIconButton = By.cssSelector("i[id^='vertical-icon-']");
	private static final By editLink = By.cssSelector("div[id='edit']");
	private static final By adminEditButton = By.cssSelector("ient-button[label='Edit']");
	private static final By deleteLink = By.cssSelector("div[id='delete']");
	private static final By impersonateLink = By.cssSelector("div[id='impersonate']");
	private static final By deleteDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By noRecordsAvailableTextMessage = By.xpath("//div[@class='ui red message ng-star-inserted' and text()=' No records available ']");
	private static final By timeZoneDropdown = By.xpath("//div[select[@id='timezone']]");
	private static final By selectedTimeZoneText = By.xpath("//div[select[@id='timezone']]/descendant::div[@aria-selected='true']");
	private static String timeZoneDropdownOption = "//div[contains(@id,'timezone') and @data-id='%s']";
	private static String timeZoneDropdownOptionByAttribute = "//div[contains(@id,'timezone') and @data-value='%s']";
	private static final By dateTimeFormatDropdown = By.xpath("//div[select[@id='dateTimeFormat']]");
	private static final By selectedDateTimeFormatDropdownText = By.xpath("//div[select[@id='dateTimeFormat']]/descendant::div[@aria-selected='true']/span");
	private static String dateTimeFormatDropdownOption = "//div[span[text()='%s']]";
	private static final By dateFormatDropdown = By.xpath("//div[select[@id='dateFormat']]");
	private static final By selectedDateFormatDropdown = By.xpath("//div[select[@id='dateFormat']]/descendant::div[@aria-selected='true']");
	private static String dateFormatDropdownOptionText = "//div[span[text()='%s']]";
	private static final By timeFormatDropdown = By.xpath("//div[select[@id='timeFormat']]");
	private static final By selectedTimeFormatDropdown = By.xpath("//div[select[@id='timeFormat']]/descendant::div[@aria-selected='true']");
	private static String timeFormatDropdownOptionText = "//div[span[text()='%s']]";
	private static final By maxUserTextBox = By.cssSelector("input[id='maxBuyerUsers']");
	private static final By maxSupplierUserTextBox = By.cssSelector("input[id='maxSupplierUsers']");
	private static final By maxTransactionTextBox = By.cssSelector("input[id='maxTransactions']");
	private static final By totalAttachmentSizeTextBox = By.cssSelector("input[id='totalAttachmentSize']");
	private static final By eulaAgreementTextArea = By.cssSelector("textarea[id='eulaContent']");
	private static final By customer = By.xpath("//div[select[@id='customerId']]");
	private static String customerDropdownOption = "//div[contains(@id,'customerId') and @data-id='%s']";
	private static final By columnSelectionButton = By.cssSelector("button[id='ient-table-col-selection-btn']");
	private static final By filterButton = By.cssSelector("button[id='ient-table-filter-btn']");
	private static final By addNewButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static final By topLeftVerticalIcon = By.cssSelector("i[id='ient-table-more-action-btn']");
	private static final By downloadOption = By.xpath("//div[contains(@class,'item') and span[text()='Download Options']]");
	private static final By downloadAsPdfOption = By.xpath("//div[contains(@class,'item') and text()=' Download as PDF ']");
	private static final By downloadAsCsvOption = By.xpath("//div[contains(@class,'item') and text()=' Download as CSV ']");
	private static final By paginationDiv = By.cssSelector("div[class='ient-pagination']");
	private static final By totalCountDiv = By.cssSelector("div[id='ient-table-item-count']");
	private static String columnNameText = "//div[@class='ient-table-header-content-label' and text()=' %s ']";
	private static final By itemsPerPageDropdown = By.cssSelector("div[id='items-per-page']");
	private static final By itemsPerPageDropdownOption = By.cssSelector("div[id='page-item-20']");
	private static final By activeCheckbox = By.xpath("//input[@name='data[status]']");
	private static final By addressRequiredMessageText = By.xpath("//div[@ref='messageContainer']/div[@class='ui pointing red basic label error']");

	private static final By customersTab = By.cssSelector("div[id='customers-card']");
	private static final By nameTextBox = By.cssSelector("input[name='data[name]']");
	private static final By descTextArea = By.cssSelector("textarea[id='description']");
	private static final By customerText = By.xpath("//div[div[select[@name='data[customerId]']]]/descendant::div[@data-item]/span");
	private static String selectedCustomerText = "//div[div[select[@name='data[customerId]']]]/descendant::div[@data-item]/span";
	private static final By firstRowInListView = By.cssSelector("tr[id='ient-table-row-no-0']");
	private static String customersDropdownOption = "//div[@role='option' and span[text()='%s']]/span";
	private static final By exportTenantDropdownButton = By.xpath("//div[ient-button[button[span[text()='Export to Tenant']]]]/descendant::ient-button[@icon='dropdown icon']");
	private static String selectTargetTenant = "//div[contains(text(),'%s')]";


	public TenantsPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - TenantsPage");
	}

	/***
	 * Clicks on tenants tab
	 */
	public void clickOnTenantsTab() {
		log.info("Start of the method clickOnTenantsTab");
		driver.safeClick(tenantsTab);
		log.info("End of the method clickOnTenantsTab");
	}

	/***
	 * Clicks on tenants management tab
	 */
	public void clickOnTenantsManagementTab() {
		log.info("Start of the method clickOnTenantsManagementTab");
		driver.safeClick(tenantsManagementTab);
		log.info("End of the method clickOnTenantsManagementTab");
	}

	/***
	 * Creates a new tenant
	 * @param organizationName : name of organization in string format
	 * @param phoneNumber : phone number in string format
	 * @param dataSourceRegionName : data source name in string format
	 * @param endPoint : end point in string format
	 * @param address1 : address1 in string format
	 * @param city : city in string format
	 * @param userName : name of user in string format
	 * @param firstName : firstname of a user in string format
	 * @param lastName : lastname of a user in string format
	 * @param emailAddress : emailAddress of a user in string format
	 * @param maxUsers : max users in string format
	 * @param maxSupplierUsers : max supplier users in string format
	 * @param maxTransactions : max transactions in string format
	 * @param totalAttachmentSize : totalAttachmentSize in string format
	 * @param customerName : customer name in string format
	 * @param eulaText : EULA text in string format
	 */
	public void createTenant(String organizationName, String phoneNumber, String dataSourceRegionName, String endPoint, String address1, String city, String state, String country, String zipCode, String userName, String firstName, String lastName, String emailAddress, String timeZone, String dateFormat, String timeFormat, String maxUsers, String maxSupplierUsers, String maxTransactions, String totalAttachmentSize, String customerName, String eulaText) {
		log.info("Start of the method createTenant");
		driver.safeClick(addButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeType(organizationNameTextBox, organizationName);
		driver.safeClearAndTypeByActionsClass(phoneNumberTextBox, phoneNumber, false);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(dataSourceRegion);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(dataSourceRegionDropdownOption, dataSourceRegionName)));
		driver.safeType(preferredContextPathTextBox, endPoint);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(timeZoneDropdown);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(timeZoneDropdownOption, timeZone)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(dateTimeFormatDropdown);
		driver.sleep(WaitTime.SMALL_WAIT);
		String dateTimeFormat = dateFormat + " " + timeFormat;
		driver.safeClick(By.xpath(String.format(dateTimeFormatDropdownOption, dateTimeFormat)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.scrollToElementViaJavaScript(dateFormatDropdown);
		driver.safeClick(dateFormatDropdown);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(dateFormatDropdownOptionText, dateFormat)));
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(timeFormatDropdown);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(timeFormatDropdownOptionText, timeFormat)));
		driver.safeType(address1TextBox, address1);
		driver.safeType(cityTextBox, city);
		driver.safeType(stateTextBox, state);
		driver.safeType(countryTextBox, country);
		driver.safeType(zipCodeTextBox, zipCode);
		driver.safeType(userNameTextBox, userName);
		driver.sleep(WaitTime.SMALL_WAIT);
        driver.safeClick(salutationDropdown);
        driver.sleep(WaitTime.SMALL_WAIT);
        driver.safeClick(salutationDropdownOption);
		driver.safeType(firstNameTextBox, firstName);
		driver.safeType(lastNameTextBox, lastName);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClearAndTypeByActionsClass(emailTextBox, emailAddress, false);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeType(maxUserTextBox, maxUsers);
		driver.safeType(maxSupplierUserTextBox, maxSupplierUsers);
		driver.safeType(maxTransactionTextBox, maxTransactions);
		driver.safeType(totalAttachmentSizeTextBox, totalAttachmentSize);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(customer);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(customerDropdownOption, customerName)));
		driver.safeType(eulaAgreementTextArea, eulaText);
		driver.sleep(WaitTime.LONG_WAIT);
		driver.scrollToElementViaJavaScript(submitButton);
		driver.safeClick(submitButton);
		driver.sleep(WaitTime.LONG_WAIT);
		driver.sleep(WaitTime.LONG_WAIT);
		log.info("End of the method createTenant");
	}

	/***
	 * Edits date time format in super admin
	 * @param updatedDateTimeFormat : updated date time format in string format
	 * @param dateFormat : date format in string format
	 * @param timeFormat : time format in string format
	 */
	public void editDateTimeFormatInSuperAdmin(String updatedDateTimeFormat, String dateFormat, String timeFormat) {
		log.info("Start of the method editDateTimeFormatInSuperAdmin");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(dateTimeFormatDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		String dateTimeFormat = updatedDateTimeFormat + " " + timeFormat;
		driver.safeClick(By.xpath(String.format(dateTimeFormatDropdownOption, dateTimeFormat)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.scrollToElementViaJavaScript(dateFormatDropdown);
		driver.safeClick(dateFormatDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(dateFormatDropdownOptionText, updatedDateTimeFormat)));
		driver.safeClick(timeFormatDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(timeFormatDropdownOptionText, timeFormat)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(submitButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		log.info("End of the method editDateTimeFormatInSuperAdmin");
	}

	/***
	 * Edits date time format in Buyer Admin
	 * @param dateFormat : date format in string format
	 * @param timeFormat : time format in string format
	 */
	public void editDateTimeFormatInBuyerAdmin(String dateFormat, String timeFormat) {
		log.info("Start of the method editDateTimeFormatInBuyerAdmin");
		driver.safeClick(adminEditButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(dateTimeFormatDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		String dateTimeFormat = dateFormat + " " + timeFormat;
		driver.safeClick(By.xpath(String.format(dateTimeFormatDropdownOption, dateTimeFormat)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.scrollToElementViaJavaScript(dateFormatDropdown);
		driver.safeClick(dateFormatDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(dateFormatDropdownOptionText, dateFormat)));
		driver.safeClick(timeFormatDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(timeFormatDropdownOptionText, timeFormat)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(submitButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method editDateTimeFormatInBuyerAdmin");
	}

	/***
	 * Edits date time format in user profile
	 * @param updatedDateTimeFormat : updated date time format in string format
	 * @param dateFormat : date format in string format
	 * @param timeFormat : time format in string format
	 */
	public void editDateTimeFormatInUserProfile(String updatedDateTimeFormat, String dateFormat, String timeFormat) {
		log.info("Start of the method editDateTimeFormatInUserProfile");
		driver.safeClick(dateTimeFormatDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		String dateTimeFormat = updatedDateTimeFormat + " " + timeFormat;
		driver.safeClick(By.xpath(String.format(dateTimeFormatDropdownOption, dateTimeFormat)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.scrollToElementViaJavaScript(dateFormatDropdown);
		driver.safeClick(dateFormatDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(dateFormatDropdownOptionText, updatedDateTimeFormat)));
		driver.safeClick(timeFormatDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(timeFormatDropdownOptionText, timeFormat)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		log.info("End of the method editDateTimeFormatInUserProfile");
	}

	/***
	 * Edits date time format
	 * @param updatedDateTimeFormat : updated date time format in string format
	 * @param timeFormat : time format in string format
	 */
	public void editDateTimeFormat(String updatedDateTimeFormat, String timeFormat) {
		log.info("Start of the method editDateTimeFormat");
		driver.safeClick(dateTimeFormatDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		String dateTimeFormat = updatedDateTimeFormat + " " + timeFormat;
		driver.safeClick(By.xpath(String.format(dateTimeFormatDropdownOption, dateTimeFormat)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.scrollToElementViaJavaScript(dateFormatDropdown);
		driver.safeClick(dateFormatDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(dateFormatDropdownOptionText, updatedDateTimeFormat)));
		driver.safeClick(timeFormatDropdown);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(timeFormatDropdownOptionText, timeFormat)));
		driver.sleep(WaitTime.XSMALL_WAIT);
		log.info("End of the method editDateTimeFormat");
	}

	/***
	 * Edits time zone in user profile
	 * @param timeZone : timezone format in string format
	 */
	public void editTimeZoneInUserProfile(String timeZone) {
		log.info("Start of the method editTimeZoneInUserProfile");
		driver.safeClick(timeZoneDropdown);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(timeZoneDropdownOptionByAttribute, timeZone)));
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method editTimeZoneInUserProfile");
	}

	/***
	 * Edits time zone
	 * @param timeZone : timezone format in string format
	 */
	public void editTimeZone(String timeZone) {
		log.info("Start of the method editTimeZone");
		driver.safeClick(timeZoneDropdown);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(timeZoneDropdownOptionByAttribute, timeZone)));
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method editTimeZone");
	}

	/***
	 * Returns time zone from user profile
	 */
	public String getTimeZoneFromUserProfile() {
		log.info("Start of the method getTimeZoneFromUserProfile");
		String timeZone = driver.safeGetAttribute(selectedTimeZoneText, "data-value");
		log.info("End of the method getTimeZoneFromUserProfile");
		return timeZone;
	}

	/***
	 * Returns time zone
	 */
	public String getTimeZone() {
		log.info("Start of the method getTimeZone");
		String timeZone = driver.safeGetAttribute(selectedTimeZoneText, "data-value");
		log.info("End of the method getTimeZone");
		return timeZone;
	}

	/***
	 * Returns date time format from user profile
	 */
	public String getDateTimeFormatFromUserProfile() {
		log.info("Start of the method getDateTimeFormatFromUserProfile");
		String dateTimeFormat = driver.safeGetText(selectedDateTimeFormatDropdownText);
		log.info("End of the method getDateTimeFormatFromUserProfile");
		return dateTimeFormat;
	}

	/***
	 * Returns date time format
	 */
	public String getDateTimeFormat() {
		log.info("Start of the method getDateTimeFormat");
		String dateTimeFormat = driver.safeGetText(selectedDateTimeFormatDropdownText);
		log.info("End of the method getDateTimeFormat");
		return dateTimeFormat;
	}

	/***
	 * Returns date format from user profile
	 */
	public String getDateFormatFromUserProfile() {
		log.info("Start of the method getDateFormatFromUserProfile");
		String dateFormat = driver.safeGetText(selectedDateFormatDropdown);
		log.info("End of the method getDateFormatFromUserProfile");
		return dateFormat;
	}

	/***
	 * Returns time format from user profile
	 */
	public String getTimeFormatFromUserProfile() {
		log.info("Start of the method getTimeFormatFromUserProfile");
		String timeFormat = driver.safeGetText(selectedTimeFormatDropdown);
		log.info("End of the method getTimeFormatFromUserProfile");
		return timeFormat;
	}



	/**
	 * Verifies whether tenant is displayed
	 * @param tenantName : tenant name in string format
	 */
	public boolean isTenantDisplayed(String tenantName) {
		log.info("Start of the method isTenantDisplayed");
		boolean isUserDisplayed = driver.isPresentAndVisible(By.xpath(String.format(tenantRow, tenantName)));
		log.info("End of the method isTenantDisplayed");
		return isUserDisplayed;
	}

	/***
	 * Searches tenant by tenant name
	 * @param tenantName : tenantName in string format
	 */
	public void searchTenantByTenantName(String tenantName) {
		log.info("Start of the method searchTenantByTenantName");
		driver.safeType(searchTextBox, tenantName);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(searchIconButton);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchTenantByTenantName");
	}

	public void clearSearchTextField() {
		log.info("Start of the method clearSearchTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearSearchTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	/***
	 * Edits existing tenant
	 * @param updatedTenantName : updated tenant name in string format
	 *
	 */
	public void editTenant(String updatedTenantName) {
		log.info("Start of the method editTenant");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeType(organizationNameTextBox, updatedTenantName);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(submitButton);
		log.info("End of the method editTenant");
	}

	/**
	 * Clicks on edit button
	 */
	public void clickOnEditButton() {
		log.info("Start of the method clickOnEditButton");
		driver.safeClick(adminEditButton);
		log.info("End of the method clickOnEditButton");
	}

	/**
	 * navigates to edit tenant view
	 */
	public void goToEditTenantView() {
		log.info("Start of the method goToEditTenantView");
		driver.safeClick(verticalIconButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(editLink);
		log.info("End of the method goToEditTenantView");
	}


	/**
	 * Edits tenant by buyer admin
	 * @param updatedTenantName : updated tenant name in string format
	 * @param address1 : address in string format
	 */
	public void editTenantByBuyerAdmin(String updatedTenantName, String address1) {
		log.info("Start of the method editTenantByBuyerAdmin");
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeType(organizationNameTextBox, updatedTenantName);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeType(address1TextBox, address1);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(submitButton);
		log.info("End of the method editTenantByBuyerAdmin");
	}



	/***
	 * Edits existing tenant
	 * @param tenantName : tenant name in string format
	 *
	 */
	public void editTenantStatusAsInactive(String tenantName) {
		log.info("Start of the method editTenantStatusAsInactive");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.safeType(organizationNameTextBox, tenantName);
		driver.sleep(WaitTime.LONG_WAIT);
		driver.jSClick(activeCheckbox);
		driver.safeClick(submitButton);
		log.info("End of the method editTenantStatusAsInactive");
	}

	/***
	 * Deletes tenant
	 */
	public void deleteTenant() {
		log.info("Start of the method deleteTenant");
		driver.safeClick(verticalIconButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		driver.sleep(WaitTime.LONG_WAIT);
		driver.sleep(WaitTime.LONG_WAIT);
		log.info("End of the method deleteTenant");
	}

	/**
	 * Verifies No Records Available Message
	 * @return returns message display status
	 */
	public boolean verifyNoRecordsMessage() {
		log.info("Start of the method verifyNoRecordsMessage");
		boolean isMessageDisplayed = driver.isPresentAndVisible(noRecordsAvailableTextMessage);
		log.info("End of the method verifyNoRecordsMessage");
		return isMessageDisplayed;
	}

	/***
	 * Impersonate to Buyer Admin
	 * @param tenantName : tenant name in string format
	 *
	 */
	public void impersonateTenant(String tenantName) {
		log.info("Start of the method impersonateTenant");
		driver.safeClick(verticalIconButton);
		driver.safeClick(impersonateLink);
		log.info("End of the method impersonateTenant");
	}

	/***
	 * Impersonate to Super Admin
	 */
	public void impersonateTenant() {
		log.info("Start of the method impersonateTenant");
		driver.safeClick(verticalIconButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(impersonateLink);
		log.info("End of the method impersonateTenant");
	}


	/**
	 * Clicks on submit button
	 */
	public void clickOnSubmitButton() {
		log.info("Start of the method clickOnSubmitButton");
		driver.safeClick(submitButton);
		log.info("End of the method clickOnSubmitButton");
	}

	/**
	 * Clicks on submit button
	 */
	public void UpdateEmptyDetailsAndSubmit() {
		log.info("Start of the method clickOnSubmitButton");
		driver.safeClick(submitButton);
		log.info("End of the method clickOnSubmitButton");
	}

	/**
	 * Clicks on add tenant button
	 */
	public void clickOnAddTenant() {
		log.info("Start of the method clickOnAddTenant");
		driver.safeClick(addButton);
		log.info("End of the method clickOnAddTenant");
	}

	/**
	 * Verifies whether SearchBar is displayed
	 */
	public boolean isSearchBarDisplayed() {
		log.info("Start of the method isSearchBarDisplayed");
		boolean isSearchBarDisplayed = driver.isPresentAndVisible(searchTextBox) && driver.isPresentAndVisible(searchIconButton);
		log.info("End of the method isSearchBarDisplayed");
		return isSearchBarDisplayed;
	}

	/**
	 * Verifies whether SelectColumns is displayed
	 */
	public boolean isSelectColumnsDisplayed() {
		log.info("Start of the method isSelectColumnsDisplayed");
		boolean isSelectColumnsDisplayed = driver.isPresentAndVisible(columnSelectionButton);
		log.info("End of the method isSelectColumnsDisplayed");
		return isSelectColumnsDisplayed;
	}

	/**
	 * Verifies whether FilterIcon is displayed
	 */
	public boolean isFilterIconDisplayed() {
		log.info("Start of the method isFilterIconDisplayed");
		boolean isFilterIconDisplayed = driver.isPresentAndVisible(filterButton);
		log.info("End of the method isFilterIconDisplayed");
		return isFilterIconDisplayed;
	}

	/**
	 * Verifies whether add new button is displayed
	 */
	public boolean isAddNewButtonDisplayed() {
		log.info("Start of the method isNewUserButtonDisplayed");
		boolean isAddNewButtonDisplayed = driver.isPresentAndVisible(addNewButton);
		log.info("End of the method isNewUserButtonDisplayed");
		return isAddNewButtonDisplayed;
	}

	/**
	 * Verifies whether edit button is displayed
	 */
	public boolean isEditButtonDisplayed() {
		log.info("Start of the method isEditButtonDisplayed");
		boolean isEditButtonDisplayed = driver.isPresentAndVisible(adminEditButton);
		log.info("End of the method isEditButtonDisplayed");
		return isEditButtonDisplayed;
	}

	/**
	 * Verifies whether DownloadAsPdf is displayed
	 */
	public boolean isDownloadAsPdfDisplayed() {
		log.info("Start of the method isDownloadAsPdfDisplayed");
		driver.safeMouseOver(downloadOption);
		boolean isDownloadAsPdfDisplayed = driver.isPresentAndVisible(downloadAsPdfOption);
		log.info("End of the method isDownloadAsPdfDisplayed");
		return isDownloadAsPdfDisplayed;
	}

	/**
	 * Verifies whether DownloadAsCsv is displayed
	 */
	public boolean isDownloadAsCsvDisplayed() {
		log.info("Start of the method isDownloadAsCsvDisplayed");
		driver.safeMouseOver(downloadOption);
		boolean isDownloadAsCsvDisplayed = driver.isPresentAndVisible(downloadAsCsvOption);
		log.info("End of the method isDownloadAsCsvDisplayed");
		return isDownloadAsCsvDisplayed;
	}

	/**
	 * Clicks on TopLeftVerticalIcon
	 */
	public void clickOnTopLeftVerticalIcon() {
		log.info("Start of the method clickOnTopLeftVerticalIcon");
		driver.safeClick(topLeftVerticalIcon);
		log.info("End of the method clickOnTopLeftVerticalIcon");
	}

	/**
	 * Verifies whether page column is displayed
	 * @param columnName : columnName in string format
	 */
	public boolean isPageColumnDisplayed(String columnName) {
		log.info("Start of the method isPageColumnDisplayed");
		boolean isPageColumnDisplayed = driver.isPresentAndVisible(By.xpath(String.format(columnNameText, columnName)));
		log.info("End of the method isPageColumnDisplayed");
		return isPageColumnDisplayed;
	}

	/**
	 * Verifies whether Edit And Delete is displayed
	 */
	public boolean isActionEditAndDeleteDisplayed() {
		log.info("Start of the method isActionEditAndDeleteDisplayed");
		driver.safeClick(verticalIconButton);
		boolean isActionEditAndDeleteDisplayed = driver.isPresentAndVisible(editLink) && driver.isPresentAndVisible(deleteLink) && driver.isPresentAndVisible(impersonateLink);
		log.info("End of the method isActionEditAndDeleteDisplayed");
		return isActionEditAndDeleteDisplayed;
	}

	/**
	 * Verifies whether number of items per page is editable
	 */
	public boolean isNumberOfItemsPerPageEditable() {
		log.info("Start of the method isNumberOfItemsPerPageEditable");
		driver.safeClick(itemsPerPageDropdown);
		boolean isNumberOfItemsPerPageEditable = driver.isPresentAndVisible(itemsPerPageDropdownOption);
		log.info("End of the method isNumberOfItemsPerPageEditable");
		return isNumberOfItemsPerPageEditable;
	}

	/**
	 * Verifies whether pagination is displayed
	 */
	public boolean isPaginationDisplayed() {
		log.info("Start of the method isPaginationDisplayed");
		boolean isPaginationDisplayed = driver.isPresentAndVisible(paginationDiv);
		log.info("End of the method isPaginationDisplayed");
		return isPaginationDisplayed;
	}

	/**
	 * Verifies whether total count is displayed
	 */
	public boolean isTotalCountDisplayed() {
		log.info("Start of the method isTotalCountDisplayed");
		boolean isTotalCountDisplayed = driver.isPresentAndVisible(totalCountDiv);
		log.info("End of the method isTotalCountDisplayed");
		return isTotalCountDisplayed;
	}

	/**
	 * Verifies whether submit button disabled
	 */
	public boolean isSubmitButtonDisabled() {
		log.info("Start of the method isSubmitButtonDisabled");
		String disabledText = driver.getAttribute(submitButton, "disabled");
		boolean isSubmitButtonDisabled = disabledText.equals("disabled");
		log.info("End of the method isSubmitButtonDisabled");
		return isSubmitButtonDisabled;
	}

	/**
	 * Verifies whether confirmation message is displayed
	 * @param organizationName : name of organization in string format
	 */
	public boolean isConfirmationMessageDisplayed(String organizationName, String expectedConfirmationMessage) {
		log.info("Start of the method isConfirmationMessageDisplayed");
		driver.safeClick(addButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		driver.safeType(organizationNameTextBox, organizationName);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(cancelButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		String actualConfirmationMessage = driver.safeGetText(confirmationMessageText);
		boolean isConfirmationMessageDisplayed = actualConfirmationMessage.equals(expectedConfirmationMessage);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(confirmationYesButton);
		driver.sleep(WaitTime.MEDIUM_WAIT);
		log.info("End of the method isConfirmationMessageDisplayed");
		return isConfirmationMessageDisplayed;
	}

	/**
	 * Verifies whether required error message is displayed
	 * @param expectedMessage : expected message in string format
	 */
	public boolean isFieldRequiredErrorMessageDisplayed(String expectedMessage) {
		log.info("Start of the method isFieldRequiredErrorMessageDisplayed");
		driver.safeClear(address1TextBox);
		driver.sleep(WaitTime.SMALL_WAIT);
		String actualMessage = driver.safeGetText(addressRequiredMessageText).trim();
		boolean isMessageDisplayed = actualMessage.equals(expectedMessage);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method isFieldRequiredErrorMessageDisplayed");
		return isMessageDisplayed;
	}

	/***
	 * Clicks on customers tab
	 */
	public void clickOnCustomersTab() {
		log.info("Start of the method clickOnCustomersTab");
		driver.safeClick(customersTab);
		driver.sleep(WaitTime.XSMALL_WAIT);
		log.info("End of the method clickOnCustomersTab");
	}

	/***
	 * Creates a new customer
	 * @param name : name in string format
	 * @param description : description in string format
	 */
	public void createNewCustomer(String name, String description) {
		log.info("Start of the method createNewCustomer");
		driver.safeClick(addButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeType(nameTextBox, name);
		driver.safeType(descTextArea, description);
		driver.safeClick(submitButton);
		log.info("End of the method createNewCustomer");
	}

	/***
	 * navigates to edit view of tenant customer
	 */
	public void goToEditView() {
		log.info("Start of the method goToEditView");
		driver.safeClick(firstRowInListView);
		driver.sleep(WaitTime.XSMALL_WAIT);
		log.info("End of the method goToEditView");
	}

	/***
	 * Edits existing customer
	 * @param updateCustomer : update customer in string format
	 */
	public void editCustomer(String updateCustomer) {
		log.info("Start of the method editCustomer");
		driver.safeClick(verticalIconButton);
		driver.safeClick(editLink);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeType(nameTextBox, updateCustomer);
		driver.safeClick(submitButton);
		log.info("End of the method editCustomer");
	}


	/**
	 * Returns customer selected in tenant creation form
	 * @return returns customerSelectedText
	 */
	public String getCustomerSelectedText() {
		log.info("Start of the method getCustomerSelectedText");
		String customerSelectedText = driver.safeGetText(customerText);
		driver.sleep(WaitTime.XSMALL_WAIT);
		log.info("End of the method getCustomerSelectedText");
		return customerSelectedText;
	}

	/***
	 * Deletes customer
	 */
	public void deleteCustomer() {
		log.info("Start of the method deleteCustomer");
		driver.safeClick(verticalIconButton);
		driver.safeClick(deleteLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(deleteDialogYesButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method deleteCustomer");
	}

	/***
	 * Selects customer in the dropdown
	 * @param linkedCustomer : customer name in string format
	 */
	public void selectCustomerInAddTenant(String linkedCustomer) {
		log.info("Start of the method selectCustomerInAddTenant");
		driver.safeClick(addButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.jSClick(customer);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(By.xpath(String.format(customersDropdownOption, linkedCustomer)));
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method selectCustomerInAddTenant");
	}

	/**
	 * Verifies customer name in the dropdown
	 * @param linkedCustomer : customer name in string format
	 */
	public boolean isCustomerDisplayedInDropdown(String linkedCustomer) {
		log.info("Start of the method isCustomerDisplayedInDropdown");
		boolean isCustomerDisplayed = driver.isPresentAndVisible(By.xpath(String.format(selectedCustomerText, linkedCustomer)));
		log.info("End of the method isCustomerDisplayedInDropdown");
		return isCustomerDisplayed;
	}

	/**
	 * Verifies whether date time format field is displayed
	 */
	public boolean isDateTimeFormatDisplayed() {
		log.info("Start of the method isDateTimeFormatDisplayed");
		boolean isDateTimeFormatDisplayed = driver.isPresentAndVisible(dateTimeFormatDropdown);
		log.info("End of the method isDateTimeFormatDisplayed");
		return isDateTimeFormatDisplayed;
	}

	/**
	 * Verifies whether time format field is displayed
	 */
	public boolean isTimeFormatDisplayed() {
		log.info("Start of the method isTimeFormatDisplayed");
		boolean isTimeFormatDisplayed = driver.isPresentAndVisible(timeFormatDropdown);
		log.info("End of the method isTimeFormatDisplayed");
		return isTimeFormatDisplayed;
	}

	/**
	 * Verifies whether date format field is displayed
	 */
	public boolean isDateFormatDisplayed() {
		log.info("Start of the method isDateFormatDisplayed");
		boolean isDateFormatDisplayed = driver.isPresentAndVisible(dateFormatDropdown);
		log.info("End of the method isDateFormatDisplayed");
		return isDateFormatDisplayed;
	}

    /**
	 * Verifies whether time zone field is displayed
	 */
	public boolean isTimeZoneDisplayed() {
		log.info("Start of the method isTimeZoneDisplayed");
		boolean isTimeZoneDisplayed = driver.isPresentAndVisible(timeZoneDropdown);
		log.info("End of the method isTimeZoneDisplayed");
		return isTimeZoneDisplayed;
	}

	public void exportToTargetTenant(String tenantName) {
		log.info("Start of the method exportToTargetTenant");
		driver.safeClick(exportTenantDropdownButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(By.xpath(String.format(selectTargetTenant, tenantName)));
		driver.sleep(WaitTime.MEDIUM_WAIT);
		log.info("End of the method exportToTargetTenant");
	}

}