package io.ient.test.pages.enterprise;

import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class AuditLogPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(AuditLogPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By auditLogTab = By.cssSelector("div[id='audit-log-card']");
	private static final By searchTextBox = By.cssSelector("input[id='search-input-field']");
	private static final By searchIconButton = By.cssSelector("i[id='search-input-icon']");
	private static String entityRow = "//tr[contains(@class,'ient-table-row') and td[span[text()=' %s ']] and  td[span[text()=' %s ']] and td[span[text()=' %s ']]]";


	public AuditLogPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - AuditLogPage");
	}

	/***
	 * Clicks on audit log tab
	 */
	public void clickOnAuditLogTab() {
		log.info("Start of the method clickOnAuditLogTab");
		driver.safeClick(auditLogTab);
		log.info("End of the method clickOnAuditLogTab");
	}

	/**
	 * Verifies whether audit log entity is displayed
	 * @param entityName : name of entity in string format
	 * @param entityId : id of a entity in string format
	 * @param action : action in string format
	 * @return returns audit log display status
	 */
	public boolean isAuditLogDisplayed(String entityName, String entityId, String action) {
		log.info("Start of the method isAuditLogDisplayed");
		boolean isAuditLogEntityDisplayed = driver.isPresentAndVisible(By.xpath(String.format(entityRow, entityName, entityId, action)));
		log.info("End of the method isAuditLogDisplayed");
		return isAuditLogEntityDisplayed;
	}

/**
	 * Verifies whether audit log entity is displayed
	 * @param entityId : name of entity in string format
	 * @param user : username in string format
	 * @param action : action in string format
	 * @return returns audit log display status
	 */
	public boolean isProblemCaseLogDisplayed(String entityId, String user, String action) {
		log.info("Start of the method isProblemCaseLogDisplayed");
		boolean isProblemCaseLogDisplayed = driver.isPresentAndVisible(By.xpath(String.format(entityRow, entityId, user, action)));
		log.info("End of the method isProblemCaseLogDisplayed");
		return isProblemCaseLogDisplayed;
	}

	/***
	 * Searches role by id
	 * @param id : id of role in string format
	 */
	public void searchById(String id) {
		log.info("Start of the method searchById");
		driver.safeType(searchTextBox, id);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(searchIconButton);
		driver.safeClick(searchIconButton);
		log.info("End of the method searchById");
	}

	public void clearSearchTextField() {
		log.info("Start of the method clearSearchTextField");
		driver.safeClear(searchTextBox);
		log.info("End of the method clearSearchTextField");
	}

	public void clickOnSearchIcon() {
		log.info("Start of the method clickOnSearchIcon");
		driver.safeClick(searchIconButton);
		log.info("End of the method clickOnSearchIcon");
	}

	public Integer getColumnNumber(String columnName)
	{
		log.info("Start of the method getColumnNumber");
		int columnNumber = driver.columnNumFromColumnHeading(columnName, By.xpath("//table[@class='ui striped table actual-table']/thead/tr"));
		log.info("End of the method getColumnNumber");
		return columnNumber;
	}

}