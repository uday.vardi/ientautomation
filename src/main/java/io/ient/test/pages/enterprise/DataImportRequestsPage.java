package io.ient.test.pages.enterprise;


import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;


public class DataImportRequestsPage extends BasePage {

	private static final Logger logger = LogManager.getLogger(DataImportRequestsPage.class);
	LogFactory log = new LogFactory(logger);

	private static final By viewButton = By.cssSelector("div[class*='visible']>div[id='view']");
	private static final By importButton = By.xpath("//button[span[text()='Import']]");
	private static final By rejectButton = By.xpath("//button[span[text()='Reject']]");
	private static final By popupDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By popupDialogNoButton = By.xpath("//button[text()='No']");
	private static final By rejectCommentTextArea = By.cssSelector("textarea[placeholder='Comment']");
	private static final By rejectPopupRejectButton = By.xpath("//button[text()='Reject']");
	private static String dataImportRequestsText = "//span[contains(text(),'%s')]";
	private static String dataImportRequestsEntityVerticalIcon = "//tr[td[span[contains(text(),'%s')]]]/td/div/i[contains(@class,'ellipsis vertical icon')]";

	public DataImportRequestsPage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - DataImportRequestsPage");
	}

	/**
	 * Verifies whether data import requests is displayed
	 */
	public boolean isDataImportRequestsDisplayed(String entityName) {
		log.info("Start of the method isDataImportRequestsDisplayed");
		boolean isDataImportRequestsDisplayed = driver.isPresentAndVisible(By.xpath(String.format(dataImportRequestsText, entityName)));
		log.info("End of the method isDataImportRequestsDisplayed");
		return isDataImportRequestsDisplayed;
	}


	public void clickOnDataImportRequestsVerticalIcon(String entityName) {
		log.info("Start of the method clickOnDataImportRequestsVerticalIcon");
		driver.safeClick(By.xpath(String.format(dataImportRequestsEntityVerticalIcon, entityName)));
		log.info("End of the method clickOnDataImportRequestsVerticalIcon");
	}

	public void importDataRequest() {
		log.info("Start of the method importDataRequest");
		driver.safeClick(viewButton);
		driver.safeClick(importButton);
		driver.safeClick(popupDialogYesButton);
		log.info("End of the method importDataRequest");
	}

	public void rejectDataRequest(String rejectComment) {
		log.info("Start of the method rejectDataRequest");
		driver.safeClick(viewButton);
		driver.safeClick(rejectButton);
		driver.safeType(rejectCommentTextArea, rejectComment);
		driver.safeClick(rejectPopupRejectButton);
		log.info("End of the method rejectDataRequest");
	}
}