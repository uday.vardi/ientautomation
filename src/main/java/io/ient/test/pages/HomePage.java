package io.ient.test.pages;

import io.ient.test.framework.exception.FrameworkException;
import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;


public class HomePage extends BasePage{

	private static final Logger logger = LogManager.getLogger(HomePage.class);
	LogFactory log = new LogFactory(logger);
	private static final By headerBannerText = By.xpath("//div[@class='home_banner']/h1[contains(text(),'Welcome')]");
	private static final By importExportHistoryTab = By.cssSelector("div[id='export-history-card']");
	private static String headerBannerUserTitle = "//div[@class='home_banner']/h1[contains(text(),'Welcome back %s')]";
	private static String tableFirstRowText = "//table[@class='ui striped table actual-table']/descendant::tbody/tr[1]/td[%s]/span";
	private static final By adminDropdownLink = By.cssSelector("i[id='logout-dropdown-icon']");
	private static final By logoutLink = By.xpath("//a[contains(@href,'logout')]");
	private static final By impersonateLink = By.xpath("//a[span[contains(text(),'Impersonate')]]");
	private static final By bulletinSubscriptionLink = By.xpath("//a[span[contains(text(),'Bulletin Subscriptions')]]");
	private static final By personLink = By.xpath("//a[span[contains(text(),'Profile')]]");
	private static final By resetButton = By.xpath("//button[contains(text(),'Reset')]");
	private static final By delegateLink = By.xpath("//a[span[contains(text(),'Delegate')]]");
	private static final By endDelegateLink = By.xpath("//a[span[contains(text(),'End Delegation')]]");
	private static final By endImpersonateLink = By.xpath("//a[span[contains(text(),'End Impersonation')]]");
	private static final By userNameLink = By.xpath("(//div[@class='header'])[1]");
	private static final By logoutButton = By.xpath("//button[text()='Logout']");
	private static final By enterpriseButton = By.cssSelector("div[data-tooltip='Settings']");
	private static final By dataImportRequestsButton = By.xpath("//div[@class='header_name' and contains(text(),' Data Import Requests ')]");
	private static final By homeButton = By.cssSelector("div[id='sb-home']");
	private static final By toDosButton = By.cssSelector("div[id='sb-todo']");
	private static final By analyticsButton = By.cssSelector("div[id='sb-reports']");
	private static final By userSearchTextField = By.cssSelector("input[placeholder='Search for User']");
	private static final By userSearchIcon = By.cssSelector("i[class='inverted circular search link icon']");
	private static final By projectButton = By.cssSelector("div[data-tooltip='Project']");
	private static final By supplyChainIssueManagementLink = By.xpath("//div[text()=' Supply Chain Issue Mgmt ']|//div[@class='header' and text()='Supply Chain Issue Mgmt']");
	private static final By bulletinsLink = By.xpath("//div[text()=' Bulletins ']|//div[@class='header' and text()=' Bulletins ']");
	private static final By supplierInvoicesLink = By.xpath("//div[text()=' Supplier Invoices ']|//div[@class='header' and text()=' Supplier Invoices ']");
	private static final By purchaseOrderLink = By.xpath("//div[text()=' Purchase Orders ']|//div[@class='header' and text()=' Purchase Orders ']");
	private static final By shippingCalendarLink = By.xpath("//div[text()=' Shipping Calendar ']|//div[@class='header' and text()='Shipping Calendar']");
	private static final By shippingLanesLink = By.xpath("//div[text()=' Shipping Lanes ']|//div[@class='header' and text()='Shipping Lanes']");
	private static final By partSourcingAttributesLink = By.xpath("//div[text()=' Part Sourcing Attributes ']|//div[@class='header' and text()='Part Sourcing Attributes']");
	private static final By supplierCapacityLink = By.xpath("//div[text()=' Supplier Capacity ']|//div[@class='header' and text()='Supplier Capacity']");
	private static final By productTransitionsLink = By.xpath("//div[text()=' Product Transitions ']|//div[@class='header' and text()='Product Transitions']");
	private static final By holidayCalendarLink = By.xpath("//div[text()=' Holiday Calendar ']|//div[@class='header' and text()='Holiday Calendar']");

	private static final By applicationApprovalLink = By.xpath("//div[text()=' Application Approval ']|//div[@class='header' and text()=' Application Approval ']");
	private static final By acceptTermsAndConditions = By.xpath("//button[contains(text(),'Accept')]");
	private static final By marketplaceButton = By.cssSelector("button[id='marketplace-btn']");
	private static final By startDateTextField = By.cssSelector("input[placeholder='Start']");
	private static final By endDateTextField = By.cssSelector("input[placeholder='End']");
	private static final By submitButton = By.xpath("//button[span[text()='Submit']]");
	private static final By activeDateText = By.xpath("//td[contains(@class,'link today')]");
	private static final By delegateUserAccessMessage = By.xpath("//div[@class='delegate' and contains(text(), 'You have already delegated your access to ')]");
	private static final By newButton = By.xpath("//span[text()='New']");
	private static final By nameTextBox = By.cssSelector("input[placeholder='Name']");
	private static final By appDescriptionTextArea = By.cssSelector("textarea[placeholder='Description']");
	private static final By createButton = By.xpath("//button[text()='Create']");
	private static final By publishButton = By.xpath("//button[text()='Publish']");
	private static final By publishDialogYesButton = By.xpath("//button[text()='Yes']");
	private static final By problemCaseAddButton = By.cssSelector("button[id='ient-table-add-btn']");
	private static final By supplyChainIssueManagementCardInProjects = By.xpath("//div[@class='card project-card ng-star-inserted' and div[div[@class='header' and text()='Supply Chain Issue Mgmt']]]");

	public HomePage(SeleniumDriver driver) {
		super(driver);
		log.info("In class constructor - HomePage");
	}

	public boolean isHomePage() {
		log.info("Start of method - isHomePage");
		boolean homepageDisplayStatus = false;
		try {
			homepageDisplayStatus = driver.isPresentAndVisible(headerBannerText);
		} catch (Exception e) {
			log.error("Failed to load homepage");
			throw new FrameworkException(e);
		}
		return homepageDisplayStatus;
	}

	public void clickOnEnterprise()
	{
		log.info("Start of the method clickOnEnterprise");
		driver.safeClick(enterpriseButton);
		log.info("End of the method clickOnEnterprise");
	}

	/***
	 * Clicks on import export history tab
	 */
	public void clickOnImportExportHistoryTab() {
		log.info("Start of the method clickOnImportExportHistoryTab");
		driver.safeClick(importExportHistoryTab);
		log.info("End of the method clickOnImportExportHistoryTab");
	}

	public void clickOnDataImportRequestsButton()
	{
		log.info("Start of the method clickOnDataImportRequestsButton");
		driver.safeClick(dataImportRequestsButton);
		log.info("End of the method clickOnDataImportRequestsButton");
	}

	/**
	 * Clicks on new button
	 */
	public void clickOnNewButton()
	{
		log.info("Start of the method clickOnNewButton");
		driver.safeClick(newButton);
		log.info("End of the method clickOnNewButton");
	}

	/**
	 * Craetes app settings
	 * @param name : name in string format
	 * @param description : description in string format
	 */
	public void createApp(String name, String description) {
		log.info("Start of the method createApp");
		driver.safeType(nameTextBox, name);
		driver.safeType(appDescriptionTextArea, description);
		log.info("End of the method createApp");
	}

	/**
	 * Creates and publishes app
	 */
	public void createAndPublishApp() {
		log.info("Start of the method createAndPublishApp");
		driver.safeClick(createButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(publishButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(publishDialogYesButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method createAndPublishApp");
	}

	/**
	 * Publishes app
	 */
	public void publishApp() {
		log.info("Start of the method publishApp");
		driver.safeClick(publishButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.safeClick(publishDialogYesButton);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method publishApp");
	}

	/**
	 * Verifies presence of enterpriseButton
	 * @return enterprise button status
	 */
	public boolean isEnterPriseButtonDisplayed()
	{
		log.info("Start of the method isEnterPriseButtonDisplayed");
		boolean isEnterPriceButtonDisplayed = driver.isPresentAndVisible(enterpriseButton);
		log.info("End of the method isEnterPriseButtonDisplayed");
		return isEnterPriceButtonDisplayed;
	}

	public void clickOnHome()
	{
		log.info("Start of the method clickOnHome");
		driver.safeClick(homeButton);
		log.info("End of the method clickOnHome");
	}

	/**
	 * Verifies presence of home button
	 * @return home button status
	 */
	public boolean isHomeButtonDisplayed()
	{
		log.info("Start of the method isHomeButtonDisplayed");
		boolean isHomeButtonDisplayed = driver.isPresentAndVisible(homeButton);
		log.info("End of the method isHomeButtonDisplayed");
		return isHomeButtonDisplayed;
	}

	/**
	 * Verifies presence of todo button
	 * @return todo button status
	 */
	public boolean isToDoButtonDisplayed()
	{
		log.info("Start of the method isToDoButtonDisplayed");
		boolean isToDoButtonDisplayed = driver.isPresentAndVisible(toDosButton);
		log.info("End of the method isToDoButtonDisplayed");
		return isToDoButtonDisplayed;
	}

	/**
	 * Clicks on todo button
	 */
	public void clickOnToDosButton() {
		log.info("Start of the method clickOnToDosButton");
		driver.safeClick(toDosButton);
		log.info("End of the method clickOnToDosButton");
	}

	/**
	 * Clicks on analytics button
	 */
	public void clickOnAnalyticsButton() {
		log.info("Start of the method clickOnAnalyticsButton");
		driver.safeClick(analyticsButton);
		log.info("End of the method clickOnAnalyticsButton");
	}

	/**
	 * Verifies presence of analytics button
	 * @return analytics button status
	 */
	public boolean isAnalyticsButtonDisplayed()
	{
		log.info("Start of the method isAnalyticsButtonDisplayed");
		boolean isAnalyticsButtonDisplayed = driver.isPresentAndVisible(analyticsButton);
		log.info("End of the method isAnalyticsButtonDisplayed");
		return isAnalyticsButtonDisplayed;
	}

	/**
	 * Verifies presence of problem case add button
	 * @return problem case add button status
	 */
	public boolean isProblemCaseAddButtonDisplayed()
	{
		log.info("Start of the method isProblemCaseAddButtonDisplayed");
		boolean isProblemCaseAddButtonDisplayed = driver.isPresentAndVisible(problemCaseAddButton);
		log.info("End of the method isProblemCaseAddButtonDisplayed");
		return isProblemCaseAddButtonDisplayed;
	}

	public void clickOnAdminDropDownLink()
	{
		log.info("Start of the method clickOnAdminDropDownLink");
		driver.safeClick(adminDropdownLink);
		log.info("End of the method clickOnAdminDropDownLink");
	}

	public void clickOnProfile()
	{
		log.info("Start of the method clickOnProfile");
		driver.safeClick(personLink);
		log.info("End of the method clickOnProfile");
	}

	public void clickOnReset()
	{
		log.info("Start of the method clickOnReset");
		driver.safeClick(resetButton);
		log.info("End of the method clickOnReset");
	}

	public void clickOnLogoutLink()
	{
		log.info("Start of the method clickOnLogoutLink");
		driver.safeClick(logoutLink);
		driver.safeClick(logoutButton);
		log.info("End of the method clickOnLogoutLink");
	}

	public void clickOnImpersonateLink()
	{
		log.info("Start of the method clickOnImpersonateLink");
		driver.safeClick(impersonateLink);
		log.info("End of the method clickOnImpersonateLink");
	}

	public void clickOnBulletinSubscriptionsLink()
	{
		log.info("Start of the method clickOnBulletinSubscriptionsLink");
		driver.safeClick(bulletinSubscriptionLink);
		log.info("End of the method clickOnBulletinSubscriptionsLink");
	}

	public void clickOnEndImpersonateLink() {
		log.info("Start of the method clickOnEndImpersonateLink");
		driver.safeClick(endImpersonateLink);
		log.info("End of the method clickOnEndImpersonateLink");
	}

	public boolean verifyUserTitle(String userLastName) {
		log.info("Start of method - verifyUserTitle");
		boolean userTitleStatus = driver.isPresentAndVisible(By.xpath(String.format(headerBannerUserTitle, userLastName)));
		log.info("End of method - verifyUserTitle");
		return userTitleStatus;
	}

	/***
	 * Searches impersonate user
	 * @param userLastName : last name of user in string format
	 */
	public void searchImpersonateUserByLastName(String userLastName) {
		log.info("Start of the method searchImpersonateUserByLastName");
		driver.safeType(userSearchTextField, userLastName);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(userSearchIcon);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method searchImpersonateUserByLastName");
	}

	/***
	 * Searches user
	 * @param userName : name of user in string format
	 */
	public void searchUserByName(String userName) {
		log.info("Start of the method searchUserByName");
		driver.safeType(userSearchTextField, userName);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(userSearchIcon);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method searchUserByName");
	}

	/**
	 * Clicks on user link
	 */
	public void clickOnUserLink() {
		log.info("Start of the method clickOnUserLink");
		driver.safeClick(userNameLink);
		log.info("End of the method clickOnUserLink");
	}

	/**
	 * Clicks on project
	 */
	public void clickOnProject() {
		log.info("Start of the method clickOnProject");
		driver.safeClick(projectButton);
		log.info("End of the method clickOnProject");
	}

	/**
	 * Verifies presence of projects button
	 * @return projects button status
	 */
	public boolean isProjectButtonDisplayed()
	{
		log.info("Start of the method isProjectButtonDisplayed");
		boolean isProjectButtonDisplayed = driver.isPresentAndVisible(projectButton);
		log.info("End of the method isProjectButtonDisplayed");
		return isProjectButtonDisplayed;
	}

	/**
	 * Clicks on supply chain issue management
	 */
	public void clickOnSupplyChainIssueManagement() {
		log.info("Start of the method clickOnSupplyChainIssueManagement");
		driver.safeClick(supplyChainIssueManagementLink);
		log.info("End of the method clickOnSupplyChainIssueManagement");
	}

	/**
	 * Clicks on bulletins app
	 */
	public void clickOnBulletinsApp() {
		log.info("Start of the method clickOnBulletinsApp");
		driver.safeClick(bulletinsLink);
		log.info("End of the method clickOnBulletinsApp");
	}

	/**
	 * Clicks on supplier invoices app
	 */
	public void clickOnSupplierInvoicesApp() {
		log.info("Start of the method clickOnSupplierInvoicesApp");
		driver.safeClick(supplierInvoicesLink);
		log.info("End of the method clickOnSupplierInvoicesApp");
	}

	/**
	 * Clicks on purchase order app
	 */
	public void clickOnPurchaseOrderApp() {
		log.info("Start of the method clickOnPurchaseOrderApp");
		driver.safeClick(purchaseOrderLink);
		log.info("End of the method clickOnPurchaseOrderApp");
	}

	/**
	 * Clicks on Shipping Calendar App
	 */
	public void clickOnShippingCalendar() {
		log.info("Start of the method clickOnShippingCalendar");
		driver.safeClick(shippingCalendarLink);
		log.info("End of the method clickOnShippingCalendar");
	}

	/**
	 * Clicks on Shipping Lanes App
	 */
	public void clickOnShippingLanes() {
		log.info("Start of the method clickOnShippingLanes");
		driver.safeClick(shippingLanesLink);
		log.info("End of the method clickOnShippingLanes");
	}

	/**
	 * Clicks on Part Sourcing Attributes App
	 */
	public void clickOnPartSourcingAttributes() {
		log.info("Start of the method clickOnPartSourcingAttributes");
		driver.safeClick(partSourcingAttributesLink);
		log.info("End of the method clickOnPartSourcingAttributes");
	}

	/**
	 * Clicks on Supplier Capacity App
	 */
	public void clickOnSupplierCapacity() {
		log.info("Start of the method clickOnSupplierCapacity");
		driver.safeClick(supplierCapacityLink);
		log.info("End of the method clickOnSupplierCapacity");
	}

	/**
	 * Clicks on Product Transitions App
	 */
	public void clickOnProductTransitions() {
		log.info("Start of the method clickOnProductTransitions");
		driver.safeClick(productTransitionsLink);
		log.info("End of the method clickOnProductTransitions");
	}

	/**
	 * Clicks on application approval
	 */
	public void clickOnApplicationApproval() {
		log.info("Start of the method clickOnApplicationApproval");
		driver.safeClick(applicationApprovalLink);
		driver.sleep(WaitTime.XSMALL_WAIT);
		log.info("End of the method clickOnApplicationApproval");
	}

	/**
	 * Clicks on accept terms and conditions
	 */
	public void clickOnAcceptTermsAndConditions()
	{
		log.info("Start of the method clickOnAcceptTermsAndConditions");
		driver.safeClick(acceptTermsAndConditions);
		driver.sleep(WaitTime.XSMALL_WAIT);
		log.info("End of the method clickOnAcceptTermsAndConditions");
	}

	/**
	 * Clicks on delegate link
	 */
	public void clickOnDelegateLink()
	{
		log.info("Start of the method clickOnDelegateLink");
		if(!driver.isPresentAndVisible(delegateLink))
		{
			driver.safeClick(endDelegateLink);
			driver.sleep(WaitTime.LONG_WAIT);
			clickOnAdminDropDownLink();
		}
		driver.safeClick(delegateLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method clickOnDelegateLink");
	}

	/**
	 * Verifies whether delegate link displayed or not
	 * @return delegation link
	 */
	public boolean isDelegationLinkDisplayed()
	{
		log.info("Start of the method isDelegationLinkDisplayed");
		boolean isDelegationLinkDisplayed = driver.isPresentAndVisible(delegateLink);
		log.info("End of the method isDelegationLinkDisplayed");
		return isDelegationLinkDisplayed;
	}

	/**
	 * Returns active date text
	 */
	public String getActiveDateText()
	{
		log.info("Start of the method getActiveDateText");
		driver.safeClick(startDateTextField);
		String activeDate = driver.safeGetText(activeDateText);
		log.info("End of the method getActiveDateText");
		return activeDate;
	}

	/***
	 * Enters start and end date
	 */
	public void delegateUserStartAndEndDate() {
		log.info("Start of the method delegateUserStartAndEndDate");
		driver.safeClick(startDateTextField);
		driver.safeClick(activeDateText);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(endDateTextField);
		driver.safeClick(activeDateText);
		driver.sleep(WaitTime.SMALL_WAIT);
		driver.safeClick(submitButton);
		driver.sleep(WaitTime.XSMALL_WAIT);
		driver.refresh();
		driver.sleep(WaitTime.XSMALL_WAIT);
		log.info("End of the method delegateUserStartAndEndDate");
	}

	/**
	 * Verifies presence of delegate access message
	 * @return delegate access message status
	 */
	public boolean isDelegateAccessMessageDisplayed() {
		log.info("Start of the method isDelegateAccessMessageDisplayed");
		boolean isDelegateAccessMessageDisplayed = driver.isPresentAndVisible(delegateUserAccessMessage);
		log.info("End of the method isDelegateAccessMessageDisplayed");
		return isDelegateAccessMessageDisplayed;
	}

	/**
	 * Clicks on end delegate link
	 */
	public void clickOnEndDelegateLink() {
		log.info("Start of the method clickOnEndDelegateLink");
		if(!driver.isPresentAndVisible(endDelegateLink))
		{
			driver.safeClick(delegateLink);
			driver.sleep(WaitTime.LONG_WAIT);
			clickOnAdminDropDownLink();
		}
		driver.safeClick(endDelegateLink);
		driver.sleep(WaitTime.SMALL_WAIT);
		log.info("End of the method clickOnEndDelegateLink");
	}

	/**
	 * Verifies whether end delegate link is displayed or not
	 * @return delegation link
	 */
	public boolean isEndDelegateLinkDisplayed() {
		log.info("Start of the method isEndDelegateLinkDisplayed");
		boolean isEndDelegateLinkDisplayed = driver.isPresentAndVisible(endDelegateLink);
		log.info("End of the method isEndDelegateLinkDisplayed");
		return isEndDelegateLinkDisplayed;
	}

	/**
	 * Clicks on marketplace button
	 */
	public void clickOnMarketPlaceButton() {
		log.info("Start of the method clickOnMarketPlaceButton");
		driver.safeClick(marketplaceButton);
		log.info("End of the method clickOnMarketPlaceButton");
	}

	/**
	 * Verifies whether supply chain issue management app is displayed in Projects if it is displayed clicks on that app
	 */
	public boolean isAppDisplayedInProjects() {
		log.info("Start of the method isInstallDisplayedInProjects");
		boolean isAppDisplayedInProjects = driver.isPresentAndVisible(supplyChainIssueManagementCardInProjects);
		if (isAppDisplayedInProjects) {
			driver.safeClick(supplyChainIssueManagementCardInProjects);
		}
		log.info("End of the method isInstallDisplayedInProjects");
		return isAppDisplayedInProjects;
	}

	public String getTableFirstRowText(String columnNumber)
	{
		log.info("Start of the method getTableFirstRowText");
		String firstRowColumnText = driver.safeGetText(By.xpath(String.format(tableFirstRowText, columnNumber)));
		log.info("End of the method getTableFirstRowText");
		return firstRowColumnText;
	}

	public void clickOnHolidayCalendar() {
		log.info("Start of the method clickOnHolidayCalendar");
		driver.safeClick(holidayCalendarLink);
		log.info("End of the method clickOnShippingCalendar");
	}
}