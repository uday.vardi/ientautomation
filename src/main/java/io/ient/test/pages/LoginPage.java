package io.ient.test.pages;

import io.ient.test.framework.exception.FrameworkException;
import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class LoginPage extends BasePage {

    private static final Logger logger = LogManager.getLogger(LoginPage.class);
    LogFactory log = new LogFactory(logger);
    private static final By changeOrganization = By.xpath("//button[text()='Change organization']");
    private static final By enterOrganizationTextBox = By.cssSelector("input[placeholder='Enter organization']");
    private static final By organizationErrorMessage = By.xpath("//div[i[@class='warning triangle icon'] and text()=' Organization information not available, please try again ']");
    private static final By loginUserText = By.cssSelector("div[class='header']");
    private static final By loginButton = By.xpath("//button[text()='Login']");
    private static final By userNameTextBox = By.cssSelector("input[id='username']");
    private static final By passwordTextBox = By.cssSelector("input[id='password']");
    private static final By signInButton = By.cssSelector("input[id='kc-login']");
    private static final By iAgreeButton = By.xpath("//button[text()='I Agree']");
    private static final By administrationConsoleLink  = By.xpath("//a[text()='Administration Console ']");

    public LoginPage(SeleniumDriver driver) {
        super(driver);
        log.info("In class constructor - LoginPage");
    }

    public void goToApplication(String url)
    {
        log.info(String.format("Opening url : ", url));
        driver.openUrl(url);
    }

    public void changeOrganizationLogin(String organizationName) {
        log.info("Changing organization login");
        try {
            driver.safeClick(changeOrganization);
            driver.safeType(enterOrganizationTextBox, organizationName);
            driver.sleep(WaitTime.SMALL_WAIT);
            driver.waitForElementToAppear(loginUserText);
            String actual_text = driver.getText(loginUserText);
            String expected_text =  String.format("Welcome %s user",organizationName);
            if(actual_text.equals(expected_text))
                driver.safeClick(loginButton);
            else throw new FrameworkException("Failed to change organization Name");
        } catch (Exception e) {
            log.error("Failed to change organization Login");
            throw new FrameworkException(e);
        }
    }

    /**
     * Returns organization change status
     * @param organizationName : organization name in string format
     * @return organization change status
     */
    public boolean isOrganizationNameChangeMessage(String organizationName) {
        log.info("Start of the method isOrganizationNameChanged");
        boolean organizationNameChangeStatus = false;
        try {
            driver.safeClick(changeOrganization);
            driver.safeType(enterOrganizationTextBox, organizationName);
            driver.sleep(WaitTime.MEDIUM_WAIT);
            organizationNameChangeStatus = driver.isPresentAndVisible(organizationErrorMessage);
        } catch (Exception e) {
            log.error("Failed to change organization Login");
            throw new FrameworkException(e);
        }
        log.info("End of the method isOrganizationNameChangeMessage");
        return organizationNameChangeStatus;
    }

    public void login(String userName, String password) {
        log.info("Logging into application");
        try {
            driver.safeType(userNameTextBox, userName);
            driver.safeType(passwordTextBox, password);
            driver.safeClick(signInButton);
            boolean cookieStatus = driver.isPresentAndVisible(iAgreeButton);
            if(cookieStatus)  driver.safeClick(iAgreeButton);
        } catch (Exception e) {
            log.error("Failed to logging into application");
            throw new FrameworkException(e);
        }
    }

    public void loginToKeyCloak(String userName, String password) {
        log.info("Logging into loginToKeyCloak");
        try {
            driver.safeClick(administrationConsoleLink);
            driver.safeType(userNameTextBox, userName);
            driver.safeType(passwordTextBox, password);
            driver.safeClick(signInButton);
        } catch (Exception e) {
            log.error("Failed to logging into loginToKeyCloak");
            throw new FrameworkException(e);
        }
    }

}

