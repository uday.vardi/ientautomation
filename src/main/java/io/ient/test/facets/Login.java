package io.ient.test.facets;

import io.ient.test.context.AppDetails;
import io.ient.test.context.FrameworkTestBase;
import io.ient.test.framework.exception.FrameworkException;
import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.selenium.WaitTime;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.pages.BasePage;
import io.ient.test.pages.HomePage;
import io.ient.test.pages.LoginPage;
import io.ient.test.pages.enterprise.KeyCloakPage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class Login extends BasePage {

    private static final Logger logger = LogManager.getLogger(Login.class);
    LogFactory log = new LogFactory(logger);
    LoginPage loginPage = new LoginPage(driver);
    HomePage homePage = new HomePage(driver);
    KeyCloakPage keyCloakPage = new KeyCloakPage(driver);

    public Login(SeleniumDriver driver) {
        super(driver);
        log.info("In class constructor - Login");
    }

    public void loginWithBuyerUser(AppDetails appDetails) {
        log.info("Start of the method - loginWithBuyerUser");
        String organizationName = appDetails.getAgency().toLowerCase();
        loginPage.changeOrganizationLogin(organizationName);
        loginPage.login(appDetails.getBuyerUser(), appDetails.getBuyerPassword());
        boolean isLoggedIn = homePage.isHomePage();
        if (!isLoggedIn) throw new FrameworkException("Failed to login as buyer user");
        log.info("End of the method - loginWithBuyerUser");
    }

    public void loginWithBuyerUser2(AppDetails appDetails) {
        log.info("Start of the method - loginWithBuyerUser2");
        String organizationName = appDetails.getAgency().toLowerCase();
        loginPage.changeOrganizationLogin(organizationName);
        loginPage.login(appDetails.getBuyerUser2(), appDetails.getBuyerPassword2());
        boolean isLoggedIn = homePage.isHomePage();
        if (!isLoggedIn) throw new FrameworkException("Failed to login as buyer user2");
        log.info("End of the method - loginWithBuyerUser2");
    }

    public void loginWithBuyerAdmin(AppDetails appDetails) {
        log.info("Start of the method - loginWithBuyerAdmin");
        String organizationName = appDetails.getAgency().toLowerCase();
        loginPage.changeOrganizationLogin(organizationName);
        loginPage.login(appDetails.getBuyerAdminUser(), appDetails.getBuyerAdminPassword());
        boolean isLoggedIn = homePage.isHomePage();
        if (!isLoggedIn) throw new FrameworkException("Failed to login as buyer admin");
        log.info("End of the method - loginWithBuyerAdmin");
    }

    public void loginWithSupplierUser(AppDetails appDetails) {
        log.info("Start of the method - loginWithSupplierUser");
        String organizationName = appDetails.getAgency().toLowerCase();
        loginPage.changeOrganizationLogin(organizationName);
        loginPage.login(appDetails.getSupplierUser(), appDetails.getSupplierPassword());
        boolean isLoggedIn = homePage.isHomePage();
        if (!isLoggedIn) throw new FrameworkException("Failed to login as supplier user");
        log.info("End of the method - loginWithSupplierUser");
    }

    public void loginWithSupplierAdmin(AppDetails appDetails) {
        log.info("Start of the method - loginWithSupplierAdmin");
        String organizationName = appDetails.getAgency().toLowerCase();
        loginPage.changeOrganizationLogin(organizationName);
        loginPage.login(appDetails.getSupplierAdminUser(), appDetails.getSupplierAdminPassword());
        boolean isLoggedIn = homePage.isHomePage();
        if (!isLoggedIn) throw new FrameworkException("Failed to login as supplier user");
        log.info("End of the method - loginWithSupplierAdmin");
    }

    public void loginWithBulletinManager(AppDetails appDetails) {
        log.info("Start of the method - loginWithBulletinManager");
        String organizationName = appDetails.getAgency().toLowerCase();
        loginPage.changeOrganizationLogin(organizationName);
        loginPage.login(appDetails.getBulletinManagerUsername(), appDetails.getBulletinManagerPassword());
        boolean isLoggedIn = homePage.isHomePage();
        if (!isLoggedIn) throw new FrameworkException("Failed to login as bulletin manager");
        log.info("End of the method - loginWithBulletinManager");
    }

    public void loginWithBulletinApprover(AppDetails appDetails) {
        log.info("Start of the method - loginWithBulletinApprover");
        String organizationName = appDetails.getAgency().toLowerCase();
        loginPage.changeOrganizationLogin(organizationName);
        loginPage.login(appDetails.getBulletinApproverUsername(), appDetails.getBulletinApproverPassword());
        boolean isLoggedIn = homePage.isHomePage();
        if (!isLoggedIn) throw new FrameworkException("Failed to login as bulletin approver");
        log.info("End of the method - loginWithBulletinApprover");
    }

    public void loginWithSuperAdmin(AppDetails appDetails) {
        log.info("Start of the method - loginWithSuperAdmin");
        String organizationName = appDetails.getSuperAdminAgency().toLowerCase();
        loginPage.changeOrganizationLogin(organizationName);
        loginPage.login(appDetails.getIentAdminUsername(), appDetails.getIentAdminPassword());
        boolean isLoggedIn = homePage.isHomePage();
        if (!isLoggedIn) throw new FrameworkException("Failed to login as super admin");
        log.info("End of the method - loginWithSuperAdmin");
    }

    public void loginToKeycloak(AppDetails appDetails) {
        log.info("Start of the method - loginToKeycloak");
        String organizationName = appDetails.getAgency().toLowerCase();
        driver.openUrl(appDetails.getKeycloakUrl());
        loginPage.loginToKeyCloak(appDetails.getKeycloakAdminUsername(), appDetails.getKeycloakAdminPassword());
        boolean isLoggedIn = keyCloakPage.isKeyCloakPage();
        if (!isLoggedIn) throw new FrameworkException("Failed to login as keycloak admin");
        log.info("End of the method - loginToKeycloak");
    }

    public boolean loginWithUserCredentials(String tenantName, String userName, String password, AppDetails appDetails) {
        log.info("Start of the method - loginWithUserCredentials");
        driver.openUrl(appDetails.getAppUrl());
        loginPage.changeOrganizationLogin(tenantName);
        loginPage.login(userName, password);
        boolean isLoggedIn = homePage.isHomePage();
        log.info("End of the method - loginWithUserCredentials");
        return isLoggedIn;
    }

    public void loginWithUserCredentialsWithoutValidation(String tenantName, String userName, String password, AppDetails appDetails) {
        log.info("Start of the method - loginWithUserCredentials");
        driver.openUrl(appDetails.getAppUrl());
        driver.sleep(WaitTime.SMALL_WAIT);
        loginPage.changeOrganizationLogin(tenantName);
        loginPage.login(userName, password);
        log.info("End of the method - loginWithUserCredentials");
    }



}
