package io.ient.test.context;


import io.ient.test.framework.Framework;
import io.ient.test.framework.exception.FrameworkException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.ITestContext;

import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * Class that manages all TestNgContext instances
 */
public class TestNgContextManager {

    public static final Logger log = LogManager.getLogger(TestNgContextManager.class);

    //Key is package.class.method name to context object
    private static HashMap<String, TestNgContext> contexts;

    // Adds a new test ng context to the collection
    // This is called by FrameworkTestBase beforeMethod
    public static synchronized TestNgContext createNewContext(ITestContext testContext, Method testMethod) {
        try {
            //setup checks
            if (contexts == null) contexts = new HashMap<>();
            String name = testMethod.getClass().getName() + "." + testMethod.getName();
            log.debug(String.format("Adding new testng context for: %s", name));
            //create if not present
            if (!contexts.containsKey(name)) {
                TestNgContext context = new TestNgContext();
                //set suite name
                context.app = setupApplication();
                context.setTestMethodName(testMethod.getDeclaringClass().getName() + "." + testMethod.getName());
                context.setTimestampedSuiteName(Framework.getSuiteName());
                //set jenkins context
                context.setIsRunningFromJenkins(JenkinsContext.isRunningFromJenkins());
                if (context.getIsRunningFromJenkins()) {
                    context.setJenkinsJobName(JenkinsContext.getJenkinsJobName());
                    context.setJenkinsBuildNumber(JenkinsContext.getJenkinsBuildNumber());
                }
                contexts.put(name, context);
                log.debug("New context created");
                return context;
            } else {
                log.debug("Context already created");
                return contexts.get(name);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            log.error(e);
            throw new FrameworkException(e);
        }
    }

    private static AppDetails setupApplication() {
        log.debug("Setting up application instance");
        try {
            AppDetails app = new AppDetails();
            log.debug("application instance created");
            return app;
        } catch (Exception e) {
            log.error(e);
            throw new FrameworkException(e);
        }
    }
}
