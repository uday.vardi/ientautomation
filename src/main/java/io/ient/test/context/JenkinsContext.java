package io.ient.test.context;

import io.ient.test.framework.exception.FrameworkException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * Class to determine if executing from jenkins and get jenkins details
 */
public class JenkinsContext {

    private static final Logger log = LogManager.getLogger(JenkinsContext.class);
    private static final String JENKINS_JOB_NAME = "JOB_NAME";
    private static final String JENKINS_BUILD_NUMBER = "BUILD_NUMBER";

    /**
     * Determines if executing from Jenkins
     * @return True/False
     */
    static Boolean isRunningFromJenkins() {
        log.debug("Checking if running from Jenkins");
        try {
            String jobName = System.getenv(JENKINS_JOB_NAME);
            Boolean result = jobName != null;
            log.debug(String.format("Running from jenkins = %s", result));
            return result;
        } catch (Exception e) {
            log.error(e);
            throw new FrameworkException(e);
        }
    }

    // Gets the jenkins job name
    static String getJenkinsJobName() {
        log.debug("Getting jenkins job name");
        try {
            String jobName = System.getenv(JENKINS_JOB_NAME);
            log.debug(String.format("Jenkins job name = %s", jobName));
            return jobName;
        } catch (Exception e) {
            log.error(e);
            throw new FrameworkException(e);
        }
    }

    // Gets the jenkins build number
    static Integer getJenkinsBuildNumber() {
        log.debug("Getting jenkins job build number");
        try {
            Integer buildnumber = Integer.parseInt(System.getenv(JENKINS_BUILD_NUMBER));
            log.debug(String.format("Jenkins job build number = %s", buildnumber));
            return buildnumber;
        } catch (Exception e) {
            log.error(e);
            throw new FrameworkException(e);
        }
    }


}
