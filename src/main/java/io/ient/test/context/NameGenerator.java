package io.ient.test.context;

import io.ient.test.framework.exception.FrameworkException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Class that generates the suite names
 */
public class NameGenerator {

    private static final Logger log = LogManager.getLogger(NameGenerator.class);

    /**
     * Generates a unique key for this suite
     * This is a datetime stamp
     * @return A datetime stamp
     */
    public static String generateSuiteKey() {
        log.debug("Generate Key for test suite");
        String key = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date());
        log.debug(String.format("Suite key = %s", key));
        return key;
    }


    /**
     * Generates a name for the test suite
     * This name is then used as the root directory on the shared storage Z:\ drive
     * Format is:
     * - Jenkins : (jobname)_(buildnumber)_(key)
     * - Local : (username)_(key)
     * @return Name of suite to use
     */
    public static String generateSuiteName(String key) {
        log.debug("Generating suite name");
        try {
            String suiteName;
            if (JenkinsContext.isRunningFromJenkins()) {
                //<jobname>_<buildnumber>_<key>
                String jobName = JenkinsContext.getJenkinsJobName().replace(" ","_").replace(".","_");
                Integer buildNumber = JenkinsContext.getJenkinsBuildNumber();
                suiteName = String.format("%s_%s", jobName, buildNumber);
            } else {
                //<username>_<key>
                suiteName = System.getProperty("user.name");
            }
            //add key
            suiteName = suiteName + "_" + key;
            log.debug(String.format("Suite name = %s", suiteName));
            return suiteName;
        } catch (Exception e) {
            log.error("Unable to generate suite name");
            log.error(e);
            throw new FrameworkException(e);
        }
    }
}
