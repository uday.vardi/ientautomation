package io.ient.test.context;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * Class that holds all context for a Test Method
 */
public class TestNgContext {

    public AppDetails app;

    //information methods
    @Getter(AccessLevel.PUBLIC) @Setter(AccessLevel.PACKAGE) private String testMethodName;
    @Getter(AccessLevel.PUBLIC) @Setter(AccessLevel.PACKAGE) private String timestampedSuiteName;
    @Getter(AccessLevel.PUBLIC) @Setter(AccessLevel.PACKAGE) private Boolean isRunningFromJenkins;
    @Getter(AccessLevel.PUBLIC) @Setter(AccessLevel.PACKAGE) private String jenkinsJobName;
    @Getter(AccessLevel.PUBLIC) @Setter(AccessLevel.PACKAGE) private Integer jenkinsBuildNumber;

}
