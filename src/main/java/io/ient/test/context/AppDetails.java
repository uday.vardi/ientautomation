package io.ient.test.context;

import io.ient.test.framework.data.YamlDataLoader;

import java.util.Map;

public class AppDetails extends YamlDataLoader {

    private static final String CLIENT_NAME = "client_name";
    private static final String ADMIN_CLIENT_NAME = "admin_client_name";
    private static final String APP_URL = "access_url";
    private static final String BUYER_ADMIN_USERNAME = "buyer_admin_username";
    private static final String BUYER_ADMIN_PASSWORD = "buyer_admin_password";
    private static final String BUYER_USER_USERNAME = "buyer_user_username";
    private static final String BUYER_USER_PASSWORD = "buyer_user_password";
    private static final String BUYER_USER2_USERNAME = "buyer_user2_username";
    private static final String BUYER_USER2_PASSWORD = "buyer_user2_password";
    private static final String SUPPLIER_ADMIN_USERNAME = "supplier_admin_username";
    private static final String SUPPLIER_ADMIN_PASSWORD = "supplier_admin_password";
    private static final String SUPPLIER_USER_USERNAME = "supplier_user_username";
    private static final String SUPPLIER_USER_PASSWORD = "supplier_user_password";
    private static final String BULLETIN_MANAGER_USERNAME = "bulletin_manager_username";
    private static final String BULLETIN_MANAGER_PASSWORD = "bulletin_manager_password";
    private static final String BULLETIN_APPROVER_USERNAME = "bulletin_approver_username";
    private static final String BULLETIN_APPROVER_PASSWORD = "bulletin_approver_password";
    private static final String IENT_ADMIN_USERNAME = "ient_admin_username";
    private static final String IENT_ADMIN_PASSWORD = "ient_admin_password";
    private static final String KEYCLOAK_ADMIN_USERNAME = "Keycloak_admin_username";
    private static final String KEYCLOAK_ADMIN_PASSWORD = "Keycloak_admin_password";
    private static final String KEYCLOAK_URL = "Keycloak_admin_url";
    private static Map<String, String> environmentDetailsMap;

    public AppDetails() {
        environmentDetailsMap = loadEnvironmentDetails();
    }

    public String getAgency(){ return environmentDetailsMap.get(CLIENT_NAME); }

    public String getSuperAdminAgency(){ return environmentDetailsMap.get(ADMIN_CLIENT_NAME); }

    public String getAppUrl() {
        return environmentDetailsMap.get(APP_URL);
    }

    public String getBuyerAdminUser() {
        return environmentDetailsMap.get(BUYER_ADMIN_USERNAME);
    }

    public String getBuyerAdminPassword() {
        return environmentDetailsMap.get(BUYER_ADMIN_PASSWORD);
    }

    public String getBuyerUser() {
        return environmentDetailsMap.get(BUYER_USER_USERNAME);
    }

    public String getBuyerUser2() {
        return environmentDetailsMap.get(BUYER_USER2_USERNAME);
    }

    public String getBuyerPassword() {
        return environmentDetailsMap.get(BUYER_USER_PASSWORD);
    }

    public String getBuyerPassword2() {
        return environmentDetailsMap.get(BUYER_USER2_PASSWORD);
    }

    public String getSupplierAdminUser() {
        return environmentDetailsMap.get(SUPPLIER_ADMIN_USERNAME);
    }

    public String getSupplierAdminPassword() {
        return environmentDetailsMap.get(SUPPLIER_ADMIN_PASSWORD);
    }

    public String getSupplierUser() {
        return environmentDetailsMap.get(SUPPLIER_USER_USERNAME);
    }

    public String getSupplierPassword() { return environmentDetailsMap.get(SUPPLIER_USER_PASSWORD); }

    public String getBulletinManagerUsername() {
        return environmentDetailsMap.get(BULLETIN_MANAGER_USERNAME);
    }

    public String getBulletinManagerPassword() { return environmentDetailsMap.get(BULLETIN_MANAGER_PASSWORD); }

    public String getBulletinApproverUsername() { return environmentDetailsMap.get(BULLETIN_APPROVER_USERNAME); }

    public String getBulletinApproverPassword() { return environmentDetailsMap.get(BULLETIN_APPROVER_PASSWORD); }

    public String getIentAdminUsername() {
        return environmentDetailsMap.get(IENT_ADMIN_USERNAME);
    }

    public String getIentAdminPassword() {
        return environmentDetailsMap.get(IENT_ADMIN_PASSWORD);
    }

    public String getKeycloakAdminUsername() {
        return environmentDetailsMap.get(KEYCLOAK_ADMIN_USERNAME);
    }

    public String getKeycloakAdminPassword() {
        return environmentDetailsMap.get(KEYCLOAK_ADMIN_PASSWORD);
    }

    public String getKeycloakUrl() {
        return environmentDetailsMap.get(KEYCLOAK_URL);
    }
}
