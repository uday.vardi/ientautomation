package io.ient.test.context;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import io.ient.test.framework.Framework;
import io.ient.test.framework.exception.FrameworkException;
import io.ient.test.framework.listener.TestAnnotationsData;
import io.ient.test.framework.selenium.DriverFactory;
import io.ient.test.framework.selenium.SeleniumConfigs;
import io.ient.test.framework.selenium.SeleniumDriver;
import io.ient.test.framework.utils.LogFactory;
import io.ient.test.framework.utils.extentreports.ExtentTestManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Base class for test classes
 * This class has the generic Before and After methods
 */
public class FrameworkTestBase {
    private static final Logger logger = LogManager.getLogger(FrameworkTestBase.class);
    LogFactory log = new LogFactory(logger);
    public ExtentTestManager extentTestManager;
    public TestAnnotationsData testAnnotations = null;
    public ExtentTest extentTest;
    public SeleniumDriver driver;
    protected TestNgContext testContext;

    @BeforeSuite(alwaysRun = true)
    public void frameworkBeforeSuite(ITestContext testContext) {
        log.info("Start of framework before suite");
        try {
            //setup framework
            Framework.setup(testContext);
            extentTestManager = new ExtentTestManager();
        } catch (Exception e) {
            log.error(e);
            throw new FrameworkException(e);
        }
        log.info("End of framework before suite");
    }

    @BeforeMethod(alwaysRun = true)
    public void frameworkBeforeMethod(ITestContext testContext, Method testMethod, ITestResult result) {
        extentTest =  extentTestManager.startTest(testMethod.getName());
        log.extentTest = extentTest;
        testAnnotations = new TestAnnotationsData(result);
        String testCaseIds = testAnnotations.getTestCaseID();
        extentTest.assignCategory(String.format("TestCase IDs: %s", testCaseIds));
        String fullName = this.getClass().getName() + "." + testMethod.getName();
        log.info(String.format("Start of framework before method for test : %s", fullName));
        try {
            //setup context

            this.testContext = TestNgContextManager.createNewContext(testContext, testMethod);
            //create a logger for this thread
            LogFactory.createLogAppender(this.testContext, extentTest);
            //open new browser
            driver = DriverFactory.getDriver(this.testContext, SeleniumConfigs.getBrowserName());
            driver.windowMaximize();
            setup();
            log.info("End of framework before method");
        } catch (Exception e) {
            //remove the logger for this thread as it is not needed anymore
            LogFactory.removeLogAppenderForThisThread(this.testContext);
            log.error("Error in framework before method");
            log.error(e);
            throw new FrameworkException(e);
        }
    }

    @AfterMethod(alwaysRun = true)
    public void frameworkAfterMethod(ITestContext testContext, Method testMethod, ITestResult result) {
        String methodName = this.getClass().getName() + "." + result.getMethod().getMethodName();
        log.info(String.format("Start of framework after method for test : %s", methodName));
        try {
            //capture screenshot if failed
            if( result.getStatus() == ITestResult.FAILURE) {
                log.warn(String.format("Test method failed : %s", methodName));
                //capture current timestamp
                Calendar calendar = Calendar.getInstance();
                // Capture error, time of failure
                Date d = calendar.getTime();
                String failureTime = d.toString();
                Throwable throwable = result.getThrowable();
                log.info("Message:\t" + throwable);
                log.info("Time of Failure:\t\t" + failureTime);
                log.info("Stacktrace:\t" + Arrays.toString(throwable.getStackTrace()));
                driver.getScreenShotService().captureScreenshot(result);
                extentTest.fail(MediaEntityBuilder.createScreenCaptureFromPath(String.valueOf(driver.getScreenShotService().screenShot_filePath)).build());
            }
            //close browser
            if (driver != null) {
                //close browser
                driver.quit();
                driver = null;
            }
            extentTest.getExtent().flush();
            //remove log appender
            //LogFactory.removeLogAppenderForThisThread(this.testContext);
            log.info(String.format("End of framework after method for test : %s", methodName));
        } catch (Exception e) {
            //remove the logger for this thread as it is not needed anymore
            LogFactory.removeLogAppenderForThisThread(this.testContext);
            log.error("Error in framework after method");
            log.error(e);
            throw new FrameworkException(e);
        }

    }

    @AfterClass(alwaysRun = true)
    public void frameworkAfterClass(ITestContext testContext) {
        log.info("Start of framework after class");
        try {
            //close browser
            if (driver!=null){
                //close browser
                driver.quit();
                driver = null;
            }
            log.info("End of framework after class");
        } catch (FrameworkException e) {
            log.error("Error in framework after suite");
            log.error(e);
        }
    }

    @AfterSuite(alwaysRun = true)
    public void frameworkAfterSuite() {
        log.info("Start of framework after suite");
        try {
            //close browser
            if (driver!=null){
                //close browser
                driver.quit();
                driver = null;
            }
        } catch (FrameworkException e) {
            log.error("Error in framework after suite");
            log.error(e);
            throw new FrameworkException(e);
        }
        log.info("End of framework after suite");
    }

    public void setup()
    {
        log.info("Start of setup");
        driver.openUrl(testContext.app.getAppUrl());
        log.info("End of setup");

    }

}
